<?php
/**
 * The main entry point into the application, loads the config and classes.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2013-04-25)
 */

require_once 'conf/conf.php';

spl_autoload_register('clsAutoload');

/**
 * Loads classes automatically from the controllers and models directories.
 * 
 * @param string $class		The name of the class to be loaded.
 */
function clsAutoload($class) {
	$class = str_replace('\\', '/', $class);
	
	if (file_exists("controllers/$class.php"))
		require_once "controllers/$class.php";
	
	else if (file_exists("models/$class.php"))
		require_once "models/$class.php";

	else if ($class == 'PHPMailer')
		require_once 'lib/PHPMailer/class.phpmailer.php';
	
	else
		DOMPDF_autoload($class);
}

//Actual kick off
$controllerCls = Controller::CONTROLLER_DEFAULT_CLASS;
$controllerFunc = Controller::CONTROLLER_DEFAULT_FUNCTION;

if (isset($_GET['controller']) && preg_match('@^[a-zA-Z][a-zA-Z0-9/_]*$@', $_GET['controller']))
	$controllerCls = str_replace('/', '\\', $_GET['controller']) . 'Controller';

if (isset($_GET['function']) && preg_match('@^[a-zA-Z][a-zA-Z0-9_]*$@', $_GET['function']))
	$controllerFunc = $_GET['function'];

Controller::checkMaintenanceMode();
Controller::callController($controllerCls, $controllerFunc);