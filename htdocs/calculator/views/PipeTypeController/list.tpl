{extends file="main.tpl"}
{block name="title"}Pipe Types{/block}
{block name="mainContent"}
	<h3>Pipe Types</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $pipeTypes as $pipeType}
				<tr>
					<td>{$pipeType->id}</td>
					<td>{$pipeType->typeName}</td>
					<td>
						<button type="button" class="btn btn-small editBut" data-id="{$pipeType->id}" data-typename="{$pipeType->typeName}">
							<i class="icon-edit"></i> Edit
						</button>
						<button type="button" class="btn btn-small deleteBut" data-id="{$pipeType->id}" data-typename="{$pipeType->typeName}">
							<i class="icon-trash"></i> Delete
						</button>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="3">No Pipe Types have been added.</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a href="{url to="ApplicationController->index"}" class="btn"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a href="{url to="ProductController->index"}" class="btn">&lt; Back to Product List</a>
		&nbsp;
		<button type="button" class="btn" id="createBut">Add new Type of Pipe</button>
	</div>
	<div class="modal hide fade" id="createModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create New Type of Pipe</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="typeName">Name:</label>
					<div class="controls">
						<input type="text" name="typeName" maxlength="50">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-primary" id="doCreateBut">Create</button>
		</div>
	</div>
	<div class="modal hide fade" id="editModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Type of Pipe</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form class="form-horizontal">
				<input type="hidden" name="pipeTypeId">
				<div class="control-group">
					<label class="control-label" for="typeName">Name:</label>
					<div class="controls">
						<input type="text" name="typeName" maxlength="50">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-primary" id="doEditBut">Edit</button>
		</div>
	</div>
	<div class="modal hide fade" id="deleteModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Type of Pipe</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form>
				<input type="hidden" name="pipeTypeId">
				<p>
					Are you sure you want to delete <strong></strong>?<br>
					If this type is currently used with products and systems they will be affected.
				</p>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-danger" id="doDeleteBut">Delete</button>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('#createBut').click(function() {
				$('#createModal .errors').hide();
				$('#createModal').modal();
			});

			$('tbody button.editBut').click(function() {
				$('#editModal .errors').hide();
				$('#editModal input[name=typeName]').val($(this).data('typename'));
				$('#editModal input[name=pipeTypeId]').val($(this).data('id'));
				$('#editModal').modal();
			});

			$('tbody button.deleteBut').click(function() {
				$('#deleteModal .errors').hide();
				$('#deleteModal input[name=pipeTypeId]').val($(this).data('id'));
				$('#deleteModal strong').text($(this).data('typename'));
				$('#deleteModal').modal();
			});

			$('#doCreateBut').click(function() {
				$('#createModal form').submit();
			});
			$('#createModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON('{url to="PipeTypeController->create"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#createModal').modal('hide');
						window.location.reload();

					} else {
						$('#createModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#doEditBut').click(function() {
				$('#editModal form').submit();
			});
			$('#editModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON('{url to="PipeTypeController->update"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editModal').modal('hide');
						window.location.reload();

					} else {
						$('#editModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#doDeleteBut').click(function() {
				$('#deleteModal form').submit();
			});
			$('#deleteModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON('{url to="PipeTypeController->delete"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#deleteModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteModal .errors').text(data.status).show('fast');
					}
				});
			});
		});
		-->
	</script>
{/block}