{extends file="main.tpl"}

{block name="title"}Construction Type Select{/block}

{block name="mainContent"}
	<h1>Construction Type Select</h1>
	<p>Select the type of construction you&apos;ll using:</p>
	<form method="post" action="{url to="CalculatorController->systemSelect"}">
		{foreach $options as $i}
			<label class="radio"><input type="radio" name="systemId" value="{$i->id}" {if $selected == $i->id}checked{/if}> {$i->name}</label>
		{/foreach}
		<hr>
		<div class="btn-group">
			<a href="{url to="ApplicationController->index"}" class="btn">&lt; Back</a>
			<input type="submit" value="Continue &gt;" class="btn btn-primary">
		</div>
	</form>
{/block}