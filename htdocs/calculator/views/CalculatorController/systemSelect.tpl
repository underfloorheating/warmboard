{extends file="main.tpl"}
{block name="title"}Select your System{/block}
{block name="mainContent"}
	<h1>System Select</h1>
	{if count($options) > 0}
		<p>Select the system you would like to use:</p>
		<form method="post" action="{url to="CalculatorController->calc"}">
			{foreach $options as $i}
				<label class="radio"><input type="radio" name="systemId" value="{$i->id}" {if $selected == $i->id}checked{/if}> {$i->name}</label>
			{/foreach}
			<hr>
			<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
			&nbsp;
			<div class="btn-group">
				<a class="btn" href="{url to="CalculatorController->index"}">&lt; Back</a>
				<input type="submit" value="Continue &gt;" class="btn btn-primary">
			</div>
		</form>
	{else}
		<p>Nothing is available for this construction type.</p>
		<p><a class="btn" href="{url to="CalculatorController->index"}">&lt; Back</a></p>
	{/if}
{/block}