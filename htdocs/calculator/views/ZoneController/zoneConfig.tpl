{extends file="main.tpl"}
{block name="title"}Room Configuration{/block}
{block name="mainContent"}
	{if $smarty.session.theme!='plumbnation'}
		<h3>Room Configuration: {$floor->name}</h3>
	{else}
		<h1>Room Configuration: {$floor->name}</h1>
		<hr />
	{/if}
	<p {if $smarty.session.theme=='plumbnation'}class="help-block alert"{/if}>
		Each floor is split up into thermostat zones, these zones can cover one or more rooms.
		(Total max floor area {$maxProcessArea}m<sup>2</sup>).
	</p>
	<form method="post" class="form" id="main" action="{url to="ZoneController->update"}">
		{foreach $floor->_manifolds as $i}
			<fieldset>
				<legend id="leg{$i@index}" data-mindex="{$i@index}">
					Manifold {$i->ident} <small>({if $i->controlType == Manifold::CONTROLTYPE_WIRELESS}Wireless{else}Wired{/if} Control)</small>
					<div class="pull-right" data-mindex="{$i@index}" data-controltype="{$i->controlType}">
						<button class="btn btn-small" type="button" data-action="edit-man">Edit</button>
						<button class="btn btn-small btn-danger" type="button" data-action="remove-man">&times;</button>
					</div>
				</legend>
				<table class="table" id="manTbl{$i@index}" data-mindex="{$i@index}" data-maxcirc="{$maxPossibleCircuits}" data-maxdist="{PipeData::fetchMaxDistFromManifold($i->getPipeType(), $i->getSystem()->pipeWidth)}">
					<thead>
						<tr>
							<th class="span2"></th>
							<th class="span2">Room Name <i class="icon-question-sign" data-help="room-name"></i></th>
							<th class="span4">Distance from Manifold (m) (<span class="maxdist">{PipeData::fetchMaxDistFromManifold($i->getPipeType(), $i->getSystem()->pipeWidth)}</span>m max.) <i class="icon-question-sign" data-help="dist-to-manifold"></i></th>
							<th class="span2">Floor Area (m&sup2;) <i class="icon-question-sign" data-help="floor-area"></i></th>
							<th class="span2"></th>
						</tr>
					</thead>
					<tbody>
						{foreach $i->_zones as $j}
							<tr data-zindex="{$j@index}">
								<th>
									Thermostat {$j->ident}
									<button type="button" class="btn btn-small" data-action="add" data-tableid="#manTbl{$i@index}" style="display:none">Add Room</button>
								</th>
								<th class="grey">
									&nbsp;
								</th>
								<th class="grey">
									&nbsp;
								</th>
								<th class="grey">
									&nbsp;
								</th>
								<th class="grey">
									<div class="pull-right">
										<button type="button" class="btn btn-small btn-danger" data-action="remove-zone" data-mindex="{$i@index}">&times;</button>
									</div>
								</th>
							</tr>
							{foreach $j->_rooms as $k}
								<tr data-rindex="{$k@index}" data-zindex="{$j@index}">
									<td></td>
									<td><input type="text" name="z[{$i@index}][{$j@index}][{$k@index}][name]" value="{$k->name|escape}" class="span2"></td>
									<td><input type="text" name="z[{$i@index}][{$j@index}][{$k@index}][distFromManifold]" class="distFromMan span4" value="{$k->distFromManifold}"></td>
									<td><input type="text" name="z[{$i@index}][{$j@index}][{$k@index}][floorArea]" class="floorArea span2" value="{$k->floorArea}"></td>
									<td class="nowrap">
										<button type="button" class="btn" data-action="add" data-tableid="#manTbl{$i@index}" style="display:none">Add Room</button>
										<button type="button" data-action="remove" class="btn btn-danger btn-small">&times;</button>
									</td>
								</tr>
							{/foreach}
							<tr data-rindex="{count($j->_rooms)}" data-zindex="{$j@index}">
								<td></td>
								<td>
									<select class="room-name-select span2">
										{foreach $roomNames as $roomName}<option>{$roomName->name|escape}</option>{/foreach}
										<option>OTHER</option>
									</select>
									<input type="text" name="z[{$i@index}][{$j@index}][{count($j->_rooms)}][name]" value="{$roomNames[0]->name|escape}" class="room-name-input span2" style="display:none"></td>
								<td><input type="text" name="z[{$i@index}][{$j@index}][{count($j->_rooms)}][distFromManifold]" class="distFromMan span4" value=""></td>
								<td><input type="text" name="z[{$i@index}][{$j@index}][{count($j->_rooms)}][floorArea]" class="floorArea span2" value=""></td>
								<td class="nowrap">
									<button type="button" class="btn" data-action="add" data-tableid="#manTbl{$i@index}">Add Room</button>
									<button type="button" data-action="remove" class="btn btn-danger pull-right btn-small">&times;</button>
								</td>
							</tr>
						{foreachelse}
							<tr>
								<td colspan="5">No thermostats have been added.</td>
							</tr>
						{/foreach}
					</tbody>
				</table>
				<button type="button" class="btn" data-action="add-zone" data-tableid="#manTbl{$i@index}">Add another thermostat to this Manifold</button>
				<div class="sizeErr" style="color:red;"></div>
			</fieldset>
		{/foreach}
		<hr>
		<button type="button" class="btn" data-action="add-man">Add another Manifold for a change of system</button>
		<i class="icon-question-sign" data-help="add-man-btn"></i>
		<hr>
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		<i class="icon-question-sign" data-help="home"></i>
		&nbsp;
		<div class="btn-group">
			<a class="btn" href="{url to="FloorController->select"}">&lt; Back</a>
			<input type="submit" class="btn btn-primary" value="Continue &gt;" name="continue">
		</div>
	</form>

	<div class="modal hide fade" id="addManModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Add New Manifold</h3>
		</div>
		<div id="manifoldImage">
			<img src="../public/images/landing-image.jpg" />
			<div>
				<p></p>
			</div>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<p class="help-block alert">Please choose your proposed floor construction from the drop down box</p>
				<div class="control-group">
					<label class="control-label" for="constructionType">Construction Type:</label>
					<div class="controls">
						<select name="constructionType">
							<option value="0">Select a construction type</option>
							{foreach $constructionTypes as $i}
								{if $i->isEmpty()}{continue}{/if}
								<option value="{$i->id}">{$i->name}</option>
							{/foreach}
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="systemId">System: <i class="icon-question-sign" data-help="system-select"></i></label>
					<div class="controls">
						<select name="systemId">
							<option value="0">Select a system</option>
						</select>
					</div>
				</div>

				<div class="control-group hide heat-source-select">
					<label class="control-label" for="add_heatSource">Heat Source: <i class="icon-question-sign" data-help="heatsource-select"></i></label>
					<div class="controls">
						<select name="tempLevelId" id="add_heatSource">
							<option value="0">Please select a heat source</option>
						</select>
					</div>
				</div>

				<div class="control-group hide property-type-select">
					<label class="control-label" for="propertyTypeId_add">Property Type: <i class="icon-question-sign" data-help="property-type"></i></label>
					<div class="controls">
						<select id="propertyTypeId_add" name='propertyTypeId'>
							{foreach $propertyTypes as $propertyType}
								<option value="{$propertyType->id}">{$propertyType->name}</option>
							{/foreach}
						</select>
					</div>
				</div>

				<div class="control-group hide property-size">
					<label class="control-label" for="propertySize_add">Property Size (m<sup>2</sup>): <i class="icon-question-sign" data-help="property-size"></i></label>
					<div class="controls">
						<input type="text" id="propertySize_add" name="propertySize">
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="pipeTypeId">Pipe Type: <i class="icon-question-sign" data-help="pipetype-select"></i></label>
					<div class="controls">
						<select name="pipeTypeId">
							<option value="0">Please Select a type of pipe</option>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="controlType">Control Type: <i class="icon-question-sign" data-help="controltype-select"></i></label>
					<div class="controls">
						<label class="radio inline"><input type="radio" name="controlType" value="0" checked> Wired</label>
						<label class="radio inline"><input type="radio" name="controlType" value="1"> Wireless</label>
					</div>
				</div>
				<p class="help-block alert">
					If you wish to upgrade your thermostats, or would like to
					know which options are available, please contact us and
					quote your project reference when you have finished your quote.
				</p>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">Cancel</button>
			<button type="button" class="btn btn-primary" id="createBut">Create</button>
		</div>
	</div>

	<div class="modal hide fade" id="editManModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Manifold</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<input type="hidden" name="manifoldIndex" value="">

				<p class="help-block alert">Select a construction type and system you want to use with this manifold.</p>
				<div class="control-group">
					<label class="control-label" for="constructionType">Construction Type:</label>
					<div class="controls">
						<select name="constructionType">
							<option value="0">Select a constuction type</option>
							{foreach $constructionTypes as $i}
								<option value="{$i->id}">{$i->name}</option>
							{/foreach}
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="systemId">System: <i class="icon-question-sign" data-help="system-select"></i></label>
					<div class="controls">
						<select name="systemId">
							<option value="0">Select a system</option>
						</select>
					</div>
				</div>
				<div class="control-group hide heat-source-select">
					<label class="control-label" for="add_heatSource">Heat Source: <i class="icon-question-sign" data-help="heatsource-select"></i></label>
					<div class="controls">
						<select name="tempLevelId" id="edit_heatSource">
							<option value="0">Please select a heat source</option>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="pipeTypeId">Pipe Type: <i class="icon-question-sign" data-help="pipetype-select"></i></label>
					<div class="controls">
						<select name="pipeTypeId">
							<option value="0">Please Select a type of pipe</option>
						</select>
					</div>
				</div>

				<div class="control-group">
					<label class="control-label" for="controlType">Control Type: <i class="icon-question-sign" data-help="controltype-select"></i></label>
					<div class="controls">
						<label class="radio inline"><input type="radio" name="controlType" value="0"> Wired</label>
						<label class="radio inline"><input type="radio" name="controlType" value="1"> Wireless</label>
					</div>
				</div>
				<p class="help-block alert">
					If you wish to upgrade your thermostats, or would like to
					know which options are available, please contact us and
					quote your project reference when you have finished your quote.
				</p>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">Cancel</button>
			<button type="button" class="btn btn-primary" id="editBut">Save</button>
		</div>
	</div>
	<div class="modal hide fade" id="tooMuchAreaModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Area Over-size</h3>
		</div>
		<div class="modal-body">
			<p>
				This calculator was designed to handle a total floor area of no
				more than {$maxProcessArea}m<sup>2</sup>.<br>
				You currently have <span></span>m<sup>2</sup> configured.
				Please contact us on 0044 (0) 1772 312847 where we will be happy
				to assist you.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		</div>
	</div>
	<div class="modal hide fade" id="tooMuchDistModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Distance From Manifold too large</h3>
		</div>
		<div class="modal-body">
			<p>
				The distance from the manifold is greater than the maximum allowable for your system.<br>
				It should be no greater than <span></span>m. It is important that you plan your manifold locations<br>
				with particular attention to the connecting pipework lengths.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		</div>
	</div>
	<script type="text/javascript">
		var isCopying = false;
		$(function() {
			$("#addManModal select[name=constructionType]").change(function() {
				switch (+$(this).val()) {
					case 1:
						$('#manifoldImage img').attr('src', '{url public="images/systemImages/screedSystems/Screed-CASTLELATION-MAT-2.jpg"}');
						break;

					case 2:
						$('#manifoldImage img').attr('src', '{url public="images/constructionImage_floating.jpg"}');
						break;

					case 3:
						$('#manifoldImage img').attr('src', '{url public="images/systemImages/suspendedSystems/heatPlates/SOLFEX-Heat-Plates--35-C1.jpg"}');
						break;
				}

				$('#manifoldImage').delay(300).fadeIn(500);

				$('#addManModal').animate({ marginLeft : -445 }, 500);
			});
			$("#addManModal select[name=systemId]").change(function() {
				$('#manifoldImage>div').html($('option[value=' + $(this).val() + ']', this).data('desc'));
				$('#manifoldImage>img').attr('src', $('option[value=' + $(this).val() + ']', this).data('img'));
			});

			$('.room-name-select').change(function() {
				var roomName = $(this).val();
				if (roomName == 'OTHER') {
					$(this).hide();
					$('.room-name-input', $(this).parent()).show();
				} else {
					$('.room-name-input', $(this).parent()).val(roomName);
				}
			});

			setupPopovers($('.icon-question-sign'));
		});
		<!--
		$(function() {
			$('form button').click(function(e) {
				btnClicked(this);
			});

			$('form input.floorArea, form input.distFromMan').change(function() {
				checkCircuitCount();
			});

			$('.modal button#createBut').click(function() {
				$('#addManModal form').submit();
			});
			$('#addManModal form').submit(function(e) {
				e.preventDefault();
				var controlType = $('#addManModal input[name=controlType]:checked').val();
				createManifold(controlType, function() {
					$('#addManModal').modal('hide');
				});
			});
			$('#addManModal').on('hidden', function() {
				if (!$(this).data('shallCopyRoom') || isCopying)
					return true;

				var oldRow;
				isCopying = true;
				var maxArea = ($(this).data('tbody').data('area-cache') / $(this).data('tbody').data('circ-cache')) * $(this).data('tbody').parent().data("maxcirc");
				$('tr', $(this).data('tbody')).each(function(index, item) {
					var area = $('.floorArea', item).val();
					if (area < maxArea && area != 0)
						oldRow = $(item);
				});
				if (oldRow == undefined) {
					oldRow = $('tr:last', $(this).data('tbody')).prev();
					if (oldRow == undefined || oldRow.children().first().is('th'))
						oldRow = $('tr:last', $(this).data('tbody'));
				}
				var newRow = $('fieldset:last tr:last');
				copyRoom(oldRow, newRow);
				if ($('.floorArea', oldRow).val() < maxArea) {
					removeRow($('button', oldRow));

				} else {
					var diff = $('.floorArea', oldRow).val() - maxArea;
					$('.floorArea', oldRow).val(maxArea);
					$('.floorArea', newRow).val(diff);
				}
				$('button[data-action=add]', newRow).click();
				isCopying = false;
				didCreate = false;
				checkCircuitCount();
			});


			$('.modal button#editBut').click(function() {
				$('#editManModal form').submit();
			});
			$('#editManModal form').submit(function(e) {
				e.preventDefault();
				var controlType = $('#editManModal input[name=controlType]:checked').val();
				var manifoldIndex = $('#editManModal input[name=manifoldIndex]').val();
				editManifold(manifoldIndex, controlType);

				$.getJSON("{url to="ManifoldController->setManifoldDetails"}",
					{
						manifold: manifoldIndex,
						systemId: $('#editManModal select[name=systemId]').val(),
						pipeTypeId: $('#editManModal select[name=pipeTypeId]').val(),
						tempLevelId: $('#editManModal select[name=tempLevelId]').val()
					},
					function(data) {
						if (data.status == 'OK') {
							$('#manTbl' + manifoldIndex).data('maxcirc', data.maxCircuits);
							$('#manTbl' + manifoldIndex).data('maxdist', data.maxDistFromManifold);
							$('#manTbl' + manifoldIndex + ' .maxdist').text(data.maxDistFromManifold);
							checkCircuitCount();
						}
					}
				);

				$('#editManModal').modal('hide');
			});

			checkCircuitCount();

			{if count($floor->_manifolds) == 0}
				$('form#main button[data-action=add-man]').click();
			{/if}

			$('select[name=constructionType]').change(function() {
				if ($(this).val() != 0) {
					var element = this;
					$.getJSON('{url to="constructionTypeController->getSystems"}', { constructionType: $(this).val() }, function(data) {
						if (data.status == "OK") {
							var e = [$("<option value=\"0\">Select a system</option>")];
							for (var i = 0; i < data.systems.length; i++) {
								e[i+1] = $('<option value="' + data.systems[i].id + '">' +
										data.systems[i].name + "</option>").data('img',
										"{url public=ImageHelper::retrieveImagePath(ImageHelper::PRODUCT_DIR,'')}" + data.systems[i].image).data('desc', data.systems[i].description);
							}
							$('select[name=systemId]', $(element).parents('form')).empty().append(e).val(0);
						} else {
							$('select[name=systemId]', $(this).parents('form')).html("<option value=\"0\">Select a system</option>").val(0);
						}
					});
				} else {
					$('select[name=systemId]', $(this).parents('form')).html("<option value=\"0\">Select a system</option>").val(0);
				}
			});
			$('select[name=systemId]').change(function() {
				if ($(this).val() != 0) {
					var element = this;
					var form = $(element).parents('form');
					$.getJSON('{url to="SystemController->getPipeTypes"}', { systemId: $(this).val() }, function(data) {
						if (data.status == "OK") {
							if (data.pipes.length > 1) {
								var htm = "<option value=\"0\">Select a type of pipe</option>";
								for (var i = 0; i < data.pipes.length; i++) {
									htm += "<option value=\"" + data.pipes[i].id + "\">" + data.pipes[i].typeName + "</option>";
								}
								$('select[name=pipeTypeId]', form).html(htm).val(0).prop('readonly', false);

							} else {
								var htm = "<option value=\"" + data.pipes[0].id + "\">" + data.pipes[0].typeName + "</option>"
								$('select[name=pipeTypeId]', form).prop('readonly', true).html(htm).val(data.pipes[0].id);
							}
						} else {
							$('select[name=pipeTypeId]', form).html("<option value=\"0\">Select a type of pipe</option>").val(0);
						}
					});
					$.getJSON('{url to="SystemController->getTemperatureLevels"}', { systemId: $(this).val() }, function(data) {
						if (data.status == "OK") {
							if (data.tempLevels.length > 1) {
								$('.heat-source-select', form).slideDown();
								var htm = "<option value=\"0\">Select a type of heat source</option>";
								for (var i = 0; i < data.tempLevels.length; i++) {
									if (data.tempLevels[i].hidden == 1) continue;
									htm += "<option value=\"" + data.tempLevels[i].id + "\">" + data.tempLevels[i].name + "</option>";
								}
								$('select[name=tempLevelId]', form).html(htm).val(0).prop('readonly', false);

							} else {
								$('.heat-source-select', form).slideUp();
								var htm = "<option value=\"" + data.tempLevels[0].id + "\">" + data.tempLevels[0].name + "</option>"
								$('select[name=tempLevelId]', form).prop('readonly', true).html(htm).val(data.tempLevels[0].id);
							}

							if (data.showPropertyType == 1) {
								$('.property-type-select, .property-size').slideDown();
							} else {
								$('.property-type-select, .property-size').slideUp();
							}
						} else {
							$('select[name=tempLevelId]', form).html("<option value=\"0\">Select a heat source</option>").val(0);
						}
					});
				} else {
					$('select[name=pipeTypeId]', $(this).parents('form')).html("<option value=\"0\">Select a type of pipe</option>").val(0);
					$('select[name=tempLevelId]', $(this).parents('form')).html("<option value=\"0\">Select a heat source</option>").val(0);
				}
			});
		});

		function btnClicked(btn) {
			switch ($(btn).data('action')) {
				case 'add':
					addRow(btn);
					break;

				case 'remove':
					removeRow(btn);
					break;

				case 'add-man':
					showAddManModal(btn, false);
					break;

				case 'edit-man':
					showEditManModal(btn);
					break;

				case 'add-man-over':
					showAddManModal(btn, true);
					break;

				case 'remove-man':
					removeManifold(btn);
					break;

				case 'add-zone':
					addZone(btn);
					break;

				case 'remove-zone':
					removeZone(btn);
					break;
			}
		}

		function addRow(btn) {
			var tblID = $(btn).data('tableid');
			var manifoldIndex = $(tblID).data('mindex');
			var zoneIndex = $(btn).parents('tr').data('zindex');
			var lastRow = $(tblID + '>tbody>tr[data-zindex=' + zoneIndex + ']:last');
			var roomIndex = lastRow.data('rindex');
			if (roomIndex == undefined)
				roomIndex = 0;
			else
				roomIndex++;

			var row = $('<tr data-zindex="' + zoneIndex + '" data-rindex="'+ roomIndex + '">\
				<td></td><td>\
					<select class="room-name-select span2">\
						{foreach $roomNames as $roomName}<option>{$roomName->name|escape}</option>{/foreach}\
						<option>OTHER</option>\
					</select>\
					<input type="text" name="z[' + manifoldIndex + '][' + zoneIndex + '][' + roomIndex + '][name]" class="room-name-input span2" value="{$roomNames[0]->name|escape}" style="display:none">\
				<td><input type="text" name="z[' + manifoldIndex + '][' + zoneIndex + '][' + roomIndex + '][distFromManifold]" class="distFromMan span4" value=""></td>\
				<td><input type="text" name="z[' + manifoldIndex + '][' + zoneIndex + '][' + roomIndex + '][floorArea]" class="floorArea span2" value=""></td>\
				<td>\
				<button type="button" class="btn" data-action="add" data-tableid="#manTbl' + manifoldIndex + '">Add Room</button>\
				<button type="button" data-action="remove" class="btn btn-danger pull-right btn-small">&times;</button>\
				</td>\
			</tr>');

			row.hide();
			lastRow.after(row.show('fast'));
			$(btn).hide('fast');

			$(tblID + '>tbody>tr[data-zindex=' + zoneIndex + ']>td[colspan=4]').parent().remove();
			$('button', row).click(function(e) {
				btnClicked(this);
			});
			$('input.floorArea, input.distFromMan', row).change(function() {
				checkCircuitCount();
			});
			$('.room-name-select', row).change(function() {
				var roomName = $(this).val();
				if (roomName == 'OTHER') {
					$(this).hide();
					$('.room-name-input', row).show();
				} else {
					$('.room-name-input', row).val(roomName);
				}
			});
			$('input:first', row).focus();
		}

		function removeRow(btn) {
			$(btn).parents('tr').hide('fast', function() {
				var par = $(this).parent();
				$(this).remove();
				var tRows = $('tr[data-zindex=' + $(this).data('zindex') + ']', par);
				if (tRows.length == 1) {
					tRows.after(
						$('<tr style="display:none" data-zindex=' + $(this).data('zindex') + '><td></td><td colspan="4">No rooms have been added.</td></tr>').show('fast'));
				}
				checkCircuitCount();
			});
		}

		function checkCircuitCount() {
			var globalArea = 0;

			$('form>fieldset').each(function(i, fld) {
				var numCirc = 0;
				var manArea = 0;
				var maxPossibleCircuits = $('table', fld).data("maxcirc");
				var maxDistFromManifold = $('table', fld).data("maxdist");
				var mIndex = $('table', fld).data("mindex");
				$('tbody>tr[data-rindex]', fld).each(function(j, row) {
					$.ajax('{url to="ManifoldController->getNumCircuits"}', {
						dataType: 'json',
						async: false,
						data: {
							manifold: mIndex,
							dist: $('input.distFromMan', row).val(),
							area: $('input.floorArea', row).val()
						},
						success: function(data) {
							if (data.status = 'OK') {
								numCirc += +data.circuits;
							}
						}
					});
					manArea += +$('input.floorArea', row).val();
				});

				$('tbody', fld).data('circ-cache', numCirc);
				$('tbody', fld).data('area-cache', manArea);

				$('.sizeErr', fld).empty();
				if (numCirc > +maxPossibleCircuits) {
					$('.sizeErr', fld).append($('<div class="row"><p class="span6">The total area entered requires ' + numCirc + ' circuits, manifolds can only have a maximum of ' + maxPossibleCircuits + '.\
						Would you like to add another manifold?</p><div class="span2"><button type="button" class="btn" data-action="add-man-over">Add Manifold</button></div></div>'));
					$('.sizeErr button', fld).click(function() {
						btnClicked(this);
					});
				}
				
				if (numCirc < +maxPossibleCircuits)
					$('button[data-action=add]:last', fld).show('fast');
				else
					$('button[data-action=add]', fld).hide('fast');

				$('input.distFromMan', fld).each(function(index, item) {
					if (+$(item).val() > +maxDistFromManifold) {
						$('#tooMuchDistModal p>span').text(maxDistFromManifold);
						$('#tooMuchDistModal').modal();
					}
				});

				globalArea += manArea;
			});

			if (!isCopying && globalArea > {$maxProcessArea}) {
				$('#tooMuchAreaModal p>span').text(globalArea);
				$('#tooMuchAreaModal').modal();
			}
		}

		function showEditManModal(btn) {
			$('#editManModal input[type=hidden]').val($(btn).parent().data('mindex'));
			$('#editManModal input[name=controlType][value=' + $(btn).parent().data('controltype') + ']').prop('checked', true);
			$('#editManModal').modal();
		}

		function showAddManModal(btn, shallCopyRoom) {
			$('#addManModal').data('tbody', $('tbody', $(btn).parents('fieldset')));
			$('#addManModal').data('shallCopyRoom', shallCopyRoom);
			$('#addManModal').modal();
		}

		function createManifold(controlType, callback) {
			$.getJSON("{url to="ManifoldController->createManifold"}", $('#addManModal form').serialize(), function(data) {
				if (data.status == 'OK') {
					var e = $('<fieldset style="display:none">\
						<legend id="leg' + data.manifoldIndex + '" data-mindex="' + data.manifoldIndex + '">\
							Manifold ' + (data.manifoldIndex + 1) + ' <small>(Wired Control)</small>\
							<div class="pull-right" data-mindex="' + data.manifoldIndex + '" data-controltype="0">\
								<button class="btn btn-small" type="button" data-action="edit-man">Edit</button>\
								<button class="btn btn-small btn-danger" type="button" data-action="remove-man">&times;</button>\
							</div>\
						</legend>\
						<table class="table" id="manTbl' + data.manifoldIndex + '" data-mindex="' + data.manifoldIndex +
								'" data-maxcirc="' + data.maxCircuits +
								'" data-maxdist="' + data.maxDistFromManifold + '">\
							<thead>\
								<tr>\
									<th class="span2"></th>\
									<th class="span2">Room Name <i class="icon-question-sign" data-help="room-name"></i></th>\
									<th class="span4">Distance from Manifold (m) (<span class="maxdist">'+data.maxDistFromManifold+'</span>m max.) <i class="icon-question-sign" data-help="dist-to-manifold"></i></th>\
									<th class="span2">Floor Area (m&sup2;) <i class="icon-question-sign" data-help="floor-area"></i></th>\
									<th class="span2"></th>\
								</tr>\
							</thead>\
							<tbody>\
								<tr>\
									<td colspan="5">No thermostats have been added.</td>\
								</tr>\
							</tbody>\
						</table>\
						<button type="button" class="btn" data-action="add-zone" data-tableid="#manTbl' + data.manifoldIndex + '">Add another thermostat to this Manifold</button>\
						<div class="sizeErr" style="color:red;"></div>\
					</fieldset>');
					$('form#main>hr:first').before(e.show('fast'));

					$('button', e).click(function(e) {
						btnClicked(this);
					});
					setupPopovers($('.icon-question-sign', e));
					editManifold(data.manifoldIndex, controlType);

					addZone($('fieldset:last button[data-action=add-zone]'));

					checkCircuitCount();
					
					if (callback != undefined)
						callback(data);

					$('#addManModal .property-type-select, #addManModal .property-size').hide();

				} else {
					alert(data.status);
				}
			});
		}

		function removeManifold(btn) {
			if ($('form#main>fieldset').size() > 1) {
				$.getJSON("{url to="ManifoldController->removeManifold"}",
					{ s: 7, manifold: $(btn).parent().data('mindex') },
					function(data) {
						if (data.status == "OK") {
							$(btn).parents('fieldset').hide('fast', function() {
								$(this).remove();	
							});
						}
					}
				);
			}
		}

		function copyRoom(oldRow, newRow) {
			var oldInputs = $('input', oldRow).toArray();
			var newInputs = $('input', newRow).toArray();
			for (var i = 0; i < oldInputs.length; i++) {
				$(newInputs[i]).val($(oldInputs[i]).val());
			}
		}

		function editManifold(manifoldIndex, controlType) {
			$.getJSON("{url to="ManifoldController->setControlType"}",
				{ manifold: manifoldIndex, controlType: controlType },
				function(data) {
					if (data.status == "OK") {
						$('#leg' + manifoldIndex + ' small').text('(' + (controlType == 0 ? 'Wired' : 'Wireless') + ' Control)');
						$('#leg' + manifoldIndex + ' div.pull-right').data('controltype', controlType);
					}
				}
			);
		}

		function addZone(btn, callback) {
			var tblID = $(btn).data('tableid');
			var manifoldIndex = $(tblID).data('mindex');

			$.getJSON("{url to="ZoneController->addZone"}", { manifoldIdx: manifoldIndex }, function(data) {
				if (data.status == "OK") {
					var row = $('<tr style="display:none" data-zindex="' + data.zone + '">\
						<th>\
							Thermostat ' + data.ident + '\
							<button type="button" class="btn btn-small" data-action="add" data-tableid="#manTbl' + manifoldIndex + '">Add Room</button>\
						</th>\
						<th class="grey">\
							&nbsp;\
						</th>\
						<th class="grey">\
							&nbsp;\
						</th>\
						<th class="grey">\
							&nbsp;\
						</th>\
						<th class="grey">\
							<div class="pull-right">\
								<button type="button" class="btn btn-small btn-danger" data-action="remove-zone" data-mindex="' + manifoldIndex + '">&times;</button>\
							</div>\
						</th>\
					</tr>\
					<tr style="display:none" data-zindex="' + data.zone + '">\
						<td></td><td colspan="4">No rooms have been added.</td>\
					</tr>');

					$(tblID + '>tbody>tr>td[colspan=5]').parent().remove();

					$(tblID + '>tbody').append(row);
					$('tr',row.parent()).show('fast');
					$('button', row).click(function(e) {
						btnClicked(this);
					});

					addRow($('button[data-action=add]', row));

					if (callback !== undefined)
						callback(row[2]);
				}
			});
		}

		function removeZone(btn) {
			var row = $(btn).parents('tr');
			var tblBody = $(row).parent();
			var data = {
				manifoldIdx: $(btn).data('mindex'),
				zoneIdx: row.data('zindex')
			}

			$.getJSON("{url to="ZoneController->removeZone"}", data, function(data) {
				if (data.status == "OK") {
					$('tr[data-zindex=' + row.data('zindex') + ']', row.parent()).hide('fast', function() {
 						$(this).remove();
						if ($(tblBody).children().length == 0) {
							$(tblBody).append(
								$('<tr style="display:none"><td colspan="5">No thermostats have been added.</td></tr>').show('fast'));
						}
					});
				}
			});
		}

		function setupPopovers(elements) {
			var title, content, position;

			elements.each(function() {
				switch ($(this).data('help')) {
					case 'floor-area':
						title = 'Area';
						content = 'Enter floor area (m<sup>2</sup>) excluding any non-heated areas, such as kitchen units, showers ect.';
						position = 'bottom';
						break;

					case 'dist-to-manifold':
						title = 'Distance to manifold';
						content = 'Distance from the room to the proposed manifold location. This should be measured using the actual route the pipework will take, rather than the most direct route.';
						position = 'bottom';
						break;

					case 'home':
						title = 'Home';
						content = 'Please note if you select the home button this will take you back to main screen and your quotation will not be saved';
						position = 'top';
						break;

					case 'room-name':
						title = 'Room name';
						content = 'Please select the room you require heating from the drop down menu, if your room is not listed, you can select other and manually enter the name';
						position = 'bottom';
						break;

					case 'system-select':
						title = '';
						content = 'These are the options available for the floor construction type';
						position = 'right';
						break;

					case 'heatsource-select':
						title='';
						content = 'For heat pump this will calculate 150 or 200mm pipe spacing&apos;s, where as a boiler will only calculate 200mm pipe spacing&apos;s.';
						position = 'right';
						break;

					case 'pipetype-select':
						title = '';
						content = 'Select the type of pipe you would like to use with this system.<br>\
							Pexline 5 layer - This is a 5 layer pipe consisting of cross linked poythene outer and inner layers, adhesive layers and an oxygen barrier EVOH layer.<br>\
							Pex-al-pex - This has a pex cross linked outer and inner layer, adhesive layers and aluminium layer.';
						position = 'right';
						break;

					case 'controltype-select':
						title = '';
						content = 'Select the type of control system to use with the thermostat.';
						position = 'right';
						break;

					case 'property-type':
						title = 'Property Type';
						content = 'Please select the most relevant property type for your project and we will calculate the estimated heat loss per square meter';
						position = 'top';
						break;

					case 'property-size':
						title = 'Property Size';
						content = 'Please select the total property size in m<sup>2</sup>';
						position = 'top';
						break;

					case 'add-man-btn':
						title = '';
						content = 'You can have different systems on the same floor but must add a new manifold';
						position = 'right';
						break;
						
					default:
						return;
				}

				$(this).popover({
					title: title,
					content: content,
					placement: position,
					trigger: 'hover',
					container: 'body',
					html: 'true',
				});
			});
		}
		-->
	</script>
{/block}
