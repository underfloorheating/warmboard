{extends file="main.tpl"}
{block name="title"}Temperature Levels{/block}
{block name="mainContent"}
	<h3>Temperature Levels</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Pipe Centres</th>
				<th>Hidden</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $tempLevels as $tempLevel}
				<tr>
					<td>{$tempLevel->id}</td>
					<td>{$tempLevel->name}</td>
					<td>{$tempLevel->pipeCentres}</td>
					<td>{if $tempLevel->hidden}Hidden{else}Visible{/if}</td>
					<td>
						<button type="button" class="btn btn-small editBut" data-id="{$tempLevel->id}" data-name="{$tempLevel->name}" data-pipecentres="{$tempLevel->pipeCentres}" data-hidden="{if $tempLevel->hidden}true{else}false{/if}">
							<i class="icon-edit"></i> Edit
						</button>
						<button type="button" class="btn btn-small deleteBut" data-id="{$tempLevel->id}" data-name="{$tempLevel->name}">
							<i class="icon-trash"></i> Delete
						</button>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="3">No Temperature Levels have been added.</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a href="{url to="ApplicationController->index"}" class="btn"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a href="{url to="ProductController->index"}" class="btn">&lt; Back to Product List</a>
		&nbsp;
		<button type="button" class="btn" id="createBut">Add new Temperature Level</button>
	</div>
	<div class="modal hide fade" id="createModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create New Temperature Level</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="create_name">Name:</label>
					<div class="controls">
						<input type="text" id="create_name" name="name" maxlength="50">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_pipecentres">Pipe Centres:</label>
					<div class="controls">
						<input type="text" id="create_pipecentres" name="pipeCentres" maxlength="8">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="hidden">Is Hidden:</label>
					<div class="controls">
						<input type="checkbox" id="hidden" name="hidden">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-primary" id="doCreateBut">Create</button>
		</div>
	</div>
	<div class="modal hide fade" id="editModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Temperature Level</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form class="form-horizontal">
				<input type="hidden" name="tempLevelId">
				<div class="control-group">
					<label class="control-label" for="edit_name">Name:</label>
					<div class="controls">
						<input type="text" id="edit_name" name="name" maxlength="50">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_pipecentres">Pipe Centres:</label>
					<div class="controls">
						<input typr="text" id="edit_pipecentres" name="pipeCentres" maxlength="8">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="hidden">Is Hidden:</label>
					<div class="controls">
						<input type="checkbox" id="hidden" name="hidden">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-primary" id="doEditBut">Edit</button>
		</div>
	</div>
	<div class="modal hide fade" id="deleteModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Temperature Level</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form>
				<input type="hidden" name="tempLevelId">
				<p>
					Are you sure you want to delete <strong></strong>?<br>
					If this temperature level is currently used with products and systems they will be affected.
				</p>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-danger" id="doDeleteBut">Delete</button>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('#createBut').click(function() {
				$('#createModal .errors').hide();
				$('#createModal').modal();
			});

			$('tbody button.editBut').click(function() {
				$('#editModal .errors').hide();
				$('#editModal input[name=name]').val($(this).data('name'));
				$('#editModal input[name=pipeCentres]').val($(this).data('pipecentres'));
				$('#editModal input[name=tempLevelId]').val($(this).data('id'));
				$('#editModal input[name=hidden]').prop('checked', $(this).data('hidden'));
				$('#editModal').modal();
			});

			$('tbody button.deleteBut').click(function() {
				$('#deleteModal .errors').hide();
				$('#deleteModal input[name=tempLevelId]').val($(this).data('id'));
				$('#deleteModal strong').text($(this).data('name'));
				$('#deleteModal').modal();
			});

			$('#doCreateBut').click(function() {
				$('#createModal form').submit();
			});
			$('#createModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON('{url to="TemperatureLevelController->create"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#createModal').modal('hide');
						window.location.reload();

					} else {
						$('#createModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#doEditBut').click(function() {
				$('#editModal form').submit();
			});
			$('#editModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON('{url to="TemperatureLevelController->update"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editModal').modal('hide');
						window.location.reload();

					} else {
						$('#editModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#doDeleteBut').click(function() {
				$('#deleteModal form').submit();
			});
			$('#deleteModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON('{url to="TemperatureLevelController->delete"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#deleteModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteModal .errors').text(data.status).show('fast');
					}
				});
			});
		});
		-->
	</script>
{/block}