{extends file="main.tpl"}
{block name="title"}Floor Name Management{/block}
{block name="mainContent"}
	<h3>Floor Name Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th></th>
		</thead>
		<tbody class="text-center">
			{foreach $names as $name}
				<tr>
					<td>{$name->id}</td>
					<td>{$name->name}</td>
					<td>
						<button class="btn btn-small" data-action="edit-name"
							data-id="{$name->id}"
							data-name="{$name->name}"
							data-orderval="{$name->orderVal}">
							<i class="icon-edit"></i> Edit
						</button>
						<button class="btn btn-small btn-danger" data-action="delete-name" data-id="{$name->id}" data-name="{$name->name}">
							<i class="icon-trash icon-white"></i> Delete
						</button>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<button type="button" class="btn" data-action="add-name">Add Floor Name</button>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="FloorNameController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="FloorNameController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="FloorNameController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="createFloorNameModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create Floor Name</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<div class="control-group">
					<label class="control-label" for="create_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="create_name">
					</div>
				</div>
				<div class="form-horizontal">
					<label class="control-label" for="create_orderval">Display Order:</label>
					<div class="controls">
						<input type="text" name="orderVal" id="create_orderval">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="add">Add</a>
		</div>
	</div>
	<div class="modal hide fade" id="editFloorNameModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Floor Name</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<input type="hidden" name="floor_name_id">
				<div class="control-group">
					<label class="control-label" for="edit_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="edit_name">
					</div>
				</div>
				<div class="form-horizontal">
					<label class="control-label" for="edit_orderval">Display Order:</label>
					<div class="controls">
						<input type="text" name="orderVal" id="edit_orderval">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="edit">Edit</a>
		</div>
	</div>
	<div class="modal hide fade" id="deleteFloorNameModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Floor Name</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<p>Are you sure you want to delete the contact <strong></strong></p>
				<input type="hidden" name="floor_name_id">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('button').click(function() {
				procButtonClick(this);
			});

			$('#createFloorNameModal #add').click(function(e) {
				e.preventDefault();
				$('#createFloorNameModal form').submit();
			});
			$('#createFloorNameModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="FloorNameController->create"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#createFloorNameModal').modal('hide');
						window.location.reload();

					} else {
						$('#createFloorNameModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#editFloorNameModal #edit').click(function(e) {
				e.preventDefault();
				$('#editFloorNameModal form').submit();
			});
			$('#editFloorNameModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="FloorNameController->edit"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editFloorNameModal').modal('hide');
						window.location.reload();

					} else {
						$('#editFloorNameModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#deleteFloorNameModal #delete').click(function(e) {
				$.getJSON('{url to="FloorNameController->delete"}', { floor_name_id: $('#deleteFloorNameModal input[name=floor_name_id]').val() }, function(data) {
					if (data.status == "OK") {
						$('#deleteFloorNameModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteFloorNameModal .errors').text(data.status).show('fast');
					}
				});
			});
		});

		function procButtonClick(button) {
			switch ($(button).data("action")) {
				case "add-name":
					$('#createFloorNameModal').modal();
					break;

				case "edit-name":
					$('#editFloorNameModal input[name=floor_name_id]').val($(button).data("id"));
					$('#editFloorNameModal input[name=name]').val($(button).data("name"));
					$('#editFloorNameModal input[name=orderVal]').val($(button).data("orderval"));
					$('#editFloorNameModal').modal();
					break;

				case "delete-name":
					$('#deleteFloorNameModal .errors').hide();
					$('#deleteFloorNameModal input[name=floor_name_id]').val($(button).data("id"));
					$('#deleteFloorNameModal strong').text($(button).data("name"));
					$('#deleteFloorNameModal').modal();
					break;
			}
		}
		-->
	</script>
{/block}