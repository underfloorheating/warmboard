{extends file="main.tpl"}
{block name="title"}Property Type Management{/block}
{block name="mainContent"}
	<h3>Property Type Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Energy Loss (W/m<sup>2</sup>)</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $types as $propertyType}
				<tr>
					<td>{$propertyType->id}</td>
					<td>{$propertyType->name|escape}</td>
					<td>{$propertyType->wLoss}</td>
					<td>
						<button class="btn btn-small" data-action="edit-type"
							data-id="{$propertyType->id}"
							data-name="{$propertyType->name|escape}"
							data-wloss="{$propertyType->wLoss}">
							<i class="icon-edit"></i> Edit
						</button>
						<button class="btn btn-small btn-danger" data-action="delete-type"
							data-id="{$propertyType->id}"
							data-name="{$propertyType->name|escape}">
							<i class="icon-trash icon-white"></i> Delete
						</button>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a href="{url to="ProductController->index"}" class="btn">&lt; Back to Product List</a>
		&nbsp;
		<button type="button" class="btn" data-action="add-type">Add Property Type</button>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="PropertyTypeController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="PropertyTypeController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="PropertyTypeController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="createTypeModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create Property Type</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<div class="control-group">
					<label class="control-label" for="create_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="create_name">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_wLoss">Energy Loss (W/m<sup>2</sup>):</label>
					<div class="controls">
						<input type="text" name="wLoss" id="create_wLoss">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="add">Add</a>
		</div>
	</div>
	<div class="modal hide fade" id="editTypeModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Property Type</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error"></div>
				<input type="hidden" name="typeId">
				<div class="control-group">
					<label class="control-label" for="edit_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="edit_name">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_wLoss">Energy Loss (W/m<sup>2</sup>):</label>
					<div class="controls">
						<input type="text" name="wLoss" id="edit_wLoss">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="edit">Edit</a>
		</div>
	</div>
	<div class="modal hide fade" id="deleteTypeModal">
		<div class="modal-header">
			<buttom class="close" data-dismiss="modal" aria-hidden="true">&times;</buttom>
			<h3>Delete Property Type</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<p> Are you sure you want to delete propery type <strong></strong></p>
				<input type="hidden" name="typeId">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		$(function () {
			$('button').click(function() {
				procButtonClick(this);
			});

			$('#createTypeModal #add').click(function(e) {
				e.preventDefault();
				$('#createTypeModal form').submit();
			});
			$('#createTypeModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="PropertyTypeController->create"}', $(this).serialize(),
					function(data) {
						if (data.status == "OK") {
							$('#createTypeModal').modal('hide');
							window.location.reload();

						} else {
							$('#createTypeModal .errors').text(data.status).show('fast');
						}
					});
			});

			$('#editTypeModal #edit').click(function(e) {
				e.preventDefault();
				$('#editTypeModal form').submit();
			});
			$('#editTypeModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="PropertyTypeController->edit"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editTypeModal').modal('hide');
						window.location.reload();

					} else {
						$('#editTypeModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#deleteTypeModal #delete').click(function(e) {
				e.preventDefault();
				$('#deleteTypeModal form').submit();
			});
			$('#deleteTypeModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="PropertyTypeController->delete"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#deleteTypeModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteTypeModal .errors').text(data.status).show('fast');
					}
				});
			});
		});

		function procButtonClick(button) {
			switch ($(button).data("action")) {
				case "add-type":
					$('#createTypeModal').modal();
					break;

				case "edit-type":
					$('#editTypeModal [name=typeId]').val($(button).data('id'));
					$('#edit_name').val($(button).data('name'));
					$('#edit_wLoss').val($(button).data('wloss'));
					$('#editTypeModal').modal();
					break;

				case "delete-type":
					$('#deleteTypeModal .errors').hide();
					$('#deleteTypeModal [name=typeId]').val($(button).data('id'));
					$('#deleteTypeModal strong').text($(button).data('name'));
					$('#deleteTypeModal').modal();
					break;
			}
		}
	</script>
{/block}