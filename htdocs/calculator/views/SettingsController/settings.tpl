{extends file="main.tpl"}
{block name="title"}Settings{/block}
{block name="mainContent"}
	<h3>Calculator Global Settings and Configuration</h3>
	{if isset($smarty.session.settingsUpdated)}
		<div class="alert alert-success">
			<p>{$smarty.session.settingsUpdated}</p>
		</div>
	{/if}
	<form class="form-horizontal" action="{url to="SettingsController->save"}">
		<div class="row">
			<div class="span6">
				<div class="control-group">
					<label class="control-label" for="{Setting::CORE_MAX_ACCEPT_AREA}">Maximum Supported Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="{Setting::CORE_MAX_ACCEPT_AREA}" value="{$values[Setting::CORE_MAX_ACCEPT_AREA]}">
							<span class="add-on">m<sup>2</sup></span>
						</div>
						<p class="help-block">
							If an installer attempts to calculate based on an
							area larger than this, they will be alerted to
							contact Solfex.<br>
							<strong>Default:</strong> <em>200</em>
						</p>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="{Setting::CORE_NOTIFICATION_EMAIL}">Notification Email:</label>
					<div class="controls">
						<input type="text" name="{Setting::CORE_NOTIFICATION_EMAIL}" value="{$values[Setting::CORE_NOTIFICATION_EMAIL]}">
						<p class="help-block">
							The email to send notices about installers
							completing calculations to, possibly more things in future.
						</p>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="pull-left">
			<a href="{url to="ApplicationController->index"}" class="btn"><i class="icon-home"></i> Home</a>
			&nbsp;
			<div class="btn-group">
				<input type="reset" value="Restore" class="btn">
				<input type="submit" value="Save" class="btn btn-primary">
			</div>
			&nbsp;
			<div class="btn-group">
				<a class="btn" href="{url to="ContactController->index"}">PDF Contact Management</a>
				<a class="btn" href="{url to="FloorNameController->index"}">Floor Names</a>
				<a class="btn" href="{url to="RoomNameController->index"}">Room Names</a>
			</div>
		</div>
	</form>
{/block}