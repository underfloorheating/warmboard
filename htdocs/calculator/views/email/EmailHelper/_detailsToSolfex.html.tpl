{extends file="email/emailMain.html.tpl"}
{block name="content"}
    <p>
      <br>
      An Installer has completed a calculation.
    </p>
    <p>
      <strong>Name:</strong> {$fullName}<br>
      <strong>Email:</strong> {$email}<br>
      <strong>Tel:</strong> {$telephone}<br>
      <strong>Recall Code:</strong> {$code}<br>
      <strong>Calculation worth:</strong> &pound;{$total|number_format:2}
    </p>
    <p>
      ~ UFH Quick Quote Calculator
      <br>
      <br>
    </p>
{/block}