{extends file="email/emailMain.html.tpl"}
{block name="content"}
    <p>
      <br>
      Hello {$name}.
    </p>
    <p>
      Your quotation {$code} has been raised with a merchant.
      <br>
      A copy of quotation has been attached to this email.
      <br>
      <br>
    </p>
{/block}