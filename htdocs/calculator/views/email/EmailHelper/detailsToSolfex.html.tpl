<html>
  <body bgcolor="#FFFFFF" text="#000000">
    <p style="height:80px;">
    </p>
    <p>
      <br>
      An Installer has completed a calculation.
    </p>
    <p>
      <strong>Name:</strong> {$fullName}<br>
      <strong>Email:</strong> {$email}<br>
      <strong>Tel:</strong> {$telephone}<br>
      <strong>Recall Code:</strong> {$code}<br>
      <strong>Calculation worth:</strong> &pound;{$total|number_format:2}
    </p>
    <p>
      ~ UFH Quick Quote Calculator
      <br>
      <br>
    </p>
    <p>
    </p>
  </body>
</html>