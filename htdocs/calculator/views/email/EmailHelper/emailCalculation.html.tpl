<html>
  <body bgcolor="#FFFFFF" text="#000000">
    <p><strong>Please note: </strong>This is an automated mailbox, please do not reply as no one will receive it.<br/></p>
    <p>
      <br>
      Thank you for using the UFH Quick Quote.
    </p>
    <p>
      Your unique reference code is <span style="font-size:2em">{$code}</span>
    </p>
    <p>
      <strong>The total cost of the project is &pound;{$total|number_format:2}</strong>
    </p>
    <p>
      {if $smarty.session.theme!='plumbnation'}
        Please take the code to one of merchants listed below, they will
        be able to process your order and honour any possible installer
        discount you may have with them.
        <ul>
        	<li><a href="https://www.travisperkins.co.uk/">Travis Perkins</a></li>
        	<li><a href="https://www.cityplumbing.co.uk/">City Plumbing</a></li>
        	<li><a href="http://www.ptsplumbing.co.uk/">PTS</a></li>
        	<li><a href="http://www.solfex.co.uk/">Solfex</a></li>
        </ul>
      {else}
        Please take the code to your nearest plumbnation store, they will
        be able to process your order and honour any possible installer
        discount you may have with them.
      {/if}
      <br>
      <br>
    </p>
   	<p>
    </p>
  </body>
</html>