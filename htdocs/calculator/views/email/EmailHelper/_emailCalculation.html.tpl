{extends file="email/emailMain.html.tpl"}
{block name="content"}
    <p>
      <br>
      Thank you for using the UFH Quick Quote.
    </p>
    <p>
      Your unique reference code is <span style="font-size:2em">{$code}</span>
    </p>
    <p>
      <strong>The total cost of the project is &pound;{$total|number_format:2}</strong>
    </p>
    <p>
      {if $smarty.session.theme!='plumbnation'}
        Please take the code to one of merchants listed below, they will
        be able to process your order and honour any possible installer
        discount you may have with them.
      {else}
        Please take the code to your nearest plumbnation store, they will
        be able to process your order and honour any possible installer
        discount you may have with them.
      {/if}
      <br>
      <br>
    </p>
{/block}