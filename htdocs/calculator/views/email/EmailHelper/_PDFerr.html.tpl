{extends file="email/emailMain.html.tpl"}
{block name="content"}
    <p>
      <br>
      Developer, I was processing an order and the PDF generator broke!!
    </p>
    <p>Exception:</p>
    <pre>
{$exceptionText|escape}
    </pre>
    <p>Data:</p>
    <pre>
{$dataText|escape}
    </pre>
{/block}