<html>
  <body bgcolor="#FFFFFF" text="#000000">
    <p><strong>Please note: </strong>This is an automated mailbox, please do not reply as no one will receive it.<br/></p>
    <p>
      <br>
      Hello {$name}.
    </p>
    <p>
      Your quotation {$code} has been raised with a merchant.
      <br>
      A copy of quotation has been attached to this email.
      <br>
      <br>
    </p>
    <p>
    </p>
  </body>
</html>