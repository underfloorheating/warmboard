{extends file="main.tpl"}
{block name="title"}Contact Management{/block}
{block name="mainContent"}
	<h3>PDF Contact Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Role</th>
				<th>Region</th>
				<th>Email</th>
				<th>Telephone</th>
				<th>Display Order</th>
				<th></th>
		</thead>
		<tbody class="text-center">
			{foreach $contacts as $contact}
				<tr>
					<td>{$contact->id}</td>
					<td>{$contact->name}</td>
					<td>{$contact->role}</td>
					<td>{$contact->region}</td>
					<td>{$contact->email}</td>
					<td>{$contact->telephone}</td>
					<td>{$contact->position}</td>
					<td>
						<button class="btn btn-small" data-action="edit-contact"
							data-id="{$contact->id}"
							data-name="{$contact->name}"
							data-role="{$contact->role}"
							data-region="{$contact->region}"
							data-email="{$contact->email}"
							data-telephone="{$contact->telephone}"
							data-position="{$contact->position}"
							<i class="icon-edit"></i> Edit
						</button>
						<button class="btn btn-small btn-danger" data-action="delete-contact" data-id="{$contact->id}" data-name="{$contact->name}">
							<i class="icon-trash icon-white"></i> Delete
						</button>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<button type="button" class="btn" data-action="add-contact">Add Contact</button>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="ContactController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="ContactController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="ContactController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="createContactModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create Contact</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<div class="control-group">
					<label class="control-label" for="create_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="create_name">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_role">Role:</label>
					<div class="controls">
						<input type="text" name="role" id="create_role">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_region">Region:</label>
					<div class="controls">
						<input type="text" name="region" id="create_region">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_email">Email:</label>
					<div class="controls">
						<input type="text" name="email" id="create_email">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_telephone">Telephone:</label>
					<div class="controls">
						<input type="text" name="telephone" id="create_telephone">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_position">Display Order:</label>
					<div class="controls">
						<input type="text" name="position" id="create_position">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="add">Add</a>
		</div>
	</div>
	<div class="modal hide fade" id="editContactModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Contact</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<input type="hidden" name="contact_id">
				<div class="control-group">
					<label class="control-label" for="edit_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="edit_name">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_role">Role:</label>
					<div class="controls">
						<input type="text" name="role" id="edit_role">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_region">Region:</label>
					<div class="controls">
						<input type="text" name="region" id="edit_region">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_email">Email:</label>
					<div class="controls">
						<input type="text" name="email" id="edit_email">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_telephone">Telephone:</label>
					<div class="controls">
						<input type="text" name="telephone" id="edit_telephone">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="edit_position">Display Order:</label>
					<div class="controls">
						<input type="text" name="position" id="edit_position">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="edit">Edit</a>
		</div>
	</div>
	<div class="modal hide fade" id="deleteContactModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Contact</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<p>Are you sure you want to delete the contact <strong></strong></p>
				<input type="hidden" name="contact_id">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('button').click(function() {
				procButtonClick(this);
			});

			$('#createContactModal #add').click(function(e) {
				e.preventDefault();
				$('#createContactModal form').submit();
			});
			$('#createContactModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="ContactController->create"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#createContactModal').modal('hide');
						window.location.reload();

					} else {
						$('#createContactModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#editContactModal #edit').click(function(e) {
				e.preventDefault();
				$('#editContactModal form').submit();
			});
			$('#editContactModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="ContactController->edit"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editContactModal').modal('hide');
						window.location.reload();

					} else {
						$('#editContactModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#deleteContactModal #delete').click(function(e) {
				$.getJSON('{url to="ContactController->delete"}', { contact_id: $('#deleteContactModal input[name=contact_id]').val() }, function(data) {
					if (data.status == "OK") {
						$('#deleteContactModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteContactModal .errors').text(data.status).show('fast');
					}
				});
			});
		});

		function procButtonClick(button) {
			switch ($(button).data("action")) {
				case "add-contact":
					$('#createContactModal').modal();
					break;

				case "edit-contact":
					$('#editContactModal input[name=contact_id]').val($(button).data("id"));
					$('#editContactModal input[name=name]').val($(button).data("name"));
					$('#editContactModal input[name=role]').val($(button).data("role"));
					$('#editContactModal input[name=region]').val($(button).data("region"));
					$('#editContactModal input[name=email]').val($(button).data("email"));
					$('#editContactModal input[name=telephone]').val($(button).data("telephone"));
					$('#editContactModal input[name=position]').val($(button).data('position'));
					$('#editContactModal').modal();
					break;

				case "delete-contact":
					$('#deleteContactModal .errors').hide();
					$('#deleteContactModal input[name=contact_id]').val($(button).data("id"));
					$('#deleteContactModal strong').text($(button).data("name"));
					$('#deleteContactModal').modal();
					break;
			}
		}
		-->
	</script>
{/block}