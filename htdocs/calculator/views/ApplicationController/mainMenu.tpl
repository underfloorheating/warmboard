{extends file="main.tpl"}

{block name="title"}Welcome to the UFH Calculator{/block}

{block name="mainContent"}
	<div class="row">
		<div id="homeImage" class="span12"></div>
	</div>
	{if $smarty.session.theme!='plumbnation'}
		<div id="mainMenu">
			<div class="row-fluid">
			{if Security::check()}
				{if $smarty.session.loggedUser->isAdmin}
					<div class="span3">
						<a href="{url to="UserController->index"}">
							<img src="./public/images/list.png" /><br />
							User and Merchant Management
						</a>
					</div>
					<div class="span3">
						<a href="{url to="ProductController->index"}">
							<img src="./public/images/list.png" /><br />
							Product Management
						</a>
					</div>
					<div class="span3">
						<a class="new" href="{url to="SettingsController->index"}">
							<img src="./public/images/list.png" /><br />
							Settings
						</a>
					</div>
					<div class="span3">
						<a href="{url to="ApplicationController->logout"}">
							<img src="./public/images/out.png" /><br />
							Logout
						</a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span3 offset3">
						<a id="recallCalculation">
							<img src="./public/images/search.png"><br>
							Retrieve Quick Calculation
						</a>
					</div>
					<div class="span3">
						<a href="{url to="StatisticsController->index"}">
							<img src="./public/images/list.png"><br>
							Statistics
						</a>
					</div>
				</div>

				{else}
					<div class="span3">
						<a class="new" href="{url to="CalculatorController->reset"}">
							<img src="./public/images/new.png" /><br />
							New Calculation
						</a>
					</div>
					<div class="span3">
						<a id="recallCalculation">
							<img src="./public/images/search.png" /><br />
							Retrieve Quick Calculation
						</a>
					</div>
					<div class="span3">
						<a href="{url to="InstallerController->index"}">
							<img src="./public/images/list.png" /><br />
							Add/Edit/Search Installers
						</a>
					</div>
					<div class="span3">
						<a href="{url to="ApplicationController->logout"}">
							<img src="./public/images/out.png" /><br />
							Logout
						</a>
					</div>
				{/if}
			{else}
				<div class="offset4 span4">
					<a class="new" href="{url to="CalculatorController->reset"}">
						<img src="./public/images/new.png" /><br />
						Installer Quick Calculation
					</a><br>
					<a id="loginButton">
						<small>Merchant Login</small>
					</a>
				</div>
			{/if}
			</div>
		</div>
		<div class="row">
			<div class="offset1 span11">
				<p>
					If you have any questions or queries please contact us on
				</p>
				{if $smarty.session.theme!='plumbnation'}
					<address>
						SOLFEX Energy Systems<br>
						Tel. 01268 744249<br>
						Fax. 01268 200137<br>
						Email. <a href="mailto:sales@solfexufh.co.uk">sales@solfexufh.co.uk</a>
					</address>
				{else}
					<address>
						PlumbNation Limited<br>
						Tel. 0333 202 5983<br>
						Fax. 0151 334 2035<br>
					</address>
				{/if}
			</div>
		</div>
	{else}
		<div class="non-semantic-protector">
			<div class="ribbon">
				<div class="ribbon-content">
					<h1><strong>Underfloor Heating Calculator</strong></h1>
					<p class="serif">Get a quote for underfloor heating in your home.</p>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6 guide-description">
				<p>Our <b>underfloor heating calculator</b> can help you work out your exact heating requirements in a matter of seconds. We will gather information about your home to establish your needs, and generate a quick quote tailored to you.</p>
				<p>Simply click the 'Installer Quick Calculation' button to start designing your underfloor heating solution.</p>
			</div>
		</div>
		<div id="plumbNationMenu" class="row-fluid">
		{if Security::check()}
			{if $smarty.session.loggedUser->isAdmin}
				<div class="span3">
					<a href="{url to="UserController->index"}" class="btn btn-question btn-thumb btn-block btn-fill6">
						<span class="thumbnail override-70">
							<span class="img-70">
								<img src="./public/images/plumbnation/register.jpg" alt="Living Room Thumbnail">
							</span>
						</span>
						<span class="btn-question-bottom">User and Merchant Management</span>
					</a>
				</div>
				<div class="span3">
					<a href="{url to="ProductController->index"}" class="btn btn-question btn-thumb btn-block btn-fill6">
						<span class="thumbnail override-70">
							<span class="img-70">
								<img src="./public/images/plumbnation/merchant.jpg" alt="Living Room Thumbnail">
							</span>
						</span>
						<span class="btn-question-bottom">Product Management</span>
					</a>
				</div>
				<div class="span3">
					<a href="{url to="SettingsController->index"}" class="btn btn-question btn-thumb btn-block btn-fill6">
						<span class="thumbnail override-70">
							<span class="img-70">
								<img src="./public/images/plumbnation/settings.jpg" alt="Living Room Thumbnail">
							</span>
						</span>
						<span class="btn-question-bottom">Settings</span>
					</a>
				</div>
			{else}
				<div class="span3">
					<a href="{url to="CalculatorController->reset"}" class="btn btn-question btn-thumb btn-block btn-fill6">
						<span class="thumbnail override-70">
							<span class="img-70">
								<img src="./public/images/plumbnation/calculator.jpg" alt="Living Room Thumbnail">
							</span>
						</span>
						<span class="btn-question-bottom">New Calculation</span>
					</a>
				</div>
				<div class="span3">
					<a id="recallCalculation" class="btn btn-question btn-thumb btn-block btn-fill6">
						<span class="thumbnail override-70">
							<span class="img-70">
								<img src="./public/images/plumbnation/quote.jpg" alt="Living Room Thumbnail">
							</span>
						</span>
						<span class="btn-question-bottom">Retrieve Quick Calculation</span>
					</a>
				</div>
				<div class="span3">
					<a href="{url to="InstallerController->index"}" class="btn btn-question btn-thumb btn-block btn-fill6">
						<span class="thumbnail override-70">
							<span class="img-70">
								<img src="./public/images/plumbnation/settings.jpg" alt="Living Room Thumbnail">
							</span>
						</span>
						<span class="btn-question-bottom">Add/Edit/Search Installers</span>
					</a>
				</div>
			{/if}
			<div class="span3">
				<a href="{url to="ApplicationController->logout"}" class="btn btn-question btn-thumb btn-block btn-fill6">
					<span class="thumbnail override-70">
						<span class="img-70">
							<img src="./public/images/plumbnation/logout.jpg" alt="Living Room Thumbnail">
						</span>
					</span>
					<span class="btn-question-bottom">Logout</span>
				</a>
			</div>
		{else}
			<div class="offset4 span4">
				<a href="{url to="CalculatorController->reset"}" class="btn btn-question btn-thumb btn-block btn-fill6">
					<span class="thumbnail override-70">
						<span class="img-70">
							<img src="./public/images/plumbnation/calculator.jpg" alt="Living Room Thumbnail">
						</span>
					</span>
					<span class="btn-question-bottom">Installer Quick Calculation</span>
				</a><br>
				<a id="loginButton" class="btn btn-question btn-thumb btn-block btn-fill6">
					<span class="btn-question-bottom">Merchant Login</span>
				</a>
			</div>
		{/if}
		</div>
	{/if}
	<div class="modal hide fade" id="recallModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Retrieve Quick Calculation</h3>
		</div>
		<div class="modal-body">
			<div class="hide errors"></div>
			<form class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="code">Calculation ID:</label>
					<div class="controls">
						<input type="text" name="code" id="code">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
			<button type="button" class="btn btn-primary" id="lookupBut">Lookup</button>
		</div>
	</div>
	<script type="text/javascript">
		$('#recallCalculation').click(function() {
			$('#recallModal').modal();
		});
		$('#recallModal form').submit(function(e) {
			e.preventDefault();
			$.getJSON("{url to="QuotationController->codeExists"}", { code: $('#code').val() }, function(data) {
				if (data.status == "OK") {
					$('#recallModal').data('data', data).modal('hide');
				} else {
					$('#recallModal .errors').text(data.status).show('fast');
				}
			});
		});

		$('#lookupBut').click(function() {
			$('#recallModal form').submit();
		});
		$('#recallModal').on('hidden', function() {
			var data = $(this).data('data');
			if (data != undefined && data.status == "OK") {
				window.location = "{url to="QuotationController->recallCalculation"}?code=" + $('#code').val();

			} else {
				$('#recallModal .errors').hide();
				$('#code').val('');
			}
		});
	</script>
{/block}