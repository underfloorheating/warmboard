<!DOCTYPE html>
<html>
	<head>
		<title>{block name="title"}{/block} - Solfex Calculator</title>
		<meta charset="utf-8">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link type="text/css" rel="stylesheet" href="{url public="bootstrap/css/bootstrap.min.css"}">
		<link type="text/css" rel="stylesheet" href="{url public="bootstrap/css/bootstrap-responsive.min.css"}">
		{if $smarty.session.theme!='plumbnation'}
			<link type="text/css" rel="stylesheet" href="{url public="css/warma-theme.css"}">
		{/if}
		<link type="text/css" rel="stylesheet" href="{url public="css/global.css"}">
		{if $smarty.session.theme=='solfex'}
			<link type="text/css" rel="stylesheet" href="{url public="css/solfex.css"}">
		{elseif $smarty.session.theme=='plumbnation'}
			<link type="text/css" rel="stylesheet" href="{url public="css/plumbnation.css"}">
		{/if}
		<script type="text/javascript" src="{url public="js/jquery-1.9.1.min.js"}"></script>
		<script type="text/javascript" src="{url public="bootstrap/js/bootstrap.min.js"}"></script>
	</head>
	<body>
		<div class="container">
			{*
			{if isset($smarty.session.loggedUser)}
			<div class="row">
				<div class="span12 registeredUser">
					<h5 class="pull-left">
						<strong>Logged in as:</strong> {$smarty.session.loggedUser->userName}
					</h5>
					<div class="btn-group pull-right">
						{if $smarty.session.loggedUser->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)}
							<a href="{url to="ProductController->index"}" class="btn"><i class="icon-shopping-cart"></i> Catalogue Management</a>
						{/if}
						{if $smarty.session.loggedUser->hasPerm(UserPerm::PERM_MANAGE_USER)}
							<a href="{url to="UserController->index"}" class="btn"><i class="icon-user"></i> Access Management</a>
						{/if}
						<a href="{url to="ApplicationController->logout"}" class="btn">Logout</a>
					</div>
				</div>
			</div>
			{/if}
			*}
			{if $_errors}
				<div class="error alert alert-error">
					<h3>An Error Occured</h3>
					<ul>
						{foreach $_errors as $err}
							<li>{$err}</li>
						{/foreach}
					</ul>
				</div>
			{/if}
			{block name="mainContent"}
			{/block}
		</div>
		{*
		<div class="modal hide fade" id="aFreshModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Start new Calculation?</h3>
			</div>
			<div class="modal-body">
				<h5 class="colored"><strong>All entered data will be lost.</strong></h5>
				<h5>Are you sure you want to continue?</h5>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">No</a>
				<a href="#" class="btn btn-primary" id="restartCalculation" data-dismiss="modal">Yes</a>
			</div>
		</div>
		*}
		<div class="modal hide fade" id="loginModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Merchant Login</h3>
			</div>
			<form class="form-horizontal" method="post" action="{url to="ApplicationController->login"}" id="loginForm">
				<div class="modal-body">
					<div class="control-group">
						<label class="control-label">Username/Merchant:</label>
						<div class="controls">
							<input type="text" name="user">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Password:</label>
						<div class="controls">
							<input type="password" name="pass">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Cancel</a>
					<input type="submit" value="Login" class="btn btn-primary">
				</div>
			</form>
		</div>
		<div class="modal hide fade" id="registerModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3>Merchant Register</h3>
			</div>
			<form class="form-horizontal" method="post" action="{url to="ApplicationController->login"}" id="loginForm">
				<div class="modal-body">
					<h5>If you would like to register as a merchant for the underfloor calculator please call us on 0044 (0) 1772 312847</h5>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn" data-dismiss="modal">Cancel</a>
				</div>
			</form>
		</div>
		<script type="text/javascript">
			<!--
			$(function() {
				{*

				/* No longer used as calculations cannot be continued. Uncomment if client changes mind.
				   Also uncomment the button ID in the mainMenu.tpl */

				$('#startAFresh .new').click(function(e) {
					$('#aFreshModal').modal();
				});
				$('a#restartCalculation').click(function(e) {
					window.location = "{url to="CalculatorController->reset"}";
				});

				*}

				$('#loginButton').click(function(e) {
					$('#loginModal').modal();
				});
				$('#registerButton').click(function(e) {
					$('#registerModal').modal();
				});
			});
			-->
		</script>
	</body>
</html>