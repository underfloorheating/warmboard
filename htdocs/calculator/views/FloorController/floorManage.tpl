{extends file="main.tpl"}
{block name="title"}Floor Selection{/block}
{block name="mainContent"}
	{if $smarty.session.theme!='plumbnation'}
		<h3>
			Project Name:
			<span id="projectRefTxt">{if $projectRef}{$projectRef}{/if}</span>
			<button type="button" class="btn" id="updateProjectRef">Update</button>
		</h3>
	{else}
		<h1>
			Project Name:
			<span id="projectRefTxt">{if $projectRef}{$projectRef}{/if}</span>
			<button type="button" class="btn pull-right" id="updateProjectRef">Update</button>
		</h1>
		<hr />
	{/if}

	{if $smarty.session.theme!='plumbnation'}<h3>Floor Manager</h3>{/if}
	<p {if $smarty.session.theme=='plumbnation'}class="help-block alert"{/if}>Specify the number of floors required for your project. <i class="icon-question-sign" data-help="floor-manager"></i></p>

	<table class="table">
		<thead>
			<tr>
				<th>Floor Level</th>
				<th class="span2">Action</th>
			</tr>
		</thead>
		<tbody id="floorList">
			{foreach $floors as $i}
			<tr>
				<td>{$i->name}</td>
				<td{if isset($i->id)} data-realid="{$i->id}"{/if}>
					<button type="button" data-action="edit" class="btn">Edit</button>
					<button type="button" data-action="remove" class="btn btn-danger">&times;</button>
				</td>
			</tr>
			{foreachelse}
			<tr>
				<td colspan="2">
					No floors have been added.
				</td>
			</tr>
			{/foreach}
		</tbody>
	</table>
	<button type="button" class="btn" id="floorAddBtn">{if count($floors) == 0}Add a Floor{else}Add Another Floor{/if}</button>
	<i class="icon-question-sign" data-help="add-floor"></i>
	<p>
		<hr>
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a><i class="icon-question-sign" data-help="home"></i>
		&nbsp;
		<div class="btn-group">
			<a href="{url to="FloorController->select"}" class="btn btn-primary{if !$projectRef} disabled{/if}" id="continueBut">Continue &gt;</a>
		</div>
	</p>
	<div class="modal hide fade" id="floorAddModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Add a new Floor</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form class="form-horizontal" id="formNewFloor">
				<div class="control-group">
					<label class="control-label" for="name">Floor Level:</label>
					<div class="controls">
						<select id="nameSelect">
							{foreach $floorNames as $floorName}
								<option>{$floorName->name|escape}</option>
							{/foreach}
							<option>OTHER</option>
						</select>
						<input type="text" id="name" class="hide">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Cancel</a>
			<button type="button" id="doAdd" class="btn btn-primary">Add</button>
		</div>
	</div>
	<div class="modal hide fade" id="projectRefModel">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Name your Project</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form class="form-horizontal" id="projectRefForm">
				<div class="control-group">
					<p class="help-block alert">This will be the title of the project - which will be printed on your finalised quotation</p>
					<label class="control-label" for="projectRef">Your Project Reference:</label>
					<div class="controls">
						<input type="text" id="projectRef" name="projectRef" maxlength="200"{if $projectRef} value="{$projectRef}"{/if}>
						<span class="help-block">For example: &apos;77 Example Rd&apos;</span>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Cancel</a>
			<button type="button" id="doProjectRef" class="btn btn-primary">Set</button>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('table button').click(function(e) {
				e.preventDefault();

				if ($(this).data('action') == 'remove')
					removeRow(this);

				else if ($(this).data('action') == 'edit')
					editRow(this);
			});

			$('#formNewFloor').submit(function(e) {
				e.preventDefault();

				var data = {
					floorAdd: true,
					name: $('#nameSelect', this).val()
				};
				if (data.name == 'OTHER')
					data.name = $('input#name', this).val();

				$('#nameSelect', this).val('');
				$('#name').hide();
				$('#nameSelect').show();

				$('input#name', this).val('');

				$.getJSON('{url to="FloorController->addFloor"}', data, function(status) {
					if (status.status == 'OK') {
						var row = $('<tr style="display:none">\
							<td>' + data.name + '</td>\
							<td>\
								<button type="button" data-action="edit" class="btn">Edit</button>\
								<button type="button" data-action="remove" class="btn btn-danger">&times;</button>\
							</td>\
						</tr>');
						$('button', row).click(function(e) {
							e.preventDefault();

							if ($(this).data('action') == 'remove')
								removeRow(this);

							else if ($(this).data('action') == 'edit')
								editRow(this);
						});

						if ($('#floorList>tr>td[colspan=2]').length > 0) { 
							$('#floorList>tr>td[colspan=2]').parent().hide('fast', function() {
								$(this).remove();
								$('#floorAddBtn').text('Add Another Floor');
								$('#floorList').append(row.show('fast'));
							});
						} else {
							$('#floorList').append(row.show('fast'));
						}

						$('#floorAddModal').modal('hide');

					} else {
						$('#floorAddModal .errors').text('Unable to add floor: ' + status.status).show('fast');
					}
				});
			});

			$('#doProjectRef').click(function() {
				$('#projectRefForm').submit();
			});
			$('#projectRefForm').submit(function(e) {
				$.getJSON('{url to="CalculatorController->updateProjectName"}', $(this).serialize(), function(data) {
					if (data.status == 'OK') {
						$('#projectRefModel').modal('hide');
						$('#projectRefTxt').text(data.ref);
						$('#continueBut').removeClass('disabled');

					} else {
						$('#projectRefModel .errors').text(data.status).show();
					}
				});
				e.preventDefault();
			});

			$('#continueBut').click(function(e) {
				if ($('#projectRefTxt').text() == '') {
					e.preventDefault();
					$('#projectRefModel').modal();
				}
			});

			$('#updateProjectRef').click(function() {
				$('#projectRefModel .errors').hide();
				$('#projectRefModel').modal();
			});

			$('#doAdd').click(function() {
				$('#formNewFloor').submit();
			});

			$('#floorAddBtn').click(function() {
				$('#floorAddModal .errors').hide();
				$('#floorAddModal').modal();
			});

			{if !$projectRef}
				$('#projectRefModel').modal();
			{/if}

			{if count($floors) == 0}
				//Auto show the modal
				$('#floorAddBtn').click();
			{/if}

			$('#nameSelect').change(function() {
				if ($(this).val() == 'OTHER') {
					$(this).hide();
					$('#name').show();
				}
			});

			$('#floorAddModal').on('hidden', function() {
				$('#nameSelect', this).val('').show();
				$('#name').hide().val('');
			});

			setupPopovers($('.icon-question-sign'));
		});

		function editRow(btn) {
			var index = $('tr', $('#floorList')).index($(btn).parents('tr'));
			var row = $('tr:nth-child(' + (index + 1) + ')', $('#floorList'));
			if ($('input[type=text]', row).length != 0) {
				var data = {
					floorEdit: true,
					name: $('input[type=text]', row).val(),
					tempId: index
				}
				$.getJSON('{url to="FloorController->updateFloor"}', data, function(status) {
					if (status.status) {
						$('td:nth-child(1)', row).text(data.name);
						$('button[data-action=edit]', row).text('Edit');
					}
				});

			} else {
				$('td:nth-child(1)', row).html('<input type="text" value="' + $('td:nth-child(1)', row).text() + '">');
				$('button[data-action=edit]', row).text('Done');
			}
		}

		function removeRow(btn) {
			var index = $('tr', $('#floorList')).index($(btn).parents('tr'));
			var data = { floorRemove: true };
			data.floorId = $(btn).parent().data("realid");
			if (data.floorId === undefined)
				data.tempId = index;

			$.getJSON('{url to="FloorController->removeFloor"}', data, function(status) {
				if (status.status == 'OK') {
					$('tr:nth-child(' + (index + 1) + ')', $('#floorList')).hide('fast', function() {
						$(this).remove();
						if ($('#floorList').children().length == 0) {
							$('#floorList').html('<tr>\
								<td colspan="2">\
									No floors have been added, add one using the form below.\
								</td>\
							</tr>');
							$('#floorAddBtn').text('Add a Floor');
						}
					});
				}
			});
		}

		function setupPopovers(elements) {
			var title, content, position;

			elements.each(function() {
				switch ($(this).data('help')) {
					case 'home':
						title = 'Home';
						content = 'Please note if you select the home button this will take you back to main screen and your quotation will not be saved';
						position = 'top';
						break;

					case 'add-floor':
						title = 'Add another floor';
						content = 'This is used for multiple floors, which can be added later in the quotation';
						position = 'top';
						break;

					case 'floor-manager':
						title = 'Floor Manager';
						content = 'please note the calculator will only calculate up to a maximum of 140m2. If the project you would like is larger than this area you can send in your plans to us, where we will happily complete your quotation';
						position = 'bottom';
						break;

					default:
						return;
				}

				$(this).popover({
					title: title,
					content: content,
					placement: position,
					trigger: 'hover',
					container: 'body',
				});
			});
		}
		-->
	</script>
{/block}