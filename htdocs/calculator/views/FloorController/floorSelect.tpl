{extends file="main.tpl"}
{block name="title"}Floor selection{/block}
{block name="mainContent"}
	{if $smarty.session.theme!='plumbnation'}
		<h3>Floor Selection</h3>
	{else}
		<h1>Floor Selection</h1>
		<hr />
	{/if}
	<p>Select a floor to configure:</p>
	<form class="form" method="get" action="{url to="FloorController->selectFloor"}">
		{foreach $floors as $i}
			<label class="radio"{if isset($i->_manifolds) && count($i->_manifolds)} style="color:red;"{/if}>
				<input type="radio" name="floorIndex" value="{$i@index}"{if $i@first} checked{/if}> {$i->name}
			</label>
		{/foreach}
		<hr>
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		<i class="icon-question-sign" data-help="home"></i>
		&nbsp;
		<div class="btn-group">
			<a class="btn" href="{url to="FloorController->index"}">&lt; Back</a>
			<input type="submit" class="btn btn-primary" value="Continue &gt;">
		</div>
	</form>
	<script type="text/javascript">
		$(function() {
			setupPopovers($('.icon-question-sign'));
		});

		function setupPopovers(elements) {
			var title, content, position;

			elements.each(function() {
				switch ($(this).data('help')) {
					case 'home':
						title = 'Home';
						content = 'Please note if you select the home button this will take you back to main screen and your quotation will not be saved';
						position = 'top';
						break;

					default:
						return;
				}

				$(this).popover({
					title: title,
					content: content,
					placement: position,
					trigger: 'hover'
				});
			});
		}
	</script>
{/block}