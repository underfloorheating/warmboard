{extends file="main.tpl"}
{block name="title"}User Management{/block}
{block name="mainContent"}
	<h3>User/Merchant Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>User Name</th>
				<th>Created</th>
				<th>Permissions</th>
				<th></th>
		</thead>
		<tbody class="text-center">
			{foreach $users as $user}
				<tr>
					<td>{$user->id}</td>
					<td>{$user->userName}</td>
					<td>{$user->registeredTime}</td>
					<td>(
						{foreach $user->getPerms() as $perm}
							{if $perm->canManageUsers()}
								<span class="icon-user" title="Manage Users"></span>
							{else if $perm->canManageProducts()}
								<span class="icon-shopping-cart" title="Manage Products and Engineering Settings"></span>
							{else if $perm->canLogin()}
								<span class="icon-briefcase" title="Login"></span>
							{else if $perm->canManageContacts()}
								<span class="icon-th-list" title="Manage PDF Contacts"></span>
							{else if $perm->canManageDropLists()}
								<span class="icon-tag" title="Manage Dropdown Lists"></span>
							{/if}
						{/foreach}
					)</td>
					<td>
						<button class="btn btn-small" data-action="edit-user"
							data-id="{$user->id}"
							data-username="{$user->userName}"
							data-puser="{if $user->hasPerm(UserPerm::PERM_MANAGE_USER)}true{else}false{/if}"
							data-pprod="{if $user->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)}true{else}false{/if}"
							data-plogi="{if $user->hasPerm(UserPerm::PERM_LOGIN)}true{else}false{/if}"
							data-pcont="{if $user->hasPerm(UserPerm::PERM_MANAGE_CONTACTS)}true{else}false{/if}"
							data-pdrop="{if $user->hasPerm(UserPerm::PERM_MANAGE_DROPLISTS)}true{else}false{/if}"
							<i class="icon-edit"></i> Edit
						</button>
						<button class="btn btn-small btn-danger" data-action="delete-user" data-id="{$user->id}" data-username="{$user->userName}">
							<i class="icon-trash icon-white"></i> Delete
						</button>
						{if !$user->isAdmin}<a href="{url to="UserController->show"}/{$user->id}" class="btn btn-small">
							<i class="icon-th-list"></i> View
						</a>{/if}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<button type="button" class="btn" data-action="add-user">Add Administrative Account</button>
		&nbsp;
		<a class="btn" href="{url to="MerchantGroupController->index"}">Merchant Groups</a>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="UserController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="UserController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="UserController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="createUserModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create Account</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<div class="control-group">
					<label class="control-label" for="userName">User Name:</label>
					<div class="controls">
						<input type="text" name="userName">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="password">Password:</label>
					<div class="controls">
						<input type="password" name="password">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="passwordRep">Repeat Password:</label>
					<div class="controls">
						<input type="password" name="passwordRep">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Permissions:</label>
					<div class="controls">
						<label><input name="perm[0]" type="checkbox"> Manage Users</label>
						<label><input name="perm[1]" type="checkbox"> Manage Products and Engineering Settings</label>
						<label><input name="perm[3]" type="checkbox"> Manage Contacts on the PDF</label>
						<label><input name="perm[4]" type="checkbox"> Manage Dropdown Lists</label>
						<label><input name="perm[2]" type="checkbox" checked> Login</label>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="add">Add</a>
		</div>
	</div>
	<div class="modal hide fade" id="editUserModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit User</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<input type="hidden" name="user_id">
				<div class="control-group">
					<label class="control-label" for="userName">User Name:</label>
					<div class="controls">
						<input type="text" name="userName">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="password">Password:</label>
					<div class="controls">
						<span class="help-block">Leave blank unless the password requires altering</span>
						<input type="password" name="password">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="passwordRep">Repeat Password:</label>
					<div class="controls">
						<input type="password" name="passwordRep">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Permissions:</label>
					<div class="controls">
						<label><input name="perm[0]" type="checkbox"> Manage Users</label>
						<label><input name="perm[1]" type="checkbox"> Manage Products and Engineering Settings</label>
						<label><input name="perm[3]" type="checkbox"> Manage Contacts on the PDF</label>
						<label><input name="perm[4]" type="checkbox"> Manage Dropdown Lists</label>
						<label><input name="perm[2]" type="checkbox" checked> Login</label>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="edit">Edit</a>
		</div>
	</div>
	<div class="modal hide fade" id="deleteUserModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete User</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<p>Are you sure you want to delete the user <strong></strong></p>
				<input type="hidden" name="user_id">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('button').click(function() {
				procButtonClick(this);
			});

			$('#createUserModal #add').click(function(e) {
				e.preventDefault();
				$('#createUserModal form').submit();
			});
			$('#createUserModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="UserController->create"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#createUserModal').modal('hide');
						window.location.reload();

					} else {
						$('#createUserModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#editUserModal #edit').click(function(e) {
				e.preventDefault();
				$('#editUserModal form').submit();
			});
			$('#editUserModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="UserController->edit"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editUserModal').modal('hide');
						window.location.reload();

					} else {
						$('#editUserModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#deleteUserModal #delete').click(function(e) {
				$.getJSON('{url to="UserController->delete"}', { user_id: $('#deleteUserModal input[name=user_id]').val() }, function(data) {
					if (data.status == "OK") {
						$('#deleteUserModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteUserModal .errors').text(data.status).show('fast');
					}
				});
			});
		});

		function procButtonClick(button) {
			switch ($(button).data("action")) {
				case "add-user":
					$('#createUserModal').modal();
					break;

				case "edit-user":
					$('#editUserModal input[name=user_id]').val($(button).data("id"));
					$('#editUserModal input[name=userName]').val($(button).data("username"));
					$('#editUserModal input[name=discount]').val($(button).data("discount"));
					$('#editUserModal input[name=perm\\[0\\]]').prop('checked', $(button).data("puser"));
					$('#editUserModal input[name=perm\\[1\\]]').prop('checked', $(button).data("pprod"));
					$('#editUserModal input[name=perm\\[2\\]]').prop('checked', $(button).data("plogi"));
					$('#editUserModal input[name=perm\\[3\\]]').prop('checked', $(button).data('pcont'));
					$('#editUserModal input[name=perm\\[4\\]]').prop('checked', $(button).data('pdrop'));
					$('#editUserModal').modal();
					break;

				case "delete-user":
					$('#deleteUserModal .errors').hide();
					$('#deleteUserModal input[name=user_id]').val($(button).data("id"));
					$('#deleteUserModal strong').text($(button).data("username"));
					$('#deleteUserModal').modal();
					break;
			}
		}
		-->
	</script>
{/block}