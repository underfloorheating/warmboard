{extends file="main.tpl"}
{block name="title"}System List{/block}
{block name="mainContent"}
	<h3>System List</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>System Name</th>
				<th>Used By</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $systems as $system}
				<tr>
					<td>{$system->name}</td>
					<td>
						{if $usedBy[$system->id]}
							<a href="{url to="ProductController->edit"}/{$usedBy[$system->id]->id}?backPath=system">{$usedBy[$system->id]->code}</a>
						{else}
							Not in use
						{/if}
					</td>
					<td>
						<button type="button" class="btn btn-small editBut"
								data-id="{$system->id}"
								data-name="{$system->name}"
								data-available="{$system->available}"
								data-constructiontypeid="{$system->constructionTypeId}"
								data-pipewidth="{$system->pipeWidth}"
								data-systemtypeid="{$system->systemTypeId}">
							<i class="icon-edit"></i> Edit
						</button>
						{if !$usedBy[$system->id]}
							<button type="button" class="btn btn-small deleteSys" data-id="{$system->id}" data-name="{$system->name}">
								<i class="icon-trash"></i> Delete
							</button>
						{/if}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="ProductController->index"}">&lt; Back to Product List</a>
		&nbsp;
		<button type="button" class="btn" id="createBut">Create System</button>
	</div>
	<div class="modal hide fade" id="createSystemModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create New System</h3>
		</div>
		<div class="modal-body">
			<div class="errors hide alert alert-error"></div>
			<form class="form-horizontal">
				<div class="control-group">
					<label for="name" class="control-label">System Name:</label>
					<div class="controls">
						<input type="text" name="name">
					</div>
				</div>
				<div class="control-group">
					<label for="systemConstructionId" class="control-label">Construction Type:</label>
					<div class="controls">
						<select name="systemConstructionId">
							{foreach $constructionTypes as $constructionType}
								<option value="{$constructionType->id}">{$constructionType->name|escape}</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="pipeWidth" class="control-label">Requires Pipe Diameter:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="pipeWidth">
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="available" class="control-label">System Availability:</label>
					<div class="controls">
						<label><input type="checkbox" name="available">
							Available
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="pipes" class="control-label">Allowed Pipes:</label>
					<div class="controls">
						<select name="pipes[]" multiple>
							{foreach $pipeTypes as $pipeType}
								<option value="{$pipeType->id}">
									{$pipeType->typeName|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="create_tempLevels" class="control-label">Allowed Temperature Levels:</label>
					<div class="controls">
						<select id="create_tempLevels" name="tempLevels[]" multiple>
							{foreach $tempLevels as $tempLevel}
								<option value="{$tempLevel->id}">
									{$tempLevel->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="systemTypeId" class="control-label">System Type:</label>
					<div class="controls">
						<select name="systemTypeId">
							{foreach $systemTypes as $systemType}
								<option value="{$systemType->id}">
									{$systemType->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<button type="button" class="btn btn-primary" id="doCreateBut">Create</button>
		</div>
	</div>
	<div class="modal hide fade" id="editSystemModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit existing System</h3>
		</div>
		<div class="modal-body">
			<div class="errors hide alert alert-error"></div>
			<form class="form-horizontal">
				<input type="hidden" name="systemId">
				<div class="control-group">
					<label for="name" class="control-label">System Name:</label>
					<div class="controls">
						<input type="text" name="name">
					</div>
				</div>
				<div class="control-group">
					<label for="systemConstructionId" class="control-label">Construction Type:</label>
					<div class="controls">
						<select name="systemConstructionId">
							{foreach $constructionTypes as $constructionType}
								<option value="{$constructionType->id}">{$constructionType->name|escape}</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="pipeWidth" class="control-label">Requires Pipe Diameter:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="pipeWidth">
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="available" class="control-label">System Availability:</label>
					<div class="controls">
						<label><input type="checkbox" name="available">
							Available
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="pipes" class="control-label">Allowed Pipes:</label>
					<div class="controls">
						<select name="pipes[]" multiple>
							{foreach $pipeTypes as $pipeType}
								<option value="{$pipeType->id}">
									{$pipeType->typeName|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="edit_tempLevels" class="control-label">Allowed Temperature Levels:</label>
					<div class="controls">
						<select id="edit_tempLevels" name="tempLevels[]" multiple>
							{foreach $tempLevels as $tempLevel}
								<option value="{$tempLevel->id}">
									{$tempLevel->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="systemTypeId" class="control-label">System Type:</label>
					<div class="controls">
						<select name="systemTypeId">
							{foreach $systemTypes as $systemType}
								<option value="{$systemType->id}">
									{$systemType->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<button type="button" class="btn btn-primary" id="doEditBut">Edit</button>
		</div>
	</div>
	<div class="modal hide fade" id="deleteModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete System</h3>
		</div>
		<div class="modal-body">
			<div class="alert alert-error hide errors"></div>
			<form>
				<p>
					Are you sure you want to delete the system <strong></strong>?
				</p>
				<input type="hidden" name="systemId">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<button type="button" class="btn btn-danger" id="delete">Delete</button>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('#createBut').click(function() {
				$('#createSystemModal .errors').hide();
				$('#createSystemModal').modal();
			});

			$('#doCreateBut').click(function() {
				$('#createSystemModal form').submit();
			});
			$('#createSystemModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON("{url to="SystemController->create"}", $(this).serialize(), function(data) {
					if (data.status == "OK") {
						window.location.reload();
						$('#createSystemModal').modal('hide');
						
					} else {
						$('#createSystemModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('.editBut').click(function() {
				$('#editSystemModal .errors').hide();
				var button = $(this)
				$.getJSON("{url to="SystemController->getPipeTypes"}", { systemId: button.data('id') }, function(data) {
					var arr = [];
					$.each(data.pipes, function(index, item) {
						arr[index] = item.id;
					});
					$('#editSystemModal select[name=pipes\\[\\]]').val(arr);
				});
				$.getJSON("{url to="SystemController->getTemperatureLevels"}", { systemId: button.data('id') }, function(data) {
					var arr = [];
					$.each(data.tempLevels, function(index, item) {
						arr[index] = item.id;
					});
					$('#editSystemModal select[name=tempLevels\\[\\]]').val(arr);
				});
				$('#editSystemModal input[name=name]').val(button.data('name'));
				$('#editSystemModal select[name=systemConstructionId]').val(button.data('constructiontypeid'));
				$('#editSystemModal input[name=pipeWidth]').val(button.data('pipewidth'));
				$('#editSystemModal input[name=maxAreaPerCircuit]').val(button.data('maxareapercircuit'));
				$('#editSystemModal input[name=available]').prop('checked', button.data('available'));
				$('#editSystemModal input[name=systemId]').val(button.data('id'));
				$('#editSystemModal select[name=systemTypeId]').val(button.data('systemtypeid'));
				$('#editSystemModal').modal();
			});

			$('#doEditBut').click(function() {
				$('#editSystemModal form').submit();
			});
			$('#editSystemModal form').submit(function(e) {
				e.preventDefault();

				$.getJSON("{url to="SystemController->update"}", $(this).serialize(), function(data) {
					if (data.status == "OK") {
						window.location.reload();
						$('#editSystemModal').modal('hide');
						
					} else {
						$('#editSystemModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('tbody button.deleteSys').click(function() {
				$('#deleteModal .errors').hide();
				$('#deleteModal input[name=systemId]').val($(this).data("id"));
				$('#deleteModal strong').text($(this).data("name"));
				$('#deleteModal').modal();
			});

			$('#deleteModal #delete').click(function() {
				$('#deleteModal form').submit();
			});
			$('#deleteModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="SystemController->delete"}', { systemId: $('input[name=systemId]', this).val() }, function(data) {
					if (data.status == "OK") {
						$('#deleteModal').modal("hide");
						window.location.reload();

					} else {
						$('#deleteModal .errors').text(data.status).show('fast');
					}
				});
			});
		});
		-->
	</script>
{/block}