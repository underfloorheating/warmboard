{extends file="main.tpl"}
{block name="title"}Merchant Group Management{/block}
{block name="mainContent"}
	<h3>Merchant Group Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Discount</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $groups as $group}
				<tr>
					<td>{$group->id}</td>
					<td><a href="{url to="MerchantGroupController->show"}/{$group->id}">{$group->name|escape}</a></td>
					<td>{$group->discount * 100}%</td>
					<td>
						{if !$archive}
							<a class="btn btn-small" href="{url to="MerchantGroupController->edit"}/{$group->id}">
								<i class="icon-edit"></i> Edit
							</a>
							<button type="button" class="btn btn-small btn-danger deleteGroup" data-id="{$group->id}" data-name="{$group->name|escape}">
								<i class="icon-trash icon-white"></i> Delete
							</button>
						{else}
							<a class="btn btn-small" href="{url to="MerchantGroupController->reinstate"}/{$group->id}">
								<i class="icon-folder-open"></i> Reinstate
							</a>
						{/if}
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="4">
						No groups to list
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="UserController->index"}">&lt Back to User Management</a>
		&nbsp;
		<a class="btn" href="{url to="MerchantGroupController->create"}">Add Merchant Group</a>
		&nbsp;
		{if !$archive}
			<a class="btn" href="{url to="MerchantGroupController->index"}?archived"><i class="icon-folder-close"></i>&nbsp;View Archived</a>
		{else}
			<a class="btn" href="{url to="MerchantGroupController->index"}"><i class="icon-list"></i>&nbsp;View Active</a>
		{/if}
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="MerchantGroupController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="MerchantGroupController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="MerchantGroupController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>

	<div class="modal hide fade" id="deleteGroupModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Merchant Group</h3>
		</div>
		<div class="modal-body">
			<form action="{url to="MerchantGroupController->delete"}" method="post">
				<input type="hidden" name="id">
			</form>
			<p>
				Are you sure you want to delete <span></span>?
			</p>
			<p>
				If the group contains any merchants then it will be archived
				instead and any merchants within the group will also be
				archived.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">Close</button>
			<button type="button" class="btn btn-danger" id="doDeleteGroup">Delete</button>
		</div>
	</div>

	<script type="text/javascript">
		<!--
		$(function() {
			$('.deleteGroup').click(function() {
				$('#deleteGroupModal p span').text($(this).data('name'));
				$('#deleteGroupModal input[name=id]').val($(this).data('id'));
				$('#deleteGroupModal').modal();
			});

			$('#doDeleteGroup').click(function() {
				$('#deleteGroupModal').modal('hide');
				$('#deleteGroupModal form').submit();
			});
		});
		-->
	</script>
{/block}