{extends file="main.tpl"}
{block name="title"}Merchant Group{/block}
{block name="mainContent"}
	<h3>Merchant Group</h3>
	<table class="table table">
		<tbody>
			<tr>
				<th>ID:</th>
				<td>{$group->id}</td>
				<th>Discount:</th>
				<td>{$group->discount*100}%</td>
			</tr>
			<tr>
				<th>Name:</th>
				<td>{$group->name|escape}</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<th>Contact Name:</th>
				<td>{$group->contactName|default:"-"|escape}</td>
				<th>Contact Email:</th>
				<td>{$group->contactEmail|default:"-"|escape}</td>
			</tr>
			<tr>
				<th>Telephone:</th>
				<td>{$group->Telephone|default:"-"|escape}</td>
				<th>Company Name:</th>
				<td>{$group->companyName|default:"-"|escape}</td>
			</tr>
			<tr>
				<th>Address:</th>
				<td>{$group->address|default:"-"|escape|nl2br}</td>
				<th>Notes:</th>
				<td>{$group->notes|default:"-"|escape|nl2br}</td>
			</tr>
		</tbody>
	</table>

	<div class="filter pull-right">
		<form class="form-search">
			<input type="text" class="input-medium search-query" name="s" value="{if isset($smarty.get.s)}{$smarty.get.s|escape}{/if}">
			<input type="submit" value="Search" class="btn">
		</form>
	</div>
	<h4>Merchants in this Group</h4>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>User Name</th>
				<th>Name</th>
				<th>Email</th>
				<th>Telephone</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $merchants as $merchant}
				{$user=$users[$merchant->id]}
				<tr>
					<td>{$merchant->id}</td>
					<td>{$user->userName}</td>
					<td>{$merchant->contactName}</td>
					<td>{$merchant->contactEmail}</td>
					<td>{$merchant->telephone}</td>
					<td>
						<a href="{url to="MerchantController->show"}/{$user->id}" class="btn btn-small">
							<i class="icon-th-list"></i> View
						</a>
						{if !$archive}
							<a class="btn btn-small" href="{url to="MerchantController->edit"}/{$user->id}">
								<i class="icon-edit"></i> Edit
							</a>
							<button class="btn btn-small btn-danger delete-user" data-id="{$user->id}" data-username="{$user->userName|escape}">
								<i class="icon-trash icon-white"></i> Delete
							</button>
						{else}
							<a class="btn btn-small" href="{url to="MerchantController->reinstate"}/{$user->id}">
								<i class="icon-folder-open"></i> Reinstate
							</a>
						{/if}
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="6">There are no merchants in this group</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="MerchantGroupController->index"}">&lt Back to Merchant Group Management</a>
		&nbsp;
		<a class="btn" href="{url to="MerchantController->add"}">Create Merchant</a>
		&nbsp;
		{if !$archive}
			<a class="btn" href="{url to="MerchantGroupController->show"}/{$group->id}?archived"><i class="icon-folder-close"></i>&nbsp;View Archived</a>
		{else}
			<a class="btn" href="{url to="MerchantGroupController->show"}/{$group->id}"><i class="icon-list"></i>&nbsp;View Active</a>
		{/if}
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="MerchantGroupController->show"}/{$group->id}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="MerchantGroupController->show"}/{$group->id}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="MerchantGroupController->show"}/{$group->id}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>

	<div class="modal hide fade" id="deleteUserModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete User</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal" method="post" action="{url to="MerchantController->delete"}">
				<div class="hide errors alert alert-error">
				</div>
				<p>
					Are you sure you want to delete the merchant <span></span>?
				</p>
				<input type="hidden" name="id">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('.delete-user').click(function() {
				$('#deleteUserModal input').val($(this).data('id'));
				$('#deleteUserModal .modal-body span').text($(this).data('username'));
				$('#deleteUserModal').modal();
			});

			$('#delete').click(function() {
				$('#deleteUserModal form').submit();
			});
		});
		-->
	</script>
{/block}