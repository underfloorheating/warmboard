{extends file="main.tpl"}
{block name="title"}{if $editMode}Edit{else}Create{/if} Merchant Group{/block}
{block name="mainContent"}
	<h3>{if $editMode}Edit{else}Create{/if} Merchant Group</h3>
	<form class="form-horizontal" method="post" action="{if $editMode}{url to="MerchantGroupController->update"}{else}{url to="MerchantGroupController->construct"}{/if}">
		{if $editMode}
			<input type="hidden" name="id" value="{$group->id}">
		{/if}
		<div class="control-group">
			<label class="control-label" for="name">Name:</label>
			<div class="controls">
				<input type="text" name="name" maxlength="100"{if $editMode} value="{$group->name}"{/if}>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="discount">Discount:</label>
			<div class="controls">
				<div class="input-append">
					<input type="text" name="discount"{if $editMode} value="{$group->discount * 100}"{/if}>
					<span class="add-on">%</span>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="contactName">Contact Name:</label>
			<div class="controls">
				<input type="text" name="contactName" maxlength="100"{if $editMode} value="{$group->contactName}"{/if}>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="companyName">Company Name:</label>
			<div class="controls">
				<input type="text" name="companyName" maxlength="100"{if $editMode} value="{$group->companyName}"{/if}>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="contactEmail">Contact Email:</label>
			<div class="controls">
				<input type="text" name="contactEmail" maxlength="100"{if $editMode} value="{$group->contactEmail}"{/if}>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="telephone">Telephone:</label>
			<div class="controls">
				<input type="text" name="telephone" maxlength="50"{if $editMode} value="{$group->telephone}"{/if}>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="address">Address:</label>
			<div class="controls">
				<textarea rows="5" name="address">{if $editMode}{$group->address}{/if}</textarea>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="notes">Notes:</label>
			<div class="controls">
				<textarea rows="5" name="notes">{if $editMode}{$group->notes}{/if}</textarea>
			</div>
		</div>
		<hr>
		<div class="pull-left">
			<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
			&nbsp;
			<a class="btn" href="{url to="MerchantGroupController->index"}">&lt; Back to Merchant Group Management</a>
			&nbsp;
			<input type="submit" class="btn btn-primary" value="Save">
		</div>
	</form>
{/block}