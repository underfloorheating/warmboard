{extends file="main.tpl"}
{block name="title"}System Type Management{/block}
{block name="mainContent"}
	<h3>System Type Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>System Class</th>
				<th>Data Class</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $types as $type}
				<tr>
					<td>{$type->id}</td>
					<td>{$type->name}</td>
					<td>controllers/system/{$type->sysClzName}</td>
					<td>models/{$type->datClzName}</td>
					<td>
						<button type="button" class="btn btn-small editBut" data-id="{$type->id}" data-name="{$type->name}" data-sysclzname="{$type->sysClzName}" data-datclzname="{$type->datClzName}" data-image="{if !empty($type->image)}{url public=ImageHelper::retrieveImagePath(ImageHelper::SYSTEM_DIR, $type->image)}{else}null{/if}">
							<i class="icon-edit"></i> Edit
						</button>
						<button type="button" class="btn btn-small delBut" data-id="{$type->id}" data-name="{$type->name}">
							<i class="icon-trash"></i> Delete
						</button>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a href="{url to="ApplicationController->index"}" class="btn"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a href="{url to="ProductController->index"}" class="btn">&lt; Back to Product List</a>
		&nbsp;
		<button type="button" class="btn" id="createSystemType">Create System Type</button>
	</div>
	<div class="modal hide fade" id="createModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create System Type</h3>
		</div>
		<div class="modal-body">
			<form class="form form-horizontal" method="post" action="{url to="SystemTypeController->create"}" enctype="multipart/form-data">
				<div class="control-group">
					<label for="name" class="control-label">Name:</label>
					<div class="controls">
						<input type="text" name="name">
					</div>
				</div>
				<div class="control-group">
					<label for="sysClzName" class="control-label">System Class:</label>
					<div class="controls">
						<div class="input-prepend input-append">
							<span class="add-on">controllers/system/</span>
							<input type="text" name="sysClzName" class="span2">
							<span class="add-on">.php</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="datClzName" class="control-label">Data Class:</label>
					<div class="controls">
						<div class="input-prepend input-append">
							<span class="add-on">models/</span>
							<input type="text" name="datClzName">
							<span class="add-on">.php</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="image" class="control-label">Brochure Image:</label>
					<div class="controls">
						<input name="image" type="file">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">Close</button>
			<button type="button" class="btn btn-primary" id="doCreateBut">Save</button>
		</div>
	</div>
	<div class="modal hide fade" id="editModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit System Type</h3>
		</div>
		<div class="modal-body">
			<form class="form form-horizontal" method="post" action="{url to="SystemTypeController->update"}" enctype="multipart/form-data">
				<input type="hidden" name="id">
				<div class="control-group">
					<label for="name" class="control-label">Name:</label>
					<div class="controls">
						<input type="text" name="name">
					</div>
				</div>
				<div class="control-group">
					<label for="sysClzName" class="control-label">System Class:</label>
					<div class="controls">
						<div class="input-prepend input-append">
							<span class="add-on">controllers/system/</span>
							<input type="text" name="sysClzName" class="span2">
							<span class="add-on">.php</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="datClzName" class="control-label">Data Class:</label>
					<div class="controls">
						<div class="input-prepend input-append">
							<span class="add-on">models/</span>
							<input type="text" name="datClzName">
							<span class="add-on">.php</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="image" class="control-label">Brochure Image:</label>
					<div class="controls">
						<div class="thumbnail" style="width: 343px;">
							<img src="" alt="Current System Image">
							<label class="checkbox">
								<input type="checkbox" name="deleteImage"> Remove this image
							</label>
						</div>
						<input type="file" name="image">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">Close</button>
			<button type="button" class="btn btn-primary" id="doEditBut">Save</button>
		</div>
	</div>
	<div class="modal hide fade" id="deleteModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete system Type</h3>
		</div>
		<div class="modal-body">
			<form method="post" action="{url to="SystemTypeController->delete"}">
				<input type="hidden" name="id">
			</form>
			<p>
				Are you sure you want delete the system type <span></span>.<br>
				Any systems which are set to this type will also be removed.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn">Close</button>
			<button type="button" class="btn btn-danger" id="doDeleteBut">Delete</button>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('#createSystemType').click(function() {
				$('#createModal').modal();
			});

			$('.editBut').click(function() {
				$('#editModal input[name=id]').val($(this).data('id'));
				$('#editModal input[name=name]').val($(this).data('name'));
				$('#editModal input[name=sysClzName]').val($(this).data('sysclzname'));
				$('#editModal input[name=datClzName]').val($(this).data('datclzname'));
				var img = $(this).data('image');
				if (img == "null" || img == null) {
					$('#editModal .thumbnail').hide();

				} else {
					$('#editModal .thumbnail>img').attr('src', img);
					$('#editModal .thumbnail').show();
				}
				$('#editModal').modal();
			});

			$('.delBut').click(function() {
				$('#deleteModal input[name=id]').val($(this).data('id'));
				$('#deleteModal p>span').text($(this).data('name'));
				$('#deleteModal').modal();
			});

			$('#doCreateBut').click(function() {
				$('#createModal form').submit();
			});

			$('#doEditBut').click(function() {
				$('#editModal form').submit();
			});

			$('#doDeleteBut').click(function() {
				$('#deleteModal form').submit();
			});
		});
		-->
	</script>
{/block}