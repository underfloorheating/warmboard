{extends file="main.tpl"}
{block name="title"}Room Name Management{/block}
{block name="mainContent"}
	<h3>Room Name Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th></th>
		</thead>
		<tbody class="text-center">
			{foreach $names as $name}
				<tr>
					<td>{$name->id}</td>
					<td>{$name->name}</td>
					<td>
						<button class="btn btn-small" data-action="edit-name"
							data-id="{$name->id}"
							data-name="{$name->name}"
							data-orderval="{$name->orderVal}">
							<i class="icon-edit"></i> Edit
						</button>
						<button class="btn btn-small btn-danger" data-action="delete-name" data-id="{$name->id}" data-name="{$name->name}">
							<i class="icon-trash icon-white"></i> Delete
						</button>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<button type="button" class="btn" data-action="add-name">Add Room Name</button>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="RoomNameController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="RoomNameController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="RoomNameController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="createRoomNameModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create Room Name</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<div class="control-group">
					<label class="control-label" for="create_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="create_name">
					</div>
				</div>
				<div class="form-horizontal">
					<label class="control-label" for="create_orderval">Display Order:</label>
					<div class="controls">
						<input type="text" name="orderVal" id="create_orderval">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="add">Add</a>
		</div>
	</div>
	<div class="modal hide fade" id="editRoomNameModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Edit Room Name</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<input type="hidden" name="room_name_id">
				<div class="control-group">
					<label class="control-label" for="edit_name">Name:</label>
					<div class="controls">
						<input type="text" name="name" id="edit_name">
					</div>
				</div>
				<div class="form-horizontal">
					<label class="control-label" for="edit_orderval">Display Order:</label>
					<div class="controls">
						<input type="text" name="orderVal" id="edit_orderval">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="edit">Edit</a>
		</div>
	</div>
	<div class="modal hide fade" id="deleteRoomNameModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Room Name</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<p>Are you sure you want to delete the contact <strong></strong></p>
				<input type="hidden" name="room_name_id">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('button').click(function() {
				procButtonClick(this);
			});

			$('#createRoomNameModal #add').click(function(e) {
				e.preventDefault();
				$('#createRoomNameModal form').submit();
			});
			$('#createRoomNameModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="RoomNameController->create"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#createRoomNameModal').modal('hide');
						window.location.reload();

					} else {
						$('#createRoomNameModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#editRoomNameModal #edit').click(function(e) {
				e.preventDefault();
				$('#editRoomNameModal form').submit();
			});
			$('#editRoomNameModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="RoomNameController->edit"}', $(this).serialize(), function(data) {
					if (data.status == "OK") {
						$('#editRoomNameModal').modal('hide');
						window.location.reload();

					} else {
						$('#editRoomNameModal .errors').text(data.status).show('fast');
					}
				});
			});

			$('#deleteRoomNameModal #delete').click(function(e) {
				$.getJSON('{url to="RoomNameController->delete"}', { room_name_id: $('#deleteRoomNameModal input[name=room_name_id]').val() }, function(data) {
					if (data.status == "OK") {
						$('#deleteRoomNameModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteRoomNameModal .errors').text(data.status).show('fast');
					}
				});
			});
		});

		function procButtonClick(button) {
			switch ($(button).data("action")) {
				case "add-name":
					$('#createRoomNameModal').modal();
					break;

				case "edit-name":
					$('#editRoomNameModal input[name=room_name_id]').val($(button).data("id"));
					$('#editRoomNameModal input[name=name]').val($(button).data("name"));
					$('#editRoomNameModal input[name=orderVal]').val($(button).data("orderVal"));
					$('#editRoomNameModal').modal();
					break;

				case "delete-name":
					$('#deleteRoomNameModal .errors').hide();
					$('#deleteRoomNameModal input[name=room_name_id]').val($(button).data("id"));
					$('#deleteRoomNameModal strong').text($(button).data("name"));
					$('#deleteRoomNameModal').modal();
					break;
			}
		}
		-->
	</script>
{/block}