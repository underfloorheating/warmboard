<!DOCTYPE html>
<html>
	<head>
		<title>Maintenance Mode</title>
	</head>
	<body>
		<h1>Maintenance Mode</h1>
		<p>
			The UFH Calculator is currently offline while we upgrade the system.<br>
			Please check back in 10 mins.
		</p>
		<p>Sorry for any inconvenience.</p>
	</body>
</html>