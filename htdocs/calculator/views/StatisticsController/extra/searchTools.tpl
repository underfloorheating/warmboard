<form class="form-inline">
	<label for="st_start">Start:</label>
	<input type="text" class="input-small" name="start" id="st_start" value="{if isset($smarty.get.start)}{$smarty.get.start|escape}{else}0000-00-00{/if}">

	<label for="st_end">End:</label>
	<input type="text" class="input-small" name="end" id="st_end" value="{if isset($smarty.get.end)}{$smarty.get.end|escape}{else}{$smarty.now|date_format:"%Y-%m-%d"}{/if}">

{if isset($showDateGroupBy) && $showDateGroupBy}
	<label for="st_groupby">Group by:</label>
	<select name="groupby" id="st_groupby">
		<option value="{StatisticsController::GROUPBY_DAY}" {if isset($smarty.get.groupby) && $smarty.get.groupby == StatisticsController::GROUPBY_DAY}selected{/if}>Day</option>
		<option value="{StatisticsController::GROUPBY_MONTH}" {if !isset($smarty.get.groupby) || $smarty.get.groupby == StatisticsController::GROUPBY_MONTH}selected{/if}>Month</option>
		<option value="{StatisticsController::GROUPBY_YEAR}" {if isset($smarty.get.groupby) && $smarty.get.groupby == StatisticsController::GROUPBY_YEAR}selected{/if}>Year</option>
	</select>
{/if}

	<button type="submit" class="btn">Filter</button>
</form>