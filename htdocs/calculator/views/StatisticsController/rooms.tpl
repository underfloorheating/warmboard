{extends file="main.tpl"}
{block name="title"}Statistics{/block}
{block name="mainContent"}
	<h3>Top Ten Popular Room Names</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th rowspan="2">Room Name</th>
				<th rowspan="2">Number of Uses</th>
				<th colspan="3">Area (m<sup>2</sup>)</th>
				<th colspan="3">Distance From Manifold (m)</th>
				<th rowspan="2">Average Order (&pound;)</th>
				<th rowspan="2">Minimum Order (&pound;)</th>
				<th rowspan="2">Maximum Order (&pound;)</th>
				<th rowspan="2">Total Revenue (&pound;)</th>
			</tr>
			<tr>
				<th>Minimum</th>
				<th>Maximum</th>
				<th>Average</th>
				<th>Minimum</th>
				<th>Maximum</th>
				<th>Average</th>
			</tr>
		</thead>
		<tbody>
			{foreach $stats as $stat}
				<tr>
					<td>{$stat.name|escape}</td>
					<td>{$stat.roomCount|number_format}</td>
					<td>{$stat.areaMin|number_format:2}</td>
					<td>{$stat.areaMax|number_format:2}</td>
					<td>{$stat.areaAvg|number_format:2}</td>
					<td>{$stat.distMin|number_format:2}</td>
					<td>{$stat.distMax|number_format:2}</td>
					<td>{$stat.distAvg|number_format:2}</td>
					<td>{$stat.priceAvg|number_format:2|default:"-"}</td>
					<td>{$stat.priceMin|number_format:2|default:"-"}</td>
					<td>{$stat.priceMax|number_format:2|default:"-"}</td>
					<td>{$stat.priceSum|number_format:2|default:"-"}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="12">No data to display, try choosing a better date range.</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="StatisticsController->index"}">&lt; Back to Statistics</a>
	</div>
	<div class="pull-right">
		{include file="./extra/searchTools.tpl" showDateGroupBy=false}
	</div>
{/block}