{extends file="main.tpl"}
{block name="title"}Statistics{/block}
{block name="mainContent"}
	<h3>Merchants Which Never Used The Calculator</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Merchant</th>
				<th>Email</th>
				<th>Company</th>
				<th>Telephone</th>
			</tr>
		</thead>
		<tbody>
			{foreach $merchants as $i}
				<tr>
					<td><a href="{url to="MerchantController->show"}/{$i->id}">{$i->contactName|escape}</a></td>
					<td>{$i->contactEmail|escape}</td>
					<td>{$i->companyName|escape}</td>
					<td>{$i->telephone|escape}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="StatisticsController->index"}">&lt; Back to Statistics</a>
	</div>
{/block}