{extends file="main.tpl"}
{block name="title"}Calculator Statistics{/block}
{block name="mainContent"}
	<h3>Statistics</h3>
	<div id="mainMenu">
		<div class="row-fluid">
			<div class="span3">
				<a href="{url to="StatisticsController->calculations"}">
					<img src="{url public="/images/list.png"}"><br>
					Calculations Over Time
				</a>
			</div>
			<div class="span3">
				<a href="{url to="StatisticsController->bestMerchants"}">
					<img src="{url public="/images/list.png"}"><br>
					Best Merchant Performance
				</a>
			</div>
			<div class="span3">
				<a href="{url to="StatisticsController->worstMerchants"}">
					<img src="{url public="/images/list.png"}"><br>
					Worst Merchants Performance
				</a>
			</div>
			<div class="span3">
				<a href="{url to="StatisticsController->merchantsNeverUsed"}">
					<img src="{url public="/images/list.png"}"><br>
					Never Used Report
				</a>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span3 offset3">
				<a href="{url to="StatisticsController->popularRoomNames"}">
					<img src="{url public="/images/list.png"}"><br>
					Popular Room Names
				</a>
			</div>
			<div class="span3">
				<a href="{url to="StatisticsController->popularFloorNames"}">
					<img src="{url public="/images/list.png"}"><br>
					Popular Floor Names
				</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
	</div>
{/block}