{extends file="main.tpl"}
{block name="title"}Statistics{/block}
{block name="mainContent"}
	<h3>{if $isBest}Top{else}Bottom{/if} Merchants by Calculations</h3>
	<div class="row-fluid">
		<div class="offset3 span6">
			<canvas id="calcCount"></canvas>
		</div>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Merchant</th>
				<th>Number Raised</th>
				<th>Revenue on Raised</th>
				<th>Revenue on Raised After Merchant Discount</th>
				<th>Minimum Order</th>
				<th>Maximum Order</th>
				<th>Average Order</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Totals</th>
				<td>{$totals.calc_count|number_format}</td>
				<td>{$totals.price_sum|number_format:2}</td>
				<td>{$totals.price_discount|number_format:2}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tfoot>
		<tbody>
			{foreach $stats.calcCount as $i}
				<tr>
					<td><a href="{url to="MerchantController->show"}/{$i.id}">{$i.contactName|escape}</a></td>
					<td>{$i.calc_count|number_format}</td>
					<td>{$i.price_sum|number_format:2}
					<td>{$i.price_discount|number_format:2}</td>
					<td>{$i.price_min|number_format:2}</td>
					<td>{$i.price_max|number_format:2}</td>
					<td>{$i.price_avg|number_format:2}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>

	<h3>{if $isBest}Top{else}Bottom{/if} Merchants by Revenue</h3>
	<div class="row-fluid">
		<div class="offset3 span6">
			<canvas id="priceSum"></canvas>
		</div>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Merchant</th>
				<th>Number Raised</th>
				<th>Revenue on Raised</th>
				<th>Revenue on Raised After Merchant Discount</th>
				<th>Minimum Order</th>
				<th>Maximum Order</th>
				<th>Average Order</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Totals</th>
				<td>{$totals.calc_count|number_format}</td>
				<td>{$totals.price_sum|number_format:2}</td>
				<td>{$totals.price_discount|number_format:2}</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</tfoot>
		<tbody>
			{foreach $stats.priceSum as $i}
				<tr>
					<td><a href="{url to="MerchantController->show"}/{$i.id}">{$i.contactName|escape}</a></td>
					<td>{$i.calc_count|number_format}</td>
					<td>{$i.price_sum|number_format:2}
					<td>{$i.price_discount|number_format:2}</td>
					<td>{$i.price_min|number_format:2}</td>
					<td>{$i.price_max|number_format:2}</td>
					<td>{$i.price_avg|number_format:2}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="StatisticsController->index"}">&lt; Back to Statistics</a>
	</div>
	<div class="pull-right">
		{include file="./extra/searchTools.tpl" showDateGroupBy=false}
	</div>

	<script type="text/javascript" src="{url public="js/Chart.min.js"}"></script>
	<script type="text/javascript">
		{$chartColours = ['#F7464A', '#46BFBD', '#FDB45C']}
		{$chartHighlights = ['#FF5A5E', '#5AD3D1', '#FFC870']}
		Chart.defaults.global.responsive = true;
		var calcCount = [
			{foreach $chart.calcCount as $i}
				{
					label: '{$i.label|escape}',
					value: {$i.value},
					color: '{$chartColours[$i@index % 3]}',
					highlight: '{$chartHighlights[$i@index % 3]}'
				},
			{/foreach}
		];
		var e = document.getElementById('calcCount');
		var ctx = e.getContext('2d');
		new Chart(ctx).Pie(calcCount);

		var priceSum = [
			{foreach $chart.priceSum as $i}
				{
					label: '{$i.label|escape}',
					value: {$i.value},
					color: '{$chartColours[$i@index % 3]}',
					highlight: '{$chartHighlights[$i@index % 3]}'
				},
			{/foreach}
		];
		var e2 = document.getElementById('priceSum');
		var ctx2 = e2.getContext('2d');
		new Chart(ctx2).Pie(priceSum);
	</script>
{/block}