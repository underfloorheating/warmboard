{extends file="main.tpl"}
{block name="title"}Statistics{/block}
{block name="mainContent"}
	<h3>Calculations Over Time</h3>
	<canvas id="chart"></canvas>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Date</th>
				<th>Conversions (excluding raised) <span style="background-color:rgb(220,220,220);width:15px;height:15px;display:inline-block"></span></th>
				<th colspan="2">Raised <span style="background-color:rgb(151,187,205);width:15px;height:15px;display:inline-block"></span></th>
				<th>Total <span style="background-color:rgb(100,100,100);width:15px;height:15px;display:inline-block"></span></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Totals</th>
				<td>{$totals.conversion}</td>
				<td>{$totals.raised}</td>
				<td>{($totals.raised / $totals.total * 100)|number_format:2}%</td>
				<td>{$totals.total}</td>
			</tr>
		</tfoot>
		<tbody>
			{foreach $labels as $i => $j}
				<tr>
					<td>{$j}</td>
					<td>{$data[$i].conversion.calc_count|number_format|default:"-"}{if !$j@first} <small style="{if $data[$i].conversion.calc_count|default:0 - $lastConv >= 0}color:#0a0;{else}color:#a00;{/if}">&Delta; {($data[$i].conversion.calc_count|default:0 - $lastConv)|number_format}{/if}</td>
					<td>{$data[$i].raised.calc_count|default:"-"}{if !$j@first} <small style="{if $data[$i].raised.calc_count|default:0 - $lastRaised >= 0}color:#0a0;{else}color:#a00;{/if}">&Delta; {($data[$i].raised.calc_count|default:0 - $lastRaised)|number_format}{/if}</td>
					<td>{($data[$i].raised.calc_count|default:0 / $data[$i].total.calc_count * 100)|number_format:2}%</td>
					<td>{$data[$i].total.calc_count}{if !$j@first} <small style="{if $data[$i].total.calc_count - $lastTotal >= 0}color:#0a0;{else}color:#a00;{/if}">&Delta; {($data[$i].total.calc_count - $lastTotal)|number_format}{/if}</td>
				</tr>
				{$lastConv = $data[$i].conversion.calc_count|default:"-"}
				{$lastRaised = $data[$i].raised.calc_count|default:"-"}
				{$lastTotal = $data[$i].total.calc_count}
			{/foreach}
		</tbody>
	</table>

	<h3>Raised Calculation Revenue</h3>
	<div class="row-fluid">
		<div class="span6">
			<canvas id="chart2"></canvas>
		</div>
		<div class="span6">
			<canvas id="chart3"></canvas>
		</div>
	</div>

	<table class="table table-striped">
		<thead>
			<tr>
				<th>Date</th>
				<th>Minimum Order (&pound;) <span style="background-color:rgb(220,220,220);width:15px;height:15px;display:inline-block"></span></th>
				<th>Maximum Order (&pound;) <span style="background-color:rgb(151,187,205);width:15px;height:15px;display:inline-block"></span></th>
				<th>Average (&pound;) <span style="background-color:rgb(205,151,205);width:15px;height:15px;display:inline-block"></span></th>
				<th>Total (&pound;) <span style="background-color:rgb(151,205,187);width:15px;height:15px;display:inline-block"></span></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Totals</th>
				<td></td>
				<td></td>
				<td></td>
				<td>{$totals.price_sum|number_format:2}</td>
			</tr>
		</tfoot>
		<tbody>
			{foreach $labels as $i => $j}
				<tr>
					<td>{$j}</td>
					<td>{$data[$i].raised.price_min|default:0|number_format:2}</td>
					<td>{$data[$i].raised.price_max|default:0|number_format:2}</td>
					<td>{$data[$i].raised.price_avg|default:0|number_format:2}</td>
					<td>{$data[$i].raised.price_sum|default:0|number_format:2}{if !$j@first} <small style="{if $data[$i].raised.price_sum|default:0 - $lastSum >= 0}color:#0a0;{else}color:#a00;{/if}">&Delta; {($data[$i].raised.price_sum|default:0 - $lastSum)|number_format:2}{/if}</td>
				</tr>
				{$lastSum = $data[$i].raised.price_sum|default:0}
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="StatisticsController->index"}">&lt; Back to Statistics</a>
	</div>
	<div class="pull-right">
		{include file="./extra/searchTools.tpl" showDateGroupBy=true}
	</div>

	<script type="text/javascript" src="{url public="js/Chart.min.js"}"></script>
	<script type="text/javascript">
		Chart.defaults.global.responsive = true;
		var data = {
			labels: [{foreach $labels as $i}'{$i}',{/foreach}],
			datasets: [
			{
				label: 'Conversions',
				fillColor: 'rgba(220,220,220,0.2)',
				strokeColor: 'rgba(220,220,220,1)',
				pointColor: 'rgba(220,220,220,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: [{foreach $data as $i}{$i.conversion.calc_count|default:0},{/foreach}]
			},
			{
				label: 'Raised',
				fillColor: 'rgba(151,187,205,0.2)',
				strokeColor: 'rgba(151,187,205,1)',
				pointColor: 'rgba(151,187,205,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(151,187,205,1)',
				data: [{foreach $data as $i}{$i.raised.calc_count|default:0},{/foreach}]
			},
			{
				label: 'Total',
				fillColor: 'rgba(0,0,0,0)',
				strokeColor: 'rgba(100,100,100,1)',
				pointColor: 'rgba(100,100,100,1)',
				pointStrokeColor: '#fff',
				pointHighlightFill: '#fff',
				pointHighlightStroke: 'rgba(100,100,100,1)',
				data: [{foreach $data as $i}{$i.total.calc_count|default:0},{/foreach}]
			}]
		};
		var e = document.getElementById('chart');
		var ctx = e.getContext('2d');
		var chart = new Chart(ctx).Line(data);

		var data2 = {
			labels: [{foreach $labels as $i}'{$i}',{/foreach}],
			datasets: [
			{
				label: 'Minimum Pricing',
				fillColor: 'rgba(220,220,220,0.5)',
				strokeColor: 'rgba(220,220,220,0.8)',
				highlightFill: 'rgba(220,220,220,0.75)',
				pointHighlightStroke: 'rgba(220,220,220,1)',
				data: [{foreach $data as $i}{$i.raised.price_min|default:0},{/foreach}]
			},
			{
				label: 'Average Pricing',
				fillColor: 'rgba(205,151,205,0.5)',
				strokeColor: 'rgba(205,151,205,0.8)',
				highlightFill: 'rgba(205,151,205,0.75)',
				pointHighlightStroke: 'rgba(205,151,205,1)',
				data: [{foreach $data as $i}{$i.raised.price_avg|default:0},{/foreach}]
			},
			{
				label: 'Maximum Pricing',
				fillColor: 'rgba(151,187,205,0.5)',
				strokeColor: 'rgba(151,187,205,0.8)',
				highlightFill: 'rgba(151,187,205,0.75)',
				pointHighlightStroke: 'rgba(151,187,205,1)',
				data: [{foreach $data as $i}{$i.raised.price_max|default:0},{/foreach}]
			}]
		};
		var e2 = document.getElementById('chart2');
		var ctx2 = e2.getContext('2d');
		var chart2 = new Chart(ctx2).Bar(data2);

		var data3 = {
			labels: [{foreach $labels as $i}'{$i}',{/foreach}],
			datasets: [
			{
				label: 'Sum Pricing',
				fillColor: 'rgba(151,205,187,0.5)',
				strokeColor: 'rgba(151,205,187,0.8)',
				highlightFill: 'rgba(151,205,187,0.75)',
				pointHighlightStroke: 'rgba(151,205,187,1)',
				data: [{foreach $data as $i}{$i.raised.price_sum|default:0},{/foreach}]
			}]
		};
		var e3 = document.getElementById('chart3');
		var ctx3 = e3.getContext('2d');
		var chart3 = new Chart(ctx3).Bar(data3);
	</script>
{/block}