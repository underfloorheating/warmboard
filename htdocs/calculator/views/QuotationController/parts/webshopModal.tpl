<div class="modal fade hide" id="webshopLoginModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Webshop Login</h3>
	</div>
	<div class="modal-body">
		<div class="hide errors alert alert-error"></div>
		<form class="form-horizontal" id="webshopLogin_form">
			<p class="help-block">
				Please enter your web shop login details below so we can
				transfer your order.
			</p>
			<div class="control-group">
				<label class="control-label" for="webshopLogin_login">Login:</label>
				<div class="controls">
					<input id="webshopLogin_login" name="username" type="text">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="webshopLogin_password">Password:</label>
				<div class="controls">
					<input id="webshopLogin_password" name="password" type="password">
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn">Cancel</button>
		<button type="button" class="btn btn-primary" id="webshopLogin_but">Login</button>
	</div>
</div>

<div class="modal fade hide" id="webshopSubmitModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Transferring you to our Webshop</h3>
	</div>
	<div class="modal-body">
		<form action="http://webportal-solfex.co.uk/templates/solfex/checkout.aspx" method="post" id="webshopSubmit_form">
			<input type="hidden" name="data" id="webshopSubmit_data" value="">
		</form>
		<p class="text-center">
			Please wait while you are forwarded...<br>
			<img src="{url public="images/ajax-loader.gif"}" alt="Loading...">
		</p>
	</div>
	<div class="modal-footer">
	</div>
</div>
<script type="text/javascript">
	$(function() {
		var recallCode = '{$calculationData->recallCode}';
		var products = getWebshopProducts();

		$('#webshopLogin_form').submit(function(e) {
			e.preventDefault();

			$('#webshopSubmitModal').modal();
			$.post('{url to="QuotationController->webshopLogin"}', $(this).serialize(), 'text')
			.done(function(guid) {
				$('#webshopSubmit_data').val(guid+"\r\n"+recallCode+products);
				$('#webshopSubmit_form').submit();
				$('#webshopLoginModal').modal('hide');
				$('#webshopLogin_form').get(0).reset();
			})
			.fail(function(obj, textStatus, errorThrown) {
				if (obj.status)
					$('#webshopLoginModal .errors').text('There was an error processing your request: ' + textStatus).slideDown();
				else
					$('#webshopLoginModal .errors').text('Unable to complete your request: '+ errorThrown).slideDown();
			})
			.always(function() {
				$('#webshopSubmitModal').modal('hide');
			});
		});
		$('#webshopLogin_but').click(function() {
			$('#webshopLogin_form').submit();
		});
		$('#webshopBut').click(function() {
			$('#webshopLoginModal .errors').hide();
			$('#webshopLoginModal').modal();
		});
	});

	function getWebshopProducts() {
		return "{foreach $productList->products as $product}\r\n{$product->code|escape},{$productList->qty[$product->code]}{/foreach}";
	}
</script>