<div class="modal hide fade" id="emailModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Email Calculation Results</h3>
	</div>
	<div class="modal-body">
		<div class="hide errors alert alert-error"></div>
		<form class="form-horizontal">
			<div class="control-group">
				<label class="control-label" for="fullName">Full Name:</label>
				<div class="controls">
					<input name="fullName" type="text" maxlength="300">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="telephone:">Telephone:</label>
				<div class="controls">
					<input name="telephone" type="text" maxlength="50">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email">Email:</label>
				<div class="controls">
					<input name="email" type="email" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<div class="controls" id="">
					<label class="checkbox"><input type="checkbox" id="emailToc"> I agree to the <a href="http://www.solfex.co.uk/UnderfloorHeating/Terms" target="_blank">Terms and Conditions</a>.</label>
					<label class="checkbox"><input type="checkbox" name="contact"> I&apos;m happy for my email address to be used in future for marketing purposes by Solfex.</label>
					<label class="checkbox"><input type="checkbox" id="emailGuide"> I acknowledge that the quoted amount is a guide only.</label>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<span>Your information will not be passed onto any third parties.</span>
		<button type="button" data-dismiss="modal" class="btn">Cancel</button>
		<button type="button" class="btn btn-primary" id="sendEmailBut" disabled>Save</button>
	</div>
</div>
<div class="modal hide fade" id="emailDoneModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Email Calculation Results</h3>
	</div>
	<div class="modal-body">
		<p>An email has been sent to <strong class="email"></strong></p>
		<p>This calculation&apos;s code is: <strong class="code"></strong></p>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-primary">OK</button>
	</div>
</div>

<script type="text/javascript">
	<!--
	$(function() {
		$('#emailModal form').submit(function(e) {
			e.preventDefault();
			$.getJSON("{url to="QuotationController->storeQuote"}",
				$('form').serialize(),
				function(data) {
					if (data.status == 'OK') {
						$('#emailModal').modal('hide');
						$('#emailDoneModal strong.email').text($('#emailModal input[name=email]').val());
						$('#emailDoneModal strong.code').text(data.code);
						$('#emailDoneModal').modal();
						$('#emailModal .errors').hide();
						$('#emailModal input').val('');

						//Google Conversion code
						$('body').append($('<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/980092277/?value=0&amp;label=QoaqCMOYlAcQ9Yqs0wM&amp;guid=ON&amp;script=0"/>'));

					} else {
						$('#emailModal .errors').text('Unable to save Calculation: ' + data.status).show('fast');
					}
			});
		});

		$('#emailModal #sendEmailBut').click(function() {
			$('#emailModal form').submit();
		});

		$('#openEmailModalBut').click(function() {
			$('#emailModal').modal();
		});

		$('#emailModal #emailToc, #emailModal #emailGuide').click(function() {
			$('#emailModal #sendEmailBut').prop('disabled', !($('#emailModal #emailToc').prop('checked') && $('#emailModal #emailGuide').prop('checked')));
		});
	});
	-->
</script>
