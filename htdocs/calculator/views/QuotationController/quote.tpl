{extends file="main.tpl"}
{block name="title"}Quotation{/block}
{block name="mainContent"}
	{if $smarty.session.theme!='plumbnation'}
		<h3>Quotation</h3>
	{else}
		<h1>Quotation</h1>
		<hr />
	{/if}
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Product Code</th>
				<th>Product Description</th>
				<th>List Price</th>
				<th>Amount Required</th>
				<th>Total Price</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="3"></td>
				<th>UFH List Price:</th>
				<td>&pound;{$productList->calculateTotal(ProductList::TOTAL_UFH)|number_format:2}</td>
			</tr>
			<tr>
				<td colspan="3"></td>
				<th>Heat Pump List Price:</th>
				<td>&pound;{$productList->calculateTotal(ProductList::TOTAL_HEATSOURCE)|number_format:2}</td>
			</tr>
			<tr>
				<td colspan="3"></td>
				<th>Total List Price:</th>
				<td>&pound;{$productList->calculateTotal()|number_format:2}</td>
			</tr>
			<tr style="display:none" class="merchantDetail">
				<td colspan="3"></td>
				<th>Merchant Buying Price:</th>
				<td>&pound;{$purchasePrice|number_format:2}</td>
			</tr>
			<tr style="display:none" class="merchantDetail">
				<td colspan="3"></td>
				<th>Installer Discount Off List Price: <i class="icon-question-sign" data-help="installer-discount"></i></th>
				<td>
					<select id="customerDiscount" name="customerDiscount" data-rrp="{$productList->calculateTotal()}" data-margin="{$discountMargin}"{if $isLocked} disabled{/if}>
						{for $i=0 to $merchantDiscount*100}
						<option value="{$i}"{if $isLocked && $calculationData->discount*100 == $i} selected{/if}>
							{$i}% - &pound;{($productList->calculateTotal() * ($i / 100))|number_format:2}
						</option>
						{/for}
					</select>
				</td>
			</tr>
			<tr style="display:none" class="merchantDetail">
				<td colspan="3"></td>
				<th>Merchant Gross Profit:</th>
				<td>&pound;<span id="marginValue">{$discountMargin|number_format:2}</span></td>
			</tr>
			<tr>
				<td colspan="3"></td>
				<th>Agreed Installer Deal Price:</th>
				<td>
					{if !$isLocked}
						&pound;<span id="grandTotal">{$productList->calculateTotal()|number_format:2}</span>
					{else}
						&pound;{($productList->calculateTotal() - ($productList->calculateTotal() * $calculationData->discount))|number_format:2}
					{/if}
				</td>
			</tr>
		</tfoot>
		<tbody>
			{foreach $productList->products as $product}
				{if $product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE}{continue}{/if}
				<tr>
					<td>{$product->code|escape}</td>
					<td>{$product->name|escape}</td>
					<td>&pound;{$product->price|number_format:2}</td>
					<td>{$productList->qty[$product->code]|number_format}</td>
					<td>&pound;{$productList->calculateProductSubTotal($product)|number_format:2}</td>
				</tr>
			{/foreach}
			{if $productList->containsHeatSource()}
				<tr><th colspan="5">Heat Sources</th></tr>
				{foreach $productList->products as $product}
					{if $product->typeId != ProductType::PRODUCTTYPE_HEAT_SOURCE}{continue}{/if}
					<tr>
						<td>{$product->code|escape}</td>
						<td>{$product->name|escape}</td>
						<td>&pound;{$product->price|number_format:2}</td>
						<td>{$productList->qty[$product->code]|number_format}</td>
						<td>&pound;{$productList->calculateProductSubTotal($product)|number_format:2}</td>
					</tr>
				{/foreach}
			{/if}
		</tbody>
	</table>
	<div class="row-fluid">
		<div class="span12">
			<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
			<i class="icon-question-sign" data-help="home"></i>
			&nbsp;
			<div class="btn-group">
				<a class="btn" href="{url to="QuotationController->intermediate"}">&lt; Back</a>
				<button class="btn" type="button" id="showDetailsBut">Show Merchant Details</button>
			</div>
			<i class="icon-question-sign" data-help="merch-detail"></i>
			&nbsp;
			{if !$isLocked}
				<button type="button" class="btn btn-primary" id="showAgreeBut">Raise Quotation</button>
				<i class="icon-question-sign" data-help="raise-quote"></i>
			{else}
				<a href="{url to="QuotationController->generatePdf"}?pdfCode={$pdfDlCode}" class="btn btn-primary">Download Quotation</a>
			{/if}
			&nbsp;
			<button type="button" class="btn btn-primary" id="webshopBut">Checkout via Webshop</button>
		</div>
	</div>
	<div class="modal hide fade" id="agreeModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Assign the Quotation to an Installer</h3>
		</div>
		<div class="modal-body">
			<div class="hide errors alert alert-error"></div>
			<form class="form-horizontal">
				<input type="hidden" name="installerId">
				<input type="hidden" name="pdfCode" value="{$pdfDlCode}">
				<h4>
					Is the installer already registered? If so you can find
					them by entering either the installer&apos;s name, telephone
					number or email address in the relevant field below and
					click Find Installer.
				</h4>
				<div class="row">
					<div class="span5">
						<div class="control-group">
							<label class="control-label" for="name">Installer Name:</label>
							<div class="controls">
								<input type="text" name="name">
							</div>
						</div>
						<div class="control-group">
							<label for="companyName" class="control-label">Company Name:</label>
							<div class="controls">
								<input type="text" name="companyName" disabled>
							</div>
						</div>
						<div class="control-group">
							<label for="address" class="control-label">Address:</label>
							<div class="controls">
								<textarea name="address" disabled></textarea>
							</div>
						</div>
					</div>
					<div class="span4">
						<div class="control-group">
							<label for="email" class="control-label">Email:</label>
							<div class="controls">
								<input type="text" name="email">
							</div>
						</div>
						<div class="control-group">
							<label for="telephone" class="control-label">Telephone:</label>
							<div class="controls">
								<input type="text" name="telephone">
							</div>
						</div>
						<div class="control-group">
							<label for="notes" class="control-label">Notes:</label>
							<div class="controls">
								<textarea name="notes" disabled></textarea>
							</div>
						</div>
					</div>
				</div>
				<button type="button" class="btn btn-secondary" id="findInstaller">Find Installer</button>
				<button type="reset" class="btn" id="resetForm">Reset Search</button>
				<button type="button" class="btn editInstallerBut" id="editInstaller" data-before-callback="editInstallerPreCallback" data-after-callback="editInstallerPostCallback" disabled>
					Edit Installer
				</button>
				<hr>
				<h4>
					<button type="button" class="btn btn-secondary" id="addInstallerBut" data-callback="addInstallerPostCallback">Create Installer</button>
					If the installer isn&apos;t already registered for the UFH Quick Quote.
				</h4>
			</form>
		</div>
		<div class="modal-footer">
			<div class="row" style="display: table">
				<span class="span7 text-left">
					By clicking &apos;Raise Quotation&apos; you agree to the <a href="http://www.solfex.co.uk/Terms/" target="_blank">Terms and Conditions</a> and the calculation will be locked.
				</span>
				<span style="display: table-cell; vertical-align: middle;">
					<button type="button" class="btn" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary" id="doAgreeDeal" disabled>Raise Quotation</button>
				</span>
			</div>
		</div>
	</div>
	<div class="modal hide fade" id="pdfLoadingModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Working</h3>
		</div>
		<div class="modal-body">
			<p class="text-center">
				The PDF is currently being generated...<br>
				Please be patient while it downloads.<br>
				<img src="{url public="images/ajax-loader.gif"}" alt="Loading...">
			</p>
		</div>
		<div class="modal-footer">
		</div>
	</div>

	{include file="InstallerController/parts/installerModals.tpl"}

	{include file="QuotationController/parts/webshopModal.tpl"}

	<script type="text/javascript">
		<!--
		$(function() {
			$('#showDetailsBut').click(function() {
				if (!$(this).data('showing')) {
					$('table>tfoot>tr.merchantDetail').show('fast');
					$(this).text('Hide Merchant Details').data('showing', true);

				} else {
					$('table>tfoot>tr.merchantDetail').hide('fast');
					$(this).text('Show Merchant Details').data('showing', false);
				}
			});

			$('#showAgreeBut').click(function() {
				$('#agreeModal .errors').hide();
				$('#agreeModal').modal();
			});

			$('#customerDiscount').change(function() {
				var percent = $(this).val() / 100;
				var rrp = $(this).data('rrp');
				var margin = $(this).data('margin');

				var total = rrp - (percent * rrp);
				var realMargin = margin - (percent * rrp);

				$('#marginValue').text(realMargin.toFixed(2));
				$('#grandTotal').text(total.toFixed(2));
			});

			$('#agreeModal #resetForm').click(function() {
				$('#agreeModal textarea').text('');
				enableInstallerButtons(false);
			});

			$('#findInstaller').click(function() {
				$('#agreeModal input[name=installerId]').val('');
				$('#agreeModal form').submit();
			});
			$('#agreeModal #doAgreeDeal').click(function() {
				$('#agreeModal').modal('hide');
				$('#agreeModal form').submit();
			});

			$('#agreeModal form').submit(function(e) {
				e.preventDefault();

				if ($('input[name=installerId]', this).val() == '') {
					var form = this;
					$.getJSON('{url to="InstallerController->searchDetailed"}', $(this).serialize(), function(data) {
						if (data.status == "OK") {
							if (data.results.length > 0) {
								$('#agreeModal .errors').hide('fast');
								$('input[name=installerId]', form).val(data.results[0].id);
								$('input[name=name]', form).val(data.results[0].name);
								$('input[name=companyName]', form).val(data.results[0].companyName == null ? '' : data.results[0].companyName);
								$('input[name=email]', form).val(data.results[0].email == null ? '' : data.results[0].email);
								$('input[name=telephone]', form).val(data.results[0].telephone == null ? '' : data.results[0].telephone);
								$('textarea[name=address]', form).val(data.results[0].address == null ? '' : data.results[0].address);
								$('textarea[name=notes]', form).val(data.results[0].notes == null ? '' : data.results[0].notes);
								enableInstallerButtons(true);

							} else {
								$('#agreeModal .errors').text('No Installer Found').show('fast');
								enableInstallerButtons(false);
							}
						} else {
							$('#agreeModal .errors').text(data.status).show('fast');
						}
					});

				} else {	//Submit form.
					var data = {
						discount: $('#customerDiscount').val() / 100,
						installerId: $('input[name=installerId]', this).val(),
						notes: $('textarea[name=notes]', this).val()
					};
					$.getJSON('{url to="QuotationController->lockCalculation"}', data, function(data) {
						if (data.status == "OK" || data.status == "Already locked") {
							// $('body').append('<iframe id="pdfLoader" src="{url to="QuotationController->generatePdf"}" style="display:none;" onload="$(\'#pdfLoadingModal\').modal(\'hide\');$(\'#pdfLoader\').remove();"></iframe>');
							window.location = "{url to="QuotationController->generatePdf"}?pdfCode={$pdfDlCode}";
							var fileDownloadCheckTimer;
							$('#pdfLoadingModal').modal();
							fileDownloadCheckTimer = window.setInterval(function () {
								var cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)fileDownloadToken\s*\=\s*([^;]*).*$)|^.*$/, "$1");
								if (cookieValue == '{$pdfDlCode}') {
									$('#pdfLoadingModal').modal('hide');
									window.clearInterval(fileDownloadCheckTimer);
								}
							}, 1000);
						} else {
							$('#agreeModal .errors').text(data.status).show('fast');
						}
					});
				}
			});

			setupPopovers($('.icon-question-sign'));
		});

		function enableInstallerButtons(enable) {
			var buttons = $('#doAgreeDeal,#editInstaller,#agreeModal textarea,#agreeModal input[name=companyName]');
			buttons.prop("disabled", !enable);
		}

		function editInstallerPreCallback() {
			$('#editInstallerModal input[name=installerId]').val($('#agreeModal input[name=installerId]').val());
			$('#editInstallerModal input[name=name]').val($('#agreeModal input[name=name]').val());
			$('#editInstallerModal input[name=companyName]').val($('#agreeModal input[name=companyName]').val());
			$('#editInstallerModal input[name=email]').val($('#agreeModal input[name=email]').val());
			$('#editInstallerModal input[name=telephone]').val($('#agreeModal input[name=telephone]').val());
			$('#editInstallerModal textarea[name=address]').val($('#agreeModal textarea[name=address]').val());
			$('#editInstallerModal textarea[name=notes]').val($('#agreeModal textarea[name=notes]').val());
			$('#agreeModal #editInstaller').attr('data-id', $('#agreeModal input[name=installerId]').val());
		}

		function editInstallerPostCallback() {
			$('#agreeModal input[name=installerId]').val($('#editInstallerModal input[name=installerId]').val());
			$('#agreeModal input[name=name]').val($('#editInstallerModal input[name=name]').val());
			$('#agreeModal input[name=companyName]').val($('#editInstallerModal input[name=companyName]').val());
			$('#agreeModal input[name=email]').val($('#editInstallerModal input[name=email]').val());
			$('#agreeModal input[name=telephone]').val($('#editInstallerModal input[name=telephone]').val());
			$('#agreeModal textarea[name=address]').val($('#editInstallerModal textarea[name=address]').val());
			$('#agreeModal textarea[name=notes]').val($('#editInstallerModal textarea[name=notes]').val());
		}

		function addInstallerPostCallback(data) {
			$('#agreeModal input[name=installerId]').val(data.installer.id);
			$('#agreeModal input[name=name]').val($('#addInstallerModal input[name=name]').val());
			$('#agreeModal input[name=companyName]').val($('#addInstallerModal input[name=companyName]').val());
			$('#agreeModal input[name=email]').val($('#addInstallerModal input[name=email]').val());
			$('#agreeModal input[name=telephone]').val($('#addInstallerModal input[name=telephone]').val());
			$('#agreeModal textarea[name=address]').val($('#addInstallerModal textarea[name=address]').val());
			$('#agreeModal textarea[name=notes]').val($('#addInstallerModal textarea[name=notes]').val());

			enableInstallerButtons(true);
		}

		function setupPopovers(elements) {
			var title, content, position;

			elements.each(function() {
				switch ($(this).data('help')) {
					case 'home':
						title = 'Home';
						content = 'Please note if you select the home button this will take you back to main screen and your quotation will not be saved';
						position = 'top';
						break;

					case 'raise-quote':
						title = 'Raise quotation';
						content = 'This will produce a PDF of your quotation';
						position = 'top';
						break;

					case 'merch-detail':
						title = 'Show merchant details';
						content = 'This will allow you to edit discount';
						position = 'top';
						break;

					case 'installer-discount':
						title = 'Installer discount';
						content = 'Please select discount you would like to offer your customer off the list price';
						position = 'top';
						break;

					default:
						return;
				}

				$(this).popover({
					title: title,
					content: content,
					placement: position,
					trigger: 'hover',
				});
			});
		}
		-->
	</script>
	{include file="QuotationController/parts/emailModal.tpl"}
{/block}