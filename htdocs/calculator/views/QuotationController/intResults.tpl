{extends file="main.tpl"}
{block name="title"}Intermediate Results{/block}
{block name="mainContent"}
	{if $smarty.session.theme!='plumbnation'}
		{if isset($smarty.session.loggedUser)}
			<h3>Intermediate Calculations</h3>
		{else}
			<h3>Quick Calculation Results</h3>
		{/if}
	{else}
		{if isset($smarty.session.loggedUser)}
			<h1>Intermediate Calculations</h1>
			<hr />
		{else}
			<h1>Quick Calculation Results</h1>
			<hr />
		{/if}
	{/if}
	<p>
		This is what I have calculated so far, please check the results, the quotation is below.
		{if isset($smarty.session.loggedUser)}
			Click Finish to show item details.
		{/if}
	</p>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Room</th>
				<th>Number of Circuits</th>
				<th>Distance From Manifold
				<th>Total Floor Area (m&sup2;)</th>
				<th>Actual Circuit Length (m)</th>
				<th>Selected Coil Length (m)</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th>Totals:</th>
				<td>{$totalCircuits}</td>
				<td></td>
				<td>{$totalArea|number_format:2}</td>
				<td>{$totalPipe|number_format:2}</td>
				<td></td>
			</tr>
		</tfoot>
		<tbody>
			{foreach $floors as $floor}
				{foreach $floor->_manifolds as $manifold}
					{foreach $manifold->_zones as $zone}
						{foreach $zone->_rooms as $room}
							<tr>
								<td>{$room->name}</td>
								<td>{$room->_numCircuits}</td>
								<td>{$room->distFromManifold}</td>
								<td>{$room->floorArea|number_format:2}</td>
								<td>{$room->_totalPipeLength|number_format:2}</td>
								<td>{$room->_pipePerCircuit|number_format:2}</td>
							</tr>
						{/foreach}
					{/foreach}
				{/foreach}
			{/foreach}
		</tbody>
	</table>
	<div class="row" style="margin-bottom: 20px;">
			<div class="span-12 int-subtotal">
			Quoted Total{if count($heatSources) > 0} (Excluding Heat Source){/if}:
			{if count($selectedHeatSources)}
				<span class="price">&pound;{($productList->calculateTotal() - $heatSourceTotal)|number_format:2}</span>
			{else}
				<span class="price">&pound;{$productList->calculateTotal()|number_format:2}</span>
			{/if}
		</div>
	</div>
	{if count($heatSources) > 0}
		{if $smarty.session.theme!='plumbnation'}
			<h3>If Required - Select A Heat Source For Your Project</h3>
		{else}
			<h1>If Required - Select A Heat Source For Your Project</h1>
		{/if}
		<form id="heatSourceForm">
		{foreach $heatSources as $level}
			<div class="row">
				<div class="span12">
					<h4>{$level.level->name}</h4>
					<input type="hidden" name="heatSourceOption[{$level.level->id}][rangeid]" value="0" id="heatSourceOption-{$level.level->id}">
					<input type="hidden" name="heatSourceOption[{$level.level->id}][level]" value="{$level.level->id}">
				</div>
			</div>
			<div class="row-fluid">
			{foreach $level.options as $option}
				<div class="span3 heatsource-block">
					<div class="heatsource-head">
						{$option->packNames()}
					</div>
					<div class="quantity">
						<ul>
						{foreach $option->getProductList() as $product}
							<li>
								{$option->getProductQty($product)}&times; {$product->code}{if $option->getProductLink($product)->productUrl}
								&nbsp;<strong><a href="{$option->getProductLink($product)->productUrl|escape}" target="_blank" title="Opens in new tab">More Info</a></strong>{/if}
							</li>
						{/foreach}
						</ul>
					</div>
					<div class="price">
						Add This Heat Source For: &pound;{$option->sumProducts()}
					</div>
					<button class="btn btn-block heatSourceSelect" type="button" data-rangeid="{$option->getHeatSourceRange()->id}" data-level="{$level.level->id}" data-price="{$option->sumProducts()}">Select</button>
				</div>
				{if $option@iteration is div by 4}</div><div class="row-fluid">{/if}
			{foreachelse}
				<div class="span-12">
					<p>No heat sources are currently available.</p>
				</div>
			{/foreach}
			</div>
			<div class="row-fluid">
				<div class="span3">
					<button class="heatSourceClear btn btn-block" type="button" data-level="{$level.level->id}">Clear Selection</button>
				</div>
			</div>
		{/foreach}
		</form>
		<div class="row">
			<div class="span12 int-subtotal" id="project-total-cost">
				Project Total Cost: <span class="price">&pound;<span id="quotedTotal">{$productList->calculateTotal()|number_format:2}</span></span>
			</div>
		</div>
	{else if Security::check()}
		{if $smarty.session.theme!='plumbnation'}
			<h3>Selected Heat Sources</h3>
		{else}
			<h1>Selected Heat Sources</h1>
		{/if}
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Temperature</th>
					<th>Heat Source</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Sub-Total</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<th>Heat Source Sub-total:</th>
					<td>&pound;{$heatSourceTotal|number_format:2}</td>
				</tr>
			</tfoot>
			<tbody>
				{foreach $selectedHeatSources as $level}
					<tr>
						<td rowspan="{count($level.heatSource)}">{$level.level->name|escape}</td>
						{foreach $level.heatSource as $heatSource}
							{if !$heatSource@first}<tr>{/if}
							<td>{$heatSource.prod->name|escape}</td>
							<td>&pound;{$heatSource.prod->price}</td>
							<td>{$heatSource.qty}</td>
							<td>&pound;{($heatSource.prod->price * $heatSource.qty)|number_format:2}</td>
							{if !$heatSource@last}</tr>{/if}
						{/foreach}
					</tr>
				{foreachelse}
					<tr>
						<td colspan="5">Not available / Nothing selected</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
		<div class="row">
			<div class="span12 int-subtotal">
				Project Total Cost: <span class="price">&pound;<span id="quotedTotal">{$productList->calculateTotal()|number_format:2}</span></span>
			</div>
		</div>
	{/if}
	<div class="row" style="margin-top: 20px;">
		<div class="span-12">
			<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
			<i class="icon-question-sign" data-help="home"></i>
			&nbsp;
			<div class="btn-group">
				<a class="btn" href="{url to="ZoneController->index"}">&lt; Back</a>
				<a class="btn" href="{url to="FloorController->index"}" id="configFloorBut">Configure Another Floor</a>
			</div>
			<i class="icon-question-sign" data-help="conf-floor"></i>
			&nbsp;
			{if Security::check()}
				<a class="btn btn-primary{if $disableFinish} disabled{/if}" href="{if $disableFinish}#{else}{url to="QuotationController->index"}{/if}">Finish</a>
				<i class="icon-question-sign" data-help="finish-quote"></i>
			{else}
				<button type="button" id="openEmailModalBut" class="btn btn-primary"{if $disableFinish} disabled{/if}>Email Calculation</button>
				<i class="icon-question-sign" data-help="email-quote"></i>
			{/if}
			{if $showFloorMessage}
				<p class="text-danger">Please finish configuring all floors before you continue.</p>
			{/if}
		</div>
	</div>
	<script type="text/javascript">
		var origPrice = {$productList->calculateTotal()};
		var quotedTotal = $('#quotedTotal');

		$(function() {
			$('.heatSourceSelect').click(function() {
				var btn = $(this);
				$('.heatSourceSelect[data-level='+btn.data('level')+']').text('Select').prop('disabled', false)
				$('#heatSourceOption-'+btn.data('level')).val(btn.data('rangeid'));
				btn.text('Selected').prop('disabled', true);
				$('#heatSourceForm').submit();
			});
			$('#heatSourceForm').submit(function(e) {
				e.preventDefault();
				$.post('{url to="QuotationController->setHeatSources"}', $(this).serialize(), null, 'json')
				.done(function(data) {
					quotedTotal.text((origPrice + calculateHeatSourcePrice()).toFixed(2));
				});
			});

			$('.heatSourceClear').click(function() {
				var btn = $(this);
				$('.heatSourceSelect[data-level='+btn.data('level')+']').text('Select').prop('disabled', false)
				$('#heatSourceOption-'+btn.data('level')).val(0);
				$('#heatSourceForm').submit();
			})

			setupPopovers($('.icon-question-sign'));
		});

		function calculateHeatSourcePrice() {
			var out = 0.0;
			$('.heatSourceSelect:disabled').each(function(i, o) {
				out += parseFloat($(o).data('price'));
			});

			return out;
		}

		function setupPopovers(elements) {
			var title, content, position;

			elements.each(function() {
				switch ($(this).data('help')) {
					case 'home':
						title = 'Home';
						content = 'Please note if you select the home button this will take you back to main screen and your quotation will not be saved';
						position = 'top';
						break;

					case 'conf-floor':
						title = 'Configure another floor';
						content = 'This will take you back to adding another floor';
						position = 'top';
						break;

					case 'finish-quote':
						title = 'Finish';
						content = 'Please select finish to generate your quotation price';
						position = 'top';
						break;

					case 'email-quote':
						title = 'Email quotation';
						content = 'This will generate an email with your unique reference for you to take into your local branch';
						position = 'top';
						break;

					default:
						return;
				}

				$(this).popover({
					title: title,
					content: content,
					placement: position,
					trigger: 'hover'
				});
			});
		}
	</script>
	{if !Security::check()}
		{include file="QuotationController/parts/emailModal.tpl"}
	{/if}
{/block}