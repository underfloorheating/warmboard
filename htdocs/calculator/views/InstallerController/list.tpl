{extends file="main.tpl"}
{block name="title"}Installer Management{if $archive} - Archive{/if}{/block}
{block name="mainContent"}
	{if $smarty.session.theme!='plumbnation'}
		<h3>Installer Management{if $archive} - Archive{/if}</h3>
	{else}
		<h1>Installer Management{if $archive} - Archive{/if}</h1>
		<hr />
	{/if}
	{if $archive}<p {if $smarty.session.theme=='plumbnation'}class="help-block alert"{/if}>If an installer is deleted but has calculations assigned to them, the installer will be archived here.</p>{/if}
	<div class="row">
		<div class="span12">
			<button type="button" class="btn" id="addInstallerBut">Add Installer</button>
		</div>
	</div>
	<br />
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Company</th>
				<th>Telephone</th>
				<th>Email</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $installers as $i}
				<tr id="installer{$i->id}">
					<td>{$i->id}</td>
					<td><a href="{url to="InstallerController->show"}/{$i->id}">{$i->name}</a></td>
					<td>{$i->companyName}</td>
					<td>{$i->telephone}</td>
					<td>{$i->email}</td>
					<td class="nowrap">
						{if !$archive}
							<button type="button" class="btn btn-small editInstallerBut" data-installerid="{$i->id}" data-before-callback="beforeEdit">
								<i class="icon-edit"></i> Edit
							</button>
							<a href="{url to="InstallerController->delete"}?installerId={$i->id}" class="btn btn-small deleteBut" data-name="{$i->name}">
								<i class="icon-trash"></i> Delete
							</a>
						{else}
							<a href="{url to="InstallerController->reinstate"}?installerId={$i->id}" class="btn btn-small">
								<i class="icon-folder-open"></i> Reinstate
							</a>
						{/if}
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="6">No Installers here</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		{if !$archive}
			<a class="btn" href="{url to="InstallerController->archive"}"><i class="icon-folder-close"></i>&nbsp;View Archived</a>
		{else}
			<a class="btn" href="{url to="InstallerController->index"}"><i class="icon-list"></i>&nbsp;View Active</a>
		{/if}
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="InstallerController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="InstallerController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="InstallerController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>

	<div class="modal hide fade" id="deleteModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Really Delete this Installer</h3>
		</div>
		<div class="modal-body">
			<p>
				Are you sure you want to delete <span></span>?<br>
				Once deleted the installer will no longer be on the system.
			</p>
			<p>
				If the installer has any calculations assigned to it, the
				installer will instead be placed in the archive.
			</p>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn" data-dismiss="modal">Cancel</button>
			<button type="button" class="btn btn-danger" id="doDeleteBut">Delete</button>
		</div>
	</div>

	{include file="./parts/installerModals.tpl"}
	<script type="text/javascript">
		<!--
		$(function() {
			$('#addInstallerModal, #editInstallerModal').on('hidden', function() {
				window.location.reload();
			});

			$('.deleteBut').click(function(e) {
				e.preventDefault();
				$('#deleteModal p>span').text($(this).data('name'));
				$('#deleteModal').data('href', $(this).attr('href'));
				$('#deleteModal').modal();
			});

			$('#doDeleteBut').click(function() {
				window.location = $('#deleteModal').data('href');
			});
		});

		function beforeEdit() {
			var data = { installerId: $('#editInstallerModal input[name=installerId]').val() };
			$.getJSON("{url to="InstallerController->fetch"}", data, function(data) {
				if (data.status == "OK") {
					$('#editInstallerModal input[name=name]').val(data.installer.name);
					$('#editInstallerModal input[name=companyName]').val(data.installer.companyName == null ? '' : data.installer.companyName);
					$('#editInstallerModal input[name=email]').val(data.installer.email == null ? '' : data.installer.email);
					$('#editInstallerModal input[name=telephone]').val(data.installer.telephone == null ? '' : data.installer.telephone);
					$('#editInstallerModal textarea[name=address]').text(data.installer.address == null ? '' : data.installer.address);
					$('#editInstallerModal textarea[name=notes]').text(data.installer.notes == null ? '' : data.installer.notes);

				} else {
					$('#editInstallerModal .errors').text(data.status).show('fast');
				}
			});
		}
		-->
	</script>
{/block}