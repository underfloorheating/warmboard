<div class="modal hide fade" id="addInstallerModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Add Installer</h3>
	</div>
	<div class="modal-body">
		<div class="hide errors alert alert-error"></div>
		<form class="form-horizontal">
			<p class="help-block">* = Required</p>
			<div class="control-group">
				<label class="control-label" for="name">Name: *</label>
				<div class="controls">
					<input type="text" name="name" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="companyName">Company:</label>
				<div class="controls">
					<input type="text" name="companyName" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email">Email:</label>
				<div class="controls">
					<input type="email" name="email" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="telephone">Telephone:</label>
				<div class="controls">
					<input type="text" name="telephone" maxlength="20">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="address">Address:</label>
				<div class="controls">
					<textarea name="address" rows="5"></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="notes">Notes:</label>
				<div class="controls">
					<textarea name="notes" rows="5"></textarea>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn">Cancel</button>
		<button type="button" id="doAddInstaller" class="btn btn-primary">Add</button>
	</div>
</div>
<div class="modal hide fade" id="editInstallerModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Edit Installer</h3>
	</div>
	<div class="modal-body">
		<div class="hide errors alert alert-error"></div>
		<form class="form-horizontal">
			<input type="hidden" name="installerId">
			<p class="help-block">* = Required</p>
			<div class="control-group">
				<label class="control-label" for="name">Name: *</label>
				<div class="controls">
					<input type="text" name="name" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="companyName">Company:</label>
				<div class="controls">
					<input type="text" name="companyName" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="email">Email:</label>
				<div class="controls">
					<input type="email" name="email" maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="telephone">Telephone:</label>
				<div class="controls">
					<input type="text" name="telephone" maxlength="20">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="address">Address:</label>
				<div class="controls">
					<textarea name="address" rows="5"></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="notes">Notes:</label>
				<div class="controls">
					<textarea name="notes" rows="5"></textarea>
				</div>
			</div>
		</form>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn">Cancel</button>
		<button type="button" id="doEditInstaller" class="btn btn-primary">Edit</button>
	</div>
</div>
<script type="text/javascript">
	<!--
	$(function() {
		$('#addInstallerBut').click(function() {
			$('#addInstallerModal input').val("");
			$('#addInstallerModal textarea').text("");
			$('#addInstallerModal .errors').hide();
			$('#addInstallerModal').modal();
		});
		$('button.editInstallerBut').click(function() {
			var callback = window[$(this).data('before-callback')];

			$('#editInstallerModal .errors').hide();
			$('#editInstallerModal input[name=installerId]').val($(this).data('installerid'));
			if (callback != undefined)
				callback();
			$('#editInstallerModal').modal();
		});

		$('#addInstallerModal #doAddInstaller').click(function() {
			$('#addInstallerModal form').submit();
		});
		$('#addInstallerModal form').submit(function(e) {
			e.preventDefault();
			var data = $(this).serialize();
			var callback = window[$('#addInstallerBut').data('callback')];
			$.getJSON("{url to="InstallerController->create"}", data, function(data) {
				if (data.status == "OK") {
					$('#addInstallerModal').modal('hide');
					if (callback != undefined)
						callback(data);
				} else {
					$('#addInstallerModal .errors').text(data.status).show('fast');
				}
			});
		});

		$('#editInstallerModal #doEditInstaller').click(function() {
			$('#editInstallerModal form').submit();
		});
		$('#editInstallerModal form').submit(function(e) {
			e.preventDefault();
			var data = $(this).serialize();
			var callback = window[$('.editInstallerBut[data-id=' + $('input[name=installerId]', this).val() + ']').data('after-callback')];
			$.getJSON("{url to="InstallerController->edit"}", data, function(data) {
				if (data.status == "OK") {
					$('#editInstallerModal').modal('hide');
					if (callback != undefined)
						callback(data);
				} else {
					$('#addInstallerModal .errors').text(data.status).show('fast');
				}
			});
		});


	});
	-->
</script>
