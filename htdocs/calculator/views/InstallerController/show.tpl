{extends file="main.tpl"}
{block name="title"}Installer Details{/block}
{block name="mainContent"}
	<h3>Installer Details</h3>
	{if !isset($installer)}
		<div class="alert alert-info">
			<p>Unable to find installer</p>
		</div>
	{else}
		<table class="table">
			<tbody>
				<tr>
					<th>ID:</th>
					<td>{$installer->id}</td>
					<th>Email:</th>
					<td>{$installer->email|escape}</td>
				</tr>
				<tr>
					<th>Name:</th>
					<td>{$installer->name|escape}</td>
					<th>Telephone:</th>
					<td>{$installer->telephone|escape}</td>
				</tr>
				<tr>
					<th>Company Name:</th>
					<td>{$installer->companyName|escape}</td>
					{if $smarty.session.loggedUser->isAdmin}
						<th>Registered With:</th>
						<td><a href="{url to="UserController->show"}/{$merchant->id}">{$merchant->userName|escape}</a></td>
					{else}
						<td></td>
						<td></td>
					{/if}
				</tr>
				<tr>
					<th>Address:</th>
					<td>{$installer->address|escape|nl2br}</td>
					<th>Notes:</th>
					<td>{$installer->notes|escape|nl2br}</td>
				</tr>
			</tbody>
		</table>
		<h4>Assigned Quotations</h4>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Code</th>
					<th>Project Reference</th>
					<th>Assignment Date/Time</th>
					<th>List Price</th>
					<th>Installer Discount</th>
					<th>Agreed Price</th>
					{if $smarty.session.loggedUser->isAdmin}<th>Followed Up</th>{/if}
				</tr>
			</thead>
			<tbody>
				{foreach $calculations as $calc}
					<tr>
						<td>
							<a href="{url to="QuotationController->recallCalculation"}?code={$calc->recallCode}">{$calc->recallCode}</a>
						</td>
						<td>{$calc->projectRef}</td>
						<td>{$calc->tsLocked}</td>
						<td>&pound;{$calc->_productList->calculateTotal()|number_format:2}</td>
						<td>{$calc->discount * 100}%</td>
						<td>&pound;{($calc->_productList->calculateTotal() - ($calc->_productList->calculateTotal() * $calc->discount))|number_format:2}</td>
						{if $smarty.session.loggedUser->isAdmin}
							<td>
								<input type="checkbox" class="followedup" data-id="{$calc->id}"{if $calc->followedUp} checked{/if}>{if $calc->followedUp}{/if}
								{if $calc->followedUp}
									<span title="{$calc->followedUpDate}">({$usernameCache[$calc->followedUp]|escape})</span>
								{/if}
							</td>
						{/if}
					</tr>
				{foreachelse}
					<tr>
						<td colspan="{if $smarty.session.loggedUser->isAdmin}6{else}5{/if}">No quotations assigned to this installer</td>
					</tr>
				{/foreach}
			</tbody>
		</table>
	{/if}
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<div class="btn-group">
			{if $smarty.session.loggedUser->isAdmin}
				<a class="btn" href="{url to="UserController->show"}/{$merchant->id}">&lt; Back</a>
			{else}
				<a class="btn" href="{url to="InstallerController->index"}">&lt; Back</a>
			{/if}
			{if isset($installer) && !$installer->archived}
				<button type="button" class="btn editInstallerBut" data-id="{$installer->id}" data-before-callback="beforeEdit" data-after-callback="afterEdit">Edit Installer</button>
			{/if}
		</div>
	</div>
	{if isset($installer)}
		<div class="pagination pagination-right">
			<ul>
				{if $curPage <= 1}
					<li class="disabled"><span>&laquo;</span></li>
				{else}
					<li><a href="{url to="InstallerController->show"}\{$installer->id}?page={$curPage - 1}">&laquo;</a></li>
				{/if}
				{for $i=1 to $maxPages}
					{if $curPage == $i}
						<li class="active"><span>{$i}</span></li>
					{else}
						<li><a href="{url to="InstallerController->show"}\{$installer->id}?page={$i}">{$i}</a></li>
					{/if}
				{/for}
				{if $curPage >= $maxPages}
					<li class="disabled"><span>&raquo;</span></li>
				{else}
					<li><a href="{url to="InstallerController->show"}\{$installer->id}?page={$curPage + 1}">&raquo;</a></li>
				{/if}
			</ul>
		</div>
		{include file="./parts/installerModals.tpl"}

		<script type="text/javascript">
			function beforeEdit() {
				$('#editInstallerModal input[name=installerId]').val({$installer->id})
				$('#editInstallerModal input[name=name]').val("{$installer->name|escape:javascript}");
				$('#editInstallerModal input[name=companyName]').val("{$installer->companyName|escape:javascript}");
				$('#editInstallerModal input[name=email]').val("{$installer->email|escape:javascript}");
				$('#editInstallerModal input[name=telephone]').val("{$installer->telephone|escape:javascript}");
				$('#editInstallerModal textarea[name=address]').text("{$installer->address|escape:javascript}");
				$('#editInstallerModal textarea[name=notes]').text("{$installer->notes|escape:javascript}");
			}

			function afterEdit() {
				window.location.reload();
			}

			$(function() {
				$('.followedup').change(function() {
					var btn = $(this);
					
					$('span', btn.parent()).remove();
					btn.after($('<small> Working...</small>'));

					$.post('{url to="InstallerController->setFollowedUp"}',{ followedUp: btn.prop('checked'), calculationId: btn.data('id')}, null, 'json')
					.done(function(data) {
						$('small', btn.parent()).remove();
						if (data.status != 'OK')
							alert(data.status);

						else if (data.userName)
							btn.after($('<span title="'+data.followedUpDate+'"> ('+data.userName+')</span>'));

						else
							$('span', btn.parent()).remove();
					})
					.fail(function(obj, errorMessage, code) {
						$('small', btn.parent()).remove();
						alert(obj.responseText);
					});
				});
			});
		</script>
	{/if}
{/block}