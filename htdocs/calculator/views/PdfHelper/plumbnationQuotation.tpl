<html>
	<head>
		<title>Plumbnation Quotation Controller</title>
		<link href="public/css/pdf/plumbnation.css" type="text/css" rel="stylesheet">
	</head>
		<body class="plumbnation">
		<div class="frontPage">
		</div>
	    <div id="footer">
	    	<img src="public/images/pdf/p1-footer.jpg">
	    </div>
		<div class="page">
			<div class="pageheader refHead no-break">
				<table class="refs">
					<tr>
						<td><h2 class="ref-title inline-title">QUOTATION REF</h2></td>
						<td><span class="ref">{$data->recallCode}</span></td>
					</tr>
					<tr>
						<td><h2 class="ref-title inline-title">PROJECT REF</h2></td>
						<td><span>{$data->projectRef|escape}</span></td>
				</table>
			</div>
			<p class="intro">
				{$merchant->contactName|escape}<br>
				{$merchant->companyName|escape}<br>
				{$merchant->address|escape|nl2br}<br>
				Tel: {$merchant->telephone|escape}
			</p>
			<p>
				{$installer->name|escape}<br>
				{$installer->address|escape|nl2br}<br>
				Tel: {$installer->telephone|escape}<br>
				{$installer->email|escape}
			</p>
			<p>
				<strong>Re: Underfloor Heating System</strong>
			</p>
			<p class="blurb">
				PlumbNation Ltd thank you for your valued enquiry and have
				pleasure in submitting our quotation for your underfloor heating
				system in accordance with the enclosed design summary
			</p>
			<table>
				<tr>
					<th>
						TOTAL SYSTEM PRICE &pound;{$totalPrice|number_format:2}
						+VAT,<br> THIS IS A LIST PRICE QUOTATION AND SUBJECT TO
						DISCOUNT
					</th>
				</tr>
			</table>
			<p class="quoteTerms">
				The quotation excludes VAT and is valid for 30 days. The initial
				price is based upon the details provided to PlumbNation Ltd,
				and is subject to a more detailed design. Upon receipt of an
				official order we will provide comprehensive underfloor heating
				floor plan layouts for your approval.
			</p>
			<table>
				<tr>
					<th>
						ALL STOCK ITEMS AVAILABLE FOR NEXT DAY DELIVERY ON ORDERS
						PLACED BEFORE 2PM
					</th>
				</tr>
			</table>
			<p>
				We trust we have interpreted your requirements correctly and hope
				to hear from you soon.
			</p>
			<p>
				Quote Provided by
			</p>
			<p>
				<strong>PlumbNation Limited</strong><br />
				Unit 5 Woodway Court<br />
				Thursby Road<br />
				Bromborough<br />
				Wirral<br />
				CH62 3PR<br />
				<strong>Tel.</strong> (+44) 0151 737 5301<br />
				<strong>Fax.</strong> (+44) 0151 334 2035
			</p>
			<p><strong>Company No:</strong> 6216887<br /> 
			<strong>VAT No:</strong> 910394640</p>
		</div>

		{foreach $systemTypes as $systemType}
			<div class="pagebreak"></div>
			<div class="sysImg">
				{if !empty($systemType->image)}
					<img src="public/{ImageHelper::retrieveImagePath(ImageHelper::SYSTEM_DIR, $systemType->image)}">
				{/if}
			</div>
		{/foreach}

		<div class="page">
			<h2 class="pageheader">FLOOR CONSTRUCTION</h2>
			{foreach $data->_floors as $floor}
				<h3>{$floor->name|upper}</h3>
				{foreach $floor->_manifolds as $manifold}
					<h4>MANIFOLD {$manifold->ident} ({if $manifold->controlType == Manifold::CONTROLTYPE_WIRED}WIRED CONTROL{else}WIRELESS CONTROL{/if})</h4>
					<table>
						<thead>
							<tr>
								<th>Room Name</th>
								<th>Contruction Type</th>
								<th>UFH System</th>
								<th>Heated area (m&sup2;)</th>
								<th>Pipe Centres (cc)</th>
							</tr>
						</thead>
						<tbody>
						{foreach $manifold->_zones as $zone}
							{foreach $zone->_rooms as $room}
								<tr>
									<td>{$room->name}</td>
									<td>{ConstructionType::fetchById($manifold->getSystem()->constructionTypeId)->name}</td>
									<td>{$manifold->getSystem()->name}</td>
									<td>{$room->floorArea}</td>
									<td>{if isset($manifold->tempLevelId)}
											{$manifold->getSelectedTemperatureLevel()->pipeCentres * 1000}
										{else}
											{$manifold->getSystem()->pipeCentres * 1000}
										{/if}
									</td>
								</tr>
							{/foreach}
						{/foreach}
						</tbody>
					</table>
				{/foreach}
			{/foreach}
		</div>

		<div class="page">
			<h2 class="pageheader">MANIFOLD DETAILS</h2>
			{foreach $data->_floors as $floor}
				<h3>MANIFOLD DETAILS: {$floor->name|upper}</h3>
				<p><small>Each floor is split up into thermostat zones, these zones can cover one or more rooms</small></p>
				{foreach $floor->_manifolds as $manifold}
					<h4>MANIFOLD {$manifold->ident} ({if $manifold->controlType == Manifold::CONTROLTYPE_WIRED}WIRED CONTROL{else}WIRELESS CONTROL{/if})</h4>
					{foreach $manifold->_zones as $zone}
						<h5>ZONE {$zone->ident}</h5>
						<table>
							<thead>
								<tr>
									<th>Room Name</th>
									<th>Dist from Manifold (m)</th>
									<th>Floor Area (m&sup2;)</th>
									<th>No. of Circuits</th>
									<th>Actual Circuit Length (m)</th>
									<th>Selected Coil Length (m)</th>
								</tr>
							</thead>
							<tbody>
							{foreach $zone->_rooms as $room}
								<tr>
									<td>{$room->name}</td>
									<td>{$room->distFromManifold}</td>
									<td>{$room->floorArea}</td>
									<td>{$room->_numCircuits}</td>
									<td>{$room->_totalPipeLength|number_format:2}</td>
									<td>{$room->_pipePerCircuit|number_format:2}</td>
								</tr>
							{/foreach}
							</tbody>
						</table>
					{/foreach}
				{/foreach}
			{/foreach}
		</div>

		<div class="page">
			<h2 class="pageheader">MATERIALS LIST</h2>
			{foreach $data->_productList->products as $product}
			<div class="product">
				<table>
					<tbody>
						<tr>
							<th>{$product->code}</th>
							<td>QTY: {$data->_productList->qty[$product->code]|number_format}</td>
						</tr>
					</tbody>
				</table>
				<table>
					<tr>
						<td class="image bg-white">
							{if !empty($product->productImage)}
								<img src="public/{ImageHelper::retrieveImagePath(ImageHelper::PRODUCT_DIR, $product->productImage)}">
							{/if}
						</td>
						<td class="detail bg-white">
							<span class="name">{$product->name}</span><br>
							{*<span class="description">{$product->description|regex_replace:"@<p(.*)>(.*)</p>@i":"<br>\\2<br>"|regex_replace:"@<table>.*</table>@":""}</span>*}
						</td>
					</tr>
				</table>
			</div>
			{/foreach}
			<div class="productTotals">
				<table>
					<tr>
						<th class="grey">LIST PRICE</th>
						<td class="red">&pound;{$totalPrice|number_format:2}+VAT</td>
					</tr>
					<tr>
						<th class="grey">AGREED INSTALLER DEAL PRICE</th>
						<td class="red">&pound;{($totalPrice - ($totalPrice * $data->discount))|number_format:2}+VAT</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="page">
			<h2 class="pageheader">CONTACT DETAILS</h2>
			<p class="blurb move-down">
				PlumbNation Ltd have a dedicated underfloor heating team to
				help with all your quotation needs, so should you have any queries,
				please don&apos;t hesitate to call or email us on the details below.
			</p>
			<img src="public/images/pdf/plumbnation-logo.jpg">
			<address>
				<strong>PlumbNation Limited</strong><br />
				Unit 5 Woodway Court<br />
				Thursby Road<br />
				Bromborough<br />
				Wirral<br />
				CH62 3PR
			</address>
			<p><strong>Tel.</strong> (+44) 0151 737 5301</p>
			<p><strong>Fax.</strong> (+44) 0151 334 2035</p>

			<p><strong>Company No:</strong> 6216887<br /> 
			<strong>VAT No:</strong> 910394640</p>

			<h2 class="pageheader">TERMS AND CONDITIONS</h2>
			<p>
				The PlumbNation Ltd online quick quotation tool, has been
				developed to make the specification of the range of PlumbNation under
				floor heating systems an easier and more straight forward task for
				the Installer. It is a necessity to confirm that you have read, and
				more importantly, agree with the following terms and conditions for
				the use for this quotation tool
			</p>
			<p>
				1. Intended use of the Quick Quote tool. The quick quotation tool
				provides an estimation for a range of under floor heating systems
				which, fits most applications. This initial quotation is suitable
				for the installer to present a quotation to the client. However,
				the tool is reliant upon the accuracy of the information provided,
				and irrespective of the inherent safety factors applied to the
				calculations, it must be clear that, the user must, verify their
				assumptions are a true representation of the project. PlumbNation Ltd
				will not be liable for the incorrect use of this online
				quotation tool, and cannot be held responsible for such problems
				that, arise from an incorrect use of the quotation tool. When using
				the quick quote tool the installer should note that:
			</p>
			<p>
				I. The online quick calculation tool, calculates the system based
				upon typical system parameters, which, vary based upon each
				individual floor construction. The user must verify that, the
				chosen system meets their project requirements, and that, the
				chosen system type can achieve the relevant heat outputs specific
				to a heat loss calculation based upon BS EN 12831. The basic
				parameters that need to be ascertained prior to using the
				calculator, can be viewed at the following link
				<a href="http://www.plumbnation.co.uk/terms-and-conditions.php">
					http://www.plumbnation.co.uk/terms-and-conditions.php
				</a>
			</p>
			<p>
				II. The online quick calculator cannot be deemed as a
				comprehensive design tool, and in the event of uncertainty, you
				must contact the PlumbNation Ltd design team for further
				guidance.
			</p>
			<p>
				III. In order to design an accurate under floor heating system, an
				additional room by room detailed heat loss calculation must be
				carried out in accordance with EN12831. This will enable you to
				fully understand specific project limitations and risk factors.
			</p>
			<p>
				IV. The software is not able to take into account any site specific
				requirements. Therefore the installer should conduct a site survey
				and consult the necessary architectural floor plans, to fully
				understand the project requirements.
			</p>
			<p>
				V. To assist the Installer a number of parameters have been
				pre-calculated as part of the calculation which, cannot be changed.
				It is the responsibility of the installer to verify that the
				outputs supplied by the calculator, are correct for the property in
				question.
			</p>
			<p>
				VI. The outputs and recommendations produced by this quotation
				tool, are based on the information provided by the user. It is the
				users responsibility to ensure all data inputs are accurate.
			</p>
			<p>
				VII. All product selections are intended to provide a quotation of
				the likely system components needed to meet the specific project
				requirement, in line with the information provided by the user.
				Whilst PlumbNation Ltd endeavour to ensure that our
				recommendations and designs accurately reflect our customer&apos;s
				requirements, we are unable to accept responsibility for any loss
				incurred as a result of them failing to do so. It is the
				responsibility of the user to ensure the results are suitable for
				the property in question. In the event of any uncertainty and
				confusion, the user must contact PlumbNation Ltd for further
				guidance and advice.
			</p>
			<p>
				VIII. When the user of the quick online quotation tool intends the
				proposed under floor heating system to be used with a heat pump,
				PlumbNation Ltd recommend that, the user of the tool contacts
				the PlumbNation Ltd technical department for further guidance
				and advice. Although the tool can be used to produce a quotation
				that is suitable for the use with a heat pump, PlumbNation Ltd
				are unable to accept responsibility for any loss incurred
				as a result of them failing to seek advice.
			</p>
			<p>
				IX. The quotation states which products are available from PlumbNation
				Ltd, the installer should take into account that
				additional materials that may need to be purchased from either
				PlumbNation, or others, to be required to complete the installation.
			</p>
			<p>
				X. Quotations contain list prices, the user should contact their
				local stockist to get the actual price for the equipment.
				Quotations are valid for 3 months from the date the report is
				printed to PDF. The user should check the stock availability with
				PlumbNation Ltd prior to placing an order, as from time to
				time PlumbNation Ltd may experience a delay from suppliers.
			</p>
			<p>
				XI. Accessing the Website Registration is only open for
				pre-registered merchants, and it is a requirement of the merchant
				to contact PlumbNation Ltd to enable an account to be opened.
				The quick calculation tool is open to installers, specifiers,
				architects and builders, however, PlumbNation Ltd recommend
				that all of the inputs within the calculator have been verified to
				be a true and accurate reflection of the project.
			</p>
			<p>
				XII. The website is not intended for use by non-professionals who
				do not have a good working knowledge of PlumbNation Ltd
				products, or those not operating in PlumbNation Ltd
				interests. The website may only be used by a person in the way it
				was intended by PlumbNation Ltd. PlumbNation Ltd
				reserves the right to terminate or suspend an account without prior
				notice at our discretion.
			</p>
			<p>
				XII. The user of the quick calc is prompted to enter an email
				address upon completion of a quick calculation. An email is then
				sent automatically by PlumbNation Ltd to the email address
				with a unique code. This unique code, once supplied to a registered
				merchant, enables the registered merchant to access detailed design
				and product information. Following this, the registered merchant at
				their discretion, will negotiate a suitable discount.
			</p>
			<p>
				To see an example of how to select a system see
				<a href="http://www.plumbnation.co.uk/UnderfloorHeating/example.pdf">
					http://www.plumbnation.co.uk/UnderfloorHeating/example.pdf
				</a>
			</p>
		</div>
	</body>
</html>