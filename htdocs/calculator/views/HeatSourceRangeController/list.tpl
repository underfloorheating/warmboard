{extends file="main.tpl"}
{block name="title"}Heat Source Range Management{/block}
{block name="mainContent"}
	<h3>Heat Source Range Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Minimum (kW)</th>
				<th>Maximum (kW)</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $ranges as $range}
				<tr>
					<td>{$range->id}</td>
					<td>{$range->rangeMin}</td>
					<td>{$range->rangeMax}</td>
					<td>
						<button class="btn btn-small btn-danger" data-action="delete-range"
							data-id="{$range->id}"
							data-name="{$range->rangeMin} - {$range->rangeMax}">
							<i class="icon-trash icon-white"></i> Delete
						</button>
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a href="{url to="ProductController->index"}" class="btn">&lt; Back to Product List</a>
		&nbsp;
		<button type="button" class="btn" data-action="add-range">Add Range</button>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="HeatSourceRangeController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="HeatSourceRangeController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="HeatSourceRangeController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="creatRangeModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Create Heat Source Range</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error"></div>
				<div class="control-group">
					<label class="control-label" for="create_min">Minimum (kW)</label>
					<div class="controls">
						<input type="text" name="rangeMin" id="create_min">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="create_max">Maximum (kW)</label>
					<div class="controls">
						<input type="text" name="rangeMax" id="create_max">
					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-primary" id="add">Add</a>
		</div>
	</div>
	<div class="modal hide fade" id="deleteRangeModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete Heat Source Range</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error"></div>
				<p>
					Note: You can only delete a range when there are no
					products assigned to that range.
				</p>
				<p>Are you sure you want to delete range <strong></strong></p>
				<input type="hidden" name="rangeId">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('button').click(function() {
				procButtonClick(this);
			});

			$('#creatRangeModal #add').click(function(e) {
				e.preventDefault();
				$('#creatRangeModal form').submit();
			});
			$('#creatRangeModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="HeatSourceRangeController->create"}', $(this).serialize(),
					function(data) {
						if (data.status == 'OK') {
							$('#creatRangeModal').modal('hide');
							window.location.reload();

						} else {
							$('#creatRangeModal .errors').text(data.status).show('fast');
						}
					});
			});

			$('#deleteRangeModal #delete').click(function(e) {
				e.preventDefault();
				$('#deleteRangeModal form').submit();
			});
			$('#deleteRangeModal form').submit(function(e) {
				e.preventDefault();
				$.getJSON('{url to="HeatSourceRangeController->delete"}', $(this).serialize(),
					function(data) {
						if (data.status == 'OK') {
							$('#deleteRangeModal').modal('hide');
							window.location.reload();

						} else {
							$('#creatRangeModal .errors').text(data.status).show('fast');
						}
					});
			});
		});
		
		function procButtonClick(button) {
			switch ($(button).data('action')) {
				case 'add-range':
					$('#creatRangeModal').modal();
					break;

				case 'delete-range':
					$('#deleteRangeModal .errors').hide();
					$('#deleteRangeModal [name=rangeId]').val($(button).data('id'));
					$('#deleteRangeModal strong').text($(button).data('name'));
					$('#deleteRangeModal').modal();
					break;
			}
		}
	</script>
{/block}