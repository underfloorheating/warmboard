{extends file="main.tpl"}
{block name="title"}Product Management{/block}
{block name="mainContent"}
	<h3>Product Management</h3>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Code</th>
				<th>Name</th>
				<th>Price</th>
				<th>Item Type</th>
				<th>Available</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			{foreach $products as $product}
				<tr>
					<td>{$product->code|escape}</td>
					<td>{$product->name|escape}</td>
					<td><span class="pull-right">&pound;{$product->price|number_format:2}</span></td>
					<td>{$types[$product->typeId]->typeName}</td>
					<td>{if $product->available}Yes{else}No{/if}</td>
					<td class="nowrap">
						<a class="btn btn-small" href="{url to="ProductController->edit"}/{$product->id}?page={$curPage}"><i class="icon-edit"></i> Edit</a>
						<button type="button" class="btn btn-small deleteBut" data-id="{$product->id}" data-code="{$product->code|escape}"><i class="icon-trash"></i> Delete</button>
					</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="6">No Products available</td>
				</tr>
			{/foreach}
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="ProductController->create"}?page={$curPage}">Create New Product</a>
		&nbsp;
		<div class="btn-group">
			<a class="btn" href="{url to="SystemController->view"}">Edit Systems</a>
			<a class="btn" href="{url to="PipeTypeController->index"}">Edit Pipe Types</a>
			<a class="btn" href="{url to="SystemTypeController->index"}">Edit System Types</a>
		</div>
		<div class="btn-group">
			<a class="btn" href="{url to="TemperatureLevelController->index"}">Edit Temperature Levels</a>
			<a class="btn" href="{url to="PropertyTypeController->index"}">Edit Property Types</a>
			<a class="btn" href="{url to="HeatSourceRangeController->index"}">Edit Heat Source Ranges</a>
		</div>
	</div>
	<div class="pagination pagination-right">
		<ul>
			{if $curPage <= 1}
				<li class="disabled"><span>&laquo;</span></li>
			{else}
				<li><a href="{url to="ProductController->index"}?page={$curPage - 1}">&laquo;</a></li>
			{/if}
			{for $i=1 to $maxPages}
				{if $curPage == $i}
					<li class="active"><span>{$i}</span></li>
				{else}
					<li><a href="{url to="ProductController->index"}?page={$i}">{$i}</a></li>
				{/if}
			{/for}
			{if $curPage >= $maxPages}
				<li class="disabled"><span>&raquo;</span></li>
			{else}
				<li><a href="{url to="ProductController->index"}?page={$curPage + 1}">&raquo;</a></li>
			{/if}
		</ul>
	</div>
	<div class="modal hide fade" id="deleteProductModal">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>Delete User</h3>
		</div>
		<div class="modal-body">
			<form class="form-horizontal">
				<div class="hide errors alert alert-error">
				</div>
				<p>Are you sure you want to delete the product <strong></strong>?</p>
				<input type="hidden" name="productId">
			</form>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
			<a href="#" class="btn btn-danger" id="delete">Delete</a>
		</div>
	</div>
	<script type="text/javascript">
		<!--
		$(function() {
			$('table button.deleteBut').click(function() {
				$('#deleteProductModal input[name=productId]').val($(this).data('id'));
				$('#deleteProductModal strong').text($(this).data('code'));
				$('#deleteProductModal').modal();
			});

			$('#deleteProductModal #delete').click(function(e) {
				e.preventDefault();
				
				$.getJSON('{url to="ProductController->delete"}', { productId: $('#deleteProductModal input[name=productId]').val() }, function(data) {
					if (data.status == "OK") {
						$('#deleteProductModal').modal('hide');
						window.location.reload();

					} else {
						$('#deleteProductModal .errors').text(data.status).show('fast');
					}
				});
			});
		});
		-->
	</script>
{/block}