{extends file="main.tpl"}
{block name="title"}{if $editMode}Edit {else}Create {/if}Product{/block}
{block name="mainContent"}
	<h3>{if $editMode}Edit {else}Create {/if}Product</h3>
	<form class="form-horizontal" method="post" enctype="multipart/form-data" action="{if $editMode}{url to="ProductController->update"}{else}{url to="ProductController->addProduct"}{/if}">
		<fieldset id="general">
			<legend>General Product Information</legend>
			{if $editMode}
				<input type="hidden" name="productId" value="{$product->id}">
			{/if}
			<input type="hidden" name="page" value="{$curPage}">
			{if isset($backPath)}
				<input type="hidden" name="backPath" value="{$backPath}">
			{/if}
			<div class="control-group">
				<label class="control-label" for="code">Product Code:</label>
				<div class="controls">
					<input type="text" name="code"{if $editMode} value="{$product->code|escape}"{/if} maxlength="20">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="name">Product Name:</label>
				<div class="controls">
					<input type="text" name="name"{if $editMode} value="{$product->name|escape}"{/if} maxlength="100">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="price">Price:</label>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on">&pound;</span>
						<input type="text" name="price"{if $editMode} value="{$product->price}"{/if} placeholder="0.00">
					</div>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Availability:</label>
				<div class="controls">
					<label><input type="checkbox" name="available"{if $editMode}{if $product->available} checked{/if}{else} checked{/if}> Available</label>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="productImage">Product Image:</label>
				<div class="controls">
					{if $editMode && !empty($product->productImage)}
						<div class="thumbnail" style="width: 390px;">
							<img src="{url public=ImageHelper::retrieveImagePath(ImageHelper::PRODUCT_DIR, $product->productImage)}" alt="Current Product Image">
							<label class="checkbox">
								<input type="checkbox" name="deleteProductImage"> Remove this image
							</label>
						</div>
					{/if}
					<input name="productImage" type="file">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="description">Description:</label>
				<div class="controls">
					<textarea name="description">{if $editMode}{$product->description}{/if}</textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="typeId">Product Type:</label>
				<div class="controls">
					<select name="typeId"{if $editMode} disabled{/if}>
						{foreach $types as $type}
							<option value="{$type->id}" {if $editMode && $product->typeId == $type->id}selected{/if}>{$type->typeName|escape}</option>
						{/foreach}
					</select>
				</div>
			</div>
		</fieldset>
		<fieldset id="productSpec"{if !$editMode || !isset($specData)} class="hide"{/if}>
			<legend>Product Specific</legend>
			<div id="productSpec-manifold"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_MANIFOLD} class="hide"{/if}>
				<div class="control-group">
					<label for="manifold[maxCircuits]" class="control-label">Maximum Circuits:</label>
					<div class="controls">
						<input type="text" name="manifold[maxCircuits]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_MANIFOLD} value="{$specData->maxCircuits}"{/if}>
					</div>
				</div>
			</div>
			<div id="productSpec-fitting"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_FITTING} class="hide"{/if}>
				<div class="control-group">
					<label for="fitting[isSingleCircuit]" class="control-label">Single Circuit:</label>
					<div class="controls">
						<label class="checkbox">
							<input type="checkbox" name="fitting[isSingleCircuit]" {if $editMode && $product->typeId == ProductType::PRODUCTTYPE_FITTING && $specData->isSingleCircuit}checked{/if}> Fitting is to be used when only one circuit is in use
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="fitting[pipeWidth]" class="control-label">Pipe Diameter:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="fitting[pipeWidth]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_FITTING} value="{$specData->pipeWidth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
			</div>
			<div id="productSpec-thermostat"{if !$editMode || ($product->typeId != ProductType::PRODUCTTYPE_THERMOSTAT && $product->typeId != ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM)} class="hide"{/if}>
				<div class="control-group">
					<label for="thermostat[isWireless]" class="control-label">Wireless:</label>
					<div class="controls">
						<label class="checkbox"><input type="checkbox" name="thermostat[isWireless]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->isWireless} checked{/if}> Wireless
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="thermostat[useOnSingleCircuit]" class="control-label">Use on Manifold:</label>
					<div class="controls">
						<label class="radio"><input type="radio" name="thermostat[useOnSingleCircuit]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->useOnSingleCircuit == ThermostatData::USE_SINGLE} checked{/if} value="{ThermostatData::USE_SINGLE}"> Single Circuit Only
						</label>
						<label class="radio"><input type="radio" name="thermostat[useOnSingleCircuit]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->useOnSingleCircuit == ThermostatData::USE_MULTI} checked{/if} value="{ThermostatData::USE_MULTI}"> Multiple Circuit Only
						</label>
						<label class="radio"><input type="radio" name="thermostat[useOnSingleCircuit]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->useOnSingleCircuit == ThermostatData::USE_BOTH} checked{/if} value="{ThermostatData::USE_BOTH}"> Both
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="thermostat[useOnSingleRoom]" class="control-label">Use on Floor:</label>
					<div class="controls">
						<label class="radio"><input type="radio" name="thermostat[useOnSingleRoom]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->useOnSingleRoom == ThermostatData::USE_SINGLE} checked{/if} value="{ThermostatData::USE_SINGLE}"> Single Room Only</label>
						<label class="radio"><input type="radio" name="thermostat[useOnSingleRoom]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->useOnSingleRoom == ThermostatData::USE_MULTI} checked{/if} value="{ThermostatData::USE_MULTI}"> Multiple Room Only</label>
						<label class="radio"><input type="radio" name="thermostat[useOnSingleRoom]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_THERMOSTAT || $product->typeId == ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM) && $specData->useOnSingleRoom == ThermostatData::USE_BOTH} checked{/if} value="{ThermostatData::USE_BOTH}"> Both</label>
					</div>
				</div>
			</div>
			<div id="productSpec-actuator"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_ACTUATOR} class="hide"{/if}>
				<div class="control-group">
					<label for="actuator[useOnSingleCircuit]" class="control-label">Use on Manifold:</label>
					<div class="controls">
						<label class="radio"><input type="radio" name="actuator[useOnSingleCircuit]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_ACTUATOR && $specData->useOnSingleCircuit == ActuatorData::USE_SINGLE} checked{/if} value="{ActuatorData::USE_SINGLE}"> Single Circuit Only</label>
						<label class="radio"><input type="radio" name="actuator[useOnSingleCircuit]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_ACTUATOR && $specData->useOnSingleCircuit == ActuatorData::USE_MULTI} checked{/if} value="{ActuatorData::USE_MULTI}"> Multiple Circuit Only</label>
						<label class="radio"><input type="radio" name="actuator[useOnSingleCircuit]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_ACTUATOR && $specData->useOnSingleCircuit == ActuatorData::USE_BOTH} checked{/if} value="{ActuatorData::USE_BOTH}"> Both</label>
					</div>
				</div>
				<div class="control-group">
					<label for="actuator[useOnSingleThermostat]" class="control-label">Use on Thermostat:</label>
					<div class="controls">
						<label class="radio"><input type="radio" name="actuator[useOnSingleThermostat]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_ACTUATOR && $specData->useOnSingleThermostat == ActuatorData::USE_SINGLE} checked{/if} value="{ActuatorData::USE_SINGLE}"> Single Thermostat Only</label>
						<label class="radio"><input type="radio" name="actuator[useOnSingleThermostat]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_ACTUATOR && $specData->useOnSingleThermostat == ActuatorData::USE_MULTI} checked{/if} value="{ActuatorData::USE_MULTI}"> Multiple Thermostat Only</label>
						<label class="radio"><input type="radio" name="actuator[useOnSingleThermostat]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_ACTUATOR && $specData->useOnSingleThermostat == ActuatorData::USE_BOTH} checked{/if} value="{ActuatorData::USE_BOTH}"> Both</label>
					</div>
				</div>
			</div>
			<div id="productSpec-heatSource"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_HEAT_SOURCE} class="hide"{/if}>
				<div class="control-group">
					<label for="heatSource[productUrl]" class="control-label">More Info URL:</label>
					<div class="controls">
						<input type="text" name="heatSource[productUrl]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE} value="{$specData->productUrl|escape}"{/if} placeholder="http://">
					</div>
				</div>
				<div class="control-group">
					<label for="heatSource[tempLevelId]" class="control-label">Temperature Level:</label>
					<div class="controls">
						<select id="heatSource[tempLevelId]" name="heatSource[tempLevelId]">
							{foreach $tempLevels as $temp}
								<option value="{$temp->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE && $specData->tempLevelId == $temp->id} selected{/if}>{$temp->name}</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div class="control-group">
					<label for="heatSource[ranges][]" class="control-label">Operates in Ranges:</label>
					<div class="controls">
						<select id="heatSource[ranges][]" name="heatSource[ranges][]" multiple>
							{foreach $ranges as $range}
								<option value="{$range->id}" data-name="{$range->rangeMin} - {$range->rangeMax}kW"
								{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE}
									{foreach $specData->getHeatSourceRanges() as $selectedRange}
										{if $selectedRange->id == $range->id}selected{break}{/if}
									{/foreach}
								{/if}>{$range->rangeMin} - {$range->rangeMax}kW</option>
							{/foreach}
						</select>
					</div>
				</div>
				<div id="heatSourceQuantities">
					{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE}
						{foreach $specData->getHeatSourceRanges() as $selectedRange}
							<div class="control-group" data-rangeid="{$selectedRange->id}">
								<label for="heatSource_qty_{$selectedRange->id}" class="control-label">Quantity for {$selectedRange->rangeMin} - {$selectedRange->rangeMax}kW:</label>
								<div class="controls">
									<input type="text" name="heatSource[qty][{$selectedRange->id}]" value="{$specData->getHeatSourceQty($selectedRange)}" id="heatSource_qty_{$selectedRange->id}">
								</div>
							</div>
						{/foreach}
					{/if}
				</div>
			</div>
			<div id="productSpec-pipe"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_PIPE} class="hide"{/if}>
				<div class="control-group">
					<label for="pipe[pipeLength]" class="control-label">Pipe Length:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="pipe[pipeLength]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_PIPE} value="{$specData->pipeLength}"{/if}>
							<span class="add-on">m</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="pipe[pipeWidth]" class="control-label">Pipe Diameter:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="pipe[pipeWidth]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_PIPE} value="{$specData->pipeWidth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="pipe[maxDistToManifold]" class="control-label">Maximum Distance from Manifold:</label>
					<div class="controls">
						<span class="help-block">
							Before altering this field, consult an engineer.
						</span>
						<div class="input-append">
							<input type="text" name="pipe[maxDistToManifold]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_PIPE} value="{$specData->maxDistToManifold}"{/if}>
							<span class="add-on">m</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="pipe[requirePipeBend]" class="control-label">Requires Pipe Bend:</label>
					<div class="controls">
						<label class="checkbox">
							<input type="checkbox" name="pipe[requirePipeBend]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_PIPE && $specData->requirePipeBend} checked{/if}>
							Required
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="pipe[pipeTypeId]" class="control-label">Pipe Type:</label>
					<div class="controls">
						<select name="pipe[pipeTypeId]">
							{foreach $pipeTypes as $pipeType}
								<option value="{$pipeType->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_PIPE && $pipeType->id == $specData->pipeTypeId} selected{/if}>
									{$pipeType->typeName|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-warmBoard"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. This will require a change in the code.</div>
				<div class="control-group">
					<label for="warmBoard[boardSize]" class="control-label">Board Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="warmBoard[boardSize]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD && isset($specData)} value="{$specData->boardSize}"{/if}>
							<span class="add-on">m<sup>2</sup></span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="warmBoard[boardsPerGlue]" class="control-label">Number of Boards per Glue:</label>
					<div class="controls">
						<input type="text" name="warmBoard[boardsPerGlue]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD && isset($specData)} value="{$specData->boardsPerGlue}"{/if}>
					</div>
				</div>
				<div class="control-group">
					<label for="warmBoard[boardDepth]" class="control-label">Board Depth:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="warmBoard[boardDepth]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD && isset($specData)} value="{$specData->boardDepth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="warmBoard[isLight]" class="control-label">Light Version:</label>
					<div class="controls">
						<label><input type="checkbox" name="warmBoard[isLight]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD && isset($specData) && $specData->isLight} checked{/if}>
							Warmboard Light
						</label>
					</div>
				</div>
				<div class="control-group">
					<label for="warmBoard[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="warmBoard[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-alu"{if !$editMode || ($product->typeId != ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING && $product->typeId != ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED)} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. This will require a change in the code.</div>
				<div class="control-group">
					<label class="control-label" for="alu[boardSize]">Spreader Plate Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="alu[boardSize]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING || $product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED) && isset($specData)} value="{$specData->boardSize}"{/if}>
							<span class="add-on">m<sup>2</sup></span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="alu[insulationArea]">Insulation Board Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="alu[insulationArea]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING || $product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED) && isset($specData)} value="{$specData->insulationArea}"{/if}>
							<span class="add-on">m<sup>2</sup></span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="alu[coverageArea]">Coverage Area</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="alu[coverageArea]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING || $product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED) && isset($specData)} value="{$specData->coverageArea * 100}"{/if}>
							<span class="add-on">%</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="alu[boardDepth]">Board Depth:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="alu[boardDepth]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING || $product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED) && isset($specData)} value="{$specData->boardDepth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="alu[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="alu[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING || $product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED) && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-neoSuspended"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. This will require a change in the code.</div>
				<div class="control-group">
					<label class="control-label" for="neo-sus[boardSize]">Board Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="neo-sus[boardSize]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED && isset($specData)} value="{$specData->boardSize}"{/if}>
							<span class="add-on">m<sup>2</sup></span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="neo-sus[coverageArea]">Coverage Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="neo-sus[coverageArea]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED && isset($specData)} value="{$specData->coverageArea * 100}"{/if}>
							<span class="add-on">%</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="neo-sus[boardDepth]">Board Depth:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="neo-sus[boardDepth]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED && isset($specData)} value="{$specData->boardDepth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="neo-sus[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="neo-sus[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-neoFloating"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. This will require a change in the code.</div>
				<div class="control-group">
					<label class="control-label" for="neo-floating[boardSize]">Board Area:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="neo-floating[boardSize]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING && isset($specData)} value="{$specData->boardSize}"{/if}>
							<span class="add-on">m<sup>2</sup></span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="neo-floating[boardDepth]">Board Depth:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="neo-floating[boardDepth]"{if $editMode && ($product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING|| $product->typeId == ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED) && isset($specData)} value="{$specData->boardDepth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="neo-floating[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="neo-floating[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-screedStaples"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. This will require a change in the code.</div>
				<div class="control-group">
					<label class="control-label" for="screed-staples[staplesPerPipeArea]">Number of Staples per metre of pipe:</label>
					<div class="controls">
						<input type="text" name="screed-staples[staplesPerPipeArea]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES && isset($specData)} value="{$specData->staplesPerPipeArea}"{/if}>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="screed-staples[staplesPerBag]">Number of Staples per Bag:</label>
					<div class="controls">
						<input type="text" name="screed-staples[staplesPerBag]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES && isset($specData)} value="{$specData->staplesPerBag}"{/if}>
					</div>
				</div>
				<div class="control-group">
					<label for="screed-staples[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="screed-staples[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-screedClipRail"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. This will require a change in the code.</div>
				<div class="control-group">
					<label class="control-label" for="screed-clip[clipRailsPerArea]">Number of Clip Rails per m<sup>2</sup>:</label>
					<div class="controls">
						<input type="text" name="screed-clip[clipRailsPerArea]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL && isset($specData)} value="{$specData->clipRailsPerArea}"{/if}>
					</div>
				</div>
				<div class="control-group">
					<label for="screed-clip[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="screed-clip[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-screedMat"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT} class="hide"{/if}>
				<div class="alert alert-warn">Certain system changes may require the system&apos;s module to be altered. this will require a change in the code.</div>
				<div class="control-group">
					<label class="control-label" for="screed-mat[castellationArea]">Number of mats per m<sup>2</sup>:</label>
					<div class="controls">
						<input type="text" name="screed-mat[castellationArea]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT && isset($specData)} value="{$specData->castellationArea}"{/if}>
					</div>
				</div>
				<div class="control-group">
					<label for="screed-mat[systemId]" class="control-label">System Type:</label>
					<div class="controls">
						<select name="screed-mat[systemId]"{if $editMode} disabled{/if}>
							<option value="-1">Not Applicable</option>
							{foreach $systems as $system}
								<option value="{$system->id}"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT && isset($specData) && $specData->systemId == $system->id} selected{/if}>
									{$system->name|escape}
								</option>
							{/foreach}
						</select>
					</div>
				</div>
			</div>
			<div id="productSpec-warmBoard-end"{if !$editMode || $product->typeId != ProductType::PRODUCTTYPE_WARMBOARD_ENDPIECE} class="hide"{/if}>
				<div class="control-group">
					<label for="warmBoard-end[depth]" class="control-label">End Piece Depth:</label>
					<div class="controls">
						<div class="input-append">
							<input type="text" name="warmBoard-end[depth]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_WARMBOARD_ENDPIECE && isset($specData)} value="{$specData->depth}"{/if}>
							<span class="add-on">mm</span>
						</div>
					</div>
				</div>
				<div class="control-group">
					<label for="warmBoard-end[numPerBoards]" class="control-label">Amount of end piece boards to WarmBoard:</label>
					<div class="controls">
						<input type="text" name="warmBoard-end[numPerBoards]"{if $editMode && $product->typeId == ProductType::PRODUCTTYPE_WARMBOARD_ENDPIECE && isset($specData)} value="{$specData->numPerBoards}"{/if}>
					</div>
				</div>
			</div>
		</fieldset>
		<fieldset id="systemSpec"{if !$editMode || !isset($specData) || !isset($specData->systemId)} class="hide"{/if}>
			<legend>System Specific</legend>
			{foreach $systems as $system}
				<div id="systemSpec-{$system->id}"{if !$editMode || !isset($specData) || !isset($specData->systemId) || $specData->systemId != $system->id} class="hide"{/if}>
					<div class="control-group">
						<label for="systemName[{$system->id}]" class="control-label">System Name:</label>
						<div class="controls">
							<input type="text" name="systemName[{$system->id}]" value="{$system->name|escape}">
						</div>
					</div>
					<div class="control-group">
						<label for="systemConstructionId[{$system->id}]" class="control-label">Construction Type:</label>
						<div class="controls">
							<select name="systemConstructionId[{$system->id}]">
								{foreach $constructionTypes as $constructionType}
									<option value="{$constructionType->id}"{if $constructionType->id == $system->constructionTypeId} selected{/if}>{$constructionType->name|escape}</option>
								{/foreach}
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="systemPipeWidth[{$system->id}]" class="control-label">Requires Pipe Diameter:</label>
						<div class="controls">
							<div class="input-append">
								<input type="text" name="systemPipeWidth[{$system->id}]" value="{$system->pipeWidth}">
								<span class="add-on">mm</span>
							</div>
						</div>
					</div>
					<div class="control-group">
						<label for="systemAvailable[{$system->id}]" class="control-label">System Availability:</label>
						<div class="controls">
							<label><input type="checkbox" name="systemAvailable[{$system->id}]"{if $system->available} checked{/if}>
								Available
							</label>
						</div>
					</div>
					<div class="control-group">
						<label for="systemPipes[{$system->id}]" class="control-label">Allowed Pipes:</label>
						<div class="controls">
							<select name="systemPipes[{$system->id}][]" multiple>
								{foreach $pipeTypes as $pipeType}
									<option value="{$pipeType->id}"
										{foreach $system->getAllowedPipes() as $allowedPipe}
											{if $pipeType->id == $allowedPipe->id} selected{break}{/if}
										{/foreach}>
										{$pipeType->typeName|escape}
									</option>
								{/foreach}
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="systemTempLevels[{$system->id}][]" class="control-label">Allowed Temperature Levels</label>
						<div class="controls">
							<select id="systemTempLevels[{$system->id}][]" name="systemTempLevels[{$system->id}][]" multiple>
								{foreach $tempLevels as $tempLevel}
									<option value="{$tempLevel->id}"
										{foreach $system->getAllowedTemperatureLevels() as $allowedTemp}
											{if $tempLevel->id == $allowedTemp->id} selected{break}{/if}
										{/foreach}>
										{$tempLevel->name|escape}
									</option>
								{/foreach}
							</select>
						</div>
					</div>
					<div class="control-group">
						<label for="systemTypeId[{$system->id}]" class="control-label">System Type:</label>
						<div class="controls">
							<select name="systemTypeId[{$system->id}]"{if $editMode} disabled{/if}>
								{foreach $systemTypes as $systemType}
									<option value="{$systemType->id}"{if $system->systemTypeId == $systemType->id} selected{/if}>
										{$systemType->name|escape}
									</option>
								{/foreach}
							</select>
						</div>
					</div>
				</div>
			{/foreach}
		</fieldset>
		<hr>
		<div class="pull-left">
			<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
			&nbsp;
			{if isset($backPath)}
				<a class="btn" href="{url to=$backPath}">&lt; Back to Systems View</a>
			{else}
				<a class="btn" href="{url to="ProductController->index"}?page={$curPage}">&lt; Back to Product List</a>
			{/if}
			&nbsp;
			<input type="submit" class="btn btn-primary" value="Save">
		</div>
	</form>
	<div class="modal hide fade" id="assignedWarning">
		<div class="modal-header">
			<button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3>System in use</h3>
		</div>
		<div class="modal-body">
			<p>
				This system is already assigned to another product, a system
				can not be assigned to more than one product.
			</p>
			<p>
				You can create a new system by going back to the Product
				Manager and clicking View Systems.
			</p>
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
		</div>
	</div>
	<script type="text/javascript" src="{url public="js/tinymce/tinymce.min.js"}"></script>
	<script type="text/javascript">
		<!--
		$(function() {
			$('select[name=typeId]').change(function() {
				switch (+$(this).val()) {
					case {ProductType::PRODUCTTYPE_MANIFOLD}:
						showForm('productSpec', 'productSpec-manifold');
						break;

					case {ProductType::PRODUCTTYPE_FITTING}:
						showForm('productSpec', 'productSpec-fitting');
						break;

					case {ProductType::PRODUCTTYPE_PIPE}:
						showForm('productSpec', 'productSpec-pipe');
						break;

					case {ProductType::PRODUCTTYPE_THERMOSTAT}:
					case {ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM}:
						showForm('productSpec', 'productSpec-thermostat');
						break;

					case {ProductType::PRODUCTTYPE_ACTUATOR}:
						showForm('productSpec', 'productSpec-actuator');
						break;

					case {ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD}:
						showForm('productSpec', 'productSpec-warmBoard', $('#productSpec-warmBoard select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING}:
					case {ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED}:
						showForm('productSpec', 'productSpec-alu', $('#productSpec-alu select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED}:
						showForm('productSpec', 'productSpec-neoSuspended', $('#productSpec-neoSuspend select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING}:
						showForm('productSpec', 'productSpec-neoFloating', $('#productSpec-neoFloating select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES}:
						showForm('productSpec', 'productSpec-screedStaples', $('#productSpec-screedStaples select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT}:
						showForm('productSpec', 'productSpec-screedMat', $('#productSpec-screedMat select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL}:
						showForm('productSpec', 'productSpec-screedClipRail', $('#productSpec-screedClipRail select[name$=systemId\\]]').val());
						break;

					case {ProductType::PRODUCTTYPE_HEAT_SOURCE}:
						showForm('productSpec', 'productSpec-heatSource');
						break;

					default:
						showForm();
						break;
				}
			});

			$('select[name$=systemId\\]]').change(function() {
				var that = $(this);
				$('fieldset#systemSpec').slideUp('fast', function() {
					$('fieldset#systemSpec>div').hide();
					$('#systemSpec-' + that.val()).show();
					$('fieldset#systemSpec').slideDown('fast');
				});

				$.getJSON('{url to="SystemController->isAssigned"}', { systemId: that.val(){if $editMode}, productId: {$product->id}{/if} }, function(data) {
					if (data.status == "OK" && data.result == true) {
						$('#assignedWarning').modal();
					}
				});
			});

			{* We need to use the selection to create and destroy the quantity fields for heat sources. *}
			var currentHeatSourceRanges = [];
			var heatSourceRangeContainer = $('#heatSourceQuantities');
			$.each(heatSourceRangeContainer.children(), function() {
				currentHeatSourceRanges[$(this).data('rangeid')] = $(this);
			});
			$('#heatSource\\[ranges\\]\\[\\]').change(function() {
				var selectedRanges = $('option:selected', this);
				$(currentHeatSourceRanges).each(function(i, o) {
					if (typeof o === 'undefined')
						return true;

					var found = false;
					selectedRanges.each(function() {
						if (o.data('rangeid') == this.value) {
							found = true;
							return false;
						}
					});
					if (!found) {
						o.slideUp(function() {
							o.remove();
						});
						currentHeatSourceRanges[i] = undefined;
					}
				});

				selectedRanges.each(function() {
					if (!currentHeatSourceRanges[this.value]) {
						var e = $('<div class="control-group hide"><label for="heatSource_qty_'+this.value+'" class="control-label">Quantity for '+this.textContent+':</label>\
							<div class="controls"><input type="text" name="heatSource[qty]['+this.value+']" value="1" id="heatSource_qty_'+this.value+'"></div>\
							</div>');
						e.data('rangeid', this.value);
						heatSourceRangeContainer.append(e);
						e.slideDown();
						currentHeatSourceRanges[this.value] = e;
					}
				});
			});

			tinymce.init({
				selector: 'textarea',
				browser_spellcheck: true,
				menubar: false,
				width: 300,
				toolbar1: "undo redo | styleselect removeformat | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link "
			});
		});

		function showForm(fieldsetId, typeId, systemId) {
			$('fieldset:not(#general)').slideUp('fast', function() {
				$('fieldset:not(#general)>div').hide();
				if (fieldsetId === undefined) return;

				$('#' + typeId).show();
				$('fieldset#' + fieldsetId).slideDown('fast');

				if (systemId !== undefined && systemId != -1) {
					$('#systemSpec-' + systemId).show();
					$('fieldset#systemSpec').slideDown('fast');
				}
			});
		}
		-->
	</script>
{/block}