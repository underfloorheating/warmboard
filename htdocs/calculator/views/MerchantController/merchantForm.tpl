{extends file="main.tpl"}
{block name="title"}{if $editMode}Edit{else}Create{/if} Merchant{/block}
{block name="mainContent"}
	<h3>{if $editMode}Edit{else}Create{/if} Merchant</h3>
	<form class="form-horizontal" action="{if $editMode}{url to="MerchantController->update"}{else}{url to="MerchantController->create"}{/if}" method="post">
		{if $editMode}
			<input type="hidden" name="id" value="{$merchant->id}">
		{/if}
		<fieldset>
			<legend>Login Credentials</legend>
			<div class="control-group">
				<label class="control-label" for="userName">User Name:</label>
				<div class="controls">
					<input type="text" name="userName"{if $editMode} value="{$merchant->userName}"{/if}>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="password">Password:</label>
				<div class="controls">
					{if $editMode}<span class="help-block">Leave blank unless altering the password</span>{/if}
					<input type="password" name="password">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="passwordRep">Repeat Password:</label>
				<div class="controls">
					<input type="password" name="passwordRep">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="allowLogin">Enable Account:</label>
				<div class="controls">
					<label class="checkbox"><input type="checkbox" name="allowLogin" {if !$editMode || $merchant->hasPerm(UserPerm::PERM_LOGIN)}checked{/if}>Enabled</label>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>Merchant Details</legend>
			<div class="control-group">
				<label class="control-label" for="merchantGroupId">Merchant Group:</label>
				<div class="controls">
					<select name="merchantGroupId">
						{foreach $groups as $group}
							<option value="{$group->id}"{if ($editMode && $merchantDetail->merchantGroupId == $group->id) || $smarty.session.lastGroupAccess->id == $group->id}selected{/if}>
								{$group->name}
							</option>
						{/foreach}
					</select>
				</div>
			</div>
			<div class="control-group">
				<label for="contactName" class="control-label">Contact Name:</label>
				<div class="controls">
					<input type="text" name="contactName" maxlength="100"{if $editMode} value="{$merchantDetail->contactName}"{/if}>
				</div>
			</div>
			<div class="control-group">
				<label for="contactEmail" class="control-label">Contact Email:</label>
				<div class="controls">
					<input type="email" name="contactEmail" maxlength="100"{if $editMode} value="{$merchantDetail->contactEmail}"{/if}>
				</div>
			</div>
			<div class="control-group">
				<label for="companyName" class="control-label">Company Name:</label>
				<div class="controls">
					<input type="text" name="companyName" maxlength="50"{if $editMode} value="{$merchantDetail->companyName}"{/if}>
				</div>
			</div>
			<div class="control-group">
				<label for="telephone" class="control-label">Telephone:</label>
				<div class="controls">
					<input type="text" name="telephone" maxlength="20"{if $editMode} value="{$merchantDetail->telephone}"{/if}>
				</div>
			</div>
			<div class="control-group">
				<label for="address" class="control-label">Address:</label>
				<div class="controls">
					<textarea name="address">{if $editMode}{$merchantDetail->address}{/if}</textarea>
				</div>
			</div>
			<div class="control-group">
				<label for="notes" class="control-label">Notes:</label>
				<div class="controls">
					<textarea name="notes">{if $editMode}{$merchantDetail->notes}{/if}</textarea>
				</div>
			</div>
		</fieldset>
		<hr>
		<div class="pull-left">
			<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
			&nbsp;
			<a class="btn" href="{url to="MerchantGroupController->show"}/{$smarty.session.lastGroupAccess->id}">&lt; Back to Merchant Group</a>
			&nbsp;
			<input type="submit" class="btn btn-primary" value="Save">
		</div>
	</form>
{/block}