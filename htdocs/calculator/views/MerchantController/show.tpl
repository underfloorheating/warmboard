{extends file="main.tpl"}
{block name="title"}Merchant Details{/block}
{block name="mainContent"}
	<h3>Merchant Details</h3>
	<table class="table">
		<tbody>
			<tr>
				<th>ID:</th>
				<td>{$merchant->id}</td>
				<th>User Name:</th>
				<td>{$merchant->userName|escape}</td>
			</tr>
			<tr>
				<th>Registered:</th>
				<td>{$merchant->registeredTime}</td>
				<th>Merchant Group:</th>
				<td><a href="{url to="MerchantGroupController->show"}/{$merchantDetail->getGroup()->id}">{$merchantDetail->getGroup()->name}</a></td>
			</tr>
			<tr>
				<th>Contact Name:</th>
				<td>{$merchantDetail->contactName|escape}</td>
				<th>Contact Email:</th>
				<td>{$merchantDetail->contactEmail|escape}</td>
			</tr>
			<tr>
				<th>Company Name:</th>
				<td>{$merchantDetail->companyName|escape}</td>
				<th>Telephone:</th>
				<td>{$merchantDetail->telephone|escape}</td>
			</tr>
			<tr>
				<th>Address:</th>
				<td>{$merchantDetail->address|escape|nl2br}</td>
				<th>Notes:</th>
				<td>{$merchantDetail->notes|escape|nl2br}</td>
			</tr>
		</tbody>
	</table>
	<div class="filter pull-right">
		<form class="form-search">
			<input type="text" class="input-medium search-query" name="s" value="{if isset($smarty.get.s)}{$smarty.get.s|escape}{/if}">
			<input type="submit" value="Search" class="btn">
		</form>
	</div>
	<h4>Installers</h4>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Company Name</th>
				<th>Email</th>
				<th>Telephone</th>
			</tr>
		</thead>
		<tbody>
			{foreach $installers as $installer}
				<tr>
					<td>{$installer->id}</td>
					<td><a href="{url to="InstallerController->show"}/{$installer->id}">{$installer->name|escape}</a></td>
					{* <td>{$installer->name|escape}</td> *}
					<td>{$installer->companyName|escape}</td>
					<td>{$installer->email|escape}</td>
					<td>{$installer->telephone|escape}</td>
				</tr>
			{foreachelse}
				<tr>
					<td colspan="5">No Installers available</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	<hr>
	<div class="pull-left">
		<a class="btn" href="{url to="ApplicationController->index"}"><i class="icon-home"></i>&nbsp;Home</a>
		&nbsp;
		<a class="btn" href="{url to="MerchantGroupController->show"}/{$merchantDetail->getGroup()->id}">&lt; Back to Merchant Group</a>
	</div>

{/block}
