<?php
/**
 * A helper for the heat source selection process.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (27 Nov 2014)
 */
class HeatSourceOptionHelper {
	
	/**
	 * 
	 * @var TemperatureLevel
	 */
	private $level;
	
	/**
	 * 
	 * @var HeatSourceRange
	 */
	private $range;
	
	/**
	 * 
	 * @var multitype:Product
	 */
	private $productList = array();
	
	/**
	 * 
	 * @var multitype:int
	 */
	private $qtyList = array();
	
	/**
	 * 
	 * @var multitype:HeatSourceData
	 */
	private $dataList = array();
	
	public function __construct(TemperatureLevel $level, HeatSourceRange $range) {
		$this->level = $level;
		$this->range = $range;
	}
	
	public function addProduct(Product $product, HeatSourceData $data, $qty) {
		$this->productList[] = $product;
		$this->qtyList[$product->id] = $qty;
		$this->dataList[$product->id] = $data;
	}
	
	public function getProductList() {
		return $this->productList;
	}
	
	public function getProductQty(Product $product) {
		return $this->qtyList[$product->id];
	}
	
	public function getProductLink(Product $product) {
		return $this->dataList[$product->id];
	}
	
	public function sumProducts() {
		$sum = 0;
		foreach ($this->productList as $product)
			$sum += $product->price * $this->getProductQty($product);
		
		return $sum;
	}
	
	public function getTemperatureLevel() {
		return $this->level;
	}
	
	public function getHeatSourceRange() {
		return $this->range;
	}
	
	public function packNames() {
		$arr = array();
		foreach ($this->productList as $product)
			$arr[] = ($this->getProductQty($product) > 1 ? $this->getProductQty($product).'x ':'').$product->name;
		
		return implode(', ', $arr);
	}
	
	public function packCodes() {
		$arr = array();
		foreach ($this->productList as $product)
			$arr[] = ($this->getProductQty($product) > 1 ? $this->getProductQty($product).'x ':'').$product->code;
		
		return implode(', ', $arr);
	}
}
