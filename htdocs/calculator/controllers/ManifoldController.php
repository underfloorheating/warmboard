<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Jun 19, 2013)
 */
class ManifoldController extends CalculatorController {

	private $floorIdx;
	private $manifoldIdx;
		
	public function __construct() {
		parent::__construct();
		
		$this->before();
	}
	
	private function before() {
		if (isset($_GET['floor']))
			$this->floorIdx = (int) $_GET['floor'];
		else
			$this->floorIdx = -1;
		
		if (isset($_GET['manifold']))
			$this->manifoldIdx = (int) $_GET['manifold'];
		else
			$this->manifoldIdx = -1;
	}
	
	public function createManifold() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		/* @var $system System */
		$system = System::fetchById((int) $_GET['systemId']);
		
		if (!isset($this->data->propertyTypeId) && $system->isHeatSourceEnabled()) {
			$this->data->propertyTypeId = $_GET['propertyTypeId'];
			
			if (empty($_GET['propertySize'])) {
				$this->displayJSON(array('status' => 'Please supply your property size'));
				return;
			}
			
			$this->data->propertySize = $_GET['propertySize'];
		}
		
		/* @var $floor Floor */
		if ($this->floorIdx == -1)
			$floor = $this->data->_currentFloor;
		else
			$floor = $this->data->_floors[$this->floorIdx];
		
		if (!$floor) {
			$this->displayJSON(array('status' => 'Unable to find floor at index: ' . $this->floorIdx));
			
		} else {
			$manifold = $floor->addManifold((int) $_GET['systemId'], (int) $_GET['pipeTypeId'], (int) $_GET['tempLevelId']);
			if (!$manifold->getSystem()) {
				$this->displayJSON(array('status' => 'Please Select a System for this Manifold'));
				$floor->removeManifold(count($floor->_manifolds) - 1);
				$floor->_nextManifoldIdent--;
				return;
			}
			
			if (!$manifold->getSelectedTemperatureLevel()) {
				$this->displayJSON(array('status' => 'Please select a heat source type for your system – if you are unsure select \'Boiler (Gas/Oil)\''));
				$floor->removeManifold(count($floor->_manifolds) - 1);
				$floor->_nextManifoldIdent--;
				return;
			}
			
			if (!$manifold->getPipeType()) {
				$this->displayJSON(array('status' => 'Please select a type of pipe'));
				$floor->removeManifold(count($floor->_manifolds) - 1);
				$floor->_nextManifoldIdent--;
				return;
			}
			
			$this->displayJSON(array(
					'status' => 'OK',
					'manifoldIndex' => ($manifold->ident - 1),
					'maxCircuits' => ManifoldData::getMaxPossibleCircuits(),
					'maxDistFromManifold' => PipeData::fetchMaxDistFromManifold($manifold->getPipeType(), $manifold->getSystem()->pipeWidth)));
		}
	}
	
	public function removeManifold() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		if ($this->floorIdx == -1)
			$floor = $this->data->_currentFloor;
		else
			$floor = $this->data->_floors[$this->floorIdx];
		
		if (!$floor) {
			$this->displayJSON(array('status' => 'Unable to find floor at index: ' . $this->floorIdx));
			
		} else {
			if (!isset($floor->_manifolds[$this->manifoldIdx])) {
				$this->displayJSON(array('status' =>
						"Unable to find manifold at index {$this->manifoldIdx} in floor {$floor->name}"));
				
			} else {
				$floor->removeManifold($this->manifoldIdx);
				$this->displayJSON(array('status' => 'OK'));
			}
		}
	}
	
	public function setControlType() {
			if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		if ($this->floorIdx == -1)
			$floor = $this->data->_currentFloor;
		else
			$floor = $this->data->_floors[$this->floorIdx];
		
		if (!$floor) {
			$this->displayJSON(array('status' => 'Unable to find floor at index: ' . $this->floorIdx));
			
		} else {
			$manifold = $floor->_manifolds[$this->manifoldIdx];
			if (!$manifold) {
				$this->displayJSON(array('status' =>
						"Unable to find manifold at index {$this->manifoldIdx} in floor {$floor->name}"));
				
			} else {
				$manifold->controlType = (int) $_GET['controlType'];
				$this->displayJSON(array('status' => 'OK'));
			}
		}
	}
	
	/**
	 * Sets the system and and pipe type on a manifold.
	 */
	public function setManifoldDetails() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		if ($this->floorIdx == -1)
			$floor = $this->data->_currentFloor;
		else
			$floor = $this->data->_floors[$this->floorIdx];
		
		if (!$floor) {
			$this->displayJSON(array('status' => 'Unable to find floor at index: ' . $this->floorIdx));
				
		} else {
			$manifold = $floor->_manifolds[$this->manifoldIdx];
			if (!$manifold) {
				$this->displayJSON(array('status' =>
						"Unable to find manifold at index {$this->manifoldIdx} in floor {$floor->name}"));
		
			} else {
				$manifold->systemId = (int) $_GET['systemId'];
				$manifold->pipeTypeId = (int) $_GET['pipeTypeId'];
				$manifold->resetCachedObjects();
				$this->displayJSON(array(
						'status' => 'OK',
						'manifoldIndex' => ($manifold->ident - 1),
						'maxCircuits' => ManifoldData::getMaxPossibleCircuits(),
						'maxDistFromManifold' => PipeData::fetchMaxDistFromManifold($manifold->getPipeType(), $manifold->getSystem()->pipeWidth)
				));
			}
		}
	}
	
	/**
	 * Calculates the number of circuits in a room for one manifold.
	 */
	public function getNumCircuits() {
		try {
			if ($this->floorIdx == -1)
				$floor = $this->data->_currentFloor;
			else
				$floor = $this->data->_floors[$this->floorIdx];
			
			if (!$floor) {
				$this->displayJSON(array('status' => 'Unable to find floor at index: ' . $this->floorIdx));
			
			} else {
				$manifold = $floor->_manifolds[$this->manifoldIdx];
				if (!$manifold) {
					$this->displayJSON(array('status' =>
							"Unable to find manifold at index {$this->manifoldIdx} in floor {$floor->name}"));
			
				} else {
					$distFromManifold = (float) $_GET['dist'];
					$floorArea = (float) $_GET['area'];
					$this->displayJSON(array(
							'status' => 'OK',
							'circuits' => $this->calcNumCircuits($manifold, $distFromManifold, $floorArea)));
				}
			}
				
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
}