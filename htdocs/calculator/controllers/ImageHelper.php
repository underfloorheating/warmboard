<?php
/**
 * A helper class that helps keep image processing code all in one place.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (22 Aug 2013)
 */
class ImageHelper {
	
	const PRODUCT_DIR = 'product';
	const SYSTEM_DIR = 'system';
	
	private static $errorMessages = array(
			UPLOAD_ERR_CANT_WRITE =>	'Unable to write the image, check server permissions',
			UPLOAD_ERR_FORM_SIZE =>		'The image you attempted to upload is too large',
			UPLOAD_ERR_INI_SIZE =>		'The image you attempted to upload is too large',
			UPLOAD_ERR_NO_FILE =>		'No file was uploaded',
			UPLOAD_ERR_PARTIAL =>		'The image upload was disrupted, please try again.',
			UPLOAD_ERR_EXTENSION =>		'The upload was stopped',
			UPLOAD_ERR_NO_TMP_DIR =>	'No tmp directory set up, please inform an administrator');
	
	/**
	 * Ensures an image is valid and moves it into the correct directory.
	 * 
	 * @param string $category	The category the image belongs to, this is used
	 * 							as the sub-directory to keep things organised.
	 * 							If necessary you can also make use of the
	 * 							constants above to help standardise dir names
	 * 							across the application.
	 * @param array $fileInfo	An array which can be found via php's
	 * 							$_FILES['x'] magic global.
	 * @return string			The file name of the image.
	 * @throws Exception		If a problem was found an exception will be
	 * 							thrown.
	 */
	public static function storeImage($category, array $fileInfo) {
		if (strstr($category, '/..'))
			throw new InvalidArgumentException('Invalid category name');

		self::ensurePathExists(UPLOAD_PATH . '/images/' . $category);
		self::checkIsValidImage($fileInfo);
		
		$now = time();
		do {
			$fileName = ($now++) . '_' . $fileInfo['name'];
		} while (file_exists(UPLOAD_PATH . "/images/$category/$fileName"));
		
		if (!move_uploaded_file($fileInfo['tmp_name'], UPLOAD_PATH . "/images/$category/$fileName"))
			throw new Exception('Unable to move file, check upload directory permissions');
		
		return $fileName;
	}
	
	/**
	 * Puts together image paths and names form something a browser can fetch.
	 * 
	 * @param string $category	The category the image belongs to, this is used
	 * 							as the sub-directory to keep things organised.
	 * 							If necessary you can also make use of the
	 * 							constants above to help standardise dir names 
	 * 							across the application.
	 * @param string $imageName	The name of the image.
	 * @return string			A path which can be used in the templates with
	 * 							the {url public=""} tag.
	 */
	public static function retrieveImagePath($category, $imageName) {
		if (strstr($category, '/..'))
			throw new InvalidArgumentException('Invalid category name');
		
		return "upload/images/$category/$imageName";
	}
	
	/**
	 * Removes an image from the filesystem.
	 * 
	 * @param string $category
	 * @param string $imageName
	 */
	public static function deleteImage($category, $imageName) {
		if (strstr($category, '/..'))
			throw new InvalidArgumentException('Invalid category name');
		if (strstr($imageName, '/..'))
			throw new InvalidArgumentException('Invalid imageName');
		
		unlink(UPLOAD_PATH . "/images/$category/$imageName");
	}
	
	/**
	 * Does some tests to ensure that the file uploaded is an image.
	 * 
	 * @param array $fileInfo	The file info passed from PHP to script about
	 * 							the upload.
	 * @throws Exception		If a problem was found an exception will be
	 * 							thrown.
	 */
	private static function checkIsValidImage(array $fileInfo) {
		if ($fileInfo['error'] != UPLOAD_ERR_OK)
			throw new Exception('Image upload failed: '.self::$errorMessages[$fileInfo['error']], $fileInfo['error']);
		
		if (getimagesize($fileInfo['tmp_name']) === false)
			throw new Exception('Image upload failed: file does not appear to be an image.');
	}
	
	/**
	 * Creates the upload path if it doesn't exist.
	 * 
	 * @param string $path	The path to test/create.
	 * @throws Exception	If there was a problem creating the directories.
	 */
	private static function ensurePathExists($path) {
		if (file_exists($path))
			return;
		
		if (!mkdir($path, 0755, true))
			throw new Exception('Unable to create upload directory ' . $path);
	}
}