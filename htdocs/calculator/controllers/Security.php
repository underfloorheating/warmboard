<?php
/**
 * Simple helper class for user login and session handling.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2013-04-25)
 */
class Security {
	
	/**
	 * Checks if a user is currently logged in.
	 */
	public static function check() {
		//Reload User object so an out dated version doesn't get stuck in the session.
		if (isset($_SESSION['loggedUser']))
			$_SESSION['loggedUser'] = User::fetchById($_SESSION['loggedUser']->id);
		
		return isset($_SESSION['loggedUser']) && $_SESSION['loggedTime']->diff(new DateTime())->days < 1;
	}
	
	/**
	 * Allows a user to login using a user name and password.
	 * 
	 * If the login is successful a 'loggedUser' parameter containing the
	 * user's model object will be set in the session. 
	 * 
	 * @param string $userName	The user name of the user attempting to log in.
	 * @param string $password	The password which the user has entered.
	 * @return boolean			true is login was successful, otherwise false.
	 */
	public static function login($userName, $password) {
		$user = User::fetch('userName = :userName', array(':userName' => $userName), 1);
		
		if (!is_array($user) || count($user) == 0)
			return false;
		
		$user = $user[0];
		if ($user->hasPerm(UserPerm::PERM_LOGIN) && $user->password == self::generatePassword($user, $password)) {
			$_SESSION['loggedUser'] = $user;
			$_SESSION['loggedTime'] = new DateTime();
			return true;
			
		} else {
			return false;
		}
	}
	
	/**
	 * Logs a user out.
	 */
	public static function logout() {
		unset($_SESSION['loggedUser']);
	}
	
	/**
	 * Generates a password hash for storing in the database, or comparing.
	 *
	 * @param string $pass		The user password to hash
	 * @throws RuntimeException	If the salt has not been loaded yet.
	 */
	public static function generatePassword(User $user, $pass) {
		if (!isset($user->registeredTime))
			throw new RuntimeException('Unable to generate password at without registeredTime being filled in');
	
		$pass = "$pass:{$user->registeredTime}";
		return sha1($pass);
	}
}