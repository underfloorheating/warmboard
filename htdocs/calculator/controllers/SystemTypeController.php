<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (23 Aug 2013)
 */
class SystemTypeController extends BackController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$this->display('list', array('types' => SystemType::fetchAll()));
	}
	
	protected function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS);
	}
	
	public function create() {
		try {
			$systemType = new SystemType();
			$systemType->name = $_POST['name'];
			$systemType->sysClzName = $_POST['sysClzName'];
			$systemType->datClzName = $_POST['datClzName'];
			if (!empty($_FILES['image']['name']))
				$systemType->image = ImageHelper::storeImage(ImageHelper::SYSTEM_DIR, $_FILES['image']);
		
			$systemType->save();
			
		} catch (Exception $e) {
			$this->addError('Unable to create system type: ' . $e->getMessage());
			error_log($e);
		}
		
		$this->redirectTo('SystemTypeController->index');
	}
	
	public function update() {
		$systemType = SystemType::fetchById((int) $_POST['id']);
		if ($systemType == null) {
			$this->addError('Unable to find system type in my database');
			
		} else {
			try {
				$systemType->name = $_POST['name'];
				$systemType->sysClzName = $_POST['sysClzName'];
				$systemType->datClzName = $_POST['datClzName'];
				
				if (isset($_POST['deleteImage'])) {
					ImageHelper::deleteImage(ImageHelper::SYSTEM_DIR, $systemType->image);
					$systemType->image = '';
				}
				
				if (!empty($_FILES['image']['name'])) {
					if (!empty($systemType->image))
						ImageHelper::deleteImage(ImageHelper::SYSTEM_DIR, $systemType->image);
					
					$systemType->image = ImageHelper::storeImage(ImageHelper::SYSTEM_DIR, $_FILES['image']);
				}
				
				$systemType->save();
				
			} catch (Exception $e) {
				$this->addError('Unable to update system type: ' . $e->getMessage());
			}
		}
		
		$this->redirectTo('SystemTypeController->index');
	}
	
	public function delete() {
		$systemType = SystemType::fetchById((int) $_POST['id']);
		if ($systemType == null) {
			$this->addError('Unable to find system type in my database');
			
		} else {
			if (!empty($systemType->image))
				ImageHelper::deleteImage(ImageHelper::SYSTEM_DIR, $systemType->image);
			$systemType->delete();
		}
		
		$this->redirectTo('SystemTypeController->index');
	}
}