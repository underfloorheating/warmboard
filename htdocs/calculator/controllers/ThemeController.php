<?php
/**
 * Sets what theme and products should be made available.
 * 
 * Makes use of the Security class.
 *
 * @author Aiden Foxx
 * @copyright Sizzle Creative (2013-07-29)
 */
class ThemeController extends FrontController {
	
	public function __construct() {
		parent::__construct();
	}

	public function solfex() {
		$_SESSION['theme'] = 'solfex';
		$this->redirectTo('ApplicationController->index');
	}

	public function warmaboard() {
		$_SESSION['theme'] = 'warmaboard';
		$this->redirectTo('ApplicationController->index');
	}

	public function plumbnation() {
		$_SESSION['theme'] = 'plumbnation';
		$this->redirectTo('ApplicationController->index');
	}

	public function reload() {
		if (isset($_SESSION['theme']))
			return;
		$this->display('reload');
	}
}