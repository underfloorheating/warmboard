<?php
namespace system;
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Jul 2013)
 */
class ScreedCastillationMat extends SystemCalc {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		/* @var $systemData \ScreedCastillationMatData */
		$systemData = $manifold->getSystem()->getData();
		
		$numMats = ceil($manifold->_area / $systemData->castellationArea);
		$numInsulation = ceil($manifold->_area / 20);
		
		$productList->addProductToList(\Product::fetchById($systemData->id), $numMats);
		$productList->addProductToList(\Product::fetchByCode('UFH-ACC-ESOT'), $numInsulation);
	}
}