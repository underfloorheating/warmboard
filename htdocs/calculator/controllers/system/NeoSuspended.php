<?php
namespace system;

use \Product;

/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (28 Jun 2013)
 */
class NeoSuspended extends SystemCalc {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		/* @var $systemData \NeoSuspendedData */
		$systemData = $manifold->getSystem()->getData();
		
		$numBoards = ceil($manifold->_area / $systemData->boardSize * $systemData->coverageArea);
		$productList->addProductToList(\Product::fetchById($systemData->id), $numBoards);
	}
}