<?php
namespace system;
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Jul 2013)
 */
class ScreedClipRail extends SystemCalc {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		/* @var $systemData \ScreedClipRailData */
		$systemData = $manifold->getSystem()->getData();
		
		$numClipRail = ceil($manifold->_area * $systemData->clipRailsPerArea);
		$numInsulation = ceil($manifold->_area / 20);
		
		$productList->addProductToList(\Product::fetchById($systemData->id), $numClipRail);
		$productList->addProductToList(\Product::fetchByCode('UFH-ACC-ESOT'), $numInsulation);
	}
}