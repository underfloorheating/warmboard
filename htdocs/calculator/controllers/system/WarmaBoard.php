<?php
namespace system;

use \Product;

/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jun 2013)
 */
class WarmaBoard extends SystemCalc {

	public function __construct() {
		parent::__construct();
	}
	
	protected function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		$systemData = $manifold->getSystem()->getData();
		
		$numBoards = ceil($manifold->_area/$systemData->boardSize);
		if ($systemData->isLight) {
			$productList->addProductToList(Product::fetchById($systemData->id), $numBoards);
			
		} else {
			$productList->addProductToList(Product::fetchById($systemData->id), $numBoards);
			$numGlue = ceil($numBoards/$systemData->boardsPerGlue);
			$productList->addProductToList(Product::fetchByCode('UFH-ACC-WMBG'), $numGlue);
		}
		
		$endSupportData = \WarmBoardEndSupportData::fetch('depth = :depth', array(':depth' => $systemData->boardDepth), 1);
		$endSupport = Product::fetchById($endSupportData[0]->id);
		if ($endSupport) {
			$productList->addProductToList($endSupport, $numBoards * $endSupportData[0]->numPerBoards);
		}
	}
}
