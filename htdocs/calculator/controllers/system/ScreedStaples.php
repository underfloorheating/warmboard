<?php
namespace system;
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Jul 2013)
 */
class ScreedStaples extends SystemCalc {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		/* @var $systemData \ScreedStaplesData */
		$systemData = $manifold->getSystem()->getData();
		
		$totalCoilLength = 0;
		foreach ($manifold->_zones as $zone) {
			foreach ($zone->_rooms as $room) {
				/* @var $room \Room */
				$totalCoilLength += $room->_pipePerCircuit * $room->_numCircuits; 
			}
		}
		
		$numStapleBags = ceil($totalCoilLength * $systemData->staplesPerPipeArea / $systemData->staplesPerBag);
		$numInsulation = ceil($manifold->_area / 20);
		
		$productList->addProductToList(\Product::fetchById($systemData->id), $numStapleBags);
		$productList->addProductToList(\Product::fetchByCode('UFH-ACC-ESOT'), $numInsulation);
	}
}
