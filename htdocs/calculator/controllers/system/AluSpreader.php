<?php
namespace system;

/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (30 Jul 2013)
 */
class AluSpreader extends SystemCalc {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		/* @var $systemData \AluSpreaderData */
		$systemData = $manifold->getSystem()->getData();
		
		$numInsulation = $manifold->_area / $systemData->insulationArea * $systemData->coverageArea;
		$numBoards = $manifold->_area / $systemData->boardSize * $systemData->coverageArea;
		
		$productList->addProductToList(\Product::fetchById($systemData->id), $numInsulation);
		$productList->addProductToList(\Product::fetchByCode('UFH-ACC-ALSP'), $numBoards);
	}
}