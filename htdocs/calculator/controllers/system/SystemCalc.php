<?php
namespace system;

/**
 * Used to help work out any system specific calculations and add products to
 * the product list accordingly.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jun 2013)
 */
abstract class SystemCalc {
	
	/**
	 * Create a new system calculator
	 */
	protected function __construct() {
		//Does nothing so far
	}
	
	/**
	 * Calculates anything system specific and adds the necessary products to
	 * the list.
	 * 
	 * @param \ProductList $productList	The product list to add items to.
	 * @param \Manifold $manifold		The manifold currently collected from
	 * 									the user to be used in working
	 * 									everything out.
	 */
	protected abstract function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold);
	
	/**
	 * Uses the given system to calculate what products need to be added to
	 * the given product list and adds them.
	 * 
	 * @param \System $system				The system which is used when
	 * 										deciding on what calculations are
	 * 										used.
	 * @param \ProductList $productList		The product list to which items will be added.
	 * @param \Manifold $manifold			The data which has so far been
	 * 										collected from the user.
	 * @throws \InvalidArgumentException	If the system type which was given
	 * 										is not supported or is invalid.
	 */
	public static function addSystemSpec(\System $system, \ProductList $productList, \Manifold $manifold) {
		$systemType = \SystemType::fetchById($system->systemTypeId);
		$cls = 'system\\' . $systemType->sysClzName;
		
		if (empty($systemType) || empty($systemType->sysClzName))
			throw new \InvalidArgumentException('Unknown system type or class name not set. System Type ID: ' . $system->systemTypeId);
		
		$calcInstance = new $cls();
		$calcInstance->addSystemSpecProducts($productList, $manifold);
	}
}
