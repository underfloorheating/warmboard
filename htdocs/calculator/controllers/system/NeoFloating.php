<?php
namespace system;

use \Product;

/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (30 Jul 2013)
 */
class NeoFloating extends SystemCalc {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function addSystemSpecProducts(\ProductList $productList, \Manifold $manifold) {
		/* @var $systemData \NeoFloatingData */
		$systemData = $manifold->getSystem()->getData();
		
		$numBoards = ceil($manifold->_area / $systemData->boardSize);
		$productList->addProductToList(\Product::fetchById($systemData->id), $numBoards);
	}
}