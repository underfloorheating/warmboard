<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2 Sep 2013)
 */
class MerchantGroupController extends BackController {
	
	const GROUPS_PER_PAGE = 20;
	const MERCHANTS_PER_PAGE = 20;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$displayArchived = (int) isset($_GET['archived']);
		
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::GROUPS_PER_PAGE);
		else
			$pagination = new Paginater(1, self::GROUPS_PER_PAGE);
		
		$maxPages = $pagination->numPages(MerchantGroup::count("archived = {$displayArchived}"));
		
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		$groups = MerchantGroup::fetch("archived = {$displayArchived}", array(), $pagination->getLimit(), $pagination->getStart(), 'name');

		$params = array('groups' => $groups,
				'curPage' => $pagination->getPage(),
				'maxPages' => $maxPages,
				'archive' => $displayArchived);
		
		$this->display('list', $params);
	}
	
	public function show() {
		$displayArchived = (int) isset($_GET['archived']);
		$group = MerchantGroup::fetchById((int) $_GET['id']);
		if ($group) {
			//Process filter
			if (isset($_GET['s'])) {
				$query = "merchantGroupId = {$group->id} AND archived = {$displayArchived} AND (id LIKE :q OR contactName LIKE :q OR contactEmail LIKE :q OR companyName LIKE :q OR telephone LIKE :q OR address LIKE :q)";
				$values = array(':q' => '%'.$_GET['s'].'%');
				
			} else {
				$query = "merchantGroupId = {$group->id} AND archived = {$displayArchived}";
				$values = array();
			}
			
			//Get pagination data
			if (isset($_GET['page']) && (int)$_GET['page'] > 0)
				$pagination = new Paginater((int)$_GET['page'], self::MERCHANTS_PER_PAGE);
			else
				$pagination = new Paginater(1, self::MERCHANTS_PER_PAGE);
			
			$maxPages = $pagination->numPages(MerchantDetail::count($query, $values));
			
			if ($pagination->getPage() > $maxPages)
				$pagination->setPage($maxPages);
			
			$merchants = MerchantDetail::fetch($query, $values, $pagination->getLimit(), $pagination->getStart(), 'contactName');
			$users = array();
			foreach($merchants as $merchant)
				$users[$merchant->id] = User::fetchById($merchant->id);
			
			$_SESSION['lastGroupAccess'] = $group;
			
			$this->display('show', array(
					'curPage' => $pagination->getPage(),
					'maxPages' => $maxPages,
					'group' => $group,
					'merchants' => $merchants,
					'users' => $users,
					'archive' => $displayArchived));
			
		} else {
			$this->addError('Unable to find the group requested');
			$this->redirectTo('MerchantGroupController->index');
		}
	}
	
	public function create() {
		$this->display('groupForm', array('editMode' => false));
	}
	
	public function edit() {
		$group = MerchantGroup::fetchById((int) $_GET['id']);
		if ($group) {
			$this->display('groupForm', array('editMode' => true, 'group' => $group));
			
		} else {
			$this->addError('Unable to find the requested group');
			$this->redirectTo('MerchantGroupController->index');
		}
	}
	
	public function construct() {
		try {
			$group = new MerchantGroup();
		
			$group->name = $_POST['name'];
			$group->discount = $_POST['discount'] / 100;
			$group->contactName = $_POST['contactName'];
			$group->contactEmail = $_POST['contactEmail'];
			$group->companyName = $_POST['companyName'];
			$group->telephone = $_POST['telephone'];
			$group->address = $_POST['address'];
			$group->notes = $_POST['notes'];
		
			$group->save();
			$this->redirectTo('MerchantGroupController->index');
			
		} catch (Exception $e) {
			$this->addError('Unable to save group: ' . $e->getMessage());
			$this->redirectTo('MerchantGroupController->create');
		}
	}
	
	public function update() {
		try {
			$group = MerchantGroup::fetchById((int) $_POST['id']);
			if (!$group)
				throw new Exception('Unable to find that group in my database');		

			$group->name = $_POST['name'];
			$group->discount = $_POST['discount'] / 100;
			$group->contactName = $_POST['contactName'];
			$group->contactEmail = $_POST['contactEmail'];
			$group->companyName = $_POST['companyName'];
			$group->telephone = $_POST['telephone'];
			$group->address = $_POST['address'];
			$group->notes = $_POST['notes'];
		
			$group->save();
			$this->redirectTo('MerchantGroupController->index');
				
		} catch (Exception $e) {
			$this->addError('Unable to save group: ' . $e->getMessage());
			$this->redirectTo('MerchantGroupController->edit');
		}
	}
	
	public function delete() {
		try {
			MerchantGroup::startTransaction();
			/* @var $group MerchantGroup */
			$group = MerchantGroup::fetchById((int) $_POST['id']);
			if (!$group) {
				$this->addError('Unable to find that Merchant Group in my database');
				
			} else {
				if (MerchantDetail::count('merchantGroupId = ' . $group->id) > 0) {
					$group->archived = true;
					error_log(print_r($group, true));
					$group->save();
					/* @var $merchant MerchantDetail */
					foreach (MerchantDetail::fetchByMerchantGroup($group) as $merchant) {
						UserPerm::revokePerm(User::fetchById($merchant->id), UserPerm::PERM_LOGIN);
						$merchant->archived = true;
						$merchant->save();
					}
					
				} else {
					$group->delete();
				}
			}
			MerchantGroup::commit();
		
		} catch (Exception $e) {
			MerchantGroup::rollback();
			error_log('Unable to delete or archive group: ' . $e);
			$this->addError($e->getMessage());
		}
		
		$this->redirectTo('MerchantGroupController->index');
	}
	
	public function reinstate() {
		try {
			$group = MerchantGroup::fetchById((int)$_GET['id']);
				
			if (!$group)
				throw new Exception('Unable to find merchant group');
	
			$group->archived = false;
			$group->save();
				
		} catch (Exception $e) {
			error_log('Unable to reinstate merchant group: ' . $e);
			$this->addError('Unable to reinstate merchant group: ' . $e->getMessage());
		}
	
		$this->redirectTo('MerchantGroupController->index?archived');
	}
}