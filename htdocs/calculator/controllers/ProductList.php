<?php
/**
 * A helper class which acts a little like a shopping cart, but rather than the
 * end user select items to go into the list, instead this will select them for
 * the end user.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Jun 5, 2013)
 */
class ProductList {
	
	const TOTAL_ALL =			0;
	const TOTAL_UFH =			1;
	const TOTAL_HEATSOURCE =	2;
	
	/**
	 * 
	 * @var multitype:Product
	 */
	public $products = array();
	public $qty = array();
	
	private $isReadOnly = false;

	/**
	 * Adds a manifold and all related products which rely on the number of
	 * circuits to the product list.
	 * 
	 * Adds a manifold product
	 * Adds a manifold mixer if required
	 * Adds fittings
	 * Adds actuators
	 * 
	 * @param int $numCircuits	The number of circuits required, ensure it is
	 * 							not over the maximum allowed.
	 * @param int $pipeWidth	The width of pipe the system uses.
	 */
	public function addManifold($numCircuits, $pipeWidth) {
		if ($numCircuits == 0 || $this->isReadOnly)
			return;
		
		$maxCirc = ManifoldData::getMaxPossibleCircuits();
		if ($numCircuits > $maxCirc)
			throw new Exception("No suitable manifold to cover $numCircuits circuits ceiling is $maxCirc circuits");
		
		//Add a manifold product
		$manifoldData = ManifoldData::fetch(
				'maxCircuits >= :numCircuits',
				array(':numCircuits' => $numCircuits),
				1, false, 'maxCircuits');
		if (count($manifoldData) == 0) {
			$this->addManifold($numCircuits + 1, $pipeWidth);
			return;
		}
		$selectedManifold = Product::fetchById($manifoldData[0]->id);
		if (!$selectedManifold->available) {
			$this->addManifold($numCircuits + 1, $pipeWidth);
			return;
		}
		
		$this->addProductToList($selectedManifold, 1);
		
		//If required add a mixer product
		if ($numCircuits > 1) {
			$selectedMixer = Product::fetch(
					'typeId = :type',
					array(':type' => ProductType::PRODUCTTYPE_MANIFOLD_MIXER), 1);
			$this->addProductToList($selectedMixer[0], 1);
		}
		
		//Add fittings
		$fittingData = FittingsData::fetch(
				'isSingleCircuit = :singleCircuit AND pipeWidth = :pipeWidth',
				array(
						':singleCircuit' => ($numCircuits == 1) ? 1 : 0,
						':pipeWidth' => $pipeWidth), 1);
		
		if (count($fittingData) == 0) {
			error_log("No fittings in system for {$pipeWidth}mm pipe width? Continuing anyway...");
			
		} else {
			$selectedFitting = Product::fetchById($fittingData[0]->id);
			$this->addProductToList($selectedFitting, $numCircuits * 2);
		}
	}
	
	/**
	 * Adds the zone wiring systems to the product list based on the given
	 * manifold.
	 * 
	 * @param Manifold $manifold	The manifold who's zone things we're adding
	 */
	public function addZoneProducts(Manifold $manifold) {
		if ($this->isReadOnly)
			return;
		
		$useQryMan = $manifold->_numCircuits == 1 ? ThermostatData::USE_SINGLE : ThermostatData::USE_MULTI;
		$useQryRm = $manifold->_numRooms > 1 ? ThermostatData::USE_MULTI : ThermostatData::USE_SINGLE;
		$thermostatData = ThermostatData::fetch(
				"isWireless = :isWireless AND (useOnSingleCircuit = $useQryMan OR useOnSingleCircuit = " . ThermostatData::USE_BOTH.') AND (useOnSingleRoom = '.$useQryRm.' OR useOnSingleRoom = '.ThermostatData::USE_BOTH.')',
				array(':isWireless' => ($manifold->controlType == Manifold::CONTROLTYPE_WIRELESS)), false, false, 'useOnSingleCircuit DESC');
		if (count($thermostatData) == 0)
			throw new Exception('No available thermostat was found');
		$ids = array();
		foreach($thermostatData as $i)
			$ids[] = $i->id;
		$strIds = implode(',', $ids);
		
		if (count($manifold->_zones) > 1) {
			$products = Product::fetch("id IN ($strIds) AND typeId = :type AND available = TRUE",
					array(':type' => ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM), 1);
			if (count($products) == 0)
				throw new Exception('No Control centres available to add to product list');
			$this->addProductToList($products[0], 1);
		}
		
		//If applicable, add wireless reciever
		if ($manifold->controlType == Manifold::CONTROLTYPE_WIRELESS && count($manifold->_zones) == 1) {
			$rec = Product::fetch('typeId = :type AND available = TRUE', array('type' => ProductType::PRODUCTTYPE_RECEIVER), 1);
			if (count($rec) == 0)
				throw new Exception('No Wireless receivers are available to add to the product list');
			$this->addProductToList($rec[0], 1);
		}
		
		//Get product to add any thermostats to any applicable zone.
		$products = Product::fetch("id IN ($strIds) AND typeId = :type AND available = TRUE",
				array(':type' => ProductType::PRODUCTTYPE_THERMOSTAT), 1);
		
		//Find the first thermostat available
		if (count($products) == 0)
			throw new Exception('No available thermostat was found');
		$product = $products[0];
		
		//Go through each zone and add a thermostat, unless it is linked to another zone.
		if ($product == null)
			throw new Exception('No Thermostats available to add to product list');
		
		foreach ($manifold->_zones as $zone) {
			if (!$zone->isLinked())
				$this->addProductToList($product, $this->numThermostats($zone));
		}
		
		//Find and add an actuator
		$useQryMan = $manifold->_numCircuits > 1 ? ActuatorData::USE_MULTI : ActuatorData::USE_SINGLE;
		$useQryThm = count($manifold->_zones) > 1 ? ActuatorData::USE_MULTI : ActuatorData::USE_SINGLE;
		$actuatorData = ActuatorData::fetch('(useOnSingleCircuit = '.$useQryMan.' OR useOnSingleCircuit = '.ActuatorData::USE_BOTH.') AND (useOnSingleThermostat = '.$useQryThm.' OR useOnSingleThermostat = '.ActuatorData::USE_BOTH.')',
				array(), 1, false, 'useOnSingleCircuit DESC');
		if (count($actuatorData)) {
			$product = Product::fetchById($actuatorData[0]->id);
			$this->addProductToList($product, $manifold->_numCircuits);
		}
		
		//Find and add a 2 Port Valve
		$valve = Product::fetch('typeId = :type', array('type' => ProductType::PRODUCTTYPE_2_PORT_VALVE), 1);
		if ($valve && count($valve) > 0)
			$this->addProductToList($valve[0], 1);
	}
	
	/**
	 * Counts the number of thermostats a user has added to the rooms contained within a zone.
	 * 
	 * @param Zone $zone	The zone in which to count the number of thermostats.
	 * @return number		The number of thermostats contained within the given zone.
	 */
	private function numThermostats(Zone $zone) {
		return 1;		//One thermostat per zone.
	}
	
	/**
	 * Adds a product to the list.
	 * 
	 * @param Product $product	The product to add to the list.
	 * @param int $qty			The number of items to add to the list.
	 */
	public function addProductToList(Product $product, $qty) {
		if ($this->isReadOnly)
			return;
		
		$this->products[$product->code] = $product;
		
		if (isset($this->qty[$product->code]))
			$this->qty[$product->code] += $qty;
		else
			$this->qty[$product->code] = $qty;
	}
	
	/**
	 * Removes products from the list.
	 * 
	 * @param Product $product	The product which needs removing.
	 * @param int $qty			The number of items to remove.
	 */
	public function removeProductFromList(Product $product, $qty) {
		if ($this->isReadOnly || !isset($this->products[$product->code]))
			return;
		
		$this->qty[$product->code] -= $qty;
		if ($this->qty[$product->code] <= 0)
			unset($this->products[$product->code], $this->qty[$product->code]);
	}
	
	/**
	 * Gets a sub-total price for a specific product.
	 * 
	 * @param Product $product			The product to get the sub-total for.
	 * @throws InvalidArgumentException	If the product given has not been added
	 * 									to the list.
	 * @return number					The sub-total price for the type of
	 * 									product.
	 */
	public function calculateProductSubTotal(Product $product) {
		if (!isset($this->qty[$product->code]))
			throw new InvalidArgumentException(
					'Product has not been added to the product List');
		
		return $product->price * $this->qty[$product->code];
	}
	
	/**
	 * Gets the total price of all the products in the list.
	 * 
	 * @return number	The sum of all product sub-totals.
	 */
	public function calculateTotal($mode = self::TOTAL_ALL) {
		$out = 0;
		foreach ($this->products as $product) {
			if (
					($mode == static::TOTAL_UFH && $product->typeId != ProductType::PRODUCTTYPE_HEAT_SOURCE) ||
					($mode == static::TOTAL_HEATSOURCE && $product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE) ||
					$mode == static::TOTAL_ALL
			)
			$out += $this->calculateProductSubTotal($product);
		}
		
		return $out;
	}
	
	/**
	 * Sorts to list by product code.
	 */
	public function sort() {
		ksort($this->products);
	}
	
	/**
	 * Creates the list of product list lines which can be entered into the
	 * database as locked line.
	 * 
	 * @return multitype:LockedProductListLine
	 */
	public function createLockedList() {
		$lines = array();
		
		foreach ($this->products as $product) {
			/* @var $product Product */
			$line = new LockedProductListLine();
			$line->originalName = $product->name;
			$line->originalCode = $product->code;
			$line->originalPrice = $product->price;
			$line->typeId = $product->typeId;
			$line->productId = $product->id;
			$line->qty = $this->qty[$product->code];
			$lines[] = $line;
		}
		$this->isReadOnly = true;
		
		return $lines;
	}
	
	/**
	 * Creates a product list from the lines of a locked list.
	 * 
	 * Additionally the list will be placed in a locked mode in which while
	 * the arrays may be altered, calling the add methods will cause the list
	 * to remain unaltered.
	 * 
	 * @param array $lines An array of LockedProductListLines.
	 */
	public static function fromLockedList(array $lines) {
		$productList = new ProductList();
		foreach ($lines as $line) {
			/* @var $line LockedProductListLine */
			
			//Fill in Original values
			$product = new Product();
			$product->id = $line->productId;
			$product->name = $line->originalName;
			$product->code = $line->originalCode;
			$product->typeId = $line->typeId;
			$product->price = $line->originalPrice;
			
			//Fill in remaining values which may have changed in the mean time.
			$upToDateProduct = Product::fetchById($product->id);
			if ($upToDateProduct) {
				$product->productImage = $upToDateProduct->productImage;
				$product->description = $upToDateProduct->description;
			}
			
			$productList->addProductToList($product, $line->qty);
		}
		
		$productList->isReadOnly = true;
		return $productList;
	}
	
	public function containsHeatSource() {
		foreach ($this->products as $product) {
			if ($product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE)
				return true;
		}
		return false;
	}
}