<?php
/**
 * Manages temperature levels via the backdoor.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (30 Oct 2014)
 */
class TemperatureLevelController extends BackController {
	
	public function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS);
	}
	
	/**
	 * Displays a list of temperature levels
	 */
	public function index() {
		$tempLevels = TemperatureLevel::fetchAll('name');
		
		$this->display('list', array('tempLevels' => $tempLevels));
	}
	
	/**
	 * Creates a new temperature level, AJAX function
	 */
	public function create() {
		try {
			$tempLevel = new TemperatureLevel();
			$tempLevel->name = $_GET['name'];
			$tempLevel->pipeCentres = (float)$_GET['pipeCentres'];
			$tempLevel->hidden = isset($_GET['hidden']);
			$tempLevel->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
	
	/**
	 * AJAX function for updating temperature levels.
	 */
	public function update() {
		try {
			$tempLevel = TemperatureLevel::fetchById((int)$_GET['tempLevelId']);
			$tempLevel->name = $_GET['name'];
			$tempLevel->pipeCentres = (float)$_GET['pipeCentres'];
			$tempLevel->hidden = isset($_GET['hidden']);
			$tempLevel->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
	
	/**
	 * AJAX function for removing temperature levels.
	 */
	public function delete() {
		try {
			$tempLevel = TemperatureLevel::fetchById((int)$_GET['tempLevelId']);
			$tempLevel->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
}