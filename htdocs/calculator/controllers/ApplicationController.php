<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class ApplicationController extends FrontController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$this->display('mainMenu', array('isRemembered' => isset($_SESSION['calcData'])));
	}
	
	public function login() {
		if (isset($_POST['user']) || isset($_POST['pass'])) {
			if (!Security::login($_POST['user'], $_POST['pass']))
				$this->addError('User name or password incorrect');
		}
		
		$this->redirectTo('ApplicationController->index');
		$this->clearErrors();
	}
	
	public function logout() {
		Security::logout();
		$this->redirectTo('ApplicationController->index');
	}
}