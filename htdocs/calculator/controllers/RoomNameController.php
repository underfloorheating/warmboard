<?php
/**
 * A CRUD for available room names
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (22 Oct 2014)
 */
class RoomNameController extends BackController {
	
	const PER_PAGE = 10;
	
	public function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_DROPLISTS);
	}
	
	public function index() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::PER_PAGE);
		else
			$pagination = new Paginater(1, self::PER_PAGE);
		
		$maxPages = $pagination->numPages(RoomName::count());
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		$names = RoomName::fetchLimit($pagination->getLimit(), $pagination->getStart(), 'orderVal');
		
		$params = array('names' => $names,
				'curPage' => $pagination->getPage(),
				'maxPages' => $maxPages
		);
		
		$this->display('nameList', $params);
	}
	
	public function create() {
		try {
			if (RoomName::count('name = :name', array('name' => $_GET['name'])))
				throw new Exception('The room name already exists');
			
			$name = new RoomName();
			$name->name = $_GET['name'];
			$name->orderVal = $_GET['orderVal'];
			
			$name->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to create room name:'.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function edit() {
		try {
			$name = RoomName::fetchById((int)$_GET['room_name_id']);
			if (!$name)
				throw new Exception('Unable to find room name');
			
			$name->name = $_GET['name'];
			$name->orderVal = $_GET['orderVal'];
			
			$name->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to update room name: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function delete() {
		try {
			$name = RoomName::fetchById((int)$_GET['room_name_id']);
			if (!$name)
				throw new Exception('Unable to find room name');
			
			$name->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to delete room name: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
}