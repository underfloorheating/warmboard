<?php

/**
 * All controllers within the calculator should sub-class this, useful
 * functions within.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class CalculatorController extends FrontController {
	
	/**
	 * @var CalculationData Data which is stored with the session for use
	 * 		later on.
	 */
	protected $data;
	
	public function __construct() {
		parent::__construct();
		
		if (!isset($_SESSION['calcData']))
			$_SESSION['calcData'] = new CalculationData();
		$this->data = $_SESSION['calcData'];
	}
	
	/**
	 * Clears all the data the user has entered into the calculator and
	 * returns the user to the beginning of the calculator system.
	 */
	public function reset() {
		unset($_SESSION['calcData']);
		$this->redirectTo('FloorController->index');
	}
	
	public function calcNumCircuits(Manifold $manifold, $distFromManifold, $floorArea) {
		$systemData = $manifold->getSystem();
		
		//List all pipes available
		$availablePipes = PipeData::fetch("pipeWidth = {$systemData->pipeWidth} AND pipeTypeId = {$manifold->getPipeType()->id} AND maxDistToManifold >= {$distFromManifold}", array());
		/* @var $pipeData PipeData */
		foreach ($availablePipes as $i => $pipeData) {
			$availablePipeProduct = Product::fetchById($pipeData->id);
			if (!$availablePipeProduct)
				unset($availablePipes[$i]);
		}
		
		$smallestWaste = -1;
		$pipeCount = 0;
		$selectedProduct = null;
		$selectedData = null;
		foreach ($availablePipes as $pipeData) {
			$usablePipeLength = $pipeData->pipeLength - ($distFromManifold * 2);
			if ($usablePipeLength <= 0)
				continue;
				
			$numCircuits = $floorArea / ($usablePipeLength * $manifold->getSelectedTemperatureLevel()->pipeCentres);
			$waste = ceil($numCircuits) - $numCircuits;
				
			if ($smallestWaste == -1 || $waste < $smallestWaste || ($smallestWaste == $waste && $selectedData->pipeLength < $pipeData->pipeLength)) {
				$smallestWaste = $waste;
				$selectedData = $pipeData;
				$pipeCount = ceil($numCircuits);
			}
		}
		
		if ($selectedData == null)
			throw new UnexpectedValueException('Unable to find a suitable pipe');
		
		return $pipeCount;
	}
		
	/**
	 * Gets the area of a floor.
	 * 
	 * @param Floor $floor	The floor to calculate the area of.
	 * @return float		The floor's total area.
	 */
	protected function getFloorArea(Floor $floor) {
		if (isset($floor->_area))
			return $floor->_area;
		
		$floor->_area = 0;
		if (is_array($floor->_manifolds)) {
			foreach ($floor->_manifolds as $manifold)
				$floor->_area += $this->getManifoldArea($manifold);
		} else {
			$floor->_area = 0;
		}
		return $floor->_area;
	}
	
	/**
	 * Gets the total area covered by a manifold.
	 * 
	 * @param Manifold $manifold	The manifold to calculate the area of.
	 * @return float				The manifold's total area.
	 */
	public function getManifoldArea(Manifold $manifold) {
		if (isset($manifold->_area))
			return $manifold->_area;
		
		$manifold->_area = 0;
		foreach ($manifold->_zones as $zone)
			$manifold->_area += $this->getZoneArea($zone);
		
		return $manifold->_area;
	}
	
	/**
	 * Gets the total area which is covered by a single zone.
	 * 
	 * @param Zone $zone	The zone to calculate the area of.
	 * @return float		The zone's total area.
	 */
	public function getZoneArea(Zone $zone) {
		if (isset($zone->_area))
			return $zone->_area;
		
		$zone->_area = 0;
		foreach ($zone->_rooms as $room)
			$zone->_area += $room->floorArea;
		
		return $zone->_area;
	}
	
	/**
	 * Gets the total area of all floors put together.
	 * 
	 * @return float	The building's total area.
	 */
	public function getTotalArea() {
		$area = 0;
		foreach ($this->data->_floors as $floor)
			$area += $this->getFloorArea($floor);
		
		return $area;
	}
	
	public function updateProjectName() {
		$this->data->projectRef = $_GET['projectRef'];
		
		if (empty($this->data->projectRef))
			$this->displayJSON(array('status' => 'This project must have a reference'));
		
		else
			$this->displayJSON(array(
					'status' => 'OK',
					'ref' => htmlspecialchars($this->data->projectRef)));
	}
}