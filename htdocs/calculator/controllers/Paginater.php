<?php
/**
 * A helper class which allows for easy pagination.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 14, 2013)
 */
class Paginater {
	
	private $limit;
	private $page;
	
	public $values;

	/**
	 * Creates a new pagination helper with the given values.
	 * 
	 * @param number $page	The current page number
	 * @param number $limit	The maximum amount of items to display on each page
	 */
	public function __construct($page = 1, $limit = 10) {
		$this->limit = $limit;
		$this->page = $page;
	}
	
	/**
	 * Works out the maximum amount of pages it would take to display the whole
	 * record set.
	 * 
	 * @param number $rowCount	The number of rows in the total set.
	 * @return number			The amount of pages it'll take to display the
	 * 							set.
	 */
	public function numPages($rowCount) {
		$nPages = ceil($rowCount / $this->limit);
		return $nPages > 0 ? $nPages : 1;
	}
	
	public function getStart() {
		return ($this->page - 1) * $this->limit;
	}
	
	public function getLimit() {
		return $this->limit;
	}
	
	public function setPage($page) {
		$this->page = $page;
	}
	
	public function getPage() {
		return $this->page;
	}
	
	public function setLimit($limit) {
		$this->limit = $limit;
	}
}