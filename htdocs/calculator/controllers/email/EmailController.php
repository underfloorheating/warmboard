<?php
namespace email;

use \Controller;

/**
 * Extends a controller to allow emailing too.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (9 Jul 2013)
 */
abstract class EmailController extends Controller {

	/**
	 *
	 * @var \PHPMailer
	 */
	protected static $emailer;

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Sends an email.
	 *
	 * Attached files should be formatted in array as:
	 * content => The content
	 * filename => The name of the file
	 * mimetype => The type of file
	 *
	 * @param string $to			The address the email is going to.
	 * @param string $subject		The subject the email will have.
	 * @param string $template		The smarty template name.
	 * @param array $values			Any values which should be sent to smarty.
	 * @param array $attachments	Content to attach.
	 */
	public function mail($to, $subject, $template, array $values = array(), array $attachments = array()) {
		static::setupSmarty();
		//static::setupEmail();

		if (count($values) != 0) {
			foreach ($values as $key => $value) {
				static::$smarty->assign($key, $value);
			}
		}

		static::$smarty->assign('_errors', $_SESSION['errors']);

		if ($_SESSION['theme']!='plumbnation') {
			static::$smarty->assign('headerSrc', 'http://www.warm-board.com/calculator/public/images/email/email-header.jpg');
			static::$smarty->assign('footerSrc', 'http://www.warm-board.com/calculator/public/images/email/emailsig.jpg');
		}
		else {
			static::$smarty->assign('headerSrc', 'http://www.warm-board.com/calculator/public/images/email/plumbnation/email-header.jpg');
			static::$smarty->assign('footerSrc', 'http://www.warm-board.com/calculator/public/images/email/plumbnation/emailsig.jpg');
		}

		$bodyHtm = static::$smarty->fetch(str_replace('\\', '/', get_class($this) . "/$template.html.tpl"));

		// static::$emailer->MsgHTML($bodyHtm);
		// static::$emailer->AddAddress($to);
		// static::$emailer->Subject = $subject;

		// if (count($attachments) > 0) {
		// 	foreach ($attachments as $attachment)
		// 		static::$emailer->AddStringAttachment($attachment['content'], $attachment['filename'], 'base64', $attachment['mimetype']);
		// }

		// static::$emailer->send();

		$name = EMAIL_NAME;
		$email_from = EMAIL_FROM;
		// $headers = "From: " . $email_from . "\n";
		// $headers .= "Reply-To: " . $email_from . "\n";
		// $headers .= "Content-Type: text/html; charset=UTF-8" . "\n";
		// $headers .= "MIME-Version: 1.0" . "\n";
		// ini_set("sendmail_from", $email_from);


		// mail($to, $subject, $bodyHtm, $headers, "-f" .$email_from);
		$this->sendEmail($to, $subject, $bodyHtm, $email_from);

	}

	private function sendEmail($to, $subject, $bodyHtml, $emailFrom)
	{
		$params = [
			'from'=> $emailFrom,
			'to'=>$to,
			'subject'=>$subject,
			'html'=>$bodyHtml,
			'o:tracking'=>'yes',
			'o:tracking-clicks'=>'yes',
			'o:tracking-opens'=>'yes',
			'h:Reply-To'=>$emailFrom
		];

		$mailgunUrl = 'https://api.mailgun.net/v3/mg.warm-board.com';
		$mailgunKey = 'key-30baad5ca04d2cdf4a7a9fad1a3e2fbd';

		$session = curl_init($mailgunUrl.'/messages');
		curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($session, CURLOPT_USERPWD, 'api:'.$mailgunKey);
		curl_setopt($session, CURLOPT_POST, true);
		curl_setopt($session, CURLOPT_POSTFIELDS, $params);
		curl_setopt($session, CURLOPT_HEADER, false);
		curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($session);
		curl_close($session);
		$results = json_decode($response, true);
	}

	// protected static function setupEmail() {
	// 	static::$emailer = new \PHPMailer(true);
	// 	static::$emailer->setFrom(EMAIL_FROM, EMAIL_NAME);

	// 	if ($_SESSION['theme']!='plumbnation') {
	// 		static::$emailer->AddEmbeddedImage('public/images/email/email-header.jpg', 'head');
	// 		static::$emailer->AddEmbeddedImage('public/images/email/emailsig.jpg', 'foot');
	// 	}
	// 	else {
	// 		static::$emailer->AddEmbeddedImage('public/images/email/plumbnation/email-header.jpg', 'head');
	// 		static::$emailer->AddEmbeddedImage('public/images/email/plumbnation/emailsig.jpg', 'foot');
	// 	}
	// }
}