<?php
namespace email;

/**
 * Helpers used to email things easily.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (9 Jul 2013)
 */
class EmailHelper extends EmailController {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Emails the given address with calculation recall code.
	 *
	 * @param string $to				The email to send to.
	 * @param \CalculationData $data	The calculation data which contains
	 * 									the recall code.
	 */
	public function emailCalculation($to, \CalculationData $data) {
		$this->mail($to,
				'Underfloor Heating Quotation',
				'emailCalculation',
				array(
						'code' => $data->recallCode,
						'total' => $data->_productList->calculateTotal()));
	}

	public function emailPdf($to, \Installer $installer, \CalculationData $data, $pdf) {
		$this->mail($to,
				'Quotation Raised',
				'emailPdf',
				array(
						'name' => $installer->name,
						'code' => $data->recallCode),
				array(
						array(
								'content' => $pdf,
								'mimetype' => 'application/pdf',
								'filename' => "quote-{$data->recallCode}.pdf"
						)
				));
	}

	public function emailPdfError($to, \CalculationData $data, \Exception $e) {
		$this->mail($to, 'Solfex-calc - PDF ERR', 'PDFerr', array(
				'dataText' => print_r($data, true),
				'exceptionText' => $e . ': ' . $e->html));
	}

	public function emailInstallerDetail($to, \CalculationData $data, $fullName, $telephone) {
		$this->mail($to, 'Underfloor Heating Quotation2', 'detailsToSolfex',
				array(
						'fullName' => $fullName,
						'email' => $data->contactEmail,
						'telephone' => $telephone,
						'code' => $data->recallCode,
						'total' => $data->_productList->calculateTotal()
				));
	}
}