<?php
/**
 * The source of all controllers
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2013-04-25)
 */
abstract class Controller {

	const CONTROLLER_DEFAULT_CLASS = 'ApplicationController';
	const CONTROLLER_DEFAULT_FUNCTION = 'index';

	protected static $smarty;

	public function __construct() {
		header('P3P: CP="CAO PSA OUR"');
		$this->setSessionCookieParams(true, true, 'none');
		if (!session_id()) session_start();
		if (!isset($_SESSION['errors']))
			$_SESSION['errors'] = false;
		if ($_SESSION['errors'] && !$_SESSION['errorsClear'])
			$this->clearErrors();
		else if ($_SESSION['errors'])
			$_SESSION['errorsClear'] = false;

		if (!isset($_SESSION['theme']) && get_class($this) != 'ThemeController')
			static::redirectTo('ThemeController->reload');

		if (!$this->authenticate())
			static::redirectTo("ApplicationController->index");
	}

	private function setSessionCookieParams($secure = true, $httpOnly = true, $sameSite = 'lax', $maxLifeTime = 0)
	{
		if(PHP_VERSION_ID < 70300) {
			session_set_cookie_params($maxLifeTime, '/; samesite='.$sameSite, $_SERVER['HTTP_HOST'], $secure, $httpOnly);
		} else {
			session_set_cookie_params([
				'lifetime' => $maxLifeTime,
				'path' => '/',
				'domain' => $_SERVER['HTTP_HOST'],
				'secure' => $secure,
				'httponly' => $httpOnly,
				'samesite' => $sameSite
			]);
		}
	}

	protected static function setupSmarty() {
		if (!isset(self::$smarty)) {
			require_once 'lib/Smarty/libs/Smarty.class.php';
			self::$smarty = new Smarty();
			self::$smarty->setTemplateDir('views/');
			self::$smarty->setCacheDir('bin/cache/');
			self::$smarty->setCompileDir('bin/compile/');
			self::$smarty->setConfigDir('conf/');
			self::$smarty->addPluginsDir('controllers/smarty');
		}
	}

	/**
	 * Displays a template from the controller's view folder.
	 *
	 * Only the name of the template need be specified, the suffix will be added
	 * and the template will be loaded from the views/{controllername}/ folder.
	 *
	 * @param string $template	The name of the template to view
	 * @param array $values		An associative array of parameters
	 */
	protected final function display($template, array $values = array()) {
		self::setupSmarty();

		if (count($values) != 0)
			foreach ($values as $key => $value)
				self::$smarty->assign($key, $value);

		self::$smarty->assign('_errors', $_SESSION['errors']);

		self::$smarty->display(str_replace('\\', '/', get_class($this) . "/$template.tpl"));
	}

	/**
	 * A simple way to output something in JSON format.
	 *
	 * @param mixed $value	The value to output
	 */
	protected final function displayJSON($value) {
		echo json_encode($value);
	}

	/**
	 * Used to check if access can be given on this controller.
	 *
	 * By default it allows total access, so if you want to limit it,
	 * override it in a subclass.
	 *
	 * @return boolean	True if access is allowed, false otherwise.
	 */
	protected function authenticate() {
		return true;
	}

	/**
	 * When called sets a forbidden header and exits the script.
	 */
	protected static function forbidden() {
		header("{$_SERVER['SERVER_PROTOCOL']} 403 Forbidden");
		self::setupSmarty();
		self::$smarty->display('error/403.tpl');
		exit();
	}

	/**
	 * When called sets a not found header and exists the script.
	 */
	protected static function notFound() {
		header("{$_SERVER['SERVER_PROTOCOL']} 404 Not Found");
		self::setupSmarty();
		self::$smarty->display('error/404.tpl');
		exit();
	}

	/**
	 * Called by the bootstrap, simply tests for maintenance mode and if it is
	 * enabled, then the simple page will be displayed and the script exited
	 * without touching the controllers.
	 */
	public static function checkMaintenanceMode() {
		if (!defined('MAINTENANCE_MODE') || !MAINTENANCE_MODE)
			return;

		self::setupSmarty();
		self::$smarty->display('error/maintenanceMode.tpl');
		exit();
	}

	/**
	 * Sets the location header to redirect the browser to somewhere else.
	 *
	 * @param string $to	Where to redirect to.
	 */
	protected static function redirect($to) {
		header("Location:" . URL . $to);
		exit();
	}

	/**
	 * Uses a controller class name and function name to redirect
	 *
	 */
	protected static function redirectTo($to) {
		require_once 'smarty/function.url.php';
		$location = smarty_function_url(array('to' => $to));
		header("Location: $location");
		exit();
	}

	/**
	 * A simple function which attempts to create the named controller and
	 * then calls the given function name.
	 *
	 * If the controller is not found or the function not found a 404 response
	 * will be sent to the client.
	 *
	 * @param string $controllerName	The name of the controller to create.
	 * @param string $functionName		The name of the function to call.
	 */
	public static function callController($controllerName, $functionName) {
		$controllerName = str_replace('/', '\\', $controllerName);

		//Capitalise the first char of a class name.
		if (strstr($controllerName, '\\')) {
			$controllerParts = explode('\\', $controllerName);
			array_push($controllerParts, ucfirst(array_pop($controllerParts)));
			$controllerName = implode('\\', $controllerParts);

		} else {
			$controllerName = ucfirst($controllerName);
		}

		if (!class_exists($controllerName, true))
			static::notFound();

		$controller = new $controllerName();

		if (!is_callable(array($controller, $functionName)))
			static::notFound();

		$controller->$functionName();
	}

	public function addError($string) {
		$_SESSION['errors'][] = $string;
		$_SESSION['errorsClear'] = true;
	}

	public function hasErrors() {
		return $_SESSION['errors'] != false;
	}

	public function clearErrors() {
		$_SESSION['errors'] = false;
		$_SESSION['errorsClear'] = true;
	}
}