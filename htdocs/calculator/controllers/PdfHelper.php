<?php
use email\EmailHelper;

/**
 * All code specific to creating PDFs can be put here.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (15 Aug 2013)
 */
class PdfHelper extends BackController {
	
	/**
	 * Creates the PDF for the merchants to print out.
	 * 
	 * Was originally meant to be two separate PDFs but now the images are
	 * just replaced, so that's just figured out in the template instead.
	 * 
	 * @param CalculationData $data	The calculation data to build into the PDF. 
	 * @param boolean email			If set to true will email the installer
	 * 								with the PDF instead of making it a download.
	 */
	public function exportWarmBoard(CalculationData $data, $email = false) {
		self::setupSmarty();
		$installer = Installer::fetchById($data->assignedInstaller);
		static::$smarty->assign('data', $data);
		static::$smarty->assign('merchant', MerchantDetail::fetchById($installer->merchantId));
		static::$smarty->assign('installer', $installer);
		$calc = new CalculatorController();
		static::$smarty->assign('totalArea', $calc->getTotalArea());
		static::$smarty->assign('totalPrice', $data->_productList->calculateTotal());
		static::$smarty->assign('contacts', Contact::fetchAll('position'));

		$systemTypes = array();
 		foreach ($data->_floors as $floor) {
 			/* @var $manifold Manifold */
 			foreach ($floor->_manifolds as $manifold) {
 				if (!isset($systemTypes[$manifold->getSystem()->systemTypeId]))
 					$systemTypes[$manifold->getSystem()->systemTypeId] = SystemType::fetchById($manifold->getSystem()->systemTypeId);
 			}
 		}
 			
 		static::$smarty->assign('systemTypes', $systemTypes);
		
		if ($_SESSION['theme']=='plumbnation')
			$html = static::$smarty->fetch('PdfHelper/plumbnationQuotation.tpl');
		else 
			$html = static::$smarty->fetch('PdfHelper/warmBoardQuotation.tpl');
		
		$pdf = new DOMPDF();
		try {
			$pdf->load_html($html);
			$pdf->set_base_path('');
			$pdf->render();
		
		} catch (Exception $e) {
			$e->html = $html;
			$e->pdf = $pdf;
			throw $e;
		}
		
		//Email the installer if an email is provided.
		if ($email && isset($data->contactEmail)) {
			$emailHelper = new EmailHelper();
			$emailHelper->emailPdf(
					$data->contactEmail,
					Installer::fetchById($data->assignedInstaller),
					$data,
					$pdf->output());
			
			$emailHelper->emailPdf(
					Setting::retrSetting(Setting::CORE_NOTIFICATION_EMAIL)->value,
					Installer::fetchById($data->assignedInstaller),
					$data,
					$pdf->output());
			
		} else if (!$email) {
			$pdf->stream("quote-{$data->recallCode}.pdf");
		}
	}
}