<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (9 Jul 2013)
 */
class InstallerController extends BackController {
	
	const INSTALLERS_PER_PAGE = 10;
	const CALCULATIONS_PER_PAGE = 50;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::INSTALLERS_PER_PAGE);
		else
			$pagination = new Paginater(1, self::INSTALLERS_PER_PAGE);
		
		$maxPages = $pagination->numPages(Installer::count('merchantId = :id AND archived = FALSE', array(':id' => $_SESSION['loggedUser']->id)));
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		
		$installers = Installer::fetch('merchantId = :id AND archived = FALSE', array(':id' => $_SESSION['loggedUser']->id), $pagination->getLimit(), $pagination->getStart(), 'name');
		
		$this->display('list', array(
				'installers' => $installers,
				'maxPages' => $maxPages,
				'curPage' => $pagination->getPage(),
				'archive' => false));
	}
	
	public function archive() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::INSTALLERS_PER_PAGE);
		else
			$pagination = new Paginater(1, self::INSTALLERS_PER_PAGE);
		
		$maxPages = $pagination->numPages(Installer::count('merchantId = :id AND archived = TRUE', array(':id' => $_SESSION['loggedUser']->id)));
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		
		$installers = Installer::fetch('merchantId = :id AND archived = TRUE', array(':id' => $_SESSION['loggedUser']->id), $pagination->getLimit(), $pagination->getStart(), 'name');
		
		$this->display('list', array(
				'installers' => $installers,
				'maxPages' => $maxPages,
				'curPage' => $pagination->getPage(),
				'archive' => true));
	}
	
	public function create() {
		$installer = new Installer();
		$installer->merchantId = $_SESSION['loggedUser']->id;
		$installer->name = $_GET['name'];
		if (isset($_GET['companyName']) && strlen($_GET['companyName']) > 0)
			$installer->companyName = $_GET['companyName'];
		
		if (isset($_GET['email']) && strlen($_GET['email']) > 0)
			$installer->email = $_GET['email'];
		
		if (isset($_GET['telephone']) && strlen($_GET['telephone']) > 0)
			$installer->telephone = $_GET['telephone'];
		
		if (isset($_GET['address']) && strlen($_GET['address']) > 0)
			$installer->address = $_GET['address'];
		
		if (isset($_GET['notes']) && strlen($_GET['notes']) > 0)
			$installer->notes = $_GET['notes'];
		
		$installer->id = $installer->save();
		
		$this->displayJSON(array('status' => 'OK', 'installer' => $installer));
	}
	
	public function edit() {
		$installer = Installer::fetchById((int) $_GET['installerId']);
		if (!$installer) {
			$this->displayJSON(array('status' => 'Installer not found'));
			return;
		}
		
		$installer->name = $_GET['name'];
		if (isset($_GET['companyName']) && strlen($_GET['companyName']) > 0)
			$installer->companyName = $_GET['companyName'];
		else
			$installer->companyName = null;
		
		if (isset($_GET['email']) && strlen($_GET['email']) > 0)
			$installer->email = $_GET['email'];
		else
			$installer->email = null;
		
		if (isset($_GET['telephone']) && strlen($_GET['telephone']) > 0)
			$installer->telephone = $_GET['telephone'];
		else
			$installer->telephone = null;
		
		if (isset($_GET['address']) && strlen($_GET['address']) > 0)
			$installer->address = $_GET['address'];
		else
			$installer->address = null;
		
		if (isset($_GET['notes']) && strlen($_GET['notes']) > 0)
			$installer->notes = $_GET['notes'];
		else
			$installer->notes = null;
		
		try {
			$installer->save();
			$this->displayJSON(array('status' => 'OK', 'installer' => $installer));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => 'Unable to save Installer: ' . $e->getMessage()));
			error_log($e);
		} 
	}
	
	/**
	 * A page to show Installer details and all the calculations they have
	 * assigned to them.
	 */
	public function show() {
		/* @var $installer Installer */
		$installer = Installer::fetchById((int)$_GET['installerId']);
		if (!$_SESSION['loggedUser']->isAdmin && $installer->merchantId != $_SESSION['loggedUser']->id)
			$installer = false;
		
		if ($_SESSION['loggedUser']->isAdmin) {
			$backPath = 'UserController->show';
			$useMerchantId = true;
		} else {
			$backPath = 'InstallerController->index';
			$useMerchantId = false;
		}
		
		if ($installer) {
			if (isset($_GET['page']) && (int)$_GET['page'] > 0)
				$pagination = new Paginater((int)$_GET['page'], self::CALCULATIONS_PER_PAGE);
			else
				$pagination = new Paginater(1, self::CALCULATIONS_PER_PAGE);
			
			$maxPages = $pagination->numPages(CalculationData::count('assignedInstaller = :id', array(':id' => $installer->id)));
			if ($maxPages < 1)
				$maxPages = 1;
			
			if ($pagination->getPage() > $maxPages)
				$pagination->setPage($maxPages);
			
			$merchant = $_SESSION['loggedUser']->isAdmin ? User::fetchById($installer->merchantId) : $_SESSION['loggedUser'];
			
			$calculations = CalculationData::fetch('assignedInstaller = :id', array(':id' => $installer->id),
					$pagination->getLimit(), $pagination->getStart(), 'tsLocked DESC');
			$usernameCache = array();
			foreach ($calculations as &$calculation) {
				$calculation->_productList = ProductList::fromLockedList(LockedProductListLine::fetchByCalculation($calculation));
				
				//Fetch and cache user names on on who followed up calculations.
				if ($calculation->followedUp && !isset($usernameCache[$calculation->followedUp])) {
					$usernameCache[(int)$calculation->followedUp] = User::fetchById($calculation->followedUp)->userName; 
				}
			}
			
			$this->display('show', array('installer' => $installer,
					'calculations' => $calculations, 'maxPages' => $maxPages,
					'curPage' => $pagination->getPage(), 'merchant' => $merchant,
					'backPath' => $backPath, 'useMerchantId' => $useMerchantId,
					'usernameCache' => $usernameCache,
			));
			
		} else {
			$this->display('show', array('backPath' => $backPath, 'useMerchantId' => $useMerchantId));
		}
	}
	
	/**
	 * An AJAX function which returns an installers details.
	 */
	public function fetch() {
		$installer = Installer::fetchById((int) $_GET['installerId']);
		
		if (!$installer || $installer->merchantId != $_SESSION['loggedUser']->id)
			$this->displayJSON(array('status' => 'Installer not found'));
		
		else
			$this->displayJSON(array('status' => 'OK', 'installer' => $installer));
	}
	
	/**
	 * An AJAX Installer search tool which will fetch any installer like the
	 * query from the database as long as it is owned by the logged in merchant.
	 */
	public function search() {
		$query = "%{$_GET['q']}%";
		if (isset($_GET['fld']) && strlen($_GET['fld']) > 0 && preg_match('/[a-zA-Z][a-zA-Z0-9_-]+/', $_GET['fld'])) {
			$results = Installer::fetch(
					"merchantId = :id AND archived = FALSE AND {$_GET['fld']} LIKE :fld",
					array(':id' => $_SESSION['loggedUser']->id, ':fld' => $query));
			
		} else {
			$results = Installer::fetch(
					'merchantId = :id AND archived = FALSE AND (id LIKE :q OR name LIKE :q OR email LIKE :q OR telephone LIKE :q OR companyName LIKE :q)',
					array(':id' => $_SESSION['loggedUser']->id, ':q' => $query));
		}
		
		$this->displayJSON(array('status' => 'OK', 'results' => $results));
	}
	
	/**
	 * An AJAX Installer search tool which will fetch installers matching the given criteria.
	 */
	public function searchDetailed() {
		$queries = array('merchantId = :merchantId AND archived = FALSE');
		$values = array(':merchantId' => $_SESSION['loggedUser']->id);
		foreach($_GET as $fld => $query) {
			if (empty($query))
				continue;
			
			switch ($fld) {
				case 'name':
					$queries[] = 'name LIKE :name';
					$values[':name'] = "%{$_GET['name']}%";
					break;
					
				case 'companyName':
					$queries[] = 'companyName LIKE :companyName';
					$values[':companyName'] = "%{$_GET['companyName']}%";
					break;
					
				case 'email':
					$queries[] = 'email LIKE :email';
					$values[':email'] = "%{$_GET['email']}%";
					break;
					
				case 'telephone':
					$queries[] = 'telephone LIKE :telephone';
					$values[':telephone'] = "%{$_GET['telephone']}%";
					break;
			}
		}
		$installers = Installer::fetch(implode(' AND ', $queries), $values);
		$this->displayJSON(array('status' => 'OK', 'results' => $installers));
	}
	
	/**
	 * Deletes an installer unless it has calculations attached to it.
	 * 
	 * If the installer is currently linked to any calculations then it will be
	 * archived instead.
	 */
	public function delete() {
		$installer = Installer::fetchById((int) $_GET['installerId']);
		if (!$installer) {
			$this->addError('Installer not found: ' . ((int) $_GET['installerId']));
			
		} else {
			//Archive if has calculations
			if (CalculationData::count('assignedInstaller = ' . $installer->id) > 0) {
				$installer->archived = true;
				$installer->save();
				
			} else {
				$installer->delete();
			}
		}
		
		$this->redirectTo('InstallerController->index');
	}
	
	public function reinstate() {
		$installer = Installer::fetchById((int)$_GET['installerId']);
		if (!$installer) {
			$this->addError('Installer not found: ' . ((int) $_GET['installerId']));
			
		} else {
			$installer->archived = false;
			$installer->save();
		}
		
		$this->redirectTo('InstallerController->index');
	}
	
	/**
	 * An AJAX function called from the installer back door by an admin.
	 */
	public function setFollowedUp() {
		if (!$_SESSION['loggedUser']->isAdmin)
			$this->forbidden();
		
		$data = CalculationData::fetchById((int)$_POST['calculationId']);
		if (!isset($data))
			$this->notFound();
		
		try {
			error_log('followedUp = '.$_POST['followedUp']);
			$data->followedUp = $_POST['followedUp'] == 'true' ? $_SESSION['loggedUser']->id : null;
			$data->followedUpDate = date('Y-m-d H:i:s');
			$data->saveFollowUp();
			
			$exportData = array('status' => 'OK');
			if ($_POST['followedUp'] == 'true') {
				$exportData['followedUpDate'] = $data->followedUpDate;
				$exportData['userName'] = $_SESSION['loggedUser']->userName;
				
			} else {
				$exportData['userName'] = false;
			}
			
			$this->displayJSON($exportData);
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
}