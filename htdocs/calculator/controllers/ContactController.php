<?php
/**
 * Manages contact CRUD
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Oct 2014)
 */
class ContactController extends BackController {
	
	const PER_PAGE = 10;	//It probably should never get to this. :p
	
	public function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_CONTACTS);
	}
	
	public function index() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::PER_PAGE);
		else
			$pagination = new Paginater(1, self::PER_PAGE);
		
		$maxPages = $pagination->numPages(Contact::count());
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		$contacts = Contact::fetchLimit($pagination->getLimit(), $pagination->getStart(), 'position');
		
		$params = array('contacts' => $contacts,
				'curPage' => $pagination->getPage(),
				'maxPages' => $maxPages
		);
		
		$this->display('contactList', $params);
	}
	
	public function create() {
		try {
			if (Contact::count('name = :name', array('name' => $_GET['name'])))
				throw new Exception('A contact with that name already exists.');
			
			$contact = new Contact();
			$contact->name = $_GET['name'];
			$contact->role = $_GET['role'];
			$contact->region = $_GET['region'];
			$contact->email = $_GET['email'];
			$contact->telephone = $_GET['telephone'];
			$contact->position = $_GET['position'];
			
			$contact->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to create Contact: ' . $e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function edit() {
		try {
			/* @var $contact Contact */
			$contact = Contact::fetchById((int)$_GET['contact_id']);
			if (!$contact)
				throw new Exception('Unable to find contact');
			
			$contact->name = $_GET['name'];
			$contact->role = $_GET['role'];
			$contact->region = $_GET['region'];
			$contact->email = $_GET['email'];
			$contact->telephone = $_GET['telephone'];
			$contact->position = $_GET['position'];
				
			$contact->save();
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to update contact: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function delete() {
		try {
			$contact = Contact::fetchById((int)$_GET['contact_id']);
			if (!$contact)
				throw new Exception('Unable to find contact');
			
			$contact->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to delete contact: ' . $e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
}