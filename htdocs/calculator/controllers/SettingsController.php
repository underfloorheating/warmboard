<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (29 Jul 2013)
 */
class SettingsController extends BackController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$settings = Setting::fetchAll();
		$values = array();
		foreach ($settings as $setting)
			$values[$setting->name] = $setting->value;
		
		$this->display('settings', array('values' => $values));
		unset($_SESSION['settingsUpdated']);
	}
	
	public function save() {
		try {
			if (!isset($_GET[Setting::CORE_MAX_ACCEPT_AREA]) || !is_numeric($_GET[Setting::CORE_MAX_ACCEPT_AREA]))
				throw new Exception('Max acceptable area must be a number.');
			
			if (empty($_GET[Setting::CORE_NOTIFICATION_EMAIL]) || !filter_var($_GET[Setting::CORE_NOTIFICATION_EMAIL], FILTER_VALIDATE_EMAIL))
				throw new Exception('Notification Email must be in email format');

			try {
				Setting::startTransaction();
				
				Setting::storeSetting(Setting::CORE_MAX_ACCEPT_AREA, (int)$_GET[Setting::CORE_MAX_ACCEPT_AREA]);
				Setting::storeSetting(Setting::CORE_NOTIFICATION_EMAIL, $_GET[Setting::CORE_NOTIFICATION_EMAIL]);
				
				Setting::commit();
				
			} catch (Exception $e) {
				Setting::rollback();
				throw $e;
			}
				
			$_SESSION['settingsUpdated'] = 'Settings updated successfully';
			
		} catch (Exception $e) {
			$this->addError('Unable to update settings: ' . $e->getMessage());
			error_log($e);
		}
		
		$this->redirectTo('SettingsController->index');
	}
}