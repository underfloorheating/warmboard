<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class ConstructionTypeController extends FrontController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function getSystems() {
		if (!isset($_GET['constructionType']) || !is_numeric($_GET['constructionType'])) {
			$this->displayJSON(array('status' => 'constructionType must be set'));
			return;
		}
		
		$constructionType = ConstructionType::fetchById((int)$_GET['constructionType']);
		if (!$constructionType) {
			$this->displayJSON(array('status' => 'constructionType not found'));
			return;
		}
		
		$systems = $constructionType->getSystems();
		$arr = array();
		foreach($systems as $system) {
			$systemData = $system->getData();
			$product = Product::fetchById($systemData->id);
			if (!$product) continue;	//The product might not be available
			$arr[] = array('id' => $system->id, 'name' => $system->name, 'image' => $product->productImage, 'description' => $product->description);
		}
		
		$this->displayJSON(array('status' => 'OK', 'systems' => $arr));
	}
}