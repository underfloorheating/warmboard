<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Nov 20, 2014)
 */
class PropertyTypeController extends BackController {
	
	const PER_PAGE = 10;
	
	protected function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS);
	}
	
	public function index() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::PER_PAGE);
		else
			$pagination = new Paginater(1, self::PER_PAGE);
		
		$maxPages = $pagination->numPages(PropertyType::count());
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		$types = PropertyType::fetchLimit($pagination->getLimit(), $pagination->getStart(), 'name');
		
		$params = array('types' => $types,
				'curPage' => $pagination->getPage(),
				'maxPages' => $maxPages
		);
		$this->display('list', $params);
	}
	
	public function create() {
		try {
			if (PropertyType::count('name = :name', array('name' => $_GET['name'])))
				throw new Exception('A Property Type with that name already exists');
			
			$type = new PropertyType();
			$type->name = $_GET['name'];
			$type->wLoss = $_GET['wLoss'];
			
			$type->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to create PropertyType: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function edit() {
		try {
			$type = PropertyType::fetchById((int)$_GET['typeId']);
			if (!$type)
				throw new Exception('Unable to find property type');
			
			$type->name = $_GET['name'];
			$type->wLoss = $_GET['wLoss'];
			
			$type->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to update Property Type: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function delete() {
		try {
			$type = PropertyType::fetchById((int)$_GET['typeId']);
			if (!$type)
				throw new Exception('Unable to find Property Type');
			
			$type->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to delete Property Type: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
}