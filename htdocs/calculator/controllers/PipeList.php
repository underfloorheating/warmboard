<?php
/**
 * A simple class which keeps track of the lengths of all the different types of pipe required.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jun 2013)
 */
class PipeList {
	
	public $pipes = array();

	public function addPipeLength(PipeType $pipeType, $pipeWidth, $pipeLength, $numCircuits) {
		$this->ensurePipeTypeInArr($pipeType, $this->pipes);
		$this->ensurePipeWidthInArr($pipeWidth, $this->pipes[$pipeType->id]);
		
		$this->pipes[$pipeType->id][$pipeWidth]['length'] += $pipeLength;
		$this->pipes[$pipeType->id][$pipeWidth]['circuits'] += $numCircuits;
	}
	
	private function ensurePipeTypeInArr(PipeType $pipeType, array &$arr) {
		if (!isset($arr[$pipeType->id]))
			$arr[$pipeType->id] = array();
	}
	
	private function ensurePipeWidthInArr($width, array &$arr) {
		if (!isset($arr[$width])) {
			$arr[$width]['length'] = 0;
			$arr[$width]['circuits'] = 0;
		}
	}
	
	public function totalForWidth($id, $width) {
		$out = 0;
		foreach ($this->pipes[$id][$width] as $length) 
			$out += $length['circuits'];
		
		return $out;
	}
}