<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Jun 19, 2013)
 */
class ZoneController extends CalculatorController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		if (!isset($this->data->_currentFloor)) {
			$this->addError('Please Select a floor to work with');
			$this->redirectTo('FloorController->select');
		}
		
		$this->data->_currentFloor->realignManifoldArray();
		$this->display('zoneConfig', array(
				'roomNames' => RoomName::fetchAll('orderVal'),
				'floor' => $this->data->_currentFloor,
				'constructionTypes' => ConstructionType::fetchAll(),
				'maxPossibleCircuits' => ManifoldData::getMaxPossibleCircuits(),
				'maxProcessArea' => Setting::retrSetting(Setting::CORE_MAX_ACCEPT_AREA)->value,
				'propertyTypes' => PropertyType::fetchAll('name')
		));
		$this->clearErrors();
	}
	
	public function update() {
		if ($this->data->isLocked) {
			$this->addError('This calculation is locked, no changes can be made.');
			$this->redirectTo('QuotationController->intermediate');
			return;
		}
		
		/* @var $manifold Manifold */
		if (isset($_POST['z']) && count($_POST['z']) > 0) {
			foreach ($_POST['z'] as $mKey => $mVal) {
				if (isset($this->data->_currentFloor->_manifolds[$mKey])) {
					$manifold = $this->data->_currentFloor->_manifolds[$mKey];
		
				} else {
					$this->addError('Unable to find manifold');
					$this->redirectTo('ZoneController->index');
				}
		
				foreach ($mVal as $zKey => $zVal) {
					if (isset($manifold->_zones[$zKey])) {
						$zone = $manifold->_zones[$zKey];
						
					} else {
						$this->addError('Unable to find zone');
						$this->redirectTo('ZoneController->index');
					}

					$zone->_rooms = array();		//Reset all room arrays before population
					
					$i = 0;
					foreach ($zVal as $rKey => $rVal) {
						try {
							$room = new Room();
							$room->name = $rVal['name'];
							$room->floorArea = abs($rVal['floorArea']);
							$room->distFromManifold = abs($rVal['distFromManifold']);
							$room->hasThermostat = isset($rVal['hasThermostat']) ? $rVal['hasThermostat'] : false;
							$zone->_rooms[] = $room;
							if (!is_numeric($rVal['floorArea']))
								throw new Exception('Floor Area must be a number');
							if ($rVal['floorArea'] <= 0)
								throw new Exception('Floor area must be positive!');
							if (!is_numeric($rVal['distFromManifold']))
								throw new Exception('Distance from the manifold must be a number, enter 0 when there is no distance from the manifold.');
							if ($room->distFromManifold > PipeData::fetchMaxDistFromManifold($manifold->getPipeType(), $manifold->getSystem()->pipeWidth))
								throw new Exception('Distance from the manifold must be less than ' .
										PipeData::fetchMaxDistFromManifold($manifold->getPipeType(), $manifold->getSystem()->pipeWidth));
									
						} catch(Exception $e) {
							//No need to handle if it is the last room, just ignore it.
							if ($i == count($zVal) - 1 && $rVal != $zVal[0]) {
								unset($zone->_rooms[count($zone->_rooms)-1]);
								
							} else {
								//Report error to user
								error_log($e);
								$this->addError($e->getMessage());
							}
						}
						$i++;
					}
					unset($zone->_area);
				}
				unset($manifold->_area);
			}
			unset($this->data->_currentFloor->_area);
			
			$maxArea = (int) Setting::retrSetting(Setting::CORE_MAX_ACCEPT_AREA)->value;
			if ($this->getTotalArea() > $maxArea)
				$this->addError("This calculator was designed to handle a
						total floor area of no more than
						{$maxArea}m<sup>2</sup>.<br>You currently have
						{$this->getTotalArea()}m<sup2</sup> configured. Please
						contact Solfex on 0044 (0) 1772 312847 where we will be
						happy to assist you.");
			
			if ($this->hasErrors())
				$this->redirectTo('ZoneController->index');
			else
				$this->redirectTo('QuotationController->intermediate');
			
		} else {
			$this->addError('You must have at least one room configured per manifold to continue.');
			$this->redirectTo('ZoneController->index');
		}
	}
	
	/**
	 * An AJAX function which adds a zone to a specific manifold within the
	 * current floor.
	 */
	public function addZone() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		if (isset($this->data->_currentFloor->_manifolds[(int)$_GET['manifoldIdx']])) {
			$manifold = $this->data->_currentFloor->_manifolds[(int)$_GET['manifoldIdx']];
			$zone = new Zone();
			
			$this->getNextZoneIdent($manifold, $zone);
			$manifold->_zones[] = $zone;
			$this->displayJSON(array(
					'status' => 'OK',
					'ident' => $zone->ident,
					'zone' => count($manifold->_zones) - 1));
						
		} else {
			$this->displayJSON(array(
					'status' => 'Unable to find manifold at index ' .
					(int)$_GET['manifoldIdx']));
		}
	}
	
	/**
	 * An AJAX function which removes a zone from a specific manifold within
	 * the current floor.
	 */
	public function removeZone() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		if (isset($this->data->_currentFloor->_manifolds[(int)$_GET['manifoldIdx']])) {
			$manifold = $this->data->_currentFloor->_manifolds[(int)$_GET['manifoldIdx']];
			
			if (isset($manifold->_zones[(int)$_GET['zoneIdx']])) {
				unset($manifold->_zones[(int)$_GET['zoneIdx']]);
				$this->displayJSON(array('status' => 'OK'));
				
			} else {
				$this->displayJSON(array(
						'status' => 'Unable to find zone at index ' .
						(int)$_GET['zoneIdx']));
			}
			
		} else {
			$this->displayJSON(array(
					'status' => 'Unable to find manifold at index ' .
					(int)$_GET['manifoldIdx']));
		}
	}
	
	/**
	 * Sets the next zone identity on a given zone and returns the new identity.
	 * 
	 * @param Manifold $currentManifold	The manifold on which the given zone
	 * 									is to be added.
	 * @param Zone $zone				The zone on which to set the new
	 * 									identity.
	 * @return string					Returns the newly created identity
	 * 									just in case it is required, but it is
	 * 									already set on the input zone anyway.
	 */
	private function getNextZoneIdent(Manifold $currentManifold, Zone $zone) {
		return $zone->ident = $this->data->_currentFloor->getNextZoneIdent();
	}
}