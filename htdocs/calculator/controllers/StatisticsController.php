<?php
/**
 * Calculates statistics and displays them.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (28 Nov 2014)
 */
class StatisticsController extends BackController {
	
	const GROUPBY_DAY = 0;
	const GROUPBY_MONTH = 1;
	const GROUPBY_YEAR = 2;
	
	/**
	 * Displays a menu of stats
	 */
	public function index() {
		$this->display('index');
	}
	
	/**
	 * Calculations over time
	 */
	public function calculations() {
		$filterGroupBy = isset($_GET['groupby']) && in_array($_GET['groupby'], array(self::GROUPBY_DAY, self::GROUPBY_MONTH, self::GROUPBY_YEAR)) ? $_GET['groupby'] : self::GROUPBY_MONTH;
		$filterStart = isset($_GET['start']) ? strtotime($_GET['start']) : 0;
		$filterEnd = isset($_GET['end']) ? strtotime($_GET['end']) : time();
		
		$stats = CalculationData::fetchStatistics($filterGroupBy, $filterStart, $filterEnd);
		$data = array();
		$labels = array();
		$totals = array('conversion' => 0, 'raised' => 0, 'total' => 0, 'price_sum' => 0);
		
		foreach ($stats as $row) {
			$labels[$row['datapoint']] = $this->chartLabelFromRow($row);
			if ($row['raised'] == 1)
				$data[$row['datapoint']]['raised'] = $row;
			else
				$data[$row['datapoint']]['conversion'] = $row;
		}
		
		foreach ($data as &$j) {
			$j['total']['calc_count'] = (isset($j['raised']['calc_count']) ? $j['raised']['calc_count'] : 0) + (isset($j['conversion']['calc_count']) ? $j['conversion']['calc_count'] : 0);
			
			$totals['conversion'] += isset($j['conversion']['calc_count']) ? $j['conversion']['calc_count'] : 0;
			$totals['raised'] += isset($j['raised']['calc_count']) ? $j['raised']['calc_count'] : 0;
			$totals['total'] += $j['total']['calc_count'];
			$totals['price_sum'] += isset($j['raised']['price_sum']) ? $j['raised']['price_sum'] : 0;
		}
		ksort($data);
		ksort($labels);
		
		$this->display('calculations', array('data' => $data, 'totals' => $totals, 'labels' => $labels));
	}
	
	/**
	 * Best merchants
	 */
	public function bestMerchants() {
		$filterGroupBy = isset($_GET['groupby']) && in_array($_GET['groupby'], array(self::GROUPBY_DAY, self::GROUPBY_MONTH, self::GROUPBY_YEAR)) ? $_GET['groupby'] : self::GROUPBY_MONTH;
		$filterStart = isset($_GET['start']) ? strtotime($_GET['start']) : 0;
		$filterEnd = isset($_GET['end']) ? strtotime($_GET['end']) : time();
		
		$stats = MerchantDetail::fetchStatistics($filterStart, $filterEnd, true);
		$totals = array('calc_count' => 0, 'price_sum' => 0, 'price_discount' => 0);
		
		foreach ($stats['calcCount'] as $i) {
			$totals['calc_count'] += $i['calc_count'];
			$totals['price_sum'] += $i['price_sum'];
			$totals['price_discount'] += $i['price_discount'];
		}
		
		//Chart data
		$chart = array('priceSum' => array(), 'calcCount' => array());
		for ($i = 0; $i < 10; $i++) {
			$chart['priceSum'][$i]['label'] = $stats['priceSum'][$i]['contactName'];
			$chart['calcCount'][$i]['label'] = $stats['calcCount'][$i]['contactName'];
			$chart['priceSum'][$i]['value'] = $stats['priceSum'][$i]['price_sum'];
			$chart['calcCount'][$i]['value'] = $stats['calcCount'][$i]['calc_count'];
		}
		
		//Work out 'other' area of chart
		$otherCount = $otherSum = 0;
		for ($i = 10; $i < count($stats['calcCount']); $i++) {
			$otherCount += $stats['calcCount'][$i]['calc_count'];
			$otherSum += $stats['priceSum'][$i]['price_sum'];
		}
		$chart['calcCount'][10]['label'] = $chart['priceSum'][10]['label'] = 'Other';
		$chart['calcCount'][10]['value'] = $otherCount;
		$chart['priceSum'][10]['value'] = $otherSum;
		
		$this->display('merchants', array('stats' => $stats, 'totals' => $totals, 'chart' => $chart, 'isBest' => true));
	}
	
	/**
	 * Worst merchants
	 */
	public function worstMerchants() {
		$filterGroupBy = isset($_GET['groupby']) && in_array($_GET['groupby'], array(self::GROUPBY_DAY, self::GROUPBY_MONTH, self::GROUPBY_YEAR)) ? $_GET['groupby'] : self::GROUPBY_MONTH;
		$filterStart = isset($_GET['start']) ? strtotime($_GET['start']) : 0;
		$filterEnd = isset($_GET['end']) ? strtotime($_GET['end']) : time();
		
		$stats = MerchantDetail::fetchStatistics($filterStart, $filterEnd, false);
		$totals = array('calc_count' => 0, 'price_sum' => 0, 'price_discount' => 0);
		
		foreach ($stats['calcCount'] as $i) {
			$totals['calc_count'] += $i['calc_count'];
			$totals['price_sum'] += $i['price_sum'];
			$totals['price_discount'] += $i['price_discount'];
		}
		
		//Chart data
		$chart = array('priceSum' => array(), 'calcCount' => array());
		for ($i = 0; $i < 10; $i++) {
			$chart['priceSum'][$i]['label'] = $stats['priceSum'][$i]['contactName'];
			$chart['calcCount'][$i]['label'] = $stats['calcCount'][$i]['contactName'];
			$chart['priceSum'][$i]['value'] = $stats['priceSum'][$i]['price_sum'];
			$chart['calcCount'][$i]['value'] = $stats['calcCount'][$i]['calc_count'];
		}
				
		$this->display('merchants', array('stats' => $stats, 'totals' => $totals, 'chart' => $chart, 'isBest' => false));
	}
	
	/**
	 * Merchants which have never used the calculator
	 */
	public function merchantsNeverUsed() {
		$merchants = MerchantDetail::fetchMerchantsNotUsingMe();
		
		$this->display('merchantsNotUsing', array('merchants' => $merchants));
	}
	
	public function popularFloorNames() {
		$filterStart = isset($_GET['start']) ? strtotime($_GET['start']) : 0;
		$filterEnd = isset($_GET['end']) ? strtotime($_GET['end']) : time();
		
		$stats = Floor::fetchStatistics($filterStart, $filterEnd);
		
		$this->display('floors', array('stats' => $stats));
	}
	
	public function popularRoomNames() {
		$filterStart = isset($_GET['start']) ? strtotime($_GET['start']) : 0;
		$filterEnd = isset($_GET['end']) ? strtotime($_GET['end']) : time();
		
		$stats = Room::fetchStatistics($filterStart, $filterEnd);
		
		$this->display('rooms', array('stats' => $stats));
	}
	
	private function chartLabelFromRow($row) {
		$out = '';
		if (isset($row['d']))
			$out .= $row['d'];
		
		if (isset($row['m']) && (!isset($row['d']) || $row['d'] == 1))
			$out .= ' '.$row['m'];
		
		if (!isset($row['m']) || ($row['m'] == 'January' && (!isset($row['d']) || $row['d'] == 1)))
			$out .= ' '.$row['y'];
		
		return ltrim($out);
	}
}