<?php
/**
 * A controller for HeatSource Range management
 * 
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Nov 2014)
 */
class HeatSourceRangeController extends BackController {
	
	const PER_PAGE = 10;
	
	protected function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS);
	}
	
	public function index() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::PER_PAGE);
		else
			$pagination = new Paginater(1, self::PER_PAGE);
		
		$maxPages = $pagination->numPages(HeatSourceRange::count());
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		$ranges = HeatSourceRange::fetchLimit($pagination->getLimit(), $pagination->getStart(), 'rangeMin, rangeMax');
		
		$params = array('ranges' => $ranges,
				'curPage' => $pagination->getPage(),
				'maxPages' => $maxPages
		);
		$this->display('list', $params);
	}
	
	public function create() {
		try {
			if (HeatSourceRange::count('rangeMin = :rangeMin AND rangeMax = :rangeMax',
					array('rangeMin' => $_GET['rangeMin'], 'rangeMax' => $_GET['rangeMax'])))
				throw new Exception('That range already exists');
			
			$range = new HeatSourceRange();
			$range->rangeMin = $_GET['rangeMin'];
			$range->rangeMax = $_GET['rangeMax'];
			
			$range->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to create range: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function delete() {
		try {
			$range = HeatSourceRange::fetchById((int)$_GET['rangeId']);
			if (!$range)
				throw new Exception('Unable to find the specified range');
			if ($range->isInUse())
				throw new Exception('This range is currently in use, reassign products before deleting');
			
			$range->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to delete heat source range: '.$e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
}