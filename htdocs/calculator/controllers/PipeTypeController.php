<?php
/**
 * Managers pipes types via backdoor.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (29 Jul 2013)
 */
class PipeTypeController extends BackController {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS);
	}
	
	/**
	 * Displays a list of pipe types.
	 */
	public function index() {
		$pipeTypes = PipeType::fetchAll();
		
		$this->display('list', array('pipeTypes' => $pipeTypes));
	}
	
	/**
	 * Creates a new type of pipe, AJAX function.
	 */
	public function create() {
		try {
			$pipeType = new PipeType();
			$pipeType->typeName = $_GET['typeName'];
			$pipeType->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
	
	/**
	 * AJAX update pipe type function
	 */
	public function update() {
		try {
			$pipeType = PipeType::fetchById((int)$_GET['pipeTypeId']);
			$pipeType->typeName = $_GET['typeName'];
			$pipeType->save();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
	
	/**
	 * AJAX delete pipe type function.
	 */
	public function delete() {
		try {
			$pipeType = PipeType::fetchById((int)$_GET['pipeTypeId']);
			$pipeType->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
}