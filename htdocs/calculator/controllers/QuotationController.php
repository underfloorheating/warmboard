<?php
use email\EmailHelper;

/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Jun 20, 2013)
 */
class QuotationController extends CalculatorController {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		if (!Security::check()) {
			$this->addError('You must be logged in to view this page');
			$this->redirectTo('ApplicationController->login');

		} else {
			//If the recall code isn't set we must save it here.
			if (!isset($this->data->recallCode)) {
				$this->data->allowContact = false;
				$this->storeQuoteNoEmail();
			}

			$listTotal = $this->data->_productList->calculateTotal();
			$purchasePrice = $listTotal - (($this->data->isLocked ? $this->data->merchantDiscount : MerchantDetail::fetchById($_SESSION['loggedUser']->id)->getGroup()->discount) * $listTotal);
			$discountMargin = $listTotal - $purchasePrice;
			if ($this->data->isLocked)
				$discountMargin = $discountMargin - ($this->data->_productList->calculateTotal() * $this->data->discount);

			$this->data->_productList->sort();
			$this->display('quote', array(
					'productList' => $this->data->_productList,
					'purchasePrice' => $purchasePrice,
					'merchantDiscount' => $this->data->isLocked ? $this->data->merchantDiscount : MerchantDetail::fetchById($_SESSION['loggedUser']->id)->getGroup()->discount,
					'discountMargin' => $discountMargin,
					'isLocked' => $this->data->isLocked,
					'calculationData' => $this->data,
					'pdfDlCode' => uniqid()));
		}
	}

	public function intermediate() {
		$showFloorMessage = $errDisableFinish = false;	//Set to true to disable the finish button.
		$totalCircuits = 0;
		$totalPipeLengthRequired = 0;
		$totalArea = $this->getTotalArea();
		$heatSources = array();
		$selectedHeatSources = array();
		$heatSourceTotal = 0;

		try {
			//Quotation setup
			if ($this->data->isLocked) {
				$productList = ProductList::fromLockedList(LockedProductListLine::fetchByCalculation($this->data));
				$hasHeatSourceSelection = false;

			} else {
				$productList = new ProductList();

				$hasHeatSourceSelection = HeatSourceSelection::fetchByCalculation($this->data);

				//Add products to the product list for any heat source selections
				if ($hasHeatSourceSelection) {
					$selections = HeatSourceSelection::fetchByCalculation($this->data);
					/* @var $selection HeatSourceSelection */
					/* @var $range HeatSourceRange */
					foreach ($selections as $selection) {
						$tempLevel = TemperatureLevel::fetchById($selection->tempLevelId);
						$range = HeatSourceRange::fetchById($selection->rangeId);

						foreach ($range->fetchHeatSources($tempLevel) as $row)
							$productList->addProductToList($row['prod'], $row['amt']);
					}
				}
			}

			foreach ($this->data->_floors as $i) {
				if (!is_array($i->_manifolds)) {
					$showFloorMessage = $errDisableFinish = true;
					continue;
				}

				foreach ($i->_manifolds as $j) {
					$j->_pipeLength = $j->_numCircuits = $j->_numRooms = 0;
					$systemData = $j->getSystem();

					if (empty($j->_zones)) {
						$showFloorMessage = $errDisableFinish = true;
						continue;
					}

					foreach ($j->_zones as $k) {
						foreach ($k->_rooms as $l) {
							$this->addPipeToList($productList, $j, $l);

							$totalPipeLengthRequired += $l->_totalPipeLength;
						}
						$j->_numRooms += count($k->_rooms);
					}
					$productList->addManifold(
							$j->_numCircuits,
							$systemData->pipeWidth);
					$productList->addZoneProducts($j);
					$totalCircuits += $j->_numCircuits;
					system\SystemCalc::addSystemSpec($systemData, $productList, $j);

					if (!$this->data->isLocked && !$hasHeatSourceSelection && isset($this->data->propertyTypeId) && !$productList->containsHeatSource()) {
						//Get heat source products to recommend
						$tempLevel = $j->getSelectedTemperatureLevel();
						if (!isset($heatSources[$tempLevel->id]) && !$tempLevel->hidden) {
							$heatSources[$tempLevel->id] = array('level' => $tempLevel);

							$options = HeatSourceRange::fetchHeatSourceOptions($this->data->propertySize * $this->data->getSelectedPropertyType()->wLoss, $tempLevel);
							$heatSources[$tempLevel->id]['options'] = $options;
						}

 					} else if (isset($j->tempLevelId)) {	//Old calculations won't have this set, skip.
 						//Compile what the customer selected for the manifolds.
 						foreach ($productList->products as $product) {
 							if ($product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE) {
 								$heatSourceTotal += $product->price * $productList->qty[$product->code];

 								if (!isset($selectedHeatSources[$j->tempLevelId]))
 									$selectedHeatSources[$j->tempLevelId] = array('level' => $j->getSelectedTemperatureLevel());

 								$selectedHeatSources[$j->tempLevelId]['heatSource'][] = array('prod' => $product, 'qty' => $productList->qty[$product->code]);
 							}
 						}
					}
				}
			}

//			$this->stripOutUnusedPipe($productList);

			$this->data->_productList = $productList;

			error_log('Heat source options: '.print_r($heatSources, true));

			$this->display('intResults',
					array(
							'totalArea' => $totalArea,
							'floors' => $this->data->_floors,
							'totalPipe' => $totalPipeLengthRequired,
							'totalCircuits' => $totalCircuits,
							'disableFinish' => $errDisableFinish,
							'showFloorMessage' => $showFloorMessage,
							'productList' => $productList,
							'heatSources' => $heatSources,
							'selectedHeatSources' => $selectedHeatSources,
							'heatSourceTotal' => $heatSourceTotal,
			));

		} catch (Exception $e) {
			$this->addError($e->getMessage());
			error_log($e);
			$this->redirectTo('ZoneController->index');
		}
	}

	public function setHeatSources() {
		if ($this->data->isLocked)
			$this->displayJSON(array('status' => 'Calculation has been locked.'));

		//First clear existing heat sources
		foreach ($this->data->_productList->products as $product) {
			if ($product->typeId == ProductType::PRODUCTTYPE_HEAT_SOURCE)
				$this->data->_productList->removeProductFromList($product, $this->data->_productList->qty[$product->code]);
		}

		$this->data->_heatSourceSelections = array();

		//Find the heat sources and append them to the list.
		foreach ($_POST['heatSourceOption'] as $option) {
			/* @var $range HeatSourceRange */
			$range = HeatSourceRange::fetchById((int)$option['rangeid']);
			$tempLevel = TemperatureLevel::fetchById((int)$option['level']);

			if (!$range || !$tempLevel)
				continue;

			foreach($range->fetchHeatSources($tempLevel) as $row) {
				error_log('Adding product: '.print_r($row, true));
				$this->data->_productList->addProductToList($row['prod'], $row['amt']);
			}

			$selection = new HeatSourceSelection();
			$selection->rangeId = $range->id;
			$selection->tempLevelId = $tempLevel->id;

			$this->data->_heatSourceSelections[] = $selection;
		}

		$this->displayJSON(array('status' => 'OK'));
	}

	private function addPipeToList(ProductList $productList, Manifold $manifold, Room $room) {
		$systemData = $manifold->getSystem();

		//List all pipes available
		error_log("pipeWidth = {$systemData->pipeWidth} AND pipeTypeId = {$manifold->getPipeType()->id} AND maxDistToManifold >= {$room->distFromManifold}");
		$availablePipes = PipeData::fetch("pipeWidth = {$systemData->pipeWidth} AND pipeTypeId = {$manifold->getPipeType()->id} AND maxDistToManifold >= {$room->distFromManifold}", array());
		$availablePipeProducts = array();	//Cache: PipeData => Product
		/* @var $pipeData PipeData */
		foreach ($availablePipes as $i => $pipeData) {
			$availablePipeProducts[$pipeData->id] = Product::fetchById($pipeData->id);
			if (!$availablePipeProducts[$pipeData->id])
				unset($availablePipes[$i]);
		}

		$smallestWaste = -1;
		$pipeCount = 0;
		$selectedProduct = null;
		$selectedData = null;

		//The pipe centres for older calculations will be stored within the System object.
		$pipeCentres = isset($manifold->tempLevelId) ? $manifold->getSelectedTemperatureLevel()->pipeCentres : $systemData->pipeCentres;
		foreach ($availablePipes as $pipeData) {
			$usablePipeLength = $pipeData->pipeLength - ($room->distFromManifold * 2);
			if ($usablePipeLength <= 0)
				continue;

			$numCircuits = $room->floorArea / ($usablePipeLength * $pipeCentres);
			$waste = ceil($numCircuits) - $numCircuits;

			if ($smallestWaste == -1 || $waste < $smallestWaste || ($smallestWaste == $waste && $selectedData->pipeLength > $pipeData->pipeLength)) {
				$smallestWaste = $waste;
				$selectedProduct = $availablePipeProducts[$pipeData->id];
				$selectedData = $pipeData;
				$pipeCount = ceil($numCircuits);
			}
		}

		if ($selectedData == null)
			throw new UnexpectedValueException('Unable to find a suitable pipe');

		$room->_numCircuits = $pipeCount;
		$room->_pipePerCircuit = $selectedData->pipeLength;
		$room->_totalPipeLength = (($room->floorArea / $pipeCount) / $pipeCentres) + ($room->distFromManifold * 2);
// 		$room->_pipeActuallyRequired = $actuallyRequired;
// 		$room->_selectedPipeData[$selectedProduct->code] = $selectedData;
// 		$room->_selectedPipeProduct[$selectedProduct->code] = $selectedProduct;
		$manifold->_numCircuits += $pipeCount;
		$manifold->_pipeLength += $room->_totalPipeLength;
		$productList->addProductToList($selectedProduct, $pipeCount);

		if ($selectedData->requirePipeBend) {
			$selectedBend = Product::fetch('typeId = :typeId', array(
					':typeId' => ProductType::PRODUCTTYPE_PIPE_BEND), 1);
			$productList->addProductToList($selectedBend[0], $pipeCount * 2);
		}
	}

// 	private function stripOutUnusedPipe(ProductList $list) {
// 		//Get a pipe bend, just in case we need to remove any.
// 		$selectedBend = Product::fetch('typeId = :typeId', array(
// 				':typeId' => ProductType::PRODUCTTYPE_PIPE_BEND), 1);

// 		//Aggregate Pipe types and the amount we want verses the amount we actually need.
// 		$selectedPipes = array();
// 		$requiredLengths = array();
// 		$pipeLengths = array();

// 		foreach ($this->data->_floors as $i) {
// 			foreach ($i->_manifolds as $j) {
// 				if (count($j->_selectedPipeProduct) > 1) error_log('More than one pipe');
// 				foreach ($j->_selectedPipeProduct as $code => $product) {
// 					if (!isset($selectedPipes[$code])) {
// 						$selectedPipes[$code] = array('data' => $j->_selectedPipeData[$code], 'product' => $product);
// 						$requiredLengths[$code] = $pipeLengths[$code] = 0;
// 					}
// 					$requiredLengths[$code] += $j->_selectedPipeData[$code]->pipeLength;
// 					$pipeLengths[$code] += $j->_selectedPipeData[$code]->pipeLength * $j->_numCircuits;
// 				}
// 			}
// 		}

// 		//Work out the waste, lets see how much we can save
// 		foreach ($selectedPipes as $code => $pipe) {
// 			$waste = $pipeLengths[$code] - $requiredLengths[$code];
// 			$pipesCanSave = floor($waste / $pipe['data']->pipeLength);
// 			error_log("Pipe $code: pipeLength=$pipeLengths[$code], requiredLength=$requiredLengths[$code], waste=$waste, pipesCanSave=$pipesCanSave");
// 			if ($pipesCanSave > 0) {
// 				$list->removeProductFromList($pipe['product'], $pipesCanSave);
// 				if ($pipe['data']->requirePipeBend)
// 					$list->removeProductFromList($selectedBend[0], $pipesCanSave * 2);
// 			}
// 		}
// 	}

	/**
	 * Stores a calculation and quote within the database and returns with a
	 * code which can be used to recall the calculation again. Also emails the
	 * user with the email they provided.
	 */
	public function storeQuote() {
		if (empty($_GET['fullName'])) {
			$this->displayJSON(array('status' => 'Please give your full name'));
			return;

		} else if (empty($_GET['telephone'])) {
			$this->displayJSON(array('status' => 'Please supply your telephone number'));
			return;

		} else if (!filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
			$this->displayJSON(array('status' => 'Please supply a valid email'));
			return;

		} else if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}

		try {
			$this->data->allowContact = isset($_GET['contact']);
			$this->data->contactEmail = $_GET['email'];

			$this->storeQuoteNoEmail();

			foreach ($this->data->_heatSourceSelections as $selection) {
				$selection->calculationId = $this->data->id;
				$selection->save();
			}


			$emailer = new EmailHelper();
			$emailer->emailCalculation($_GET['email'], $this->data);
			$emailer->emailInstallerDetail(
					Setting::retrSetting(Setting::CORE_NOTIFICATION_EMAIL)->value,
					$this->data, htmlspecialchars($_GET['fullName']),
					htmlspecialchars($_GET['telephone']));

			$this->displayJSON(array('status' => 'OK', 'code' => $this->data->recallCode));

		} catch (PDOException $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log('Unable to save calculation: ' . $e);

		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log('Unable to email: ' . $e);
		}
	}

	/**
	 * Stores a calculation and quote within the database with a
	 * code which can be used to recall the calculation again.
	 */
	private function storeQuoteNoEmail() {
		if ($this->data->isLocked)
			throw new Exception('This calculation is locked, no changes can be made');

		try {
			if (!isset($this->data->id)) {
				GeneralModel::startTransaction();

				$calculationId = $this->data->save();

				//Generate the lovely code which the installer needs to take to the merchant.
				//Just does some silly things with the time and ID
				$code = sprintf('%\'09X-%\'04X', time(), rand(1, 0xFFFF) ^ ($calculationId & 0xFFFF));
				$this->data->id = $calculationId;
				$this->data->recallCode = $code;
				$this->data->save();

				GeneralModel::commit();
			}

		} catch (PDOException $e) {
			GeneralModel::rollback();
			throw $e;
		}
	}

	/**
	 * AJAX which checks if a certain code actually exists.
	 */
	public function codeExists() {
		if (!Security::check()) {
			$this->displayJSON(array('status' => 'You must be logged in to perform this action'));
			return;
		}

		$code = strtoupper($_GET['code']);
		$codeMatch = preg_match('/^[A-F0-9]{9}-[A-F0-9]{4}$/', $code);
		if ($codeMatch === 0) {
			$this->displayJSON(array('status' => 'Please format the code correctly, expected XXXXXXXXX-XXXX'));

		} else if (!$codeMatch) {
			$this->displayJSON(array('status' => 'Server error'));

		} else {
			if (CalculationData::existsByCode($code))
				$this->displayJSON(array('status' => 'OK'));
			else
				$this->displayJSON(array('status' => 'Unable to find the given code'));
		}
	}

	/**
	 * Primary used by the merchant to look up a calculation.
	 */
	public function recallCalculation() {
		if (!Security::check()) {
			$this->addError('You must be logged in to perform this action');
			$this->redirectTo('ApplicationController->index');
		}

		$code = strtoupper($_GET['code']);
		$codeMatch = preg_match('/^[A-F0-9]{9}-[A-F0-9]{4}$/', $code);
		if ($codeMatch === 0) {
			$this->addError('Please format the code correctly, expected XXXXXXXXX-XXXX');
			$this->redirectTo('ApplicationController->merchant');

		} else if (!$codeMatch) {
			$this->displayJSON('A problem occurred while processing your request');
			$this->redirectTo('ApplicationController->merchant');

		} else if (!CalculationData::existsByCode($code)) {
			$this->addError('Unable to find the given code.');
			$this->redirectTo('ApplicationController->merchant');

		} else {
			$this->restoreCalc($code);
			$this->redirectTo('QuotationController->intermediate');
		}
	}

	/**
	 * Loads all calculation data stored within the database into the session
	 * cache.
	 *
	 * @param string $code	The code used to lookup a calculation by, this
	 * 						should already have been validated.
	 */
	private function restoreCalc($code) {
		$calculationData = CalculationData::fetchByCode($code);
		$calculationData->_floors = Floor::fetch('calculationDataId = :id', array(':id' => $calculationData->id));
		foreach ($calculationData->_floors as $floor) {
			$floor->_manifolds = Manifold::fetch('floorId = :id', array(':id' => $floor->id));

			foreach ($floor->_manifolds as $manifold) {
				$manifold->_zones = Zone::fetch('manifoldId = :id', array(':id' => $manifold->id));

				foreach ($manifold->_zones as $zone) {
					$zone->_rooms = Room::fetch('zoneId = :id', array(':id' => $zone->id));
				}
			}
		}
		$_SESSION['calcData'] = $calculationData;
		$this->data = $calculationData;
	}

	/**
	 * An AJAX function which when called stores any extra calculation details
	 * and then locks the calculation to stop it from being altered in the
	 * future.
	 */
	public function lockCalculation() {
		if (!Security::check()) {
			$this->addError('You must be logged in to perform this action');
			$this->redirectTo('ApplicationController->index');
		}

		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'Already locked'));	//Used in script, no i18n.
			return;
		}

		CalculationData::startTransaction();
		try {
			$installer = Installer::fetchById((int)$_GET['installerId']);
			if (!$installer)
				throw new Exception('Installer not found');

			$installer->notes = $_GET['notes'];
			$installer->save();

			$this->data->assignedInstaller = $installer->id;
			$this->data->discount = $_GET['discount'];
			$this->data->merchantDiscount = MerchantDetail::fetchById($_SESSION['loggedUser']->id)->getGroup()->discount;
			$this->data->lockCalculation();

			foreach ($this->data->_productList->createLockedList() as $listLine) {
				/* @var $listLine LockedProductListLine */
				$listLine->calculationDataId = $this->data->id;
				$listLine->save();
			}

			//We no longer need this
			HeatSourceSelection::deleteAllForCalculation($this->data);

			CalculationData::commit();
			$pdf = new PdfHelper();
			$pdf->exportWarmBoard($this->data, true);
			$this->displayJSON(array('status' => 'OK'));

		} catch(Exception $e) {
			CalculationData::rollback();
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}

	public function generatePdf() {
		if (!Security::check()) {
			$this->addError('You must be logged in to perform this action');
			$this->redirectTo('ApplicationController->index');
		}

		if (!$this->data->isLocked) {
			$this->addError('PDF can not be created until the quotation has been raised');
			$this->redirectTo('QuotationController->index');
		}

		setcookie('fileDownloadToken', $_GET['pdfCode']);
		try {
			$pdf = new PdfHelper();
			$pdf->exportWarmBoard($this->data);

		} catch (Exception $e) {

			//Email me with the error and some details.
			$email = new EmailHelper();
			$email->emailPdfError('steven@sizzlecreative.co.uk', $this->data, $e);

			error_log('An error occurred while generating the PDF (Also emailed): ' . $e);
			$this->addError('An error occurred while generating the PDF: ' . $e->getMessage());
			$this->redirectTo('QuotationController->index');
		}
	}

	/**
	 * A simple proxy to web shop login
	 */
	public function webshopLogin() {
		$c = curl_init('http://webportal-solfex.co.uk/templates/solfex/login.aspx');
		curl_setopt($c, CURLOPT_POST, 1);
		curl_setopt($c, CURLOPT_POSTFIELDS, $_POST);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

		$result = curl_exec($c);
		if ($result)
			echo $result;
		else
			header($_SERVER['SERVER_PROTOCOL'].' '.curl_errno($c).' '.curl_error($c));

		curl_close($c);
	}
}