<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 2, 2013)
 */
class UserController extends BackController {
	
	const USERS_PER_PAGE = 20;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_USER);
	}
	
	public function index() {
		if (isset($_GET['page']) && (int)$_GET['page'] > 0)
			$pagination = new Paginater((int)$_GET['page'], self::USERS_PER_PAGE);
		else
			$pagination = new Paginater(1, self::USERS_PER_PAGE);
		
		$maxPages = $pagination->numPages(User::count('isAdmin = TRUE'));
		if ($pagination->getPage() > $maxPages)
			$pagination->setPage($maxPages);
		$users = User::fetch('isAdmin = TRUE', array(), $pagination->getLimit(), $pagination->getStart(), 'userName');

		$params = array('users' => $users,
				'curPage' => $pagination->getPage(),
				'maxPages' => $maxPages);

		$this->display('userList', $params);
	}

	public function create() {
		try {
			if (User::userNameExists($_GET['userName']))
				throw new Exception('A user with that login name already exists.');
			
			$user = new User();
			$user->userName = $_GET['userName'];
			$user->isAdmin = true;
			
			$date = new DateTime();
			$user->registeredTime = $date->format('Y-m-d H:i:s');
			if ($_GET['password'] != $_GET['passwordRep'])
				throw new Exception('Passwords don\'t match');
			
			$user->password = Security::generatePassword($user, $_GET['password']);
			$user->id = $user->save();
			
			if (isset($_GET['perm'][0]) && $_GET['perm'][0])
				UserPerm::assign($user, UserPerm::PERM_MANAGE_USER);
			if (isset($_GET['perm'][1]) && $_GET['perm'][1])
				UserPerm::assign($user, UserPerm::PERM_MANAGE_PRODUCTS);
			if (isset($_GET['perm'][2]) && $_GET['perm'][2])
				UserPerm::assign($user, UserPerm::PERM_LOGIN);
			if (isset($_GET['perm'][3]) && $_GET['perm'][3])
				UserPerm::assign($user, UserPerm::PERM_MANAGE_CONTACTS);
			if (isset($_GET['perm'][4]) && $_GET['perm'][4])
				UserPerm::assign($user, UserPerm::PERM_MANAGE_DROPLISTS);
				
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to create User: ' . $e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function edit() {
		try {
			/* @var $user User */
			$user = User::fetchById((int)$_GET['user_id']);
			
			if (!$user)
				throw new Exception('Unable to find user');
			
			$user->userName = $_GET['userName'];
			if ($_GET['password'] != $_GET['passwordRep'])
				throw new Exception('Passwords don\'t match');
			
			if (isset($_GET['password']) && $_GET['password'] != '')
				$user->password = Security::generatePassword($user, $_GET['password']);
			$user->id = $user->save();
				
			if (isset($_GET['perm'][0]) && $_GET['perm'][0] && !$user->hasPerm(UserPerm::PERM_MANAGE_USER) && $user->isAdmin)
				UserPerm::assign($user, UserPerm::PERM_MANAGE_USER);
			else if ((!isset($_GET['perm'][0]) || !$_GET['perm'][0]) && $user->hasPerm(UserPerm::PERM_MANAGE_USER))
				UserPerm::revokePerm($user, UserPerm::PERM_MANAGE_USER);
			
			if (isset($_GET['perm'][1]) && $_GET['perm'][1] && !$user->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS) && $user->isAdmin)
				UserPerm::assign($user, UserPerm::PERM_MANAGE_PRODUCTS);
			else if ((!isset($_GET['perm'][1]) || !$_GET['perm'][1]) && $user->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS))
				UserPerm::revokePerm($user, UserPerm::PERM_MANAGE_PRODUCTS);
			
			if (isset($_GET['perm'][2]) && $_GET['perm'][2] && !$user->hasPerm(UserPerm::PERM_LOGIN))
				UserPerm::assign($user, UserPerm::PERM_LOGIN);
			else if ((!isset($_GET['perm'][2]) || !$_GET['perm'][2]) && $user->hasPerm(UserPerm::PERM_LOGIN))
				UserPerm::revokePerm($user, UserPerm::PERM_LOGIN);
			
			if (isset($_GET['perm'][3]) && $_GET['perm'][3] && !$user->hasPerm(UserPerm::PERM_MANAGE_CONTACTS))
				UserPerm::assign($user, UserPerm::PERM_MANAGE_CONTACTS);
			else if ((!isset($_GET['perm'][3]) || !$_GET['perm'][3]) && $user->hasPerm(UserPerm::PERM_MANAGE_CONTACTS))
				UserPerm::revokePerm($user, UserPerm::PERM_MANAGE_CONTACTS);

			if (isset($_GET['perm'][4]) && $_GET['perm'][4] && !$user->hasPerm(UserPerm::PERM_MANAGE_DROPLISTS))
				UserPerm::assign($user, UserPerm::PERM_MANAGE_DROPLISTS);
			else if ((!isset($_GET['perm'][4]) || !$_GET['perm'][4]) && $user->hasPerm(UserPerm::PERM_MANAGE_DROPLISTS))
				UserPerm::revokePerm($user, UserPerm::PERM_MANAGE_DROPLISTS);

			$this->displayJSON(array('status' => 'OK'));
				
		} catch (Exception $e) {
			error_log('Unable to update User: ' . $e->getMessage());
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
	
	public function delete() {
		try {
			$user = User::fetchById($_GET['user_id']);
			if (!$user)
				throw new Exception('Unable to find user');
			
			$user->delete();
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			error_log('Unable to delete User: ' . $e);
			$this->displayJSON(array('status' => $e->getMessage()));
		}
	}
}