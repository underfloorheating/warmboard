<?php

/**
 * A Smarty plugin to reverse controllers to links and resources.
 * 
 * @param unknown $params
 * @param Smarty_Internal_Template $template
 */
function smarty_function_url($params, Smarty_Internal_Template $template = NULL) {
	$direct = isset($params['direct']) && strtolower($params['direct']) == 'true';
	
	if (isset($params['to'])) {
		$out = $direct ? URL_DIRECT : URL;
		$toParts = explode('->', $params['to'], 2);
		
		if ($toParts[0] != Controller::CONTROLLER_DEFAULT_CLASS) {
			$controllerParts = explode('\\', $toParts[0]);
			array_push($controllerParts, lcfirst(array_pop($controllerParts)));
			$toParts[0] = implode('/', $controllerParts);
			$toParts[0] = str_replace('Controller', '', $toParts[0]);
			$out .= $toParts[0] . '/';
		}
		
		if (count($toParts) > 1 && $toParts[1] != Controller::CONTROLLER_DEFAULT_FUNCTION)
			$out .= $toParts[1];
		
		return $out;
		
	} else if(isset($params['public'])) {
		return ($direct ? URL_DIRECT : URL) . 'public/' . $params['public'];
	}
}