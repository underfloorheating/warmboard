<?php
/**
 * All controllers which you need to log in for should subclass this.
 * 
 * Makes use of the Security class.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2013-04-25)
 */
abstract class BackController extends Controller {
	
	public function __construct() {
		parent::__construct();
	}
	
	protected function authenticate() {
		return Security::check();
	}
}