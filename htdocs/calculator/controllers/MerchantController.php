<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (5 Sep 2013)
 */
class MerchantController extends BackController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function show() {
		/* @var $user User */
		$user = User::fetchById((int)$_GET['id']);
		if (!$user) {
			$this->addError('Unable to find a user with that ID');
			$this->redirectTo('UserController->index');
			
		} else if ($user->isAdmin) {
			$this->addError('No details are available for administrative accounts.');
			$this->redirectTo('UserController->index');
		}
		
		//Get merchant's installer list
		if (isset($_GET['s'])) {
			$installers = Installer::fetch('merchantId = :id AND archived = FALSE AND (id LIKE :q OR name LIKE :q OR email LIKE :q OR telephone LIKE :q OR companyName LIKE :q)', array(':id' => $user->id, ':q' => '%'.$_GET['s'].'%'));
		} else {
			$installers = Installer::fetch('merchantId = :id AND archived = FALSE', array(':id' => $user->id));
		}
		
		$this->display('show', array(
				'merchant' => $user,
				'merchantDetail' => MerchantDetail::fetchByUser($user),
				'groups' => MerchantGroup::fetchAll('name'),
				'installers' => $installers));
	}
	
	public function add() {
		$this->display('merchantForm', array(
				'groups' => MerchantGroup::fetchAll('name'),
				'editMode' => false));
	}
	
	public function edit() {
		/* @var $user User */
		$user = User::fetchById((int)$_GET['id']);
		if (!$user) {
			$this->addError('Unable to find a user with that ID');
			$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id);
		}
		
		$this->display('merchantForm', array(
				'merchant' => $user,
				'merchantDetail' => MerchantDetail::fetchByUser($user),
				'groups' => MerchantGroup::fetchAll('name'),
				'editMode' => true));
	}
	
	public function create() {
		try {
			User::startTransaction();
			$user = new User();
			$user->userName = $_POST['userName'];
			$user->isAdmin = false;
			
			$date = new DateTime();
			$user->registeredTime = $date->format('Y-m-d H:i:s');
			if ($_POST['password'] != $_POST['passwordRep'])
				throw new Exception('Passwords don\'t match');
			
			$user->password = Security::generatePassword($user, $_POST['password']);
			$user->id = $user->save();
			
			if (isset($_POST['allowLogin']) && $_POST['allowLogin'])
				UserPerm::assign($user, UserPerm::PERM_LOGIN);
			
			$merchantDetail = new MerchantDetail();
			$merchantDetail->id = $user->id;
			$merchantDetail->merchantGroupId = $_POST['merchantGroupId'];
			$merchantDetail->contactName = $_POST['contactName'];
			$merchantDetail->companyName = $_POST['companyName'];
			$merchantDetail->contactEmail = $_POST['contactEmail'];
			$merchantDetail->telephone = $_POST['telephone'];
			$merchantDetail->address = $_POST['address'];
			$merchantDetail->notes = $_POST['notes'];
			$merchantDetail->save();
			
			User::commit();
			$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id);
			
		} catch (Exception $e) {
			User::rollback();
			error_log('Unable to create User: ' . $e);
			$this->addError('Unable to create User: ' . $e->getMessage());
			$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id);
		}
	}
	
	public function update() {
		try {
			User::startTransaction();
			/* @var $user User */
			$user = User::fetchById((int)$_POST['id']);
			
			if (!$user)
				throw new Exception('Unable to find user');
			
			$user->userName = $_POST['userName'];
			if ($_POST['password'] != $_POST['passwordRep'])
				throw new Exception('Passwords don\'t match');
			
			if (isset($_POST['password']) && $_POST['password'] != '')
				$user->password = Security::generatePassword($user, $_POST['password']);
			$user->save();
		
			if (isset($_POST['allowLogin']) && $_POST['allowLogin'] && !$user->hasPerm(UserPerm::PERM_LOGIN))
				UserPerm::assign($user, UserPerm::PERM_LOGIN);
			else if (!isset($_POST['allowLogin']) && $user->hasPerm(UserPerm::PERM_LOGIN))
				UserPerm::revokePerm($user, UserPerm::PERM_LOGIN);
				
			$merchantDetail = MerchantDetail::fetchById($user->id);
			$merchantDetail->merchantGroupId = $_POST['merchantGroupId'];
			$merchantDetail->contactName = $_POST['contactName'];
			$merchantDetail->companyName = $_POST['companyName'];
			$merchantDetail->contactEmail = $_POST['contactEmail'];
			$merchantDetail->telephone = $_POST['telephone'];
			$merchantDetail->address = $_POST['address'];
			$merchantDetail->notes = $_POST['notes'];
			$merchantDetail->save();
			
			User::commit();
			$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id);
			
		} catch (Exception $e) {
			User::rollback();
			error_log('Unable to update merchant: ' . $e);
			$this->addError('Unable to update merchant: ' . $e->getMessage());
			$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id);
		}
	}
	
	public function delete() {
		try {
			User::startTransaction();
			/* @var $user User */
			$user = User::fetchById((int)$_POST['id']);
				
			if (!$user)
				throw new Exception('Unable to find user');
			
			if (Installer::count('merchantId = ' . $user->id) == 0) {
				$user->delete();
				
			} else {
				UserPerm::revokePerm($user, UserPerm::PERM_LOGIN);
				$detail = MerchantDetail::fetchByUser($user);
				$detail->archived = true;
				$detail->save();
			}
			
			User::commit();
			
		} catch (Exception $e) {
			User::rollback();
			error_log('Unable to delete merchant: ' . $e);
			$this->addError('Unable to delete merchant:' . $e->getMessage());
		}
	
		$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id);
	}
	
	public function reinstate() {
		try {
			User::startTransaction();
			$user = User::fetchById((int)$_GET['id']);
			
			if (!$user)
				throw new Exception('Unable to find merchant');

			$detail = MerchantDetail::fetchByUser($user);
			$detail->archived = false;
			$detail->save();
			UserPerm::assign($user, UserPerm::PERM_LOGIN);
			
			User::commit();
			
		} catch (Exception $e) {
			User::rollback();
			error_log('Unable to reinstate merchant: ' . $e);
			$this->addError('Unable to reinstate merchant: ' . $e->getMessage());
		}
		
		$this->redirectTo('MerchantGroupController->show/'.$_SESSION['lastGroupAccess']->id.'?archived');
	}
}