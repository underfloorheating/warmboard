<?php
/**
 * Used to manage products and their any calculation attributes.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (23 Jul 2013)
 */
class ProductController extends BackController {
	
	const ITEMS_PER_PAGE = 10;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function authenticate() {
		return parent::authenticate() && $_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS);
	}
	
	/**
	 * Lists the products, systems, etc. available.
	 */
	public function index() {
		if (isset($_GET['page']) && is_numeric($_GET['page']))
			$pagenation = new Paginater($_GET['page'], self::ITEMS_PER_PAGE);
		else
			$pagenation = new Paginater(1, self::ITEMS_PER_PAGE);
		
		$maxPages = $pagenation->numPages(Product::count());
		if ($pagenation->getPage() > $maxPages)
			$pagenation->setPage($maxPages);
		
		$products = Product::fetchLimit($pagenation->getLimit(), $pagenation->getStart(), 'typeId, code');
		
		$types = array();
		foreach ($products as $product) {
			if (!isset($types[$product->typeId]))
				$types[$product->typeId] = ProductType::fetchById($product->typeId);
		}
			
		
		$this->display('list', array(
				'products' => $products,
				'maxPages' => $maxPages,
				'types' => $types,
				'curPage' => $pagenation->getPage()));
	}
	
	public function create() {
		$this->display('productForm', array(
				'types' => ProductType::fetchAll(),
				'editMode' => false,
				'types' => ProductType::fetchAll(),
				'pipeTypes' => PipeType::fetchAll(),
				'tempLevels' => TemperatureLevel::fetchAll('name'),
				'ranges' => HeatSourceRange::fetchAll('rangeMin, rangeMax'),
				'systems' => System::fetchAll(),
				'constructionTypes' => ConstructionType::fetchAll(true),
				'systemTypes' => SystemType::fetchAll(),
				'curPage' => (int) $_GET['page']));
	}
	
	public function addProduct() {
		$product = new Product();

		Product::startTransaction();
		try {
			$product->code = $_POST['code'];
			$product->name = $_POST['name'];
			$product->price = (float) $_POST['price'];
			$product->available = isset($_POST['available']);
			$product->description = $_POST['description'];
			$product->typeId = (int) $_POST['typeId'];
			
			$product->id = $product->save();
			$this->addOrUpdateData($product);
			
			Product::commit();
			
		} catch (Exception $e) {
			Product::rollback();
			$this->addError('Unable to create product: ' . $e->getMessage());
			error_log($e);
		}
		
		$this->redirectTo('ProductController->index?page=' . $_POST['page']);
	}
	
	public function edit() {
		$product = Product::fetchById((int) $_GET['productId'], true);
		if ($product == null) {
			$this->addError('Unable to find that product in my database');
			$this->redirectTo('ProductController->index?page=' . $_GET['page']);
		}
		
		$values = array(
				'editMode' => true,
				'types' => ProductType::fetchAll(),
				'product' => $product,
				'pipeTypes' => PipeType::fetchAll(),
				'tempLevels' => TemperatureLevel::fetchAll('name'),
				'ranges' => HeatSourceRange::fetchAll('rangeMin, rangeMax'),
				'systems' => System::fetchAll(),
				'constructionTypes' => ConstructionType::fetchAll(true),
				'curPage' => isset($_GET['page']) ? (int) $_GET['page'] : 1,
				'systemTypes' => SystemType::fetchAll(),
				'specData' => $this->getProductData($product));
		if (isset($_GET['backPath']) && $_GET['backPath'] == 'system')
			$values['backPath'] = 'SystemController->view';
		
		$this->display('productForm', $values);
	}
	
	public function update() {
		/* @var $product Product */
		$product = Product::fetchById((int)$_POST['productId'], true);
		if ($product == null) {
			$this->addError('Unable to find that product in my database');
			$this->redirectTo('ProductController->index?page=' . $_GET['page']);
		}
		
		Product::startTransaction();
		try {
			$product->code = $_POST['code'];
			$product->name = $_POST['name'];
			$product->price = (float) $_POST['price'];
			$product->available = isset($_POST['available']);
			if (isset($_POST['typeId']) && $product->typeId != $_POST['typeId'])
				$this->removeData($product);
			$product->description = $_POST['description'];
			
			if (isset($_POST['deleteProductImage'])) {
				ImageHelper::deleteImage(ImageHelper::PRODUCT_DIR, $product->productImage);
				$product->productImage = '';
			}
			
			if (!empty($_FILES['productImage']['name'])) {
				if (!empty($product->productImage))
					ImageHelper::deleteImage(ImageHelper::PRODUCT_DIR, $product->productImage);
				$product->productImage = ImageHelper::storeImage(ImageHelper::PRODUCT_DIR, $_FILES['productImage']);
			}
			
			$this->addOrUpdateData($product);
			
			$product->save();
			Product::commit();
			
		} catch (Exception $e) {
			Product::rollback();
			$this->addError('Unable to update product: ' . $e->getMessage());
			error_log($e);
		}
		if (isset($_POST['backPath']))
			$this->redirectTo($_POST['backPath']);
		else
			$this->redirectTo('ProductController->index?page=' . $_POST['page']);
	}
	
	public function delete() {
		$product = Product::fetchById((int)$_GET['productId']);
		if ($product == null) {
			$this->displayJSON(array('status' => 'That product was not found'));
			
		} else {
			$this->removeData($product);
			$product->delete();
			
			$this->displayJSON(array('status' => 'OK'));
		}
	}
	
	/**
	 * Fetches the product data depending on what type of product it is.
	 * 
	 * @param Product $product	The product for which the data is required.
	 * @return ProductData		The data requested or null if none was found or
	 * 							data is not applicable for the type of product.
	 */
	private function getProductData(Product $product) {
		switch ($product->typeId) {
			case ProductType::PRODUCTTYPE_MANIFOLD:
				$data = ManifoldData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_FITTING:
				$data = FittingsData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_THERMOSTAT:
			case ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM:
				$data = ThermostatData::fetchById($product->id);
				break;
				
			case ProductType::PRODUCTTYPE_ACTUATOR:
				$data = ActuatorData::fetchById($product->id);
				break;
				
			case ProductType::PRODUCTTYPE_PIPE:
				$data = PipeData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD:
				$data = WarmaBoardData::fetchById($product->id);
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL:
				$data = NeoFloatingData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED:
				$data = NeoSuspendedData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT:
				$data = ScreedCastillationMatData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL:
				$data = ScreedClipRailData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES:
				$data = ScreedStaplesData::fetchById($product->id);
				break;
		
			case ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING:
			case ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED:
				$data = AluSpreaderData::fetchById($product->id);
				break;
				
			case ProductType::PRODUCTTYPE_WARMBOARD_ENDPIECE:
				$data = WarmBoardEndSupportData::fetchById($product->id);
				break;
				
			case ProductType::PRODUCTTYPE_HEAT_SOURCE:
				$data = HeatSourceData::fetchById($product->id);
				break;
				
			default:
				return null;
		}
		
		return $data;
	}
	
	/**
	 * Removes any data attached to the given product.
	 * 
	 * @param Product $product	The product which the data needs removing from.
	 */
	private function removeData(Product $product) {
		$data = $this->getProductData($product);		
		if ($data != null)
			$data->delete();
	}
	
	/**
	 * Uses GET params to either add or update product data.
	 * If the data exists it will be updated, if it is not found then it will
	 * be created.
	 * 
	 * @param Product $product	The product to which the data will be attached.
	 */
	private function addOrUpdateData(Product $product) {
		$data = $this->getProductData($product);

		//Store product specific data depending on the type of product.
		switch ($product->typeId) {
			case ProductType::PRODUCTTYPE_MANIFOLD:
				if ($data == null)
					$data = new ManifoldData();
				
				$data->maxCircuits = $_POST['manifold']['maxCircuits'];
				break;
				
			case ProductType::PRODUCTTYPE_FITTING:
				if ($data == null)
					$data = new FittingsData();
				
				$data->isSingleCircuit = isset($_POST['fitting']['isSingleCircuit']);
				$data->pipeWidth = $_POST['fitting']['pipeWidth'];
				break;
				
			case ProductType::PRODUCTTYPE_THERMOSTAT:
			case ProductType::PRODUCTTYPE_ZONE_WIRING_SYSTEM:
				if ($data == null)
					$data = new ThermostatData();
				
				$data->isWireless = isset($_POST['thermostat']['isWireless']);
				
				if (isset($_POST['thermostat']['useOnSingleCircuit']))
					$data->useOnSingleCircuit = $_POST['thermostat']['useOnSingleCircuit'];
				else
					$data->useOnSingleCircuit = ThermostatData::USE_BOTH;
				
				if (isset($_POST['thermostat']['useOnSingleRoom']))
					$data->useOnSingleRoom = $_POST['thermostat']['useOnSingleRoom'];
				else
					$data->useOnSingleRoom = ThermostatData::USE_BOTH;
				
				break;
				
			case ProductType::PRODUCTTYPE_ACTUATOR:
				if ($data == null)
					$data = new ActuatorData();
				
				if (isset($_POST['actuator']['useOnSingleCircuit']))
					$data->useOnSingleCircuit = $_POST['actuator']['useOnSingleCircuit'];
				else
					$data->useOnSingleCircuit = ActuatorData::USE_BOTH;
				
				if (isset($_POST['actuator']['useOnSingleThermometer']))
					$data->useOnSingleThermostat = $_POST['actuator']['useOnSingleThermostat'];
				else
					$data->useOnSingleThermostat = ActuatorData::USE_BOTH;
				
				break;
				
			case ProductType::PRODUCTTYPE_PIPE:
				if ($data == null)
					$data = new PipeData();
				
				$data->pipeLength = $_POST['pipe']['pipeLength'];
				$data->pipeWidth = $_POST['pipe']['pipeWidth'];
				$data->pipeTypeId = $_POST['pipe']['pipeTypeId'];
				$data->maxDistToManifold = $_POST['pipe']['maxDistToManifold'];
				$data->requirePipeBend = isset($_POST['pipe']['requirePipeBend']); 
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_WARMABOARD:
				if ($data == null)
					$data = new WarmaBoardData();
				
				$data->boardSize = $_POST['warmBoard']['boardSize'];
				$data->boardsPerGlue = $_POST['warmBoard']['boardsPerGlue'];
				$data->boardDepth = $_POST['warmBoard']['boardDepth'];
				$data->isLight = isset($_POST['warmBoard']['isLight']);
				if (isset($_POST['warmBoard']['systemId']) && $data->systemId != $_POST['warmBoard']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['warmBoard']['systemId']))
					$data->systemId = $_POST['warmBoard']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_ALUSPREADER_FLOATING:
			case ProductType::PRODUCTTYPE_ALUSPREADER_SUSPENDED:
				if ($data == null)
					$data = new AluSpreaderData();
				
				$data->boardSize = $_POST['alu']['boardSize'];
				$data->insulationArea = $_POST['alu']['insulationArea'];
				$data->coverageArea = $_POST['alu']['coverageArea']/100;
				$data->boardDepth = $_POST['alu']['boardDepth'];
				if (isset($_POST['alu']['systemId']) && $data->systemId != $_POST['alu']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['alu']['systemId']))
						$data->systemId = $_POST['alu']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED:
				if ($data == null)
					$data = new NeoSuspendedData();
				
				$data->boardSize = $_POST['neo-sus']['boardSize'];
				$data->coverageArea = $_POST['neo-sus']['coverageArea']/100;
				$data->boardDepth = $_POST['neo-sus']['boardDepth'];
				if (isset($_POST['neo-sus']['systemId']) && $data->systemId != $_POST['neo-sus']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['neo-sus']['systemId']))
					$data->systemId = $_POST['neo-sus']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING:
				if ($data == null)
					$data = new NeoFloatingData();
				
				$data->boardSize = $_POST['neo-floating']['boardSize'];
				$data->boardDepth = $_POST['neo-floating']['boardDepth'];
				if (isset($_POST['neo-floating']['systemId']) && $data->systemId != $_POST['neo-floating']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['neo-floating']['systemId']))
					$data->systemId = $_POST['neo-floating']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_SCREED_STAPLES:
				if ($data == null)
					$data = new ScreedStaplesData();
				
				$data->staplesPerPipeArea = $_POST['screed-staples']['staplesPerPipeArea'];
				$data->staplesPerBag = $_POST['screed-staples']['staplesPerBag'];
				if (isset($_POST['screed-staples']['systemId']) && $data->systemId != $_POST['screed-staples']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['screed-staples']['systemId']))
					$data->systemId = $_POST['screed-staples']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL:
				if ($data == null)
					$data = new ScreedClipRailData();
				
				$data->clipRailsPerArea = $_POST['screed-clip']['clipRailsPerArea'];
				if (isset($_POST['screed-clip']['systemId']) && $data->systemId != $_POST['screed-clip']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['screed-clip']['systemId']))
					$data->systemId = $_POST['screed-clip']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT:
				if ($data == null)
					$data = new ScreedCastillationMatData();
				
				$data->castellationArea = $_POST['screed-mat']['castellationArea'];
				if (isset($_POST['screed-mat']['systemId']) && $data->systemId != $_POST['screed-mat']['systemId'])
					$this->removeSystemData($data);
				if (isset($_POST['screed-mat']['systemId']))
					$data->systemId = $_POST['screed-mat']['systemId'];
				$this->updateSystemData($product, $data);
				break;
				
			case ProductType::PRODUCTTYPE_WARMBOARD_ENDPIECE:
				if ($data == null)
					$data = new WarmBoardEndSupportData();
				
				$data->depth = $_POST['warmBoard-end']['depth'];
				$data->numPerBoards = $_POST['warmBoard-end']['numPerBoards'];
				break;
				
			case ProductType::PRODUCTTYPE_HEAT_SOURCE:
				if ($data == null)
					$data = new HeatSourceData();
				
				$data->tempLevelId = $_POST['heatSource']['tempLevelId'];
				$data->productUrl = trim($_POST['heatSource']['productUrl']);
				
				if (!$data->isNew())
					HeatSourceRange::resetProductAssignments($data);
				
				foreach($_POST['heatSource']['ranges'] as $rangeId) {
					$range = HeatSourceRange::fetchById((int)$rangeId);
					if (!$range)
						throw new Exception('Unable to find range: '.(int)$rangeId);
					
						
					if ($data->isNew()) {
						//Needs to have a reference to save quantities against.
						$data->id = $product->id;
						$data->save();
					}
					
					$range->assignProduct($data, $_POST['heatSource']['qty'][(int)$rangeId]);
				}
				break;
				
			default:
				return;
		}
		
		if ($data != null) {
			$data->id = $product->id;
			$data->save();
		}
	}
	
	private function removeSystemData(SystemData $data) {
		if ($data != null) {
			$data->systemId = null;
			unset($data->_system);
		}
	}
	
	private function updateSystemData(Product $product, SystemData $data) {
		/* @var $system System */
		$system = isset($data->_system) ? $data->_system : System::fetchById($data->systemId);
		
		if ($system->isAssignedToProduct($product->id))
			throw new Exception("The system {$system->name} is already in use with another product.");

		$system->name = $_POST['systemName'][$data->systemId];
		$system->constructionTypeId = $_POST['systemConstructionId'][$data->systemId];
		$system->pipeWidth = $_POST['systemPipeWidth'][$data->systemId];
		$system->available = isset($_POST['systemAvailable'][$data->systemId]);
		
		$systemImages = array();
		foreach($_FILES['systemImage'] as $key1 => $value1)
			foreach($value1 as $key2 => $value2)
				$systemImages[$key2][$key1] = $value2;
		
		if (isset($_POST['deleteSystemImage'][$data->systemId])) {
			ImageHelper::deleteImage(ImageHelper::SYSTEM_DIR, $system->image);
			$system->image = '';
		}
		
		if (!empty($systemImages[$data->systemId]['name'])) {
			if (!empty($system->image))
				ImageHelper::deleteImage(ImageHelper::SYSTEM_DIR, $system->image);
			$system->image = ImageHelper::storeImage(ImageHelper::SYSTEM_DIR, $systemImages[$data->systemId]);
		}
		
		if (isset($_POST['systemTypeId'][$data->systemId]))
			$system->systemTypeId = (int) $_POST['systemTypeId'][$data->systemId];
		
		//Update allowed pipes
		PipeType::setSystemAllowance($system, $_POST['systemPipes'][$data->systemId]);
		unset($system->_allowedPipes);
		
		//Update allow temperature levels
		TemperatureLevel::setAllowedTemperatureLevels($system, $_POST['systemTempLevels'][$data->systemId]);
		unset($system->_allowedTemperatureLevels);
		
		$system->save();
	}
}