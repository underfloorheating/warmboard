<?php
/**
 * The front controller, front facing controllers must extend this class.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2013-04-25)
 */
abstract class FrontController extends Controller {
	
	public function __construct() {
		parent::__construct();
	}
}