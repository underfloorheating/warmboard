<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jun 2013)
 */
class SystemController extends CalculatorController {
	
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * AJAX function used to get the types of pipe that can be used with a
	 * certain system.
	 */
	public function getPipeTypes() {
		$system = System::fetchById((int) $_GET['systemId']);
		
		if ($system) {
			$out = array();
			foreach(PipeType::fetchBySystemAllowance($system) as $i)
				$out[] = array('id' => $i->id, 'typeName' => $i->typeName);
			
			$this->displayJSON(array('status' => 'OK', 'pipes' => $out));
			
		} else {
			$this->displayJSON(array('status' => 'Unable to find that system on record.'));
		}
	}
	
	public function getTemperatureLevels() {
		/* @var $system System */
		$system = System::fetchById((int) $_GET['systemId']);
		
		if ($system) {
			$out = array();
			$hasShowingTemperatureLevel = false;
			foreach ($system->getAllowedTemperatureLevels() as $i) {
				$out[] = array('id' => $i->id, 'name' => $i->name, 'pipeCentres' => $i->pipeCentres, 'hidden' => $i->hidden);
			}
			
			$this->displayJSON(array('status' => 'OK', 'tempLevels' => $out, 'showPropertyType' => (!isset($this->data->propertyTypeId) && $system->isHeatSourceEnabled())));
			
		} else {
			$this->displayJSON(array('status' => 'Unable to find that system on record.'));
		}
	}
	
	/**
	 * Lists the current systems allowing the user to see what is used where
	 * and add and remove systems.
	 */
	public function view() {
		if (!Security::check() || !$_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)) {
			$this->addError('You must be logged in to view this page');
			$this->redirectTo('ApplicationController->index');
		}
		
		$systems = System::fetchAll();
		$usedBy = array();
		foreach ($systems as $system) {
			try {
				$data = $system->getData();
				if ($data != null)
					$usedBy[$system->id] = Product::fetchById($data->id, true);
				else
					throw new InvalidArgumentException();
				
			} catch (InvalidArgumentException $e) {
				$usedBy[$system->id] = false;
			}
		}
		
		$this->display('list', array(
				'systems' => $systems,
				'usedBy' => $usedBy,
				'systemTypes' => SystemType::fetchAll(),
				'constructionTypes' => ConstructionType::fetchAll(true),
				'pipeTypes' => PipeType::fetchAll(),
				'tempLevels' => TemperatureLevel::fetchAll()));
	}
	
	/**
	 * An AJAX function which tests if a system has been assigned to a product
	 * already.
	 */
	public function isAssigned() {
		if (!Security::check() || !$_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)) {
			$this->displayJSON(array('status' => 'You must be logged in to view this page'));
			return;
		}
		
		try {
			$system = System::fetchById((int) $_GET['systemId']);
			if ($system == null) {
				$this->displayJSON(array('status' => 'Unable to find that system'));
				return;
			}
			$result = $system->isAssignedToProduct(isset($_GET['productId']) ? $_GET['productId'] : false);
			$this->displayJSON(array('status' => 'OK', 'result' => $result));
			
		} catch (InvalidArgumentException $e) {
			$this->displayJSON(array('status' => 'OK', 'result' => false));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
	
	/**
	 * An AJAX function which deletes a system.
	 * 
	 * If the system is currently in use an error will be returned.
	 */
	public function delete() {
		if (!Security::check() || !$_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)) {
			$this->displayJSON(array('status' => 'You must be logged in to view this page'));
			return;
		}
		
		/* @var $system System */
		$system = System::fetchById((int) $_GET['systemId']);
		if ($system == null) {
			$this->displayJSON(array('status' => 'That system was not found in my database.'));
			
		} else if ($system->isAssignedToProduct()) {
			$this->displayJSON(array('status' => 'System is assigned to a product. Please edit the product to unassign before deleting this system.'));
			
		} else {
			try {
				$system->delete();
				$this->displayJSON(array('status' => 'OK'));
				
			} catch (Exception $e) {
				$this->displayJSON(array('status' => $e->getMessage()));
				error_log($e);
			}
		}
	}
	
	/**
	 * An AJAX function which creates a new system.
	 */
	public function create() {
		if (!Security::check() || !$_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)) {
			$this->displayJSON(array('status' => 'You must be logged in to view this page'));
			return;
		}
		try {
			$system = new System();
			$system->constructionTypeId = $_GET['systemConstructionId'];
			$system->name = $_GET['name'];
			$system->pipeWidth = $_GET['pipeWidth'];
			$system->available = isset($_GET['available']);
			$system->systemTypeId = $_GET['systemTypeId'];
			$system->id = $system->save();
			PipeType::setSystemAllowance($system, $_GET['pipes']);
			TemperatureLevel::setAllowedTemperatureLevels($system, $_GET['tempLevels']);
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => 'Unable to create system: ' . $e->getMessage()));
			error_log($e);
		}
	}
	
	/**
	 * An AJAX function which updates a system
	 */
	public function update() {
		if (!Security::check() || !$_SESSION['loggedUser']->hasPerm(UserPerm::PERM_MANAGE_PRODUCTS)) {
			$this->displayJSON(array('status' => 'You must be logged in to view this page'));
			return;
		}
		
		$system = System::fetchById((int) $_GET['systemId']);
		if ($system == null) {
			$this->displayJSON(array('status' => 'That system was not found in my database.'));
			return;
		}
		
		try {
			$system->constructionTypeId = $_GET['systemConstructionId'];
			$system->name = $_GET['name'];
			$system->pipeWidth = $_GET['pipeWidth'];
			$system->available = isset($_GET['available']);
			$system->systemTypeId = $_GET['systemTypeId'];
			$system->id = $system->save();
			PipeType::setSystemAllowance($system, isset($_GET['pipes']) ? $_GET['pipes'] : array());
			TemperatureLevel::setAllowedTemperatureLevels($system, isset($_GET['tempLevels']) ? $_GET['tempLevels'] : array());
			
			$this->displayJSON(array('status' => 'OK'));
			
		} catch (Exception $e) {
			$this->displayJSON(array('status' => $e->getMessage()));
			error_log($e);
		}
	}
	
	private function allowPropertyType(System $system, $hasShowingTemperatureLevel) {
		return !isset($this->data->propertyTypeId) &&
				$system->pipeWidth == 16 &&
				$hasShowingTemperatureLevel;
	}
}