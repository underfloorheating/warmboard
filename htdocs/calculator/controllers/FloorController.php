<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Jun 19, 2013)
 */
class FloorController extends CalculatorController {
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index() {
		$this->display('floorManage', array(
				'floors' => $this->data->_floors,
				'projectRef' => !empty($this->data->projectRef) ? $this->data->projectRef : false,
				'floorNames' => FloorName::fetchAll('orderVal')
		));
	}
	
	public function addFloor() {
		if (!isset($_GET['name']) || strlen($_GET['name']) == 0) {
			$this->displayJSON(array('status' => 'A floor must have a name.'));
			return;
			
		} else if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		$floor = new Floor();
		$floor->name = $_GET['name'];
		$this->data->_floors[] = $floor;
		$this->displayJSON(array(
				'status' => 'OK',
				'tmpId' => count($this->data->_floors) - 1));
	}
	
	public function removeFloor() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		if (isset($_GET['floorId'])) {
			error_log("TODO: Remove real floor: {$_GET['floorId']}");
			
		} else if (isset($_GET['tempId'])) {
			unset($this->data->_floors[(int) $_GET['tempId']]);
			$this->data->_floors = array_values($this->data->_floors);
			$this->displayJSON(array('status' => 'OK'));
			
		} else {
			error_log('Unable to process request: ' . print_r($_GET, true));
			$this->displayJSON(array('status' => 'Request not understood'));
		}
	}
	
	public function updateFloor() {
		if ($this->data->isLocked) {
			$this->displayJSON(array('status' => 'This calculation is locked, no changes can be made.'));
			return;
		}
		
		$this->data->_floors[(int)$_GET['tempId']]->name = $_GET['name'];
		$this->displayJSON(array('status' => 'OK'));
	}
	
	public function selectFloor() {
		if (isset($_GET['floorIndex']) && is_numeric($_GET['floorIndex']) &&
				isset($this->data->_floors[$_GET['floorIndex']])) {
				
			$this->data->_currentFloor = $this->data->_floors[(int)$_GET['floorIndex']];
			$this->redirectTo('ZoneController->index');
			
		} else {
			error_log('Unable to process request: ' . print_r($_GET, true));
			$this->addError('Request not understood');
			$this->redirectTo('FloorController->select');
		}
	}
	
	public function select() {
		if (!is_array($this->data->_floors) || count($this->data->_floors) == 0) {
			$this->addError('There must be at least one floor added to continue');
			$this->redirectTo('FloorController->index');
			
		} else {
			$this->display('floorSelect', array(
					'floors' => $this->data->_floors));
		}
	}
}