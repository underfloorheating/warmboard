<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class Manifold extends ObjectModel {
	
	const CONTROLTYPE_WIRED = 0;
	const CONTROLTYPE_WIRELESS = 1;
	
	public $ident;
	public $floorId;
	public $pipeTypeId;
	public $tempLevelId;
	public $controlType = self::CONTROLTYPE_WIRED;
	public $systemId;
	
	public $_zones;
	
	public $_area;
	public $_numCircuits;
	public $_numRooms;
	public $_pipeLength;
	
	private $_system;
	private $_selectedPipeType;
	private $_selectedTempLevel;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->ident) || strlen($this->ident) > 4)
			throw new Exception('Manifold Identifier is required and must be less than 4 characters');
		
		if (!isset($this->floorId) || !is_numeric($this->floorId))
			throw new Exception('Floor ID is required and must be a number');
		
		if (!isset($this->pipeTypeId) || !is_numeric($this->pipeTypeId))
			throw new Exception('Pipe Type ID is required and must be a number');
		
		if (!isset($this->controlType) || ($this->controlType != self::CONTROLTYPE_WIRED && $this->controlType != self::CONTROLTYPE_WIRELESS))
			throw new Exception('Control type is required and must be of either WIRED or WIRELESS');
		
		if (!isset($this->systemId) || !is_numeric($this->systemId))
			throw new Exception('System ID is required and must be a number');
	}
	
	public function getPipePerCircuit() {
		if ($this->_numCircuits == 0)
			return 0;
		return $this->_pipeLength / $this->_numCircuits;
	}
	
	/**
	 * Gets the system type which was selected for this manifold.
	 * 
	 * @return System
	 */
	public function getSystem() {
		if (!isset($this->_system))
			$this->_system = System::fetchById($this->systemId);
		
		return $this->_system;
	}
	
	public function getPipeType() {
		if (!isset($this->_selectedPipeType))
			$this->_selectedPipeType = PipeType::fetchById($this->pipeTypeId);
		
		return $this->_selectedPipeType;
	}
	
	public function getSelectedTemperatureLevel() {
		if (!isset($this->_selectedTempLevel))
			$this->_selectedTempLevel = TemperatureLevel::fetchById($this->tempLevelId);
		
		return $this->_selectedTempLevel;
	}
	
	/**
	 * Unsets the cached system and pipe type objects.
	 * 
	 * Should be used after altering the $pipTypeId and $systemId.
	 */
	public function resetCachedObjects() {
		unset($this->_system);
		unset($this->_selectedPipeType);
	}
	
	public function realignZoneArray() {
		if (is_array($this->_zones))
			$this->_zones = array_values($this->_zones);
	}
	
	public function save() {
		$id = parent::save();
		
		foreach ($this->_zones as $zone) {
			$zone->manifoldId = $id;
			$zone->id = $zone->save();
		}
		
		return $id;
	}
}