<?php
/**
 * Levels of temperature which can be added to a manifold.
 * 
 * This is for calculating pipe length on floors and can help advise customers
 * on getting a heat source product.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (29 Oct 2014)
 */
class TemperatureLevel extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var float
	 */
	public $pipeCentres;
	
	/**
	 * 
	 * @var boolean
	 */
	public $hidden;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	/**
	 * Gets temporature levels which are supported by the system given.
	 * 
	 * @param System $system
	 * @return multitype:TemperatureLevel
	 */
	public static function fetchAllowedTemperatureLevel(System $system) {
		$db = static::db();
		$records = $db->query('SELECT tl.* FROM '.DB_PREFIX.'SystemTempLevel stl INNER JOIN '.static::getTableName().' tl ON stl.tempLevelId = tl.id AND systemId = '.$system->id);
		
		$out = array();
		while ($record = $records->fetch(PDO::FETCH_ASSOC))
			$out[] = new TemperatureLevel($record);
		
		return $out;
	}
	
	/**
	 * Sets the allowed temperature levels on certain systems
	 * 
	 * @param System $system		The system to allow the levels on
	 * @param array $allowedIds		The IDs of the levels to allow.
	 */
	public static function setAllowedTemperatureLevels(System $system, array $allowedIds) {
		$allowedTemps = static::fetchAllowedTemperatureLevel($system);
		$db = static::db();
		
		// Delete temp levels which are no longer allowed, ones tht exist remove from the array.
		$tempIdx = array_flip($allowedIds);
		foreach ($allowedTemps as $temp) {
			if (!isset($tempIdx[$temp->id]))
				$db->query('DELETE FROM '.DB_PREFIX.'SystemTempLevel WHERE tempLevelId = '.$temp->id.' AND systemId = '.$system->id);
			else
				unset($allowedIds[$tempIdx[$temp->id]]);
		}
		
		//Set remaining temp levels which do not exist yet
		foreach ($allowedIds as $id)
			$db->query('INSERT INTO '.DB_PREFIX.'SystemTempLevel (tempLevelId, systemId) VALUES ('.(int)$id.', '.$system->id.')');
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 50)
			throw new Exception('Temperature Level Name is required and must be less than 50 characters');
		
		if (empty($this->pipeCentres) || !is_numeric($this->pipeCentres))
			throw new Exception('Pipe Centres is required and must be a number');
		
		if (!isset($this->hidden) || (!is_numeric($this->hidden) && !is_bool($this->hidden)))
			throw new Exception('Hidden is required and must be numeric');
	}
}