<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (8 Jul 2013)
 */
abstract class GeneralModel {
	
	private static $_conn;
	

	/**
	 * Creates a new model
	 *
	 * @param array $data		The data to place into this object.
	 */
	protected function __construct(array $data = array()) {

		if (count($data) > 0)
			$this->loadEntityData($data);
	}
	
	/**
	 * Returns a cached database connection.
	 *
	 * @return \PDO
	 */
	protected static function db() {
		if (!isset(self::$_conn)) {
			try {
				self::$_conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD,
						array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			}
			catch(PDOException $ex){
				error_log('Unable to connect to DB: ' . $ex->getMessage());
				die("Site Unavailable - Check your database");
			}
		}
		return self::$_conn;
	}

	public static function startTransaction() {
		if (!static::db()->beginTransaction())
			error_log('Unable to start transaction!');
	}

	public static function commit($startNewTransaction = false) {
		if (!static::db()->commit())
			error_log('Unable to commit changes');

		if ($startNewTransaction)
			static::startTransaction();
	}

	public static function rollback($startNewTransaction = false) {
		if (!static::db()->rollBack())
			error_log('Unable to roll back');

		if ($startNewTransaction)
			static::startTransaction();
	}

	/**
	 * Takes the input data and uses it to set the model's attributes.
	 *
	 * If there is a set method then that will be called instead of the
	 * property set.
	 *
	 * Any property which is not found within the class will be ignored.
	 *
	 * @param array $data	The data to set.
	 */
	protected function loadEntityData($data) {
		foreach ($data as $attr => $value) {
			if (method_exists($this, 'set'.ucfirst($attr)))
				$this->$attr($value);

			else if (property_exists($this, $attr))
				$this->$attr = $value;
		}
	}

	/**
	 * A standard place to get the name of the model's table.
	 */
	public static function getTableName() {
		return DB_PREFIX .str_replace('\\', '_', get_called_class());
	}

	/**
	 * Fetches objects from the database using a PDO formatted query and an array of values.
	 *
	 * @param $where string	The where clause
	 * @param $values array	The values to bind to the where clause.
	 * @param $limit int	If defined then limits the amount of objects returned.
	 * @param $start int	If defined then starts at a certain location.
	 *
	 * @return			An array of objects.
	 */
	public static function fetch($where, array $values, $limit = false, $start = false, $orderBy = false) {
		$db = static::db();

		$tableName = static::getTableName();
		$sql = $db->prepare("SELECT * FROM $tableName WHERE $where" .
				($orderBy ? " ORDER BY $orderBy" : '') .
				($limit ? ' LIMIT ' . ($start ? "$start, " : '') . $limit : ''));

		foreach($values as $key => $value)
			$sql->bindValue($key, $value);

		$sql->execute();

		$arr = array();
		$cls = get_called_class();
		foreach ($sql->fetchAll(PDO::FETCH_ASSOC) as $record) {
			$arr[] = new $cls($record);
		}

		return $arr;
	}

	/**
	 * Counts the number of records in a database which match a certain query
	 * or if no query is passed in then it returns the total count.
	 *
	 * @param string $where		The where query to select on.
	 * @param unknown $value	A list of values which are bound to the query
	 * 							in a safe manner.
	 * @return number
	 */
	public static function count($where = false, array $values = array()) {
		$db = static::db();

		$tableName = static::getTableName();
		$sql = $db->prepare("SELECT COUNT(*) FROM $tableName" . ($where ? " WHERE $where" : ''));
		foreach($values as $key => $value)
			$sql->bindValue($key, $value);

		$sql->execute();
		$out = $sql->fetchAll(PDO::FETCH_NUM);
		return $out[0][0];
	}

	/**
	 * Returns all objects from the database.
	 *
	 * @return multitype:NULL
	 */
	public static function fetchAll($orderBy = false) {
		$db = static::db();

		$tableName = static::getTableName();
		$sql = $db->prepare("SELECT * FROM $tableName" . ($orderBy ? " ORDER BY $orderBy" : ''));
		$sql->execute();

		$arr = array();
		$cls = get_called_class();
		foreach ($sql->fetchAll(PDO::FETCH_ASSOC) as $record) {
			$arr[] = new $cls($record);
		}

		return $arr;
	}

	/**
	 * Returns objects from the database with limitation.
	 * If a where is needed too, use fetch(...).
	 *
	 * @param int $limit		The maximum amount of objects to fetch.
	 * @param int $start		An optional offset
	 * @param string $orderBy	Optionally order by a column.
	 * @return multitype:NULL
	 */
	public static function fetchLimit($limit, $start = false, $orderBy = false) {
		$db = static::db();

		$tableName = static::getTableName();
		$sql = $db->prepare("SELECT * FROM $tableName " . ($orderBy ? " ORDER BY $orderBy " : '') . 'LIMIT ' . ($start ? "$start, " : '') . $limit);

		$sql->execute();

		$arr = array();
		$cls = get_called_class();
		foreach ($sql->fetchAll(PDO::FETCH_ASSOC) as $record) {
			$arr[] = new $cls($record);
		}

		return $arr;
	}

	/**
	 * Stores the model in the database.
	 *
	 * Before the save occurs GeneralModel::validate() is called to ensure all
	 * the data is safe.
	 *
	 * @return int	The ID number of the object stored.
	 */
	public function save() {
		//Should throw an exception is something is wrong.
		$this->validate();

		$tableName = static::getTableName();
		$setsArr = get_object_vars($this);

		if (!$this->isNew()) {
			return $this->update($tableName, $setsArr);
				
		} else {
			return $this->insert($tableName, $setsArr);
		}
	}

	/**
	 * Updates the object with the database.
	 *
	 * This method should not be called directly, use GeneralModel::save().
	 *
	 * @param string $tableName		The name of table to update.
	 * @param array $setsArr		An array of property names and values.
	 * @return int					The ID number of the object updated.
	 */
	protected abstract function update($tableName, array $setsArr);
	
	/**
	 * Inserts the object into the database.
	 *
	 * This method should not be called directly, use GeneralModel::save().
	 *
	 * @param string $tableName		The name of the table to insert into.
	 * @param array $setsArr		An array of property names and values.
	 */
	protected function insert($tableName, array $setsArr) {
		$keys = array();
		$values = array();
		foreach($setsArr as $key => $value) {
			if (!isset($value) || substr($key, 0, 1) == '_')
				continue;

			$keys[] = $key;
			$values[] = ":$key";
		}
			
		$keyStr = implode(', ', $keys);
		$valueStr = implode(", ", $values);
			
		error_log('Query: ' . "INSERT INTO $tableName ($keyStr) VALUES ($valueStr)");
		$sql = static::db()->prepare("INSERT INTO $tableName ($keyStr) VALUES ($valueStr)");
		foreach($setsArr as $key => $value) {
			if (!isset($value) || substr($key, 0, 1) == '_')
				continue;
			if (is_bool($value))
				$value = (int) $value;
			$sql->bindValue($key, $value);
		}
		
		$sql->execute();
		return static::db()->lastInsertId();
	}

	/**
	 * Deletes the model from the database.
	 *
	 * @return boolean	true or false depending on success.
	 */
	public abstract function delete();
	
	/**
	 * Gets if this object is new or not.
	 * This is used when saving the object and by default it depends on if the
	 * ID column is set.
	 *
	 * This method is here to be overridden if need be.
	 *
	 * @return boolean	True if this object is new and should be INSERTed
	 * 					rather than UPDATEd.
	 */
	protected abstract function isNew();

	/**
	 * Called when an object's properties require validating, especially before
	 * getting inserted or updated within table.
	 *
	 * Throw a ValidationException if something shouldn't be allowed and please
	 * make the description detailed enough to know what was wrong.
	 */
	public abstract function validate();
}