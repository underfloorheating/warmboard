<?php
/**
 * A variable enum of all the different products which are available.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class ProductType extends ObjectModel {
	
	//Product type enum (Should tally with ProductType database table)
	const PRODUCTTYPE_MANIFOLD						= 1;
	const PRODUCTTYPE_MANIFOLD_MIXER				= 2;
	const PRODUCTTYPE_FITTING						= 3;
	const PRODUCTTYPE_ACTUATOR						= 4;
	const PRODUCTTYPE_ZONE_WIRING_SYSTEM			= 5;
	const PRODUCTTYPE_THERMOSTAT					= 6;
	const PRODUCTTYPE_PIPE							= 7;
	const PRODUCTTYPE_PIPE_BEND						= 8;
	const PRODUCTTYPE_SYSTEM_WARMABOARD				= 9;
	const PRODUCTTYPE_SYSTEM_NEOFOIL				= 10;
	const PRODUCTTYPE_SYSTEM_NEOFOIL_FLOATING		= 10;
	const PRODUCTTYPE_ALUSPREADER_PLATE				= 11;
	const PRODUCTTYPE_ALUSPREADER_SUSPENDED			= 12;
	const PRODUCTTYPE_ALUSPREADER_FLOATING			= 13;
	const PRODUCTTYPE_EDGE_INSULATION				= 15;
	const PRODUCTTYPE_SYSTEM_NEOFOIL_SUSPENDED		= 18;
	const PRODUCTTYPE_SYSTEM_SCREED_STAPLES			= 19;
	const PRODUCTTYPE_SYSTEM_SCREED_CASTILLATIONMAT	= 20;
	const PRODUCTTYPE_SYSTEM_SCREED_CLIPRAIL		= 21;
	const PRODUCTTYPE_WARMBOARD_ENDPIECE			= 22;
	const PRODUCTTYPE_WARMBOARD_GLUE				= 23;
	const PRODUCTTYPE_HEAT_SOURCE					= 24;
	const PRODUCTTYPE_2_PORT_VALVE					= 25;
	const PRODUCTTYPE_RECEIVER						= 26;
	
	/**
	 * 
	 * @var string
	 */
	public $typeName;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->typeName) || strlen($this->typeName) > 50)
			throw new Exception('Product Type Name is required and must be less than 50 characters');
	}
	
	/**
	 * Gets an enum type by name.
	 * 
	 * @param string $name
	 * @return ProductType
	 */
	public static function fetchByName($name) {
		return static::fetchById(static::ordinal($name));
	}
	
	public static function ordinal($name) {
		$name = strtoupper("PRODUCTTYPE_$name");
		return static::$name;
	}
}