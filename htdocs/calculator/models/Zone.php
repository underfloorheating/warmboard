<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class Zone extends ObjectModel {
	
	/**
	 * 
	 * @var int
	 */
	public $ident;
	
	/**
	 * 
	 * @var int
	 */
	public $manifoldId;
	
	/**
	 * 
	 * @var int
	 */
	public $linkToZoneId;
	
	//Object cache
	public $_rooms;
	
	//Calc storage
	public $_area;
	
	/**
	 * 
	 * @var Zone
	 */
	public $_linkedZone;
		
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->ident) || strlen($this->ident) > 4)
			throw new Exception('Zone Identifier is required and must be no more than 4 characters long');
		
		if (!isset($this->manifoldId) || !is_numeric($this->manifoldId))
			throw new Exception('Manifold ID is required and must be a number');
		
		if (isset($this->linkToZoneId) && !is_numeric($this->linkToZoneId))
			throw new Exception('Link To Zone ID must be a number');
	}
	
	public function isLinked() {
		return isset($this->_linkedZone) || isset($this->linkToZoneId);
	}
	
	public function save() {
		$id = parent::save();
		
		foreach ($this->_rooms as $room) {
			$room->zoneId = $id;
			$room->id = $room->save();
		}
		
		return $id;
	}
}