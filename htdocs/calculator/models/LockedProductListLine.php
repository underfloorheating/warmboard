<?php
/**
 * Since the products can be edited we need to store all the data for locked
 * calculations for historical records.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (18 Jul 2013)
 */
class LockedProductListLine extends ObjectModel {
	
	/**
	 * 
	 * @var int
	 */
	public $calculationDataId;
	
	/**
	 * 
	 * @var int
	 */
	public $productId;
	
	/**
	 * 
	 * @var string
	 */
	public $originalName;
	
	/**
	 * 
	 * @var string
	 */
	public $originalCode;
	
	/**
	 * 
	 * @var float
	 */
	public $originalPrice;
	
	/**
	 * 
	 * @var int
	 */
	public $qty;
	
	/**
	 * 
	 * @var int
	 */
	public $typeId;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->calculationDataId) || !is_numeric($this->calculationDataId))
			throw new Exception('Calculation Data ID is required and must be a number');
		
		if (isset($this->productId) && !is_numeric($this->productId))
			throw new Exception('Product ID must be a number');
		
		if (empty($this->originalName) || strlen($this->originalName) > 100)
			throw new Exception('Original Product Name is required and must be less than 100 characters');
		
		if (empty($this->originalCode) || strlen($this->originalCode) > 20)
			throw new Exception('Product Code is required and must be less than 20 characters');
		
		if (!isset($this->originalPrice) || !is_numeric($this->originalPrice))
			throw new Exception('Original price is required and must be a number');
		
		if (!isset($this->qty) || !is_numeric($this->qty))
			throw new Exception('Quantity is required and must be a number');
		
		if (isset($this->typeId) && !is_numeric($this->typeId))
			throw new Exception('Type ID is required and must eb number');
	}
	
	public static function fetchByCalculation(CalculationData $calcData) {
		return static::fetch('calculationDataId = :id', array(':id' => $calcData->id));
	}
}