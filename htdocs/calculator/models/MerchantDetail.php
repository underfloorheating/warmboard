<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (22 Jul 2013)
 */
class MerchantDetail extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $contactName;
	
	/**
	 * 
	 * @var string
	 */
	public $contactEmail;
	
	/**
	 * 
	 * @var string
	 */
	public $companyName;
	
	/**
	 * 
	 * @var string
	 */
	public $telephone;
	
	/**
	 * 
	 * @var string
	 */
	public $address;
	
	/**
	 * 
	 * @var int
	 */
	public $merchantGroupId;
	
	/**
	 * 
	 * @var string
	 */
	public $notes;
	
	/**
	 * 
	 * @var MerchantGroup
	 */
	public $_merchantGroup;
	
	/**
	 * Should be set true to hide a merchant
	 * 
	 * @var boolean
	 */
	public $archived = false;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->contactName) || strlen($this->contactName) > 100)
			throw new Exception('Contact name is required and must be less than 100 characters');
		
		if (isset($this->contactEmail) && (strlen($this->contactEmail) > 100 || !filter_var($this->contactEmail, FILTER_VALIDATE_EMAIL)))
			throw new Exception('Contact Email must be less than 100 characters and in email format');
		
		if (isset($this->companyName) && strlen($this->companyName) > 50)
			throw new Exception('Company Name must be less than 50 characters');
		
		if (isset($this->telephone) && strlen($this->telephone) > 20)
			throw new Exception('Telephone must be less than 20 characters in length');
		
		if (!isset($this->archived) || (!is_numeric($this->archived) && !is_bool($this->archived)))
			throw new Exception('Archived is required and must be a boolean');
	}
	
	/**
	 * Gets the details for the merchant.
	 * 
	 * @param User $merchant	The merchant object for which we want extra detail.
	 * @throws Exception		If the User object is an admin.
	 * @return MerchantDetail
	 */
	public static function fetchByUser(User $merchant) {
		if ($merchant->isAdmin)
			throw new Exception('Administrative account passed into Merchant only function!');
		
		return static::fetchById($merchant->id);
	}
	
	/**
	 * Gets a list of merchants for a certain group.
	 * 
	 * @param MerchantGroup $group	The Group to search on.
	 * @param int $limit			Limit to a number of results.
	 * @param int $start			When limiting start from a result index.
	 * @param string $orderBy		The column name to order the results by.
	 * @param boolean $archived		True if only archived should be returned,
	 * 								false if only live merchants should be
	 * 								returned.
	 * @return array				An array containing the merchant details.
	 */
	public static function fetchByMerchantGroup(MerchantGroup $group, $limit = false, $start = false, $orderBy = false, $archived = false) {
		$archived = (int) $archived;
		$merchants = static::fetch("merchantGroupId = {$group->id} AND archived = $archived", array(), $limit, $start, $orderBy);
		foreach ($merchants as &$merchant)
			$merchant->_merchantGroup = $group;
		
		return $merchants;
	}
	
	protected function insert($tableName, array $setsArr, $skipParent = false) {
		parent::insert($tableName, $setsArr, $skipParent);
	}
	
	public function isNew() {
		return static::fetchById($this->id) == null;
	}
	
	public function getGroup() {
		if (!isset($this->_merchantGroup))
			$this->_merchantGroup = MerchantGroup::fetchById($this->merchantGroupId);
		
		return $this->_merchantGroup;
	}
	
	public static function fetchStatistics($start = false, $end = false, $best = true) {
		if ($start && $end)
			$dateFilter = 'tsLocked BETWEEN \''.date('Y-m-d H:i:s', $start).'\' AND \''.date('Y-m-d H:i:s', $end).'\'';
		
		else if ($start)
			$dateFilter = 'tsLocked > \''.date('Y-m-d H:i:s', $start).'\'';
		
		else if ($end)
			$dateFilter = 'tsLocked < \''.date('Y-m-d H:i:s', $end).'\'';
		
		else
			$dateFilter = '1';
		
		return array (
				'priceSum'	=> static::db()->query('SELECT m.id, m.contactName, cd.merchantDiscount, SUM(orderPrice.v) AS price_sum, SUM(orderPrice.v) - SUM(orderPrice.discount) AS price_discount, AVG(orderPrice.v) AS price_avg, MIN(orderPrice.v) AS price_min, MAX(orderPrice.v) as price_max, COUNT(DISTINCT cd.id) AS calc_count FROM calc_CalculationData cd INNER JOIN (SELECT calculationDataId, SUM(originalPrice) AS v, SUM(originalPrice) * cd.merchantDiscount AS discount FROM calc_LockedProductListLine ll INNER JOIN calc_CalculationData cd ON cd.id = ll.calculationDataId GROUP BY ll.calculationDataId) orderPrice ON orderPrice.calculationDataId = cd.id INNER JOIN calc_Installer i ON i.id = cd.assignedInstaller INNER JOIN calc_MerchantDetail m ON i.merchantId = m.id WHERE '.$dateFilter.' GROUP BY m.id ORDER BY price_sum'.($best ? ' DESC': ''))->fetchAll(PDO::FETCH_ASSOC),
				'calcCount'	=> static::db()->query('SELECT m.id, m.contactName, cd.merchantDiscount, SUM(orderPrice.v) AS price_sum, SUM(orderPrice.v) - SUM(orderPrice.discount) AS price_discount, AVG(orderPrice.v) AS price_avg, MIN(orderPrice.v) AS price_min, MAX(orderPrice.v) as price_max, COUNT(DISTINCT cd.id) AS calc_count FROM calc_CalculationData cd INNER JOIN (SELECT calculationDataId, SUM(originalPrice) AS v, SUM(originalPrice) * cd.merchantDiscount AS discount FROM calc_LockedProductListLine ll INNER JOIN calc_CalculationData cd ON cd.id = ll.calculationDataId GROUP BY ll.calculationDataId) orderPrice ON orderPrice.calculationDataId = cd.id INNER JOIN calc_Installer i ON i.id = cd.assignedInstaller INNER JOIN calc_MerchantDetail m ON i.merchantId = m.id WHERE '.$dateFilter.' GROUP BY m.id ORDER BY calc_count'.($best ? ' DESC': ''))->fetchAll(PDO::FETCH_ASSOC),
		);
	}
	
	public static function fetchMerchantsNotUsingMe() {
		$results = static::db()->query('SELECT md.* FROM calc_MerchantDetail md LEFT JOIN calc_Installer i ON i.merchantId = md.id LEFT JOIN calc_CalculationData cd ON cd.assignedInstaller = i.id WHERE md.archived = 0 AND i.id IS NULL OR cd.id IS NULL GROUP BY md.id ORDER BY md.contactName')->fetchAll(PDO::FETCH_ASSOC);
		$out = array();
		foreach ($results as $result)
			$out[] = new MerchantDetail($result);
		
		return $out;
	}
}