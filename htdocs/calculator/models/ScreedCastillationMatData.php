<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Jul 2013)
 */
class ScreedCastillationMatData extends SystemData {
	
	/**
	 * 
	 * @var float
	 */
	public $castellationArea;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		parent::validate();
		
		if (!isset($this->castellationArea) || !is_numeric($this->castellationArea))
			throw new Exception('Castellation Area is required and must be a number');
	}
}