<?php
/**
 * Model for floor names which appear in a drop down list on the floor
 * management page.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (22 Oct 2014)
 */
class FloorName extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var int
	 */
	public $orderVal;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if(empty($this->name) || strlen($this->name) > 100)
			throw new Exception('Name is required and must be less than 100 characters');
		
		if (empty($this->orderVal) || !is_numeric($this->orderVal))
			throw new Exception('An order value must be specified');
	}
}