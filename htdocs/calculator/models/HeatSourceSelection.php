<?php
class HeatSourceSelection extends GeneralModel {
	
	public $calculationId;
	
	public $tempLevelId;
	
	public $rangeId;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function isNew() {
		return static::count('calculationId = :c AND tempLevelId = :t AND rangeId = :r', array(
				'c' => $this->calculationId,
				't' => $this->tempLevelId,
				'r' => $this->rangeId,
		)) == 0;
	}
	
	public function validate() {
		if (!isset($this->calculationId) || !is_numeric($this->calculationId))
			throw new Exception('Calculation ID is not set');
		
		if (!isset($this->tempLevelId) || !is_numeric($this->tempLevelId))
			throw new Exception('Temperature Level is not set');
		
		if (!isset($this->rangeId) || !is_numeric($this->rangeId))
			throw new Exception('Range ID is not set');
	}
	
	public function update($tableName, array $setsArr) {
		//Can't update this table;
		throw new BadMethodCallException('This type can\'t be updated');
	}
	
	public function delete() {
		static::db()->exec('DELETE FROM '.static::getTableName().' WHERE calculationId = '.$this->calculationId.' AND tempLevelId = '.$this->tempLevelId.' AND rangeId = '.$this->rangeId);
	}
	
	public static function deleteAllForCalculation(CalculationData $data) {
		static::db()->exec('DELETE FROM '.static::getTableName().' WHERE calculationId = '.$data->id);
	}
	
	public static function fetchByCalculation(CalculationData $data) {
		return static::fetch('calculationId = :id', array('id' => $data->id));
	}
	
	public static function calculationHasSelection(CalculationData $data) {
		return static::count('calculationId = :c', array('c' => $data->id)) > 0;
	}
}