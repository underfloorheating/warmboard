<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jun 2013)
 */
class PipeType extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $typeName;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->typeName) || strlen($this->typeName) > 50)
			throw new Exception('Type Name is required and must be less than 50 characters');
	}
	
	/**
	 * Gets all the pipe types which are allowed to be used with the specified system.
	 * 
	 * @param System $system		The system to get the pipes allowed for.
	 * @return multitype:PipeType	The list of allowed pipe types.
	 */
	public static function fetchBySystemAllowance(System $system) {
		$db = static::db();
		$records = $db->query('SELECT pt.* FROM ' . DB_PREFIX . 'SystemPipeType s, ' . static::getTableName() . " pt WHERE s.pipeTypeId = pt.id AND systemId = {$system->id}");
		
		$out = array();
		while ($record = $records->fetch(PDO::FETCH_ASSOC))
			$out[] = new PipeType($record);
		
		return $out;
	}
	
	/**
	 * Sets the allowed pipe types.
	 * 
	 * @param System $system	The system to set the allowance on.
	 * @param array $pipeIds	A list of PipeType ids to set.
	 */
	public static function setSystemAllowance(System $system, array $pipeIds) {
		$allowedPipes = static::fetchBySystemAllowance($system);
		$db = static::db();
		
		// Delete pipe allowances which are no longer set, ensure allowances
		// which won't be changed are not touched.
		$pipeIdsIdx = array_flip($pipeIds);
		foreach ($allowedPipes as $pipe) {
			if (!in_array($pipe->id, $pipeIds))
				$db->query('DELETE FROM ' . DB_PREFIX . "SystemPipeType WHERE pipeTypeId = {$pipe->id} AND systemId = {$system->id}");
			else
				unset($pipeIds[$pipeIdsIdx[$pipe->id]]);
		}
		
		//Set remaining pipe allowances which do not yet exist.
		foreach ($pipeIds as $id)
			$db->query('INSERT INTO ' . DB_PREFIX . "SystemPipeType (pipeTypeId, systemId) VALUES ($id, {$system->id})");
	}
}