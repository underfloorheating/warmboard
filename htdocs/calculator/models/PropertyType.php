<?php
/**
 * 
 * @author Steven Sulley
 * @copyright Sizzle Creative (12 Sep 2014)
 */
class PropertyType extends ObjectModel {
	
	/**
	 * The name of the property type which is displayed to the user.
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * The wattage lost by this property type.
	 * 
	 * @var float
	 */
	public $wLoss;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 100)
			throw new Exception('Name is required and must be 100 characters or fewer');
		
		if (!is_numeric($this->wLoss) || $this->wLoss <= 0)
			throw new Exception('Heat Loss must be a number and greater than zero');
	}
}