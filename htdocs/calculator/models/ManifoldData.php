<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class ManifoldData extends ProductData {
	
	/**
	 * 
	 * @var int
	 */
	public $maxCircuits;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->maxCircuits) || !is_numeric($this->maxCircuits))
			throw new Exception('Maximum Circuits is required and must be a number');
	}
	
	public static function getMaxPossibleCircuits() {
		$db = static::db();
		
		$tblName = static::getTableName();
		$prodTbl = Product::getTableName();
		$tblId = 'm.' . static::getIdColumnName();
		$prodId = 'p.' . Product::getIdColumnName();
		
		$sql = $db->query("SELECT MAX(maxCircuits) FROM $tblName m, $prodTbl p WHERE $tblId = $prodId AND p.available = 1");
		$sql->execute();
		return $sql->fetchColumn();
	}
}