<?php
/**
 * Serves as the basis to any object model which should exist within the database.
 * 
 * @author Steven Sulley
 * @copyright Sizzle Creative 2013
 */
abstract class ObjectModel extends GeneralModel {
	
	public $id;
	
	/**
	 * Creates a new model
	 *
	 * @param array $data		The data to place into this object.
	 */
	protected function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	/**
	 * Gets the column name from the database which should be the primary key.
	 *
	 * By default it works it out to be 'id'.
	 * Usage notes: don't use self::getIdColumnName but instead always use
	 * static::getIdColumnName as this allows the method to be overridden by
	 * means of late static binding.
	 *
	 * @return string	The primary key's column name.
	 */
	public static function getIdColumnName() {
		return 'id';
	}
		
	/**
	 * Fetches a single object by its ID.
	 * 
	 * @param number $id	The ID to fetch
	 * @return 				The instantiated object found or null.
	 */
	public static function fetchById($id) {
		$idFld = static::getIdColumnName();
		$objs =  static::fetch("$idFld = :id", array(':id' => $id), 1);
		return count($objs) > 0 ? $objs[0] : null;
	}
	
	/**
	 * Updates the object with the database.
	 * 
	 * This method should not be called directly, use ObjectModel::save().
	 * 
	 * @param string $tableName		The name of table to update.
	 * @param array $setsArr		An array of property names and values.
	 * @return int					The ID number of the object updated.
	 */
	protected function update($tableName, array $setsArr) {
		$idFld = static::getIdColumnName();
		unset($setsArr[$idFld]);	//Don't want to save the ID again.
		
		$sets = '';
		foreach ($setsArr as $key => $value) {
			if (substr($key, 0, 1) == '_')
				continue;
		
			$sets .= "$key = :$key, ";
		}
		$sets = substr($sets, 0, -2);
		
		error_log('Query: ' . "UPDATE $tableName SET $sets WHERE $idFld = {$this->$idFld}");
			
		$sql = static::db()->prepare("UPDATE $tableName SET $sets WHERE $idFld = {$this->$idFld}");
			foreach ($setsArr as $key => $value) {
			if (substr($key, 0, 1) == '_')
				continue;
		
			if (is_bool($value))
				$value = (int) $value;
			$sql->bindValue(":$key", $value);
		}
		$sql->execute();
		return $this->$idFld;
	}
	
	protected function insert($tableName, array $setsArr, $skipParent = true) {
		if ($skipParent) {
			$idFld = static::getIdColumnName();
			unset($setsArr[$idFld]);	//Don't want to insert the ID
		}
		
		return parent::insert($tableName, $setsArr);
	}
	
	/**
	 * Deletes the model from the database.
	 * 
	 * @return boolean	true or false depending on success.
	 */
	public function delete() {
		$db = static::db();
		
		$tableName = static::getTableName();
		$idFld = static::getIdColumnName();
		
		$sql = $db->prepare("DELETE FROM $tableName WHERE $idFld = {$this->$idFld}");
		return $sql->execute();
	}
	
	/**
	 * Gets if this object is new or not.
	 * This is used when saving the object and by default it depends on if the
	 * ID column is set.
	 * 
	 * This method is here to be overridden if need be.
	 * 
	 * @return boolean	True if this object is new and should be INSERTed
	 * 					rather than UPDATEd.
	 */
	protected function isNew() {
		$idFld = static::getIdColumnName();
		return !isset($this->$idFld);
	}
}