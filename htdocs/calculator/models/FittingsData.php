<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class FittingsData extends ProductData {
	
	/**
	 * 
	 * @var boolean
	 */
	public $isSingleCircuit;
	
	/**
	 * 
	 * @var int
	 */
	public $pipeWidth;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->isSingleCircuit) || (!is_numeric($this->isSingleCircuit) && !is_bool($this->isSingleCircuit)))
			throw new Exception('Is Single Circuit is required and must be numeric');
		
		if (!isset($this->pipeWidth) || !is_numeric($this->pipeWidth))
			throw new Exception('Pipe Width is required and must be a number');
	}
}