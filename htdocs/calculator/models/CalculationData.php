<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class CalculationData extends ObjectModel {

	/**
	 *
	 * @var string
	 */
	public $recallCode;

	/**
	 *
	 * @var boolean
	 */
	public $isLocked = false;

	/**
	 *
	 * @var int
	 */
	public $assignedInstaller;

	/**
	 *
	 * @var float
	 */
	public $discount = 0;

	/**
	 * Stores the discount the merchant had at the time the calculation was
	 * locked.
	 *
	 * @var float
	 */
	public $merchantDiscount = 0;

	/**
	 *
	 * @var int
	 */
	public $tsLocked;

	/**
	 *
	 * @var boolean
	 */
	public $allowContact = false;

	/**
	 *
	 * @var string
	 */
	public $contactEmail;

	/**
	 *
	 * @var string
	 */
	public $projectRef;

	/**
	 * The selected property Id
	 *
	 * @var int
	 */
	public $propertyTypeId;

	/**
	 * The total size of the property in m2
	 *
	 * @var int
	 */
	public $propertySize = 0;

	/**
	 * Save the amount of loss for future reference.
	 *
	 * @var float
	 */
	public $wattageLoss = 0;

	/**
	 *
	 * @var int
	 */
	public $followedUp = null;

	/**
	 *
	 * @var string
	 */
	public $followedUpDate;

	/**
	 * @var Floor
	 */
	public $_currentFloor;

	//Model lists
	/**
	 * @var Floor[]
	 */
	public $_floors;

	/**
	 *
	 * @var ProductList
	 */
	public $_productList;

	/**
	 *
	 * @var Installer
	 */
	public $_installer;

	/**
	 *
	 * @var PropertyType
	 */
	private $_propertyType;

	/**
	 *
	 * @var multitype<HeatSourceSelection>
	 */
	public $_heatSourceSelections = array();

	public function __construct($data = array()) {
		parent::__construct($data);
	}

	public function validate() {
		if (isset($this->assignedInstaller) && !is_numeric($this->assignedInstaller))
			throw new Exception('Installer ID is must be a number');

		if (isset($this->discount) && !is_numeric($this->discount))
			throw new Exception('Discount must be a number');

		if (!isset($this->isLocked) || (!is_numeric($this->isLocked) && !is_bool($this->isLocked)))
			throw new Exception('isLocked must be a number');

		if (isset($this->merchantDiscount) && !is_numeric($this->merchantDiscount))
			throw new Exception('Merchant Discount must be a number');

		if (isset($this->recallCode) && strlen($this->recallCode) > 14)
			throw new Exception('Recall Code is too large');

		if (isset($this->tsLocked) && $this->tsLocked > time())
			throw new Exception('tsLocked can not be in the future');

		if (!isset($this->allowContact) || (!is_numeric($this->allowContact) && !is_bool($this->allowContact)))
			throw new Exception('Allow Contact is required and must be a number');

		if (isset($this->contactEmail) && strlen($this->contactEmail) > 100)
			throw new Exception('Contact email must be less than 100 characters in length');

		if (!isset($this->projectRef) || strlen($this->projectRef) > 200)
			throw new Exception('Project reference should be less than 200 characters in length');

		if (isset($this->propertyTypeId) && !is_numeric($this->propertyTypeId))
			throw new Exception('Invalid Property type selected');

		if (isset($this->followedUp) && !is_numeric($this->followedUp))
			throw new Exception('Invalid follow up ID provided');
	}

	public function save() {
		if ($this->isLocked)
			throw new RuntimeException('Calculation is locked, you can not save it');

		$id = parent::save();

		foreach($this->_floors as $floor) {
			$floor->calculationDataId = $id;
			$floor->id = $floor->save();
		}

		return $id;
	}

	/**
	 * A normal save won't work when the calculation is locked.
	 * This method will only save the follow up fields.
	 */
	public function saveFollowUp() {
		static::db()->exec('UPDATE '.static::getTableName().' SET followedUp = '.(isset($this->followedUp) ? (int)$this->followedUp : 'NULL').', followedUpDate = '.static::db()->quote($this->followedUpDate).' WHERE id = '.(int)$this->id);

		return (int) $this->id;
	}

	/**
	 * Checks if a calculation exists within the database.
	 *
	 * @param string $code	The code to check for.
	 * @return boolean		true if the calculation exists for this code.
	 */
	public static function existsByCode($code) {
		return static::count(
				'recallCode = :code', array(':code' => $code)) > 0;
	}

	/**
	 * Fetches calculation data by the recall code.
	 *
	 * This will only fetch the calculation code, it will not completely
	 * deserialise it and everything normally connected to it.
	 *
	 * @param string $code			The code to lookup
	 * @return NULL|CalculationData	The Calculation data for the given code.
	 */
	public static function fetchByCode($code) {
		$result = static::fetch('recallCode = :code', array(':code' => $code));
		if (count($result) == 0)
			return null;

		return $result[0];
	}

	public function getLockedProductListLines() {
		return LockedProductListLine::fetchByCalculation($this);
	}

	public function lockCalculation() {
		if (isset($this->assignedInstaller) && isset($this->discount)) {
			$this->tsLocked = date('Y-m-d H:i:s', time());
			$this->isLocked = true;
			parent::save();
		}

		return $this->isLocked;
	}

	public function getSelectedPropertyType() {
		if (!isset($this->propertyTypeId))
			return null;

		if (!isset($this->_propertyType))
			$this->_propertyType = PropertyType::fetchById($this->propertyTypeId);

		return $this->_propertyType;
	}

	public static function fetchStatistics($groupBy = StatisticsController::GROUPBY_MONTH, $start = false, $end = false) {
		switch ($groupBy) {
			case StatisticsController::GROUPBY_DAY:
				$groupByFilter = 'YEAR(tsLocked), MONTH(tsLocked), DAY(tsLocked)';
				$select = 'DATE_FORMAT(tsLocked, \'%Y%m%d\') AS datapoint, YEAR(tsLocked) AS y, MONTHNAME(tsLocked) AS m, DAY(tsLocked) AS d';
				break;

			case StatisticsController::GROUPBY_YEAR:
				$groupByFilter = 'YEAR(tsLocked)';
				$select = 'DATE_FORMAT(tsLocked, \'%Y\') AS datapoint, YEAR(tsLocked) AS y';
				break;

			case StatisticsController::GROUPBY_MONTH:
			default:
				$groupByFilter = 'YEAR(tsLocked), MONTH(tsLocked)';
				$select = 'DATE_FORMAT(tsLocked, \'%Y%m\') AS datapoint, YEAR(tsLocked) AS y, MONTHNAME(tsLocked) AS m';
				break;
		}

		if ($start && $end)
			$dateFilter = 'tsLocked BETWEEN \''.date('Y-m-d H:i:s', $start).'\' AND \''.date('Y-m-d H:i:s', $end).'\'';

		else if ($start)
			$dateFilter = 'tsLocked > \''.date('Y-m-d H:i:s', $start).'\'';

		else if ($end)
			$dateFilter = 'tsLocked < \''.date('Y-m-d H:i:s', $end).'\'';

		else
			$dateFilter = '1';

		return static::db()->query('SELECT '.$select.', isLocked AS raised, SUM(orderPrice.v) AS price_sum, AVG(orderPrice.v) AS price_avg, MIN(orderPrice.v) AS price_min, MAX(orderPrice.v) as price_max, COUNT(DISTINCT cd.id) AS calc_count FROM calc_CalculationData cd LEFT JOIN (SELECT calculationDataId, SUM(originalPrice) AS v FROM calc_LockedProductListLine GROUP BY calculationDataId) orderPrice ON orderPrice.calculationDataId = cd.id WHERE '.$dateFilter.' GROUP BY isLocked, '.$groupByFilter)->fetchAll(PDO::FETCH_ASSOC);
	}
}