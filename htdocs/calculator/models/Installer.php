<?php
/**
 * Represents an installer.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (9 Jul 2013)
 */
class Installer extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var string
	 */
	public $companyName;
	
	/**
	 * 
	 * @var string
	 */
	public $telephone;
	
	/**
	 * 
	 * @var string
	 */
	public $email;
	
	/**
	 * 
	 * @var string
	 */
	public $address;
	
	/**
	 * 
	 * @var string
	 */
	public $notes;
	
	/**
	 * 
	 * @var int
	 */
	public $merchantId;
	
	/**
	 * Should only be set to true in order to hide an installer.
	 * 
	 * @var boolean
	 */
	public $archived = false;
	
	/**
	 * 
	 * @var User
	 */
	public $_merchant;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 100)
			throw new Exception('Name is required and must be less than 100 characters');
		
		if (isset($this->companyName) && strlen($this->companyName) > 100)
			throw new Exception('Company Name is too long');
		
		if (isset($this->email) && (strlen($this->email) > 100 || !filter_var($this->email, FILTER_VALIDATE_EMAIL)))
			throw new Exception('Email must be less than 100 characters and be in the correct format.');
		
		if (isset($this->merchantId) && !is_numeric($this->merchantId))
			throw new Exception('Merchant ID must be a number');
		
		if (!isset($this->archived) || (!is_numeric($this->archived) && !is_bool($this->archived)))
			throw new Exception('Archived is required and must be a number');
	}
}