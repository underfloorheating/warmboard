<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class ThermostatData extends ProductData {
	
	/*
	 * Thermostats may be used with single circuit manifolds, multiple circuit
	 * manifolds or both.
	 */
	/**
	 * The Thermostat can be used with both single and multiple circuit manifolds.
	 * 
	 * @var int
	 */
	const USE_BOTH = 0;
	
	/**
	 * The Thermostat can be used with only single circuit manifolds.
	 * 
	 * @var int
	 */
	const USE_SINGLE = 1;
	
	/**
	 * The Thermostat can only be used with multiple circuit manifolds.
	 * @var int
	 */
	const USE_MULTI = 2;
	
	/**
	 * 
	 * @var boolean
	 */
	public $isWireless;
	
	/**
	 * 
	 * @var int
	 */
	public $useOnSingleCircuit = 0;
	
	/**
	 * 
	 * @var int
	 */
	public $useOnSingleRoom = 0;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->isWireless) || (!is_numeric($this->isWireless) && !is_bool($this->isWireless)))
			throw new Exception('Is Wireless is required and must be a number');

		if (!isset($this->useOnSingleCircuit) || !is_numeric($this->useOnSingleCircuit))
			throw new Exception('Use On Single Circuit is required and must be a number');
		
		if (!isset($this->useOnSingleRoom) || !is_numeric($this->useOnSingleRoom))
			throw new Exception('Use On Single Room is required and must be a number');
	}
}