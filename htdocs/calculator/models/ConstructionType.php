<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class ConstructionType extends ObjectModel {
	
	const TYPE_SCREED = 1;
	const TYPE_FLOATING = 2;
	const TYPE_SUSPENDED = 3;
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var boolean
	 */
	public $available;
	
	public $_systems;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 50)
			throw new Exception('Name is required and must be less than 50 characters.');
		
		if (!isset($this->available) || !is_numeric($this->available))
			throw new Exception('Available is required and must be numeric');
	}
	
	public function getSystems() {
		if (!isset($this->_systems))
			$this->_systems = System::fetchByConstructionType($this,
					isset($_SESSION['theme']) && $_SESSION['theme'] == 'warmaboard');
		
		return $this->_systems;
	}
	
	public static function fetchAll($includeHidden = false) {
		if ($includeHidden)
			return parent::fetchAll();
		
		else
			return static::fetch('available = TRUE', array());
	}
	
	public function isEmpty() {
		return count($this->getSystems()) == 0;
	}
}