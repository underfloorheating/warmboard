<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class WarmaBoardData extends SystemData {
	
	/**
	 * 
	 * @var float
	 */
	public $boardSize;
	
	/**
	 * 
	 * @var float
	 */
	public $boardsPerGlue;
	
	/**
	 * 
	 * @var float
	 */
	public $boardDepth;
	
	/**
	 * 
	 * @var boolean
	 */
	public $isLight;
		
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		parent::validate();
		
		if (!isset($this->boardSize) || !is_numeric($this->boardSize))
			throw new Exception('Number of Boards Per m2 is required and must be a number');
		
		if (!isset($this->boardsPerGlue) || !is_numeric($this->boardsPerGlue))
			throw new Exception('Amount of Glue per Boards is required and must be a number');
		
		if (!isset($this->boardDepth) || !is_numeric($this->boardDepth))
			throw new Exception('Board Depth is required and must be a number');
		
		if (!isset($this->isLight) || (!is_numeric($this->isLight) && !is_bool($this->isLight)))
			throw new Exception('Is Light is required and must be numeric');
	}
}