<?php
/**
 * An abstract class which needs to be extended by data specific to systems.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jul 2013)
 */
abstract class SystemData extends ProductData {
	
	/**
	 *
	 * @var int
	 */
	public $systemId;
	
	/**
	 *
	 * @var System
	 */
	public $_system;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	/**
	 * Gets the data for a system.
	 *
	 * @param System $system	The system data to fetch.
	 * @return SystemData		The found system data.
	 */
	public static function fetchBySystem(System $system) {
		$out = static::fetch('systemId = :id', array(':id' => $system->id), 1);
		if (!is_array($out) || count($out) == 0)
			throw new InvalidArgumentException(
					"No specific product data found for system {$system->name}[{$system->id}]");
	
		else
			return $out[0];
	}
	
	public function validate() {
		if (!isset($this->systemId) || !is_numeric($this->systemId))
			throw new Exception('System Id is required and must be a number');
	}
}