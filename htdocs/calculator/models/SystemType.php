<?php
/**
 * Each system has a specific type, this includes specific code and data as
 * well, this class allows the classes which are required and the image which
 * is to be used in the brochure.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (23 Aug 2013)
 */
class SystemType extends ObjectModel {
	
	/**
	 * The name of the system, this is shown in the backdoor and is also used
	 * as the title on the systems pages of the PDF.
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * The name of the class which contained system specific code. The class
	 * must be in controllers/system and be in the system name space.
	 * 
	 * @var string
	 */
	public $sysClzName;
	
	/**
	 * The name of the data model class which has any extra data about the
	 * system.
	 * 
	 * @var string
	 */
	public $datClzName;
	
	/**
	 * The name of the brochure image.
	 * 
	 * @var string
	 */
	public $image;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->name) || strlen($this->name) > 100)
			throw new Exception('Name is required and must be be less than 100 characters');
		
		if (!isset($this->sysClzName) || strlen($this->sysClzName) > 100)
			throw new Exception('System class name is required and must be less than 100 characters');
		
		if (!isset($this->datClzName) || strlen($this->datClzName) > 100)
			throw new Exception('Data class name is required and must be less than 100 characters');
		
		if (isset($this->image) && strlen($this->image) > 255)
			throw new Exception('System Image name must be less than 255 characters');
		
		if (!class_exists("system\\{$this->sysClzName}", true))
			throw new Exception('That system class does not appear to exist');
		
		if (!class_exists($this->datClzName, true))
			throw new Exception('That data class does not appear to exist');
	}
}