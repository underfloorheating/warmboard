<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (30 Jul 2013)
 */
class AluSpreaderData extends SystemData {
	
	/**
	 * 
	 * @var float
	 */
	public $boardSize;
	
	/**
	 * 
	 * @var float
	 */
	public $insulationArea;
	
	/**
	 * 
	 * @var float
	 */
	public $coverageArea;
	
	/**
	 * 
	 * @var float
	 */
	public $boardDepth;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		parent::validate();
		
		if (!isset($this->boardDepth) || !is_numeric($this->boardDepth))
			throw new Exception('Board Depth is required and must be a number');
		
		if (!isset($this->boardSize) || !is_numeric($this->boardSize))
			throw new Exception('Number of Boards per m2 is required and must be a number');
		
		if (!isset($this->insulationArea) || !is_numeric($this->insulationArea))
			throw new Exception('Insulation Area is required and must be a number');
		
		if (!isset($this->coverageArea) || !is_numeric($this->coverageArea))
			throw new Exception('Coverage Area is required and must be a number');
	}
}