<?php
/**
 * Extra product data for heat sources.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Oct 2014)
 */
class HeatSourceData extends ProductData {
	
	/**
	 * 
	 * @var int
	 */
	public $tempLevelId;
	
	/**
	 * 
	 * @var string
	 */
	public $productUrl;
	
	/**
	 * 
	 * @var TemperatureLevel
	 */
	public $_tempLevel;
	
	/**
	 * 
	 * @var multitype:HeatSourceRange
	 */
	private $_heatSourceRanges;
	
	private $_heatSourceQuantities;
	
	public function validate() {
		if (empty($this->tempLevelId) || !is_numeric($this->tempLevelId))
			throw new Exception('Temperature Level ID is required and must be a number');
		
		if (!empty($this->productUrl) && strpos($this->productUrl, 'http://') !== 0)
			throw new Exception('Please ensure you have a valid URL, http:// included.');
	}
	
	public function getTemperatureLevel() {
		if (!isset($this->_tempLevel))
			$this->_tempLevel = TemperatureLevel::fetchById($this->tempLevelId);
		
		return $this->_tempLevel;
	}
	
	/**
	 * Returns the heat source ranges which have been applied to this Heat
	 * Source product.
	 * 
	 * @return multitype:HeatSourceRange
	 */
	public function getHeatSourceRanges() {
		if (!isset($this->_heatSourceRanges))
			$this->_heatSourceRanges = HeatSourceRange::fetch(
					'id IN (SELECT heatSourceRangeId FROM '.DB_PREFIX.'RangeHeatSource WHERE heatSourceId = :id)',
					array('id' => $this->id));
		
		return $this->_heatSourceRanges;
	}
	
	/**
	 * Returns the quantity of product which should be added on a certain range
	 * 
	 * @param HeatSourceRange $range	The range to get the quantity for.
	 * @return int						The amount of product to add to the
	 * 									list.
	 */
	public function getHeatSourceQty(HeatSourceRange $range) {
		if (!isset($this->_heatSourceQuantities)) {
			$this->_heatSourceQuantities = array();
			$result = static::db()->query('SELECT heatSourceRangeId, amount FROM '.DB_PREFIX.'RangeHeatSource WHERE heatSourceId = '.$this->id);
			
			foreach ($result as $row)
				$this->_heatSourceQuantities[$row['heatSourceRangeId']] = $row['amount'];
		}
		
		return $this->_heatSourceQuantities[$range->id];
	}
}