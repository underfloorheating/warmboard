<?php
/**
 * Keeps details of all users using the system.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2013-04-25)
 */
class User extends ObjectModel {
	
	public $userName;		//required, max length 50
	public $password;		//required, max length 255, hash
	public $registeredTime;	//required, in past
	public $isAdmin;		//Must be false for all merchants
	
	//Object cache
	private $_perms;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->userName))
			throw new RuntimeException('userName is required');
		
		else if (strlen($this->userName) > 50)
			throw new RuntimeException('userName is too long');
		
		else if (!isset($this->registeredTime))
			throw new RuntimeException('registeredTime is required');
		
		else if ($this->registeredTime > time())
			throw new RuntimeException('registeredTime can not be in the future');
	}
	
	public function getPerms() {
		if (!isset($this->_perms))
			$this->_perms = UserPerm::fetchPermsForUser($this);
		
		return $this->_perms;
	}
	
	public function hasPerm($perm) {
		return UserPerm::hasPerm($this, $perm);
	}
	
	public function delete() {
		//Stop from deleting the super admin user.
		if ($this->id != SUPER_ADMIN)
			parent::delete();
	}
	
	public static function userNameExists($name) {
		return static::count('userName = :userName', array(':userName' => $name)) > 0;
	}
}