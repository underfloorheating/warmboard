<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (Jun 5, 2013)
 */
class Product extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var string
	 */
	public $code;
	
	/**
	 * 
	 * @var float
	 */
	public $price;
	
	/**
	 * 
	 * @var int
	 */
	public $typeId;
	
	/**
	 * 
	 * @var boolean
	 */
	public $available;
	
	/**
	 * 
	 * @var string
	 */
	public $description;
	
	/**
	 * 
	 * @var string
	 */
	public $productImage;
		
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public static function fetchByCode($code, $showHidden = false) {
		$sql = 'code = :code';

		if (!$showHidden)
			$sql .= ' AND available = 1';
		
		$objs =  static::fetch($sql, array(':code' => $code), 1);
		return count($objs) > 0 ? $objs[0] : null;
	}
	
	public static function fetchById($id, $showHidden = false) {
		if ($showHidden)
			return parent::fetchById($id);

		$idFld = static::getIdColumnName();
		$objs =  static::fetch("$idFld = :id AND available = 1", array(':id' => $id), 1);
		return count($objs) > 0 ? $objs[0] : null;
	}
	
	public function validate() {
		if (!isset($this->name) || strlen($this->name) > 100)
			throw new Exception('A Product Name is required and must be less than 100 characters');
		
		if (!isset($this->code) || strlen($this->code) > 20)
			throw new Exception('Product Code is required and must be less than 20 characters');
		
		if (!isset($this->price) || !is_numeric($this->price))
			throw new Exception('Product price is required and must be a number');
		
		if (!isset($this->typeId) || !is_numeric($this->typeId))
			throw new Exception('Product Type ID is required and must be a number');
		
		if (!isset($this->available) || (!is_numeric($this->available) && !is_bool($this->available)))
			throw new Exception('Product availability is required and must be numeric');
		
		if (isset($this->productImage) && strlen($this->productImage) > 255)
			throw new Exception('product image name is too long, must be less than 255 characters');
	}
}