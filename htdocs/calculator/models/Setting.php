<?php
/**
 * A model to represent the settings stored in the database.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (29 Jul 2013)
 */
class Setting extends GeneralModel {
	
	/**
	 * The core settings key for the maximum area the calculator will attempt
	 * to calculate.
	 * 
	 * @var string
	 */
	const CORE_MAX_ACCEPT_AREA = 'CORE_MAX_ACCEPT_AREA';
	
	/**
	 * The key to fetch the email to which notifications should be sent.
	 * 
	 * @var string
	 */
	const CORE_NOTIFICATION_EMAIL = 'CORE_NOTIFICATION_EMAIL';
	
	/**
	 * The name of the setting, used to identify each setting so must be unique.
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * The value stored in the database as a string.
	 * 
	 * @var string
	 */
	public $value;
	
	/**
	 * Settings cache to save on queries.
	 * 
	 * @var array
	 */
	private static $_cache = array();
	
	protected function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 50)
			throw new Exception('A setting Name is required and must be less than 50 characters');
		
		if (empty($this->value) || strlen($this->value) > 255)
			throw new Exception('A setting Value is required and must be less than 255 characters');
	}
	
	/**
	 * Sets or changes a setting to a value.
	 * 
	 * @param string $name	The unique name for the setting to change.
	 * @param string $value	The value of to set.
	 * 
	 * @return Setting	The model representing the setting.
	 */
	public static function storeSetting($name, $value) {
		$set = static::retrSetting($name);
		if ($set == null) {
			$set = new Setting();
			$set->name = $name;
		}
		
		$set->value = $value;
		$set->save();
		
		static::$_cache[$name] = $set;
		
		return $set;
	}
	
	/**
	 * Fetches the named setting from the database if it exists.
	 * 
	 * Fetches are cached, so this function can be called multiple times for
	 * the same setting without it costing time and resources on the DB.
	 * 
	 * @param string $name	The name of the setting to get.
	 * @return Setting		The setting requested or null if it was not found.
	 */
	public static function retrSetting($name) {
		if (isset(static::$_cache[$name]))
			return static::$_cache[$name];
		
		$set = static::fetch('name = :name', array(':name' => $name), 1);
		
		if (count($set) == 0) {
			return null;
		} else {
			static::$_cache[$name] = $set[0];
			return $set[0];
		}
	}
	
	protected function isNew() {
		return static::count('name = :name', array(':name' => $this->name)) == 0;
	}
	
	protected function update($tableName, array $setsArr) {
		$db = static::db();
		$sql = $db->prepare("UPDATE $tableName SET value = :value WHERE name = :name");
		$sql->bindValue(':value', $this->value);
		$sql->bindValue(':name', $this->name);
		
		$sql->execute();
		
		return $this->name;
	}
	
	public function delete() {
		$db = static::db();
		$sql = $db->prepare('DELETE FROM ' . static::getTableName() . ' WHERE name = :name');
		$sql->bindValue(':name', $this->name);
		
		$sql->execute();
		
		return $this->name;
	}
}