<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Jun 2013)
 */
class PipeData extends ProductData {
		
	/**
	 * 
	 * @var float
	 */
	public $pipeLength;
	
	/**
	 * 
	 * @var float
	 */
	public $pipeWidth;
	
	/**
	 * 
	 * @var int
	 */
	public $pipeTypeId;
	
	/**
	 * 
	 * @var float
	 */
	public $maxDistToManifold;
	
	/**
	 * 
	 * @var boolean
	 */
	public $requirePipeBend;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->pipeLength) || !is_numeric($this->pipeLength))
			throw new Exception('Pipe Length is required and must be a number');
		
		if (!isset($this->pipeWidth) || !is_numeric($this->pipeWidth))
			throw new Exception('Pipe Width is required and must be a number');
		
		if (!isset($this->pipeTypeId) || !is_numeric($this->pipeTypeId))
			throw new Exception('Pipe Type ID is required and must be a number');
		
		if (!isset($this->maxDistToManifold) || !is_numeric($this->maxDistToManifold))
			throw new Exception('Maximum Distance to Manifold is required and must be a number.');
		
		if (!isset($this->requirePipeBend) || (!is_numeric($this->requirePipeBend) && !is_bool($this->requirePipeBend)))
			throw new Exception('Pipe Bend Requirement is required and must be numeric');
	}
	
	public static function fetchMaxPipeLength(PipeType $pipeType, $pipeWidth) {
		$db = static::db();
		$tbl = static::getTableName();
		$tblProd = Product::getTableName();
		$id = static::getIdColumnName();
		$idProd = Product::getIdColumnName();
		
		$result = $db->query("SELECT MAX(pipeLength) FROM $tbl pd, $tblProd p WHERE pipeTypeId = {$pipeType->id} AND pipeWidth = $pipeWidth AND pd.$id = p.$idProd AND available = 1");
		return $result->fetchColumn();
	}
	
	public static function fetchMaxDistFromManifold(PipeType $pipeType, $pipeWidth) {
		$db = static::db();
		$tbl = static::getTableName();
		$tblProd = Product::getTableName();
		$id = static::getIdColumnName();
		$idProd = Product::getIdColumnName();
		
		$result = $db->query("SELECT MAX(maxDistToManifold) FROM $tbl pd, $tblProd p WHERE pipeTypeId = {$pipeType->id} AND pipeWidth = $pipeWidth AND pd.$id = p.$idProd AND available = 1");
		return $result->fetchColumn();
	}
}