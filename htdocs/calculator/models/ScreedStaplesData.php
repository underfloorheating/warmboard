<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Jul 2013)
 */
class ScreedStaplesData extends SystemData {
	
	/**
	 * 
	 * @var float
	 */
	public $staplesPerPipeArea;
	
	/**
	 * 
	 * @var int
	 */
	public $staplesPerBag;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		parent::validate();
		
		if (!isset($this->staplesPerPipeArea) || !is_numeric($this->staplesPerPipeArea))
			throw new Exception('Staples Per Pipe Area is required and must be a number');
		
		if (!isset($this->staplesPerBag) || !is_numeric($this->staplesPerBag))
			throw new Exception('Staples Per Bag is required and must eb a number');
	}
}