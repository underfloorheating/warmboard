<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (31 Jul 2013)
 */
class ScreedClipRailData extends SystemData {
	
	/**
	 * 
	 * @var float
	 */
	public $clipRailsPerArea;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		parent::validate();
		
		if (!isset($this->clipRailsPerArea) || !is_numeric($this->clipRailsPerArea))
			throw new Exception('Clip Rails Per m2 is required and must be a number');
	}
}