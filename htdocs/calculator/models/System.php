<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class System extends ObjectModel {
	
	public $name;
	public $systemTypeId;		//References SystemType
	public $constructionTypeId;	//References ConstructionType
	public $available;
	
	/**
	 * The number of pipe centres this system supports.
	 * 
	 * @deprecated	This is no longer used, please use temperature levels.
	 * @var float
	 */
	public $pipeCentres;
	
	/**
	 * 
	 * @var int
	 */
	public $pipeWidth;

	/**
	 * 
	 * @var SystemData
	 */	
	public $_data;
	
	/**
	 * 
	 * @var multitype:PipeType
	 */
	public $_allowedPipes;
	
	/**
	 * 
	 * @var multitype:TemperatureLevel
	 */
	public $_allowedTemperatureLevels;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 50)
			throw new Exception('System Name is required and must be less than 50 characters');
		
		if (!isset($this->systemTypeId) || !is_numeric($this->systemTypeId))
			throw new Exception('System Type ID is required and must be a number');
		
		if (!isset($this->constructionTypeId) || !is_numeric($this->constructionTypeId))
			throw new Exception('Construction Type ID is required and must be a number');
		
		if (!isset($this->available) || (!is_numeric($this->available) && !is_bool($this->available)))
			throw new Exception('System Availability is required and must be a number');
		
		if (!isset($this->pipeWidth) || !is_numeric($this->pipeWidth))
			throw new Exception('Pipe Width is required and must be a number');
	}
	
	/**
	 * Gets a custom system data object.
	 * These change depending on what type of system this is.
	 * 
	 * @return SystemData
	 */
	public function getData() {
		if (!isset($this->_data))
			$this->loadData();
		
		return $this->_data;
	}
	
	private function loadData() {
		$systemType = SystemType::fetchById($this->systemTypeId);
		$cls = $systemType->datClzName;
		$this->_data = $cls::fetchBySystem($this);
	}
	
	public static function fetchByConstructionType(ConstructionType $constructionType, $warmaboardOnly, $showHidden = false) {
		$sql = 'constructionTypeId = :id';
		$params = array(':id' => $constructionType->id);
		
		if (!$showHidden)
			$sql .= ' AND available = 1';
		
		if ($warmaboardOnly) {
			$ids = implode(', ', self::warmaboardIds());
			$sql .= " AND id IN ($ids)";
		}
		
		return static::fetch($sql, $params);
	}
	
	private static function warmaboardIds() {
		$systemData = WarmaBoardData::fetchAll();
		$systems = array();
		foreach ($systemData as $system)
			$systems[] = $system->systemId;
		return $systems;
	}

	/**
	 * Utility function to fetch the pipes which can be used by the system to
	 * be listed.
	 * The data is cached.
	 * 
	 * @return multitype:PipeType
	 */
	public function getAllowedPipes() {
		if (!isset($this->_allowedPipes))
			$this->_allowedPipes = PipeType::fetchBySystemAllowance($this);
	
		return $this->_allowedPipes;
	}
	
	public function getAllowedTemperatureLevels() {
		if (!isset($this->_allowedTemperatureLevels))
			$this->_allowedTemperatureLevels = TemperatureLevel::fetchAllowedTemperatureLevel($this);
		
		return $this->_allowedTemperatureLevels;
	}
	
	/**
	 * Checks if a system has already been assigned to another product.
	 *
	 * @param string $productId	If set the function will not return true if the
	 * 							system is already assigned to a product with
	 * 							the same ID.
	 * @return boolean			True if it is already assigned.
	 */
	public function isAssignedToProduct($productId = false) {
		try {
			return $this->getData() != null && (!$productId || $this->getData()->id != $productId);
			
		} catch (InvalidArgumentException $e) {
			return false;
		}
	}
	
	public function isHeatSourceEnabled() {
		$hasShowingTemperatureLevel = false;
		foreach ($this->getAllowedTemperatureLevels() as $i)
			$hasShowingTemperatureLevel |= !$i->hidden;
				
		return $this->pipeWidth == 16 && $hasShowingTemperatureLevel;
	}
	
}