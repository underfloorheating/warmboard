<?php
/**
 * Merchants can be grouped together to share discounts.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (2 Sep 2013)
 */
class MerchantGroup extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var float
	 */
	public $discount;
	
	/**
	 *
	 * @var string
	 */
	public $contactName;
	
	/**
	 *
	 * @var string
	 */
	public $contactEmail;
	
	/**
	 *
	 * @var string
	 */
	public $companyName;
	
	/**
	 *
	 * @var string
	 */
	public $telephone;
	
	/**
	 *
	 * @var string
	 */
	public $address;
	
	/**
	 *
	 * @var string
	 */
	public $notes;
	
	/**
	 * Should be set true to hide a merchant
	 *
	 * @var boolean
	 */
	public $archived = false;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 100)
			throw new Exception('A group name is required and must be no more than 1000 characters long.');
		
		if (!isset($this->discount) || !is_numeric($this->discount))
			throw new Exception('Group discount is required and must be a number');
		
		if (isset($this->contactName) && strlen($this->contactName) > 100)
			throw new Exception('Contact name must be no more than 100 characters');
		
		if (isset($this->contactEmail) && strlen($this->contactEmail) > 100)
			throw new Exception('Contact email must be no more than 100 characters');
		
		if (isset($this->companyName) && strlen($this->companyName) > 100)
			throw new Exception('Company name must be no more than 100 characters');
		
		if (isset($this->telephone) && strlen($this->telephone) > 50)
			throw new Exception('Telephone must be no more than 50 characters');
		
		if (!isset($this->archived) || (!is_numeric($this->archived) && !is_bool($this->archived)))
			throw new Exception('Archived is required and must be a boolean');
	}
}