<?php
/**
 * A contact at the end of the PDF
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (21 Oct 2014)
 */
class Contact extends ObjectModel {
	
	/**
	 * 
	 * @var string
	 */
	public $name;
	
	/**
	 * 
	 * @var string
	 */
	public $role;
	
	/**
	 * @var string
	 */
	public $region;
	
	/**
	 * 
	 * @var string
	 */
	public $email;
	
	/**
	 * 
	 * @var string
	 */
	public $telephone;
	
	/**
	 * 
	 * @var int
	 */
	public $position;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (empty($this->name) || strlen($this->name) > 300)
			throw new Exception('Name is required and must be less than 300 characters');
		
		if (empty($this->role) || strlen($this->role) > 100)
			throw new Exception('Role is required and must be less than 100 characters');
		
		if (empty($this->region) || strlen($this->region) > 200)
			throw new Exception('Region is required and must be less than 200 characters');
		
		if (empty($this->email) || strlen($this->email) > 255)
			throw new Exception('Email is required and must be less than 255 characters');
		
		if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
			throw new Exception('Email must be formatted correctly');
		
		if (empty($this->telephone) || strlen($this->telephone) > 50)
			throw new Exception('Telephone is required and must be less than 50 characters');
		
		if (empty($this->position) || !is_numeric($this->position))
			throw new Exception('Order Position is required and must be a number');
	}
}