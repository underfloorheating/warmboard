<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (28 Jun 2013)
 */
class NeoSuspendedData extends SystemData {

	/**
	 * 
	 * @var float
	 */
	public $boardSize;
			
	/**
	 * 
	 * @var float
	 */
	public $coverageArea;
	
	/**
	 * 
	 * @var float
	 */
	public $boardDepth;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		parent::validate();
		
		if (!isset($this->boardSize) || !is_numeric($this->boardSize))
			throw new Exception('The Number of Boards per Area is required and must be a number');
		
		if (!isset($this->coverageArea) || !is_numeric($this->coverageArea))
			throw new Exception('The Coverage Area is required and must be a number');
		
		if (!isset($this->boardDepth) || !is_numeric($this->boardDepth))
			throw new Exception('The Board Depth is required and must be a number');
	}
}