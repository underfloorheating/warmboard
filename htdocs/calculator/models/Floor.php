<?php
/**
 * Represents a floor in a building which contains manifolds.
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 24, 2013)
 */
class Floor extends ObjectModel {
	
	public $name;
	public $calculationDataId;
	
	public $_manifolds;
	
	public $_area;
	
	public $_nextManifoldIdent = 0;
	public $_nextZoneIdent = 0;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if(empty($this->name) || strlen($this->name) > 100)
			throw new Exception('Name is required and must be less than 100 characters');
		
		if (!isset($this->calculationDataId) || !is_numeric($this->calculationDataId))
			throw new Exception('Calculation Data ID is required and must be a number');
	}

	/**
	 * Adds a new manifold to this floor.
	 * 
	 * Note: this does not make any changes to the database.
	 * 
	 * @param $systemId int		The system ID this manifold will be connected to.
	 * 
	 * @return Manifold	The newly added manifold.
	 */
	public function addManifold($systemId, $pipeTypeId, $temperatureLevelId) {
		$manifold = new Manifold();
		
		$manifold->ident = $this->getNextManifoldIdent();
		$manifold->systemId = $systemId;
		$manifold->pipeTypeId = $pipeTypeId;
		$manifold->tempLevelId = $temperatureLevelId;
		$this->_manifolds[] = $manifold;
		
		return $manifold;
	}
	
	/**
	 * Removes a manifold from the floor by index.
	 * @param int $idx	The index in the manifold array.
	 */
	public function removeManifold($idx) {
		unset($this->_manifolds[$idx]);
	}
	
	/**
	 * Realigns the manifold array so the array indexes match properly.
	 */
	public function realignManifoldArray() {
		if (is_array($this->_manifolds)) {
			$this->_manifolds = array_values($this->_manifolds);
			
			foreach ($this->_manifolds as $manifold)
				$manifold->realignZoneArray();
		}
	}
	
	public function save() {
		$id = parent::save();
		
		foreach ($this->_manifolds as $manifold) {
			$manifold->floorId = $id;
			$manifold->id = $manifold->save();
		}
		
		return $id;
	}
	
	public function getNextManifoldIdent() {
		return ++$this->_nextManifoldIdent;
	}
	
	public function getNextZoneIdent() {
		return ++$this->_nextZoneIdent;
	}
	
	public static function fetchStatistics($start = false, $end = false) {
		if ($start && $end)
			$dateFilter = 'tsLocked BETWEEN \''.date('Y-m-d H:i:s', $start).'\' AND \''.date('Y-m-d H:i:s', $end).'\'';
	
		else if ($start)
			$dateFilter = 'tsLocked > \''.date('Y-m-d H:i:s', $start).'\'';
	
		else if ($end)
			$dateFilter = 'tsLocked < \''.date('Y-m-d H:i:s', $end).'\'';
	
		else
			$dateFilter = '1';
	
		return static::db()->query('SELECT f.name, COUNT(*) AS floorCount, AVG(floorArea) AS areaAvg, MIN(floorArea) AS areaMin, MAX(floorArea) AS areaMax, AVG(distFromManifold) AS distAvg, MIN(distFromManifold) AS distMin, MAX(distFromManifold) AS distMax, SUM(orderPrice.v) AS priceSum, AVG(orderPrice.v) AS priceAvg, MIN(orderPrice.v) AS priceMin, MAX(orderPrice.v) AS priceMax FROM calc_Floor f INNER JOIN calc_CalculationData cd ON f.calculationDataId = cd.id LEFT JOIN (SELECT calculationDataId, SUM(originalPrice) AS v FROM calc_LockedProductListLine ll INNER JOIN calc_CalculationData cd ON cd.id = ll.calculationDataId GROUP BY ll.calculationDataId) orderPrice ON orderPrice.calculationDataId = cd.id INNER JOIN (SELECT floorId, SUM(floorArea) AS floorArea, SUM(distFromManifold) AS distFromManifold FROM calc_Room r INNER JOIN calc_Zone z ON r.zoneId = z.id INNER JOIN calc_Manifold m ON z.manifoldId = m.id GROUP BY floorId) area ON f.id = area.floorId WHERE '.$dateFilter.' GROUP BY f.name ORDER BY floorCount DESC LIMIT 10')->fetchAll(PDO::FETCH_ASSOC);
	}
}
