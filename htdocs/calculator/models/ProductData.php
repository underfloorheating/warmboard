<?php
/**
 * Certain products have separate tables which contain more data, the tables
 * that represent this in the model must subclass this. 
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Jul 2013)
 */
abstract class ProductData extends ObjectModel {
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function isNew() {
		return parent::isNew() || static::count('id =' . $this->id) == 0;
	}
	
	protected function insert($tableName, array $setsArr, $skipParent = false) {
		parent::insert($tableName, $setsArr, $skipParent);
	}
}