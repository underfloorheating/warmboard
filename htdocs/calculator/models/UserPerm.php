<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 3, 2013)
 */
class UserPerm extends GeneralModel {
	
	//Permissions
	const PERM_MANAGE_USER = 1;
	const PERM_MANAGE_PRODUCTS = 2;
	const PERM_LOGIN = 3;
	const PERM_MANAGE_CONTACTS = 4;
	const PERM_MANAGE_DROPLISTS = 5;
	
	public $userpermId;
	public $userId;
	
	protected function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function canManageUsers() {
		return $this->userpermId == self::PERM_MANAGE_USER;
	}
	
	public function canManageProducts() {
		return $this->userpermId == self::PERM_MANAGE_PRODUCTS;
	}
	
	public function canLogin() {
		return $this->userpermId == self::PERM_LOGIN;
	}
	
	public function canManageContacts() {
		return $this->userpermId == self::PERM_MANAGE_CONTACTS;
	}
	
	public function canManageDropLists() {
		return $this->userpermId == self::PERM_MANAGE_DROPLISTS;
	}
	
	public static function assign(User $user, $perm) {
		$userPerm = new UserPerm();
		$userPerm->userId = $user->id;
		$userPerm->userpermId = (int) $perm;
		
		$userPerm->save();
	}
	
	public static function revokePerm(User $user, $perm) {
		$perms = $user->getPerms();
		foreach ($perms as $i) {
			if ($i->userpermId == $perm)
				$i->revoke();
		}
	}
	
	public function isNew() {
		return static::count('userpermId = :userperm AND userId = :user',
				array(':userperm' => $this->userpermId,
						':user' => $this->userId)) == 0;
	}
	
	protected function update($tableName, array $setsArr) {
		//Can't update this table;
		throw new BadMethodCallException('This type can\'t be updated');
	}
	
	protected function insert($tableName, array $setsArr) {
		$db = static::db();
		
		$db->query("INSERT INTO $tableName (userId, userpermId) VALUES ({$this->userId}, {$this->userpermId})");
		
		return $db->lastInsertId();
	}
	
	public function delete() {
		//Don't allow permissions to be removed from the super admin
		if ($this->userId == SUPER_ADMIN)
			return;
		
		$db = static::db();
		$tblName = static::getTableName();
		
		$db->query("DELETE FROM $tblName WHERE userId = {$this->userId} AND userpermId = {$this->userpermId}");
	}
	
	public static function hasPerm(User $user, $perm) {
		$perms = $user->getPerms();
		foreach ($perms as $i) {
			if ($i->userpermId == $perm) {
				return true;
			}
		}
		return false;
	}
	
	public function revoke() {
		$this->delete();
	}
	
	public static function fetchPermsForUser(User $user) {
		return static::fetch('userId = :id', array(':id' => $user->id));
	}
	
	public function validate() {
		if (!isset($this->userId) || !is_numeric($this->userId))
			throw new Exception('User ID is required and must be a number');
		
		if (!isset($this->userpermId) || !is_numeric($this->userpermId))
			throw new Exception('User Permission ID is required and must be a number');
	}
}