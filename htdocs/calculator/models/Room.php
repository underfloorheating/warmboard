<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (May 29, 2013)
 */
class Room extends ObjectModel {
	
	public $name;
	public $zoneId;
	public $distFromManifold;
	public $floorArea;
	public $hasThermostat;
	
	public $_totalPipeLength;
	public $_pipePerCircuit;
	public $_numCircuits;
	
	public function __construct($data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->name) || strlen($this->name) > 50)
			throw new Exception('Floor Level is required and must be less than 50 characters');
		
		if (!isset($this->zoneId) || !is_numeric($this->zoneId))
			throw new Exception('Zone ID is required and must be a number');
		
		if (!isset($this->distFromManifold) || !is_numeric($this->distFromManifold))
			throw new Exception('Distance from manifold is required and must be a number');
		
		if (!isset($this->floorArea) || !is_numeric($this->floorArea))
			throw new Exception('Floor area is required and must be a number');
		
		if (!isset($this->hasThermostat) || (!is_numeric($this->hasThermostat) && !is_bool($this->hasThermostat)))
			throw new Exception('Has Thermostat is required and must be a number');
	}
	
	public static function fetchStatistics($start = false, $end = false) {
		if ($start && $end)
			$dateFilter = 'tsLocked BETWEEN \''.date('Y-m-d H:i:s', $start).'\' AND \''.date('Y-m-d H:i:s', $end).'\'';
		
		else if ($start)
			$dateFilter = 'tsLocked > \''.date('Y-m-d H:i:s', $start).'\'';
		
		else if ($end)
			$dateFilter = 'tsLocked < \''.date('Y-m-d H:i:s', $end).'\'';
		
		else
			$dateFilter = '1';
		
		return static::db()->query('SELECT r.name, COUNT(*) AS roomCount, AVG(floorArea) AS areaAvg, MIN(floorArea) AS areaMin, MAX(floorArea) AS areaMax, AVG(distFromManifold) AS distAvg, MIN(distFromManifold) AS distMin, MAX(distFromManifold) AS distMax, SUM(orderPrice.v) AS priceSum, AVG(orderPrice.v) AS priceAvg, MIN(orderPrice.v) AS priceMin, MAX(orderPrice.v) AS priceMax FROM calc_Room r INNER JOIN calc_Zone z ON r.zoneId = z.id INNER JOIN calc_Manifold m ON z.manifoldId = m.id INNER JOIN calc_Floor f ON m.floorId = f.id INNER JOIN calc_CalculationData cd ON f.calculationDataId = cd.id LEFT JOIN (SELECT calculationDataId, SUM(originalPrice) AS v FROM calc_LockedProductListLine ll INNER JOIN calc_CalculationData cd ON cd.id = ll.calculationDataId GROUP BY ll.calculationDataId) orderPrice ON orderPrice.calculationDataId = cd.id WHERE '.$dateFilter.' GROUP BY r.name ORDER BY roomCount DESC LIMIT 10')->fetchAll(PDO::FETCH_ASSOC);
	}
}