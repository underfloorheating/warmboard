<?php
/**
 * A power range which heat sources should fall into.
 * 
 * @author Steven Sulley
 * @copyright Sizzle Creative (25 Nov 2014)
 */
class HeatSourceRange extends ObjectModel {
	
	/**
	 * The minimum kW range of products
	 *  
	 * @var float
	 */
	public $rangeMin;
	
	/**
	 * The maximum kW range of products
	 * 
	 * @var float
	 */
	public $rangeMax;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!is_numeric($this->rangeMin) || $this->rangeMin < 0)
			throw new Exception('A minimum is required and it must be positive');
		
		if (!is_numeric($this->rangeMax) || $this->rangeMax <= $this->rangeMin)
			throw new Exception('A maximum is required and it must be greater than the minimum range');
	}
	
	/**
	 * Gets all heat sources for this heat source range.
	 * Can optionally be limited by temperature level.
	 * 
	 * @param int wRange							The amount of W required.
	 * @param TemperatureLevel $tempLevel			Limit the products by a
	 * 												temperature level.
	 * @return multitype:multitype:HeatSourceData	All the heat sources found.
	 */
	public static function fetchHeatSourceData($wRange, TemperatureLevel $tempLevel = NULL) {
		$db = static::db();
		$wRange /= 1000;	 //DB stores it in kW
		error_log('Searching for heat source within range of '.$wRange.' and temp level '.$tempLevel->name);
		$result = $db->query('SELECT r.amount, d.* FROM '.HeatSourceData::getTableName().' d INNER JOIN '.DB_PREFIX.'RangeHeatSource r ON r.heatSourceId = d.id INNER JOIN '.HeatSourceRange::getTableName().' s ON r.heatSourceRangeId = s.id
				WHERE s.rangeMin <= '.$wRange.' AND s.rangeMax >= '.$wRange.(isset($tempLevel) ? ' AND d.tempLevelId = '.$tempLevel->id : ''));
		
		//Create an array of products and amounts
		$out = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$arrRow = array();
			$arrRow['amt'] = $row['amount'];
			unset($row['amount']);
			$arrRow['data'] = new HeatSourceData($row);
			$out[] = $arrRow;
		}
		
		error_log('Found: '.print_r($out, true));
		
		return $out;
	}
	
	/**
	 * Gets all the heat sources within range of the requested value and placed
	 * into options for the intermediate page.
	 * 
	 * @param int $wRange					The amount of W required.
	 * @param TemperatureLevel $tempLevel	Temperature level required.
	 * 
	 * @return multitype:HeatSourceOptionHelper	The list of options available.
	 */
	public static function fetchHeatSourceOptions($wRange, TemperatureLevel $tempLevel) {
		$wRange /= 1000;
		$ranges = HeatSourceRange::fetch('rangeMin <= :kw AND rangeMax >= :kw', array('kw' => $wRange));
		$options = array();
		foreach ($ranges as $range) {
			$option = new HeatSourceOptionHelper($tempLevel, $range);
			$products = $range->fetchHeatSources($tempLevel);
			
			if (count($products) == 0)
				continue;
			
			foreach ($products as $productRow)
				$option->addProduct($productRow['prod'], $productRow['data'], $productRow['amt']);
			
			$options[] = $option;
		}
		
		return $options;
	}
	
	public function fetchHeatSources(TemperatureLevel $tempLevel = NULL) {
		$db = static::db();
		$result = $db->query('SELECT r.amount, p.*, d.* FROM '.HeatSourceData::getTableName().' d
				INNER JOIN '.DB_PREFIX.'RangeHeatSource r ON r.heatSourceId = d.id
				INNER JOIN '.Product::getTableName().' p ON d.id = p.id
				WHERE r.heatSourceRangeId = '.$this->id.(isset($tempLevel) ? ' AND d.tempLevelId = '.$tempLevel->id : ''));
		
		$out = array();
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$arrRow = array();
			$arrRow['amt'] = $row['amount'];
			unset($row['amount']);
			$arrRow['prod'] = new Product($row);
			$arrRow['data'] = new HeatSourceData($row);
			$out[] = $arrRow;
		}
		
		return $out;
	}
	
	/**
	 * Tests if this range is currently in use by products.
	 * 
	 * @return boolean	true if this range has products assigned.
	 */
	public function isInUse() {
		$result = static::db()->query('SELECT COUNT(*) FROM '.DB_PREFIX.'RangeHeatSource WHERE heatSourceRangeId = '.$this->id);
		return $result && $result->fetchColumn() > 0;
	}
	
	public static function resetProductAssignments(HeatSourceData $heatSourceData) {
		static::db()->exec('DELETE FROM '.DB_PREFIX.'RangeHeatSource WHERE heatSourceId = '.$heatSourceData->id);
	}
	
	public function assignProduct(HeatSourceData $heatSourceData, $qty) {
		$qty = (int) $qty;
		static::db()->exec('INSERT INTO '.DB_PREFIX.'RangeHeatSource (heatSourceId, heatSourceRangeId, amount) VALUES ('.$heatSourceData->id.', '.$this->id.', '.$qty.')');
	}
}