<?php
/**
 *
 * @author Steven Sulley
 * @copyright Sizzle Creative (14 Aug 2013)
 */
class WarmBoardEndSupportData extends ProductData {
	
	/**
	 * 
	 * @var float
	 */
	public $depth;
	
	/**
	 * 
	 * @var float
	 */
	public $numPerBoards;
	
	public function __construct(array $data = array()) {
		parent::__construct($data);
	}
	
	public function validate() {
		if (!isset($this->depth) || !is_numeric($this->depth))
			throw new Exception('Depth is required and must be a number');
		
		if (!isset($this->numPerBoards) || !is_numeric($this->numPerBoards))
			throw new Exception('Number per Board is required and must be a number');
	}
}