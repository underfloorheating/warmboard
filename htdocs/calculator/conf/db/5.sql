ALTER TABLE calc_System ADD COLUMN pipeCentres DECIMAL(4,2) UNSIGNED NOT NULL;
UPDATE calc_System SET pipeCentres = 0.2 WHERE id IN(1, 2, 3, 4, 5, 6, 7, 8, 11, 12, 13);
UPDATE calc_System SET pipeCentres = 0.15 WHERE id IN(9, 10, 14, 15);

INSERT INTO calc_ProductType VALUES
	(22, 'WARMBOARD_ENDPIECE'),
	(23, 'WARMBOARD_GLUE');

UPDATE calc_Product SET typeId = 22 WHERE id IN(34, 35);
UPDATE calc_Product SET typeId = 23 WHERE id = 33;

ALTER TABLE calc_Product ADD COLUMN description TEXT, ADD COLUMN productImage VARCHAR(255);