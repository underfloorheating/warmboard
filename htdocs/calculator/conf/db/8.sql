CREATE TABLE calc_SystemType (
	id			INT(11) UNSIGNED		PRIMARY KEY AUTO_INCREMENT,
	name		VARCHAR(100)			UNIQUE NOT NULL,
	sysClzName	VARCHAR(100)			NOT NULL,
	datClzName	VARCHAR(100)			NOT NULL,
	image		VARCHAR(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO calc_SystemType (id, name, sysClzName, datClzName) VALUES
	(1,		'Floating Warm-board',		'WarmaBoard',				'WarmaBoardData'),
	(2,		'Suspended Foil',			'NeoSuspended',				'NeoSuspendedData'),
	(3,		'Suspended Alu',			'AluSpreader',				'AluSpreaderData'),
	(4,		'Screed Staples',			'ScreedStaples',			'ScreedStaplesData'),
	(5,		'Floating Foil',			'NeoFloating',				'NeoFloatingData'),
	(6,		'Floating Alu',				'AluSpreader',				'AluSpreaderData'),
	(7,		'Screed Castillation Mat',	'ScreedCastillationMat',	'ScreedCastillationMatData'),
	(8,		'Screed Clip Rail',			'ScreedClipRail',			'ScreedClipRailData');

ALTER TABLE calc_System ADD FOREIGN KEY (systemTypeId) REFERENCES calc_SystemType(id) ON DELETE CASCADE;