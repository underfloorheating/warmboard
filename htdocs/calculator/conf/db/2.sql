CREATE TABLE calc_MerchantDetail (
	id				INT(11) UNSIGNED			PRIMARY KEY,
	contactName		VARCHAR(100)				NOT NULL,
	contactEmail	VARCHAR(100),
	companyName		VARCHAR(50),
	telephone		VARCHAR(20),
	address			TEXT,
	notes			TEXT,

	FOREIGN KEY (id) REFERENCES calc_User(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO calc_MerchantDetail (id, contactName, contactEmail, companyName, telephone, address, notes) VALUES
	(2, 'Example Merchant', 'eg.merchant@merchant.eg', 'Example Merchant\'s Shop', '01204 152425', '56 Some place, Some Where, SW1 123', 'This is an example merchant.');