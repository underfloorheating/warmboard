-- ----------------------------------------------------------------------------
-- Creates a new database for warmaboard calculation
--
-- Author: Steven Sulley, Sizzle Creative (2013-05-24)
-- ----------------------------------------------------------------------------

CREATE TABLE calc_ConstructionType (
	id		 				INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	name					VARCHAR(50)						NOT NULL,
	available				TINYINT(1) UNSIGNED				NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_PipeType (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	typeName				VARCHAR(50)						UNIQUE NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_System (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	name					VARCHAR(50)						NOT NULL,
	constructionTypeId		INT(11) UNSIGNED				NOT NULL,
	systemTypeId			INT(11) UNSIGNED				NOT NULL,
	pipeWidth				INT(2) UNSIGNED					NOT NULL,
	maxAreaPerCircuit		DECIMAL(5,2)					NOT NULL,
	available				TINYINT(1) UNSIGNED				NOT NULL,

	FOREIGN KEY (constructionTypeId) REFERENCES calc_ConstructionType(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_SystemPipeType (
	systemId				INT(11) UNSIGNED,
	pipeTypeId				INT(11) UNSIGNED,

	PRIMARY KEY (systemId, pipeTypeId),
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE,
	FOREIGN KEY (pipeTypeId) REFERENCES calc_PipeType(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_ProductType (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	typeName				VARCHAR(50)						UNIQUE NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_Product (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	name					VARCHAR(100)					NOT NULL,
	code					VARCHAR(20)						UNIQUE NOT NULL,
	price					DECIMAL(5,2)					NOT NULL,
	typeId					INT(11) UNSIGNED				NOT NULL,
	available				TINYINT(1) UNSIGNED				NOT NULL DEFAULT 1,

	FOREIGN KEY (typeId) REFERENCES calc_ProductType(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_ManifoldData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	maxCircuits				INT(2) UNSIGNED					NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_FittingsData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	isSingleCircuit			TINYINT(1) UNSIGNED				NOT NULL,
	pipeWidth				INT(3) UNSIGNED					NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_ThermostatData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	isWireless				TINYINT(1) UNSIGNED				NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_PipeData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	pipeLength				DECIMAL(5,2) UNSIGNED			NOT NULL,
	pipeWidth				DECIMAL(5,2) UNSIGNED			NOT NULL,
	pipeTypeId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (pipeTypeId) REFERENCES calc_PipeType(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_WarmaBoardData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	boardPerArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	boardsPerGlue			DECIMAL(9,2) UNSIGNED			NOT NULL,
	boardDepth				DECIMAL(9,2) UNSIGNED			NOT NULL,
	isLight					TINYINT(1) UNSIGNED				NOT NULL,
	systemId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_NeoSuspendedData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	boardPerArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	pipeCentres				DECIMAL(3,2) UNSIGNED			NOT NULL,
	coverageArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	boardDepth				DECIMAL(9,2) UNSIGNED			NOT NULL,
	systemId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_NeoFloatingData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	boardPerArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	pipeCentres				DECIMAL(3,2) UNSIGNED			NOT NULL,
	boardDepth				DECIMAL(9,2) UNSIGNED			NOT NULL,
	systemId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_AluSpreaderSuspendedData (
	id						INT(11) UNSIGNED				PRIMARY KEY,
	boardPerArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	pipeCentres				DECIMAL(3,2) UNSIGNED			NOT NULL,
	coverageArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	insultationArea			DECIMAL(3,2) UNSIGNED			NOT NULL,
	boardDepth				DECIMAL(9,2) UNSIGNED			NOT NULL,
	systemId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_User (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	userName				VARCHAR(50)						UNIQUE NOT NULL,
	password				VARCHAR(225)					NOT NULL,
	registeredTime			TIMESTAMP						NOT NULL DEFAULT CURRENT_TIMESTAMP,
	enabled					TINYINT(1)						NOT NULL,
	merchantDiscount		DECIMAL(5,2) UNSIGNED,
	isAdmin					TINYINT(1) UNSIGNED				NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_UserPerm (
	userpermId				INT(11) UNSIGNED				NOT NULL,
	userId					INT(11) UNSIGNED				NOT NULL,

	PRIMARY KEY (userpermId, userId),
	FOREIGN KEY (userId) REFERENCES calc_User(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_Installer (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	name					VARCHAR(100)					NOT NULL,
	companyName				VARCHAR(100),
	telephone				VARCHAR(20),
	email					VARCHAR(100),
	address					TEXT,
	notes					TEXT,
	merchantId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (merchantId) REFERENCES calc_User(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_CalculationData (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	recallCode				VARCHAR(14)						UNIQUE,
	assignedInstaller		INT(11) UNSIGNED,
	discount				DECIMAL(3,2) UNSIGNED,
	isLocked				TINYINT(1)						NOT NULL DEFAULT 0,
	merchantDiscount		DECIMAL(3,2) UNSIGNED,
	tsLocked				TIMESTAMP,

	INDEX (recallCode),
	FOREIGN KEY (assignedInstaller) REFERENCES calc_Installer(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_Floor (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	name					VARCHAR(100)					NOT NULL,
	calculationDataId		INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (calculationDataId) REFERENCES calc_CalculationData(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_Manifold (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	ident					VARCHAR(4)						NOT NULL,
	floorId					INT(11) UNSIGNED				NOT NULL,
	pipeTypeId				INT(11) UNSIGNED				NOT NULL,
	controlType				TINYINT(1) UNSIGNED				NOT NULL,
	systemId				INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (floorId) REFERENCES calc_Floor(id) ON DELETE CASCADE,
	FOREIGN KEY (pipeTypeId) REFERENCES calc_PipeType(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_Zone (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	ident					VARCHAR(4)						NOT NULL,
	manifoldId				INT(11) UNSIGNED				NOT NULL,
	linkToZoneId			INT(11) UNSIGNED,

	FOREIGN KEY (manifoldId) REFERENCES calc_Manifold(id) ON DELETE CASCADE,
	FOREIGN KEY (linkToZoneId) REFERENCES calc_Zone(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_Room (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	name					VARCHAR(50)						NOT NULL,
	zoneId					INT(11) UNSIGNED				NOT NULL,
	distFromManifold		DECIMAL(5,2) UNSIGNED			NOT NULL,
	floorArea				DECIMAL(6,2) UNSIGNED			NOT NULL,
	hasThermostat			TINYINT(1) UNSIGNED				NOT NULL,

	FOREIGN KEY (zoneId) REFERENCES calc_Zone(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_LockedProductListLine (
	id						INT(11) UNSIGNED				PRIMARY KEY AUTO_INCREMENT,
	calculationDataId		INT(11) UNSIGNED				NOT NULL,
	productId				INT(11) UNSIGNED,
	originalName			VARCHAR(100)					NOT NULL,
	originalCode			VARCHAR(20)						NOT NULL,
	originalPrice			DECIMAL(5,2)					NOT NULL,
	typeId					INT(11) UNSIGNED,
	qty						INT(11) UNSIGNED				NOT NULL,

	FOREIGN KEY (calculationDataId) REFERENCES calc_CalculationData(id) ON DELETE CASCADE,
	FOREIGN KEY (productId) REFERENCES calc_Product(id) ON DELETE SET NULL,
	FOREIGN KEY (typeId) REFERENCES calc_ProductType(id) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `calc_ConstructionType` (`id`, `name`, `available`) VALUES
	(1,		'Screed Systems',		0),
	(2,		'Floating Systems',		1),
	(3,		'Suspended Systems',	0);

INSERT INTO `calc_PipeType` (`id`, `typeName`) VALUES
	(1,		'PEX 5 LAYER'),
	(2,		'PEX ALU PEX'),
	(3,		'MULTI-PEX');

INSERT INTO `calc_System` (`id`, `name`, `constructionTypeId`, `systemTypeId`, `pipeWidth`, `available`, `maxAreaPerCircuit`) VALUES
	(1,		'Suspended Alu-Spreader 50mm',		3,		3,		16,		0,		20),
	(2,		'Suspended Alu-Spreader 35mm',		3,		3,		16,		0,		20),
	(3,		'Suspended NEO-Foil 50mm',			3,		2,		16,		0,		20),
	(4,		'Suspended NEO-Foil 35mm',			3,		2,		16,		0,		20),
	(5,		'Floating Alu-Spreader 35mm',		2,		6,		16,		0,		20),
	(6,		'Floating Alu-Spreader 50mm',		2,		6,		16,		0,		20),
	(7,		'Floating NEO-Foil 50mm',			2,		5,		16,		0,		20),
	(8,		'Floating NEO-Foil 35mm',			2,		5,		16,		0,		20),
	(9,		'Floating Warma-board 18mm',		2,		1,		12,		1,		10),
	(10,	'Floating Warma-board Light 18mm',	2,		1,		12,		1,		10),
	(11,	'Screed Castillation Mat',			1,		4,		16,		0,		20),
	(12,	'Screed Clip Track',				1,		4,		16,		0,		20),
	(13,	'Screed Staples',					1,		4,		16,		0,		20),
	(14,	'Floating Warma-board 15mm',		2,		1,		10,		1,		7),
	(15,	'Floating Warma-board Light 15mm',	2,		1,		10,		1,		7);

INSERT INTO `calc_ProductType` (`id`, `typeName`) VALUES
	(1, 	'MANIFOLD'),
	(2, 	'MANIFOLD_MIXER'),
	(3, 	'FITTINGS'),
	(4, 	'ACTUATOR'),
	(5, 	'ZONE_WIRING_SYSTEM'),
	(6, 	'THERMOSTAT'),
	(7, 	'PIPE'),
	(8, 	'PIPE_BEND'),
	(9, 	'SYSTEM_WARMABOARD'),
	(10,	'SYSTEM_NEOFOIL_FLOATING'),
	(11,	'ALUSPREADER_PLATE'),
	(12,	'SYSTEM_ALUSPREADER_SUSPENDED'),
	(13,	'SYSTEM_ALUSPREADER_FLOATING'),
	(14,	'CASTILLATION'),
	(15,	'EDGE_INSULATION'),
	(16,	'STAPLES'),
	(17,	'CLIP_RAIL'),
	(18,	'SYSTEM_NEOFOIL_SUSPENDED');

INSERT INTO `calc_Product` (`id`, `name`, `code`, `price`, `typeId`, `available`) VALUES
	(1,		'MANIFOLD MIXER PACK WITH "A" RATED PUMP',								'UFH-MAN-MIX',			230.77,	2,	1),
	(2,		'ISIS 230V MANIFOLD ELECTRONIC ACTUATOR',								'UFH-MAN-ACTU',			14.33,	4,	1),
	(3,		'PIPE LEADING BEND TO MANIFOLD 14-18MM',								'UFH-ACC-PLB-14/18',	0.87,	8,	1),
	(4,		'SINGLE ROOM PACK WITH "A" RATED PUMP',									'UFH-MAN-SR',			288.46,	1,	1),
	(5,		'1" UFH MANIFOLD KIT 2 CIRCUIT',										'UFH-MAN-02CT',			112.81,	1,	1),
	(6,		'1" UFH MANIFOLD KIT 3 CIRCUIT',										'UFH-MAN-03CT',			132.03,	1,	1),
	(7,		'1" UFH MANIFOLD KIT 4 CIRCUIT',										'UFH-MAN-04CT',			149.75,	1,	1),
	(8,		'1" UFH MANIFOLD KIT 5 CIRCUIT',										'UFH-MAN-05CT',			168.16,	1,	1),
	(9,		'1" UFH MANIFOLD KIT 6 CIRCUIT',										'UFH-MAN-06CT',			188.28,	1,	1),
	(10,	'1" UFH MANIFOLD KIT 7 CIRCUIT',										'UFH-MAN-07CT',			206.59,	1,	1),
	(11,	'1" UFH MANIFOLD KIT 8 CIRCUIT',										'UFH-MAN-08CT',			225.00,	1,	1),
	(12,	'1" UFH MANIFOLD KIT 9 CIRCUIT',										'UFH-MAN-09CT',			249.59,	1,	1),
	(13,	'1" UFH MANIFOLD KIT 10 CIRCUIT',										'UFH-MAN-10CT',			263.66,	1,	1),
	(14,	'1" UFH MANIFOLD KIT 11 CIRCUIT',										'UFH-MAN-11CT',			279.81, 1,	1),
	(15,	'1" UFH MANIFOLD KIT 12 CIRCUIT',										'UFH-MAN-12CT',			300.63,	1,	1),
	(16,	'MANIFOLD EUROCONUS FITTINGS PACK 12MM',								'UFH-MAN-CF12',			2.33,	3,	1),
	(17,	'12MM EUROCONUS FITTING PACK x 1/2" MALE IRON',							'UFH-MAN-SRCF12',		3.31,	3,	1),
	(18,	'MANIFOLD EUROCONUS FITTINGS PACK 16MM',								'UFH-MAN-CF16',			2.33,	3,	1),
	(19,	'16MM EUROCONUS FITTING PACK x 1/2" MALE IRON',							'UFH-MAN-SRCF16',		3.31,	3,	1),
	(20,	'DIGITAL PROGRAMABLE ROOM THERMOSTAT',									'UFH-CON-PRT',			52.31,	6,	1),
	(21,	'DIGITAL WIRELESS PROGRAMMABLE THERMOSTAT',								'UFH-CON-PRT-W',		52.31,	6,	1),
	(22,	'UH3 8 ZONE WIRING CENTRE',												'UFH-CON-UH3',			70.38,	5,	1),
	(23,	'UH1-W 8 ZONE WIRING CENTRE',											'UFH-CON-UH1-W',		120.63,	5,	1),
	(24,	'MULTI-PEX 5 LAYER PIPE 12MM x 2.0MM x 80M',							'UFH-PIPE-MP12/080',	76.00,	7,	1),
	(25,	'MULTI-PEX 5 LAYER PIPE 16MM x 2.0MM x 75M',							'UFH-PIPE-MP16/075',	84.75,	7,	1),
	(26,	'MULTI-PEX 5 LAYER PIPE 16MM x 2.0MM x 100M',							'UFH-PIPE-MP16/100',	113.00,	7,	1),
	(27,	'PEX AL PEX MULTILAYER PIPE 16MM x 2.0MM x 75M',						'UFH-PIPE-PAP16/075',	97.50,	7,	1),
	(28,	'PEX AL PEX MULTILAYER PIPE 16MM x 2.0MM x 100M',						'UFH-PIPE-PAP16/100',	130.00,	7,	1),
	(29,	'Warma-board floating floor panel (LxWxD) 800mm x 600mm x 18mm',		'UFH-ACC-WMB18',		9.29,	9,	1),
	(30,	'Warma-board floating floor panel (LxWxD) 800mm x 600mm x 15mm',		'UFH-ACC-WMB15',		9.29,	9,	1),
	(31,	'Warma-board Light floating floor panel (LxWxD) 1200mm x 600mm x 18mm',	'UFH-ACC-WMB-L18',		9.29,	9,	1),
	(32,	'Warma-board Light floating floor panel (LxWxD) 1200mm x 600mm x 15mm',	'UFH-ACC-WMB-L15',		9.29,	9,	1),
	(33,	'WARMA-BOARD JOINTING GLUE',											'UFH-ACC-WMBG',			14.14,	9,	1),
	(34,	'Warma-board End Support 18mm',											'UFH-ACC-END18',		0.00,	9,	1),
	(35,	'Warma-board End Support 15mm',											'UFH-ACC-END15',		0.00,	9,	1),
	(36,	'MULTI PEX 10MM 60M',													'UFH-PIPE-MP-10/60',	0.00,	7,	1),
	(37,	'Euro cone 10mm fitting',												'UFH-MAN-CF10',			0.00,	3,	1),
	(38,	'NEO-FOIL FLOATING FLOOR 35MM',											'UFH-ACC-NEO-FF35',		23.55,	10,	1),
	(39,	'NEO-FOIL FLOATING FLOOR 50MM',											'UFH-ACC-NEO-FF50',		27.16,	10,	1),
	(40,	'ALLUMINIUM SPREADER PLATE 1200mmx398mm',								'UFH-ACC-ALSP',			5.55,	11,	1),
	(41,	'SUSPENDED FLOOR INSULATION PANEL 1200MM X 350MM 35MM',					'UFH-ACC-INS-SF35',		6.07,	12,	1),
	(42,	'SUSPENDED FLOOR INSULATION PANEL 1200MM X 350MM 50MM',					'UFH-ACC-INS-SF50',		0.00,	12,	1),
	(43,	'FLOATING FLOOR INSULATION PANEL 1200MM X 1200MM',						'UFH-ACC-INS-FF35',		17.80,	13,	1),
	(44,	'FLOATING FLOOR INSULATION PANEL 1200MM X 1200MM',						'UFH-ACC-INS-FF50',		0.00,	13, 1),
	(45,	'1450MM 850MM PLASTIC CASTELLATION MAT',								'UFH-ACC-PCM',			9.29,	14,	1),
	(46,	'8MM X 150MM X 25M EDGE INSULATION',									'UFH-ACC-ESOT',			15.54,	15,	1),
	(47,	'50MM STAPLES FOR 15-20MM PIPE',										'UFH-ACC-STPL-15/20',	11.00,	16,	1),
	(48,	'CLIP RAIL WITH ADHESIVE FOR 16-20MM PIPE',								'UFH-ACC-CR-15/20',		1.85,	17,	1),
	(49,	'NEO-FOIL SUSPENDED FLOOR 35MM',										'UFH-ACC-NEO-SF35',		23.55,	18,	1),
	(50,	'NEO-FOIL SUSPENDED FLOOR 50MM',										'UFH-ACC-NEO-SF50',		27.16,	18,	1);

INSERT INTO `calc_ManifoldData` (`id`, `maxCircuits`) VALUES
	(4,		1),
	(5,		2),
	(6,		3),
	(7,		4),
	(8,		5),
	(9,		6),
	(10,	7),
	(11,	8),
	(12,	9),
	(13,	10),
	(14,	11),
	(15,	12);

INSERT INTO `calc_FittingsData` (`id`, `isSingleCircuit`, `pipeWidth`) VALUES
	(16,	0,		12),
	(17,	1,		12),
	(18,	0,		16),
	(19,	1,		16),
	(37,	0,		10);

INSERT INTO `calc_ThermostatData` (`id`, `isWireless`) VALUES
	(20,	0),
	(21,	1),
	(22,	0),
	(23,	1);

INSERT INTO `calc_PipeData` (`id`, `pipeLength`, `pipeWidth`, `pipeTypeId`) VALUES
	(24,	80,		12,		1),
	(25,	75,		16,		1),
	(26,	100,	16,		1),
	(27,	75,		16,		2),
	(28,	100,	16,		2),
	(36,	60,		10,		3);

INSERT INTO `calc_WarmaBoardData` (`id`, `boardPerArea`, `boardsPerGlue`, `boardDepth`, `isLight`, `systemId`) VALUES
	(29,	0.48,	10,		18,		0,		9),
	(30,	0.48,	10,		15,		0,		14),
	(31,	0.72,	10,		18,		1,		10),
	(32,	0.72,	10,		15,		1,		15);

INSERT INTO calc_NeoSuspendedData (id, boardPerArea, pipeCentres, coverageArea, boardDepth, systemId) VALUES
	(50,	0.42,	0.2,	0.8,	50,		3),
	(49,	0.42,	0.2,	0.8,	35,		4);

INSERT INTO calc_NeoFloatingData (id, boardPerArea, pipeCentres, boardDepth, systemId) VALUES
	(38,	1.44,	0.2,	35,		8),
	(39,	1.44,	0.2,	50,		7);

-- INSERT INTO calc_AluSpreaderSuspendedData (id, maxAreaPerCircuit, boardPerArea, pipeCentres, coverageArea, insultationArea, systemId) VALUES
--	()

INSERT INTO `calc_SystemPipeType` (`systemId`, `pipeTypeId`) VALUES
	(1,		1),
	(1,		2),
	(2,		1),
	(2,		2),
	(3,		1),
	(3,		2),
	(4,		1),
	(4,		2),
	(5,		1),
	(5,		2),
	(6,		1),
	(6,		2),
	(7,		1),
	(7,		2),
	(8,		1),
	(8,		2),
	(9,		1),
	(10,	1),
	(11,	1),
	(11,	2),
	(12,	1),
	(12,	2),
	(13,	1),
	(13,	2),
	(14,	3),
	(15,	3);

INSERT INTO calc_User (id, userName, password, registeredTime, enabled, merchantDiscount, isAdmin) VALUES
	(1,		'sizzle',	'827d957fb0c7afb128cefb81611fee56af1f1a86',		0,		1,		NULL,	1),
	(2,		'merchant',	'97566b9d5e3d2e40f6f9864750fe63a5409c0d2f',		0,		1,		0.2,	0);

INSERT INTO calc_UserPerm (userId, userpermId) VALUES
	(1,		1),
	(1,		2),
	(1,		3),
	(2,		3);

INSERT INTO calc_Installer (id, name, companyName, telephone, email, address, notes, merchantId) VALUES
	(1,		'Installer A',		'AAA Installers Ltd.',		'01204 123456',		'an.installer@aaa-installers.co.uk',	NULL,	NULL,	2),
	(2,		'Installer B',		'AAB Installers Ltd.',		'01204 123456',		'an.installer@aab-installers.co.uk',	NULL,	NULL,	2),
	(3,		'Installer C',		'AAC Installers Ltd.',		'01204 123456',		'an.installer@aac-installers.co.uk',	NULL,	NULL,	2),
	(4,		'Installer D',		'AAD Installers Ltd.',		'01204 123456',		'an.installer@aad-installers.co.uk',	NULL,	NULL,	2),
	(5,		'Installer E',		'AAE Installers Ltd.',		'01204 123456',		'an.installer@aae-installers.co.uk',	NULL,	NULL,	2),
	(6,		'Installer F',		'AAF Installers Ltd.',		'01204 123456',		'an.installer@aaf-installers.co.uk',	NULL,	NULL,	2),
	(7,		'Installer G',		'AAG Installers Ltd.',		'01204 123456',		'an.installer@aag-installers.co.uk',	NULL,	NULL,	2),
	(8,		'Installer H',		'AAH Installers Ltd.',		'01204 123456',		'an.installer@aah-installers.co.uk',	NULL,	NULL,	2),
	(9,		'Installer I',		'AAI Installers Ltd.',		'01204 123456',		'an.installer@aai-installers.co.uk',	NULL,	NULL,	2),
	(10,	'Installer J',		'AAJ Installers Ltd.',		'01204 123456',		'an.installer@aaj-installers.co.uk',	NULL,	NULL,	2),
	(11,	'Installer K',		'AAK Installers Ltd.',		'01204 123456',		'an.installer@aak-installers.co.uk',	NULL,	NULL,	2),
	(12,	'Installer L',		'AAL Installers Ltd.',		'01204 123456',		'an.installer@aal-installers.co.uk',	NULL,	NULL,	2),
	(13,	'Installer M',		'AAM Installers Ltd.',		'01204 123456',		'an.installer@aam-installers.co.uk',	NULL,	NULL,	2),
	(14,	'Installer N',		'AAN Installers Ltd.',		'01204 123456',		'an.installer@aan-installers.co.uk',	NULL,	NULL,	2),
	(15,	'Installer O',		'AAO Installers Ltd.',		'01204 123456',		'an.installer@aao-installers.co.uk',	NULL,	NULL,	2),
	(16,	'Installer P',		'AAP Installers Ltd.',		'01204 123456',		'an.installer@aap-installers.co.uk',	NULL,	NULL,	2),
	(17,	'Installer Q',		'AAQ Installers Ltd.',		'01204 123456',		'an.installer@aaq-installers.co.uk',	NULL,	NULL,	2);	