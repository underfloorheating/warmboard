CREATE TABLE calc_WarmBoardEndSupportData (
	id				INT(11) UNSIGNED			PRIMARY KEY,
	depth			DECIMAL(5,2) UNSIGNED		NOT NULL,
	numPerBoards	DECIMAL(5,2) UNSIGNED		NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO calc_WarmBoardEndSupportData (id, depth, numPerBoards) VALUES
	(34,	18,		1),
	(35,	15,		1);

ALTER TABLE calc_CalculationData ADD COLUMN allowContact TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, ADD COLUMN contactEmail VARCHAR(100);

ALTER TABLE calc_System DROP COLUMN maxAreaPerCircuit;

ALTER TABLE calc_AluSpreaderData ADD COLUMN boardSize DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT 0;
UPDATE calc_AluSpreaderData SET boardSize = boardPerArea;
ALTER TABLE calc_AluSpreaderData DROP COLUMN boardPerArea;

ALTER TABLE calc_NeoFloatingData ADD COLUMN boardSize DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT 0;
UPDATE calc_NeoFloatingData SET boardSize = boardPerArea;
ALTER TABLE calc_NeoFloatingData DROP COLUMN boardPerArea;

ALTER TABLE calc_NeoSuspendedData ADD COLUMN boardSize DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT 0;
UPDATE calc_NeoSuspendedData SET boardSize = boardPerArea;
ALTER TABLE calc_NeoSuspendedData DROP COLUMN boardPerArea;

ALTER TABLE calc_WarmaBoardData ADD COLUMN boardSize DECIMAL(5,2) UNSIGNED NOT NULL DEFAULT 0;
UPDATE calc_WarmaBoardData SET boardSize = boardPerArea;
ALTER TABLE calc_WarmaBoardData DROP COLUMN boardPerArea;