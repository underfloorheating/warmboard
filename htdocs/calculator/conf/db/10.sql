CREATE TABLE calc_MerchantGroup (
	id				INT(11) UNSIGNED		PRIMARY KEY AUTO_INCREMENT,
	name			VARCHAR(100)			NOT NULL,
	discount		DECIMAL(5,2) UNSIGNED	NOT NULL DEFAULT 0.0,
	contactName		VARCHAR(100),
	contactEmail	VARCHAR(100),
	companyName		VARCHAR(100),
	telephone		VARCHAR(50),
	address			TEXT,
	notes			TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO calc_MerchantGroup (id, name) VALUES
	(1, 'Default Group');

ALTER TABLE calc_MerchantDetail ADD COLUMN merchantGroupId INT(11) UNSIGNED NOT NULL;
UPDATE calc_MerchantDetail SET merchantGroupId = 1;

ALTER TABLE calc_MerchantDetail ADD FOREIGN KEY (merchantGroupId) REFERENCES calc_MerchantGroup(id);
ALTER TABLE calc_User DROP COLUMN merchantDiscount;