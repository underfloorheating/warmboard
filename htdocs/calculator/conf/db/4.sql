ALTER TABLE calc_NeoSuspendedData DROP COLUMN pipeCentres;
ALTER TABLE calc_NeoFloatingData DROP COLUMN pipeCentres;
ALTER TABLE calc_AluSpreaderSuspendedData DROP COLUMN pipeCentres, CHANGE COLUMN insultationArea insulationArea DECIMAL(3,2) UNSIGNED NOT NULL, RENAME TO calc_AluSpreaderData;

UPDATE calc_System SET available = 1;

INSERT INTO calc_AluSpreaderData (id, boardPerArea, insulationArea, coverageArea, boardDepth, systemId) VALUES
	(41,	0.48,	0.48,	0.8,	35,		2),
	(42,	0.48,	0.48,	0.8,	50,		1),
	(43,	0.48,	1.44,	0.8,	35,		5),
	(44,	0.48,	1.44,	0.8,	50,		6);

CREATE TABLE calc_ScreedStaplesData (
	id					INT(11) UNSIGNED			PRIMARY KEY,
	staplesPerPipeArea	DECIMAL(9,2) UNSIGNED		NOT NULL,
	staplesPerBag		INT(11) UNSIGNED			NOT NULL,
	systemId			INT(11) UNSIGNED			NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_ScreedCastillationMatData (
	id					INT(11) UNSIGNED			PRIMARY KEY,
	castellationArea	DECIMAL(9,2) UNSIGNED		NOT NULL,
	systemId			INT(11) UNSIGNED			NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE calc_ScreedClipRailData (
	id					INT(11) UNSIGNED			PRIMARY KEY,
	clipRailsPerArea	DECIMAL(9,2) UNSIGNED		NOT NULL,
	systemId			INT(11) UNSIGNED			NOT NULL,

	FOREIGN KEY (id) REFERENCES calc_Product(id) ON DELETE CASCADE,
	FOREIGN KEY (systemId) REFERENCES calc_System(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO calc_ScreedStaplesData (id, staplesPerPipeArea, staplesPerBag, systemId) VALUES
	(47,	5,		250,	13);

INSERT INTO calc_ScreedCastillationMatData (id, castellationArea, systemId) VALUES
	(45,	1.23,	11);

INSERT INTO calc_ScreedClipRailData (id, clipRailsPerArea, systemId) VALUES
	(48,	1,		12);

UPDATE calc_System SET systemTypeId = 7 WHERE id = 11;
UPDATE calc_System SET systemTypeId = 8 WHERE id = 12;

INSERT INTO calc_ProductType (id, typeName) VALUES
	(19,	'SYSTEM_SCREED_STAPLES'),
	(20,	'SYSTEM_SCREED_CASTILLATIONMAT'),
	(21,	'SYSTEM_SCREED_CLIPRAIL');

UPDATE calc_Product SET typeId = 19 WHERE id = 47;
UPDATE calc_Product SET typeId = 20 WHERE id = 45;
UPDATE calc_Product SET typeId = 21 WHERE id = 48;

DELETE FROM calc_ProductType WHERE id IN (14, 16, 17);