-- Add Archiving to Merchants and groups
ALTER TABLE calc_MerchantDetail ADD COLUMN archived TINYINT(1) UNSIGNED NOT NULL DEFAULT FALSE;
ALTER TABLE calc_MerchantGroup ADD COLUMN archived TINYINT(1) UNSIGNED NOT NULL DEFAULT FALSE;