/*
Remove numbers from comment author
*/

document.addEventListener('DOMContentLoaded', function (event) {
    document.getElementById("commentform").querySelector("#author").addEventListener(
        'blur',
        function () {
            this.value = this.value.replace(/\d+/g, '');
        },
        false
    );
})
