        <!--FOOTER-->
        <?php  global $gcdata;?>
        <div class="footer">
            <div class="container">
                <div class="row">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer") ) : ?>                
                    <?php endif; ?> 
                </div>
            </div>
        </div>
        <?php if($gcdata['bottom_line_show'] == true ) { ?>
        <div class="bottom_line">
            <div class="container">
            	<div class="row">
                    <div class="span6">
                        <span class="bottomlinetext"><?php echo stripslashes($gcdata['bottom_line_text']) ?></span>
                    </div>
                    
                </div>
            </div>
        </div>
        <?php } ?>
        <!--/FOOTER-->
        </div>

	</body>
    <?php wp_footer(); ?>
</html>