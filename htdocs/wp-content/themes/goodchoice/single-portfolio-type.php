			<?php get_header(); ?>
            
            <?php if (!is_front_page()){ ?>
				<?php if($gcdata['revolution_index'] == true ) { ?>
                    <?php putRevSlider("main_slider") ?>
                <?php } ?>
            <?php } ?>
            
			<?php
			$title = get_the_title();
			if ( $title == "Valera Project")  $gcdata['sl_portfolio_details_style'] = "With Sidebar";
			if ( $title == "Pride Template")  $gcdata['sl_portfolio_details_style'] = "With Sidebar";
			if ( $title == "Valera Project") $gcdata['portfolio_sidebar_position'] = "Left Sidebar";
			if ( $title == "Pride Template") $gcdata['portfolio_sidebar_position'] = "Right Sidebar";
			
			if ( $title == "Builder Theme") $gcdata['sl_portfolio_details_style'] = "Landscape Style";
			?>
			
			
			<?php
				$custom = get_post_custom($post->ID);
				$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); 
				$small_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'blog'); 
				$small_p_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'portfolio-three'); 
				$cat = get_the_category($post->ID); 
				$cat = $cat[0]; 
			?>
            <?php $img1 = get_post_meta($post->ID, 'image', true); ?>
            <?php $img2 = get_post_meta($post->ID, 'image2', true); ?>
            <?php $img3 = get_post_meta($post->ID, 'image3', true); ?>            
            
			<?php if ($gcdata['sl_portfolio_details_style'] == "Portrait Style") { ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                
                <?php if (!is_front_page()){ ?>
					<?php if($gcdata['revolution_index'] == true ) { ?>
                        <?php putRevSlider("main_slider") ?>
                    <?php } ?>
                <?php } ?>
                
                <div class="main_content_area">
                <div class="container inner_content">
                    <section class="nopaddding">
                        <div class="row">
                            <div class="span8">
                            	<div class="slider_area">
									<?php if (!((get_post_meta($post->ID, image, true)) || (get_post_meta($post->ID, image2, true)) || (get_post_meta($post->ID, image3, true)) || (get_post_meta($post->ID, video, true)))) { ?>
                                        <div class="row">
                                            <div class="span8 portfolio_item nolink">
                                                <div class="view view-first">
                                                    <img src="<?php echo $large_image_url[0]; ?>" alt="" />
                                                    <div class="mask">
                                                        <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (get_post_meta($post->ID, video, true));{ ?><?php echo get_post_meta($post->ID, video, true); ?><?php }?>	
                                    <?php if ((get_post_meta($post->ID, 'image', true)) || (get_post_meta($post->ID, 'image2', true)) || (get_post_meta($post->ID, 'image3', true))){ ?>
                                    <div class="theme-default">
                                        <div id="slider" class="nivoSlider">
                                            <?php if (get_post_meta($post->ID, 'image', true)) { ?>
                                                <img src="<?php echo get_post_meta($post->ID, image, true); ?>" alt="" />
                                            <?php } ?>
                                            <?php if (get_post_meta($post->ID, 'image2', true)) { ?>
                                                <img src="<?php echo get_post_meta($post->ID, image2, true); ?>" alt="" />
                                            <?php } ?>
                                            <?php if (get_post_meta($post->ID, 'image3', true)) { ?>
                                                <img src="<?php echo get_post_meta($post->ID, image3, true); ?>" alt="" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                            	</div>
                            </div>
                            <div class="span4 portfolio-description">
                            	<div class="portfolio_post_item_description" >
                                    <div><h4 ><?php the_title(); ?></h4><?php if($gcdata['portfolio_details_pagination'] == true ) { ?><div class="meta"><span> <?php previous_post_link('<strong>< %link</strong>'); ?> </span> <span class="last_item"><?php  next_post_link('<strong>%link ></strong>'); ?></span></div><?php } ?></div>
                                    <?php the_content(''); ?>
                            	</div>
                            </div>
                        </div>
                    </section>
                   
                </div>
                </div>
           
            <?php endwhile;  ?>
	 		<?php endif; ?>
            
            <?php if ($gcdata['portfolio_details_related'] == true) {?>
            <div class="container">
            <section top_pad>
                <div class="row">
                    <div class="span12">
                    	<h4><?php echo $gcdata['portfolio_details_related_title']?></h4>
                        <p><?php echo $gcdata['portfolio_details_related_description']?></p>
                        <div id="portfolio" class="row">
                    	
                        <?php
						
						wp_reset_postdata();
						global $wp_query;
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$args = array(
							'post_type' 		=> 'portfolio-type',
							'posts_per_page' => 3,
							'post_status' 		=> 'publish',
							'orderby' 			=> 'date',
							'order' 			=> 'DESC',
							'paged' 			=> $paged,
							'limit' => '3'
						);
						
						$wp_query = new WP_Query($args);

						
					 if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
							<?php
								$custom = get_post_custom($post->ID);
								$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); 
								 
								$cat = get_the_category($post->ID); 
							?>
							<?php $cur_terms = get_the_terms( $post->ID, 'portfolio-category' ); 
									foreach($cur_terms as $cur_term){  
								};
								
								$catt = get_the_terms( $post->ID, 'portfolio-category' );
								$slugg = ''; 
								
								foreach($catt  as $vallue=>$key){
									$slugg .= strtolower($key->slug) . " ";
								}
							?>
							
								<div class="span4 portfolio_item block <?php echo $slugg; ?>" data-filter="">
									<div class="view view-first <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon only") { ?>nolink <?php } ?> <?php if ($gcdata['portfolio_image_hover_icons'] == "Link icon only") { ?>noinfo <?php } ?>">
										<a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto"><img src="<?php echo $large_image_url[0]; ?>" alt="" /></a>
										<div class="mask">
											<?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon + Link icon") { ?>
											<a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
											<a href="<?php echo get_permalink(); ?>" class="link"></a>
											<?php } ?>
											<?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon only") { ?>
											<a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
											<?php } ?>
											<?php if ($gcdata['portfolio_image_hover_icons'] == "Link icon only") { ?>
											<a href="<?php echo get_permalink(); ?>" class="link"></a>
											<?php } ?>
										</div>
									</div>
									<?php if($gcdata['portfolio_descr_show'] == true ) { ?>
									<div class="descr">
										<h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
										<?php if($gcdata['portfolio_descr_clo_text'] == true ) { ?><p class="clo"><?php echo get_post_meta($post->ID, 'port-descr', 1); ?></p><?php } ?>
									</div>
									<?php } ?>
								</div>
							<?php endwhile; endif; ?>
                            </div>
                            </div>
                            </div>
				</section>
                </div>
                <?php } ?>
            
            <?php } ?>
            
            
            
            <?php if ($gcdata['sl_portfolio_details_style'] == "Landscape Style") { ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="main_content_area">
                <div class="container inner_content">
                    <section class="nopaddding">
                        <div class="row">
                            <div class="span12">
                            	<div class="slider_area">
									<?php if (!((get_post_meta($post->ID, image, true)) || (get_post_meta($post->ID, image2, true)) || (get_post_meta($post->ID, image3, true)) || (get_post_meta($post->ID, video, true)))) { ?>
                                        <div class="row">
                                            <div class="span12 portfolio_item nolink">
                                                <div class="view view-first">
                                                    <img src="<?php echo $large_image_url[0]; ?>" alt="" />
                                                    <div class="mask">
                                                        <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if (get_post_meta($post->ID, video, true));{ ?><?php echo get_post_meta($post->ID, video, true); ?><?php }?>	
                                    <?php if ((get_post_meta($post->ID, 'image', true)) || (get_post_meta($post->ID, 'image2', true)) || (get_post_meta($post->ID, 'image3', true))){ ?>
                                    <div class="theme-default">
                                        <div id="slider" class="nivoSlider">
                                            <?php if (get_post_meta($post->ID, 'image', true)) { ?>
                                                <img src="<?php echo get_post_meta($post->ID, image, true); ?>" alt="" />
                                            <?php } ?>
                                            <?php if (get_post_meta($post->ID, 'image2', true)) { ?>
                                                <img src="<?php echo get_post_meta($post->ID, image2, true); ?>" alt="" />
                                            <?php } ?>
                                            <?php if (get_post_meta($post->ID, 'image3', true)) { ?>
                                                <img src="<?php echo get_post_meta($post->ID, image3, true); ?>" alt="" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                            	</div>
                            </div>
                            <div class="clearfix"></div>
                            <div>
                                <div class="span12 portfolio-description">
                                    <div class="portfolio_post_item_description">
                                    	<div><h4><?php the_title(); ?></h4><?php if($gcdata['portfolio_details_pagination'] == true ) { ?><div class="meta"><span> <?php previous_post_link('<strong>< %link</strong>'); ?> </span> <span class="last_item"><?php  next_post_link('<strong>%link ></strong>'); ?></span></div><?php } ?></div>
                                        <?php the_content(''); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                </div>
           
            <?php endwhile;  ?>
	 		<?php endif; ?>
            
            <?php if ($gcdata['portfolio_details_related'] == true) {?>
            <div class="container">
                <section class="top_pad">
                    <div class="row-fluid">
                        <div class="span12">
                            <h4><?php echo $gcdata['portfolio_details_related_title']?></h4>
                            <p><?php echo $gcdata['portfolio_details_related_description']?></p>
                            <div id="portfolio" class="row-fluid">
                            
                            <?php
                            
                            wp_reset_postdata();
                            global $wp_query;
                            $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                            $args = array(
                                'post_type' 		=> 'portfolio-type',
                                'posts_per_page' => 3,
                                'post_status' 		=> 'publish',
                                'orderby' 			=> 'date',
                                'order' 			=> 'DESC',
                                'paged' 			=> $paged,
                                'limit' => '3'
                            );
                            
                            $wp_query = new WP_Query($args);
    
                            
                         if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
                                <?php
                                    $custom = get_post_custom($post->ID);
                                    $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); 
                                     
                                    $cat = get_the_category($post->ID); 
                                ?>
                                <?php $cur_terms = get_the_terms( $post->ID, 'portfolio-category' ); 
                                        foreach($cur_terms as $cur_term){  
                                    };
                                    
                                    $catt = get_the_terms( $post->ID, 'portfolio-category' );
                                    $slugg = ''; 
                                    
                                    foreach($catt  as $vallue=>$key){
                                        $slugg .= strtolower($key->slug) . " ";
                                    }
                                ?>
                                
                                    <div class="span4 portfolio_item block <?php echo $slugg; ?>" data-filter="">
                                        <div class="view view-first <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon only") { ?>nolink <?php } ?> <?php if ($gcdata['portfolio_image_hover_icons'] == "Link icon only") { ?>noinfo <?php } ?>">
                                            <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto"><img src="<?php echo $large_image_url[0]; ?>" alt="" /></a>
                                            <div class="mask">
                                                <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon + Link icon") { ?>
                                                <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                <a href="<?php echo get_permalink(); ?>" class="link"></a>
                                                <?php } ?>
                                                <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon only") { ?>
                                                <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                <?php } ?>
                                                <?php if ($gcdata['portfolio_image_hover_icons'] == "Link icon only") { ?>
                                                <a href="<?php echo get_permalink(); ?>" class="link"></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php if($gcdata['portfolio_descr_show'] == true ) { ?>
                                        <div class="descr">
                                            <h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
                                            <?php if($gcdata['portfolio_descr_clo_text'] == true ) { ?><p class="clo"><?php echo get_post_meta($post->ID, 'port-descr', 1); ?></p><?php } ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                <?php endwhile; endif; ?>
                                </div>
                                </div>
                                </div>
                    </section>
                    </div>
                    <?php } ?>
            
            
            <?php } ?>
            
            
            <?php if ($gcdata['sl_portfolio_details_style'] == "With Sidebar") { ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="main_content_area">
                    <div class="container inner_content">
                        <section class="nopaddding">
                            <div class="row">
                            	<?php if ($gcdata['portfolio_sidebar_position'] == "Left Sidebar") { ?>
                                <div class="span3 portfolio_sidebar">
                                    <div class="myps">
										<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Portfolio Sidebar") ) : ?>                
                                        <?php endif; ?> 
                                    </div>
                                </div>
                                <?php } ?>
                            	<div class="span9">
                                <div class="row">
                                <div class="span9">
                                    <div class="slider_area">
                                        <?php if (!((get_post_meta($post->ID, image, true)) || (get_post_meta($post->ID, image2, true)) || (get_post_meta($post->ID, image3, true)) || (get_post_meta($post->ID, video, true)))) { ?>
                                            <div class="row">
                                            <div class="span9 portfolio_item nolink">
                                                <div class="view view-first">
                                                    <img src="<?php echo $large_image_url[0]; ?>" alt="" />
                                                    <div class="mask">
                                                        <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                    </div>
                                                </div>
                                                <div class="my_clear"></div>
                                            </div>
                                            <div class="my_clear"></div>
                                        </div>
                                        <div class="my_clear"></div>
                                        <?php } ?>
                                        <?php if (get_post_meta($post->ID, video, true));{ ?><?php echo get_post_meta($post->ID, video, true); ?><?php }?>	
                                        <?php if ((get_post_meta($post->ID, 'image', true)) || (get_post_meta($post->ID, 'image2', true)) || (get_post_meta($post->ID, 'image3', true))){ ?>
                                        <div class="theme-default">
                                            <div id="slider" class="nivoSlider">
                                                <?php if (get_post_meta($post->ID, 'image', true)) { ?>
                                                    <img src="<?php echo get_post_meta($post->ID, image, true); ?>" alt="" />
                                                <?php } ?>
                                                <?php if (get_post_meta($post->ID, 'image2', true)) { ?>
                                                    <img src="<?php echo get_post_meta($post->ID, image2, true); ?>" alt="" />
                                                <?php } ?>
                                                <?php if (get_post_meta($post->ID, 'image3', true)) { ?>
                                                    <img src="<?php echo get_post_meta($post->ID, image3, true); ?>" alt="" />
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div>
                                    <div class="span9 portfolio-description">
                                        <div class="portfolio_post_item_description">
		                                    <div><h4><?php the_title(); ?></h4><?php if($gcdata['portfolio_details_pagination'] == true ) { ?><div class="meta"><span> <?php previous_post_link('<strong>< %link</strong>'); ?> </span> <span class="last_item"><?php  next_post_link('<strong>%link ></strong>'); ?></span></div><?php } ?></div>
                                            <?php the_content(''); ?>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile;  ?>
	 							<?php endif; ?>
                                </div>
                                <?php if ($gcdata['portfolio_details_related'] == true) {?>
                                    <section class="top_pad">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <h4><?php echo $gcdata['portfolio_details_related_title']?></h4>
                                                <p><?php echo $gcdata['portfolio_details_related_description']?></p>
                                                <div id="portfolio" class="row-fluid">
                                                
                                                <?php
                                                
                                                wp_reset_postdata();
                                                global $wp_query;
                                                $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                                                $args = array(
                                                    'post_type' 		=> 'portfolio-type',
                                                    'posts_per_page' => 3,
                                                    'post_status' 		=> 'publish',
                                                    'orderby' 			=> 'date',
                                                    'order' 			=> 'DESC',
                                                    'paged' 			=> $paged,
                                                    'limit' => '3'
                                                );
                                                
                                                $wp_query = new WP_Query($args);
                        
                                                
                                             if ( have_posts() ) : while ( have_posts() ) : the_post(); ?> 
                                                    <?php
                                                        $custom = get_post_custom($post->ID);
                                                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large'); 
                                                         
                                                        $cat = get_the_category($post->ID); 
                                                    ?>
                                                    <?php $cur_terms = get_the_terms( $post->ID, 'portfolio-category' ); 
                                                            foreach($cur_terms as $cur_term){  
                                                        };
                                                        
                                                        $catt = get_the_terms( $post->ID, 'portfolio-category' );
                                                        $slugg = ''; 
                                                        
                                                        foreach($catt  as $vallue=>$key){
                                                            $slugg .= strtolower($key->slug) . " ";
                                                        }
                                                    ?>
                                                    
                                                        <div class="span4 portfolio_item block <?php echo $slugg; ?>" data-filter="">
                                                            <div class="view view-first <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon only") { ?>nolink <?php } ?> <?php if ($gcdata['portfolio_image_hover_icons'] == "Link icon only") { ?>noinfo <?php } ?>">
                                                                <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto"><img src="<?php echo $large_image_url[0]; ?>" alt="" /></a>
                                                                <div class="mask">
                                                                    <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon + Link icon") { ?>
                                                                    <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                                    <a href="<?php echo get_permalink(); ?>" class="link"></a>
                                                                    <?php } ?>
                                                                    <?php if ($gcdata['portfolio_image_hover_icons'] == "Zoom icon only") { ?>
                                                                    <a href="<?php echo $large_image_url[0]; ?>" rel="prettyPhoto" title="<?php the_title(); ?>" class="info"></a>
                                                                    <?php } ?>
                                                                    <?php if ($gcdata['portfolio_image_hover_icons'] == "Link icon only") { ?>
                                                                    <a href="<?php echo get_permalink(); ?>" class="link"></a>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <?php if($gcdata['portfolio_descr_show'] == true ) { ?>
                                                            <div class="descr">
                                                                <h5><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h5>
                                                                <?php if($gcdata['portfolio_descr_clo_text'] == true ) { ?><p class="clo"><?php echo get_post_meta($post->ID, 'port-descr', 1); ?></p><?php } ?>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php endwhile; endif; ?>
                                                    </div>
                                                    </div>
                                                    </div>
                                        </section>
                                        <?php } ?>
                                </div>
                            	<?php if ($gcdata['portfolio_sidebar_position'] == "Right Sidebar") { ?>
                                <div class="span3 portfolio_sidebar">
                                    <div class="psdb">
										<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Portfolio Sidebar") ) : ?>                
                                        <?php endif; ?> 
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </section>
                    </div>
                </div>
           
            
            
            <?php } ?>
            
            
        <!--End main container-->
        <!--Footer-->
        <?php get_footer(); ?>