<?php
global $gcdata; 
?>

<!-- Custom CSS Codes
========================================================= -->
	
<style>
<?php if ($gcdata['theme_layout'] == "Fullwidth and 960px container") { ?>
	.wide_cont { max-width:100% !important;  margin:0px auto; background-color:#fdfdfd; }
<?php } ?>

<?php if ($gcdata['theme_layout'] == "Boxed and 960px container") { ?>
	.wide_cont { max-width:1000px !important;  margin:<?php echo $gcdata['theme_boxed_margin']; ?>px auto !important; box-shadow:0px 0px 3px #b4b4b4 !important;}
    body {
      background: <?php echo $gcdata['theme_boxed_bg_color']; ?> <?php echo 'url("'.strip_tags($gcdata['theme_boxed_bg']).'")'; ?> fixed !important;
      
    }
<?php } ?>

<?php if ($gcdata['theme_layout'] == "Fullwidth and 1170px container") { ?>
	.wide_cont { max-width:100% !important;  margin:0px auto !important; background-color:#fdfdfd;}
<?php } ?>

<?php if ($gcdata['theme_layout'] == "Boxed and 1170px container") { ?>
	.wide_cont { max-width:1240px !important;  margin:<?php echo $gcdata['theme_boxed_margin']; ?>px auto; box-shadow:0px 0px 3px #b4b4b4 !important;}
	body {
      background: <?php echo $gcdata['theme_boxed_bg_color']; ?> <?php echo 'url("'.strip_tags($gcdata['theme_boxed_bg']).'")'; ?> fixed !important;
    }
<?php } ?>


.caption.commander_heading{	color:<?php echo $gcdata['main_conent_links']; ?>;}
.caption.commander_small_heading{ color:<?php echo $gcdata['main_conent_links']; ?>;}
.postformat:hover {background-color:<?php echo $gcdata['main_conent_links']; ?> !important;}
.my_aside a:hover, .my_quote a:hover {color:<?php echo $gcdata['main_conent_links']; ?> !important;}
a { color: <?php echo $gcdata['main_conent_links']; ?>;}
a:hover {color:<?php echo $gcdata['main_conent_links_hover']; ?>;}
.tp-rightarrow:hover {background-color:<?php echo $gcdata['logo_and_menu_menu_item_li_hover']; ?> }
.tp-leftarrow:hover {background-color:<?php echo $gcdata['logo_and_menu_menu_item_li_hover']; ?>}

.wide_cont {background-color:<?php echo $gcdata['theme_conatiner_bg_color']; ?>;}

.colored {color: <?php echo $gcdata['main_conent_links']; ?> !important;}
.top_line {background-color: <?php echo $gcdata['theme_colors_top_line']; ?> !important; background-image: <?php echo 'url("'.strip_tags($gcdata['theme_colors_top_line_bg_image']).'")'; ?> !important;}
.top_line p {color: <?php echo $gcdata['theme_colors_top_line_text']; ?> !important;}
.top_line a {color: <?php echo $gcdata['theme_colors_top_line_a']; ?> !important;}
.top_line a:hover {color: <?php echo $gcdata['theme_colors_top_line_a_hover']; ?> !important;}

.page_head {padding-top: <?php echo $gcdata['logo_and_menu_t_padding']; ?>px !important;
padding-bottom: <?php echo $gcdata['logo_and_menu_b_padding']; ?>px !important;
background-image: <?php echo 'url("'.strip_tags($gcdata['logo_and_menu_bg_image']).'")'; ?> !important;
background-color: <?php echo $gcdata['logo_and_menu_bg']; ?> !important;
}
.logo {margin-top: <?php echo $gcdata['logo_and_menu_logo_margin']; ?>px !important;}
.mc_top_menu {
	margin-top: <?php echo $gcdata['logo_and_menu_menu_margin']; ?>px;
}
.page_head .menu a {
    padding: <?php echo $gcdata['logo_and_menu_menu_item_vertical_padding']; ?>px <?php echo $gcdata['logo_and_menu_menu_item_horizontal_padding']; ?>px;
    color: <?php echo $gcdata['logo_and_menu_menu_item_color']; ?>;
    font-size:<?php echo $gcdata['logo_and_menu_menu_item_fontsize']; ?>px;
    <?php if($gcdata['logo_and_menu_menu_item_uppercase'] == true ) { ?>
    text-transform:uppercase;
    <?php } ?>
    font-weight:<?php echo $gcdata['logo_and_menu_menu_item_fontweight']; ?>;
}
.page_head .sub { font-size:<?php echo $gcdata['logo_and_menu_menu_item_sub_fontsize']; ?>px; text-transform:none; color:<?php echo $gcdata['logo_and_menu_menu_item_sub_color']; ?>; margin-top:<?php echo $gcdata['logo_and_menu_menu_item_sub_top_margin']; ?>px; font-weight:400;}
.page_head .menu ul {background:<?php echo $gcdata['logo_and_menu_menu_item_ul_color']; ?>;}
.page_head .menu ul a {color:<?php echo $gcdata['logo_and_menu_menu_item_ul_textcolor']; ?> !important;}

.page_head .menu li:hover {background:<?php echo $gcdata['logo_and_menu_menu_item_li_hover']; ?>}
.page_head .menu a:hover {color:<?php echo $gcdata['logo_and_menu_menu_item_li_text_hover']; ?> !important}
.page_head .menu li:hover .sub {color:<?php echo $gcdata['logo_and_menu_menu_item_sub_text_hover']; ?>}
.page_head .menu ul a:hover {background-color:<?php echo $gcdata['logo_and_menu_menu_item_ul_bg_hover']; ?>; color:<?php echo $gcdata['logo_and_menu_menu_item_ul_text_hover']; ?> !important}
.page_head .menu .current-menu-item, .page_head .menu .current_page_parent, .page_head .menu .current-menu-parent { background:<?php echo $gcdata['logo_and_menu_menu_item_bg_current']; ?>;}
.page_head .menu .current-menu-item a, .page_head .menu .current_page_parent a, .page_head .menu .current-menu-parent a {color:<?php echo $gcdata['logo_and_menu_menu_item_color_current']; ?>;} 
.current-menu-item .sub, .current_page_parent .sub, .current-menu-parent .sub {color:<?php echo $gcdata['logo_and_menu_menu_item_sub_color_current']; ?>;}
.page_head .menu ul {margin-top:<?php echo ((2*$gcdata['logo_and_menu_menu_item_vertical_padding']) + 34)?>px !important;}
.page_head .menu ul ul { margin-top:0px !important; top:0px !important;}

.welcome { background-image: <?php echo 'url("'.strip_tags($gcdata['tag_line_custom_bg']).'")'; ?>; <?php if (!is_front_page() || ($gcdata['tag_line_position'] == "After Slider")){ ?>border-bottom: 1px solid <?php echo $gcdata['tag_line_border_bottom']; ?>;<?php } ?> <?php if (!is_front_page() || ($gcdata['tag_line_position'] == "Before Slider")){ ?>border-top: 1px solid <?php echo $gcdata['tag_line_border_top']; ?>;<?php } ?> background-color: <?php echo $gcdata['tag_line_bg']; ?> }
.welcome {padding-bottom: <?php echo $gcdata['tag_line_padding_bottom']; ?>px; padding-top: <?php echo $gcdata['tag_line_padding_top']; ?>px}
.main_content_area .recent-post-widget a:hover {color:<?php echo $gcdata['logo_and_menu_menu_item_li_hover']; ?> !important}
.footer {
	color: <?php echo $gcdata['footer_text_color']; ?>;
	background-image: <?php echo 'url("'.strip_tags($gcdata['footer_bg_image']).'")'; ?>;
    background-color: <?php echo $gcdata['footer_bg_color']; ?>;
	margin-top:<?php echo $gcdata['footer_margin_top']; ?>px; 
	padding-top:<?php echo $gcdata['footer_padding_top']; ?>px; 
    padding-bottom: <?php echo $gcdata['footer_padding_bottom']; ?>px;
    border-top: <?php echo $gcdata['footer_border_value']; ?>px solid <?php echo $gcdata['footer_border_color']; ?>;
}
.footer p { color: <?php echo $gcdata['footer_text_color']; ?>;}
.footer strong {color: <?php echo $gcdata['footer_text_strong_color']; ?>;}
#jstwitter .tweet {color: <?php echo $gcdata['footer_text_color']; ?>;}
#jstwitter .tweet .time {color: <?php echo $gcdata['footer_text_small_color']; ?>;}
#jstwitter .tweet a:hover {color: <?php echo $gcdata['footer_text_a_hover_color']; ?>;}
.small-meta { color:<?php echo $gcdata['footer_text_small_color']; ?>;}
.small-meta a { color: <?php echo $gcdata['footer_text_small_color']; ?> !important;}
.footer a {color: <?php echo $gcdata['footer_text_a_color']; ?>;}
.footer a:hover {color: <?php echo $gcdata['footer_text_a_hover_color']; ?>;}
.small-meta a:hover { color: <?php echo $gcdata['footer_text_a_hover_color']; ?> !important;}

.footer h5 { color:<?php echo $gcdata['footer_text_header_color']; ?>; }
.footer hr{ border-top-color: <?php echo $gcdata['footer_hr_color']; ?>;  margin-top:6px; margin-bottom:15px;}
.bottom_line { background-color: <?php echo $gcdata['theme_colors_bottom_line']; ?> !important; background-image: <?php echo 'url("'.strip_tags($gcdata['theme_colors_bottom_line_bg_image']).'")'; ?>; }
.bottom_line { color: <?php echo $gcdata['theme_colors_bottom_line_text']; ?>;}
.bottom_line a {color: <?php echo $gcdata['theme_colors_bottom_line_a']; ?>;}
.bottom_line a:hover {color: <?php echo $gcdata['theme_colors_bottom_line_a_hover']; ?>;}
.main_content_area {margin-top: <?php echo $gcdata['main_content_margin_top']; ?>px;}

.main_content_area .date { background: <?php echo $gcdata['blog_date_bg']; ?>; border-radius:<?php echo $gcdata['blog_date_border_radius']; ?>px;}
.main_content_area .date h6 { color:<?php echo $gcdata['blog_date_color']; ?>;  text-shadow:0px 1px <?php echo $gcdata['blog_date_text_shadow']; ?>;}

.blog_item .view-first .mask {background-color: rgba(<?php echo $gcdata['blog_image_bg_1']; ?>,<?php echo $gcdata['blog_image_bg_2']; ?>,<?php echo $gcdata['blog_image_bg_3']; ?>, <?php echo $gcdata['blog_image_bg_op']; ?>)}
.blog_item .view a.info {background-color:<?php echo $gcdata['blog_image_icons_bg']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_image_icons_zoom']).'")'; ?>}
.blog_item .view a.info:hover {background-color:<?php echo $gcdata['blog_image_icons_bg_hover']; ?>;}
.blog_item .view a.link {background-color:<?php echo $gcdata['blog_image_icons_bg']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_image_icons_link']).'")'; ?>}
.blog_item .view a.link:hover {background-color:<?php echo $gcdata['blog_image_icons_bg_hover']; ?>;}

.blog_item_description { background-color:<?php echo $gcdata['blog_item_description_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_item_description_bg_image']).'")'; ?>; padding:<?php echo $gcdata['blog_item_description_padding']; ?>px; border:1px solid <?php echo $gcdata['blog_item_description_border_color']; ?>; color:<?php echo $gcdata['blog_item_description_text_color']; ?>}

.pride_pg a {padding: <?php echo $gcdata['pagination_padding_v']; ?>px <?php echo $gcdata['pagination_padding_h']; ?>px; border-radius:<?php echo $gcdata['pagination_border_radius']; ?>px; background-color: <?php echo $gcdata['pagination_bg_color']; ?>; color: <?php echo $gcdata['pagination_text_color']; ?>; text-shadow: <?php echo $gcdata['pagination_text_shadow']; ?> 0px 1px 0px; background-image: <?php echo 'url("'.strip_tags($gcdata['pagination_bg_image']).'")'; ?>; }
.pride_pg .current {padding: <?php echo $gcdata['pagination_padding_v']; ?>px <?php echo $gcdata['pagination_padding_h']; ?>px; border-radius:<?php echo $gcdata['pagination_border_radius']; ?>px; background-color: <?php echo $gcdata['pagination_hover_bg_color']; ?>;  color:<?php echo $gcdata['pagination_hover_text_color']; ?>;  background-image: <?php echo 'url("'.strip_tags($gcdata['pagination_bg_image']).'")'; ?>;}
.pride_pg a:hover  {
	background-color: <?php echo $gcdata['pagination_hover_bg_color']; ?>;
	color:<?php echo $gcdata['pagination_hover_text_color']; ?>;
	text-shadow: none;
	background-image: <?php echo 'url("'.strip_tags($gcdata['pagination_bg_image']).'")'; ?>;
}

.portfolio_post_item_description { background-color:<?php echo $gcdata['portfolio_post_item_description_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['portfolio_post_item_description_bg_image']).'")'; ?>; padding:<?php echo $gcdata['portfolio_post_item_description_padding']; ?>px; border:1px solid <?php echo $gcdata['portfolio_post_item_description_border_color']; ?>; color:<?php echo $gcdata['portfolio_post_item_description_text_color']; ?>}

.blog_author_item_description { background-color:<?php echo $gcdata['blog_author_item_description_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_author_item_description_bg_image']).'")'; ?>; padding:<?php echo $gcdata['blog_author_item_description_padding']; ?>px; border:1px solid <?php echo $gcdata['blog_author_item_description_border_color']; ?>; color:<?php echo $gcdata['blog_author_item_description_text_color']; ?>}
.share {padding:<?php echo $gcdata['blog_share_padding']; ?>px; background-color:<?php echo $gcdata['blog_share_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_share_bg_color']).'")'; ?>; color:<?php echo $gcdata['blog_share_text_color']; ?>;}
.comments_div {border-left:1px solid <?php echo $gcdata['blog_comments_border_color']; ?>; padding-left:<?php echo $gcdata['blog_comments_padding']; ?>px;}
.blog_item_comments_description { background-color:<?php echo $gcdata['blog_comments_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_comments_bg_image']).'")'; ?>; padding:<?php echo $gcdata['blog_comments_li_padding']; ?>px; border:1px solid <?php echo $gcdata['blog_comments_border_color']; ?>; color:<?php echo $gcdata['blog_comments_text_color']; ?>}


.blog_sidebar {background-color:<?php echo $gcdata['blog_sidebar_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_sidebar_bg_image']).'")'; ?>; border-radius:<?php echo $gcdata['blog_sidebar_border_radius']; ?>px;}

.blog_sidebar .well hr { border-bottom-color:<?php echo $gcdata['blog_sidebar_widget_hr']; ?>;}
.blog_sidebar .well {border:1px solid <?php echo $gcdata['blog_sidebar_widget_border_color']; ?>; background-color:<?php echo $gcdata['blog_sidebar_widget_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['blog_sidebar_widget_bg_image']).'")'; ?>; border-radius:<?php echo $gcdata['blog_sidebar_widget_border_radius']; ?>px;}
.blog_sidebar h5 { color:<?php echo $gcdata['blog_sidebar_widget_header_color']; ?>;}
.blog_sidebar a{ color:<?php echo $gcdata['blog_sidebar_widget_links_color']; ?>;}
.blog_sidebar a:hover{ color:<?php echo $gcdata['blog_sidebar_widget_links_color_hover']; ?>;}
.blog_sidebar { color:<?php echo $gcdata['blog_sidebar_widget_text_color']; ?>;}
.blog_sidebar ul li { border-bottom:1px dashed <?php echo $gcdata['blog_sidebar_widget_hr']; ?>}
.blog_sidebar .current-menu-item a {color:<?php echo $gcdata['blog_sidebar_widget_links_color_hover']; ?>;}


.filter_button {  font-size:<?php echo $gcdata['portfolio_filter_text_size']; ?>px; margin-right:<?php echo $gcdata['portfolio_filter_margin']; ?>px; padding:<?php echo $gcdata['portfolio_filter_padding_v']; ?>px <?php echo $gcdata['portfolio_filter_padding_h']; ?>px; background-color:<?php echo $gcdata['portfolio_filter_bg_color']; ?>; border:1px solid <?php echo $gcdata['portfolio_filter_border_color']; ?>; border-radius:<?php echo $gcdata['portfolio_filter_border_radius']; ?>px; color:<?php echo $gcdata['portfolio_filter_text_color']; ?>; text-shadow:1px 1px <?php echo $gcdata['portfolio_filter_text_shadow']; ?>;}
.filter_button:hover {background-color:<?php echo $gcdata['portfolio_filter_bg_color_hover']; ?>; color:<?php echo $gcdata['portfolio_filter_text_color_hover']; ?>; border-color:<?php echo $gcdata['portfolio_filter_bg_color_hover']; ?> }
.filter_current { background-color:<?php echo $gcdata['portfolio_filter_bg_color_hover']; ?>; border-color:<?php echo $gcdata['portfolio_filter_bg_color_hover']; ?>; color:<?php echo $gcdata['portfolio_filter_text_color_hover']; ?>;}

.portfolio_item .view-first .mask {background-color: rgba(<?php echo $gcdata['portfolio_image_bg_1']; ?>,<?php echo $gcdata['portfolio_image_bg_2']; ?>,<?php echo $gcdata['portfolio_image_bg_3']; ?>, <?php echo $gcdata['portfolio_image_bg_op']; ?>)}
.portfolio_item .view a.info {background-color:<?php echo $gcdata['portfolio_image_icons_bg']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['portfolio_image_icons_zoom']).'")'; ?>}
.portfolio_item .view a.info:hover {background-color:<?php echo $gcdata['portfolio_image_icons_bg_hover']; ?>;}
.portfolio_item .view a.link {background-color:<?php echo $gcdata['portfolio_image_icons_bg']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['portfolio_image_icons_link']).'")'; ?>}
.portfolio_item .view a.link:hover {background-color:<?php echo $gcdata['portfolio_image_icons_bg_hover']; ?>;}


.nivo-prevNav, .nivo-nextNav {background-color:<?php echo $gcdata['blog_image_icons_bg']; ?>; border:1px solid <?php echo $gcdata['blog_image_icons_bg']; ?> }
.nivo-prevNav:hover, .nivo-nextNav:hover {background-color:<?php echo $gcdata['blog_image_icons_bg_hover']; ?>; border-color:#ffffff;}


.descr {background-color:<?php echo $gcdata['portfolio_descr_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['portfolio_descr_bg_image']).'")'; ?>; border:1px solid <?php echo $gcdata['portfolio_descr_border_color']; ?>;}
.descr a { color: <?php echo $gcdata['portfolio_descr_links_color']; ?>;}
.descr a:hover { color: <?php echo $gcdata['portfolio_descr_links_color_hover']; ?>;}
.clo { font-size:<?php echo $gcdata['portfolio_descr_text_size']; ?>px; color:<?php echo $gcdata['portfolio_descr_text_color']; ?> !important;}

.portfolio_sidebar .well hr { border-bottom-color:<?php echo $gcdata['portfolio_sidebar_widget_hr']; ?>;}
.portfolio_sidebar .well {border:1px solid <?php echo $gcdata['portfolio_sidebar_widget_border_color']; ?>; background-color:<?php echo $gcdata['portfolio_sidebar_widget_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['portfolio_sidebar_widget_bg_image']).'")'; ?>; border-radius:<?php echo $gcdata['portfolio_sidebar_widget_border_radius']; ?>px;}
.portfolio_sidebar h5 { color:<?php echo $gcdata['portfolio_sidebar_widget_header_color']; ?>;}
.portfolio_sidebar a{ color:<?php echo $gcdata['portfolio_sidebar_widget_links_color']; ?>;}
.portfolio_sidebar a:hover{ color:<?php echo $gcdata['portfolio_sidebar_widget_links_color_hover']; ?>;}
.portfolio_sidebar { color:<?php echo $gcdata['portfolio_sidebar_widget_text_color']; ?>;}
.portfolio_sidebar ul li { border-bottom:1px dashed <?php echo $gcdata['portfolio_sidebar_widget_hr']; ?>}
.portfolio_sidebar .current-menu-item a {color:<?php echo $gcdata['portfolio_sidebar_widget_links_color_hover']; ?>;}

.blog_item .meta a, .blog_item .meta span, .blog_item .meta span a:after{ color:<?php echo $gcdata['blog_show_posts_meta_color']; ?>;}
.blog_item .meta a:hover { color:<?php echo $gcdata['blog_show_posts_meta_color_hover']; ?>;}

.blog_post_item_description .meta a, .blog_post_item_description .meta a:after, .blog_post_item_description .meta span{ color:<?php echo $gcdata['blog_post_show_posts_meta_color']; ?>;}
.blog_post_item_description .meta a:hover { color:<?php echo $gcdata['blog_post_show_posts_meta_color_hover']; ?>;}


.portfolio_post_item_description .meta a, .portfolio_post_item_description .meta a:after, .portfolio_post_item_description .meta span{ color:<?php echo $gcdata['portfolio_post_show_posts_meta_color']; ?>;}
.portfolio_post_item_description .meta a:hover { color:<?php echo $gcdata['portfolio_post_show_posts_meta_color_hover']; ?>;}


#filters_sidebar a { border-bottom:1px dashed <?php echo $gcdata['portfolio_sidebar_widget_hr']; ?>;}
.filter_sidebar_current { color:<?php echo $gcdata['portfolio_sidebar_widget_links_color_hover']; ?>;}



.page_sidebar .well hr { border-bottom-color:<?php echo $gcdata['page_sidebar_widget_hr']; ?>;}
.page_sidebar .well {border:1px solid <?php echo $gcdata['page_sidebar_widget_border_color']; ?>; background-color:<?php echo $gcdata['page_sidebar_widget_bg_color']; ?>; background-image: <?php echo 'url("'.strip_tags($gcdata['page_sidebar_widget_bg_image']).'")'; ?>; border-radius:<?php echo $gcdata['page_sidebar_widget_border_radius']; ?>px;}
.page_sidebar h5 { color:<?php echo $gcdata['page_sidebar_widget_header_color']; ?>;}
.page_sidebar a{ color:<?php echo $gcdata['page_sidebar_widget_links_color']; ?>;}
.page_sidebar a:hover{ color:<?php echo $gcdata['page_sidebar_widget_links_color_hover']; ?>;}
.page_sidebar { color:<?php echo $gcdata['page_sidebar_widget_text_color']; ?>;}
.page_sidebar ul li { border-bottom:1px dashed <?php echo $gcdata['page_sidebar_widget_hr']; ?>}
.page_sidebar .main_content_area .menu li { border-bottom:1px dashed <?php echo $gcdata['page_sidebar_widget_hr']; ?>; padding:0px !important;}
.page_sidebar .main_content_area .menu li a { color:<?php echo $gcdata['page_sidebar_widget_text_color']; ?>;} 
.page_sidebar .main_content_area .menu li a:hover { color:<?php echo $gcdata['page_sidebar_widget_links_color_hover']; ?>;}
.page_sidebar .current-menu-item a {color:<?php echo $gcdata['page_sidebar_widget_links_color_hover']; ?>;}
<?php
$head_font_one = $gcdata['headers_font_one'];
$head_font_two = $gcdata['headers_font_two'];
$head_font_three = $gcdata['headers_font_three'];
$head_font_four = $gcdata['headers_font_four'];
$head_font_five = $gcdata['headers_font_five'];
$head_font_six = $gcdata['headers_font_six'];
$commander_body_font = $gcdata['body_font'];
?>

body {
	font-family: <?php echo $commander_body_font['face']; ?> !important;
	color: <?php echo $commander_body_font['color']; ?> !important;
	font-style: <?php echo $commander_body_font['style']; ?> !important;
	font-size: <?php echo $commander_body_font['size']; ?> !important; 
}

h1 {
	font-family: <?php echo $head_font_one['face']; ?> !important;
	color: <?php echo $head_font_one['color']; ?> !important;
	font-style: <?php echo $head_font_one['style']; ?> !important;
	font-size: <?php echo $head_font_one['size']; ?> !important; 
	
}
h2{
	font-family: <?php echo $head_font_two['face']; ?>;
	color: <?php echo $head_font_two['color']; ?>;
	font-style: <?php echo $head_font_two['style']; ?>;
	font-size: <?php echo $head_font_two['size']; ?>; 
	
}
h3 {
	font-family: <?php echo $head_font_three['face']; ?>;
	color: <?php echo $head_font_three['color']; ?>;

	font-style: <?php echo $head_font_three['style']; ?>;
	font-size: <?php echo $head_font_three['size']; ?>; 
	
}
h4{
	font-family: <?php echo $head_font_four['face']; ?>;
	color: <?php echo $head_font_four['color']; ?>;
	font-style: <?php echo $head_font_four['style']; ?>;
	font-size: <?php echo $head_font_four['size']; ?>; 
	
}
h5 {
	font-family: <?php echo $head_font_five['face']; ?>;
	color: <?php echo $head_font_five['color']; ?>;
	font-style: <?php echo $head_font_five['style']; ?>;
	font-size: <?php echo $head_font_five['size']; ?>; 
	
}
h6 {
	font-family: <?php echo $head_font_six['face']; ?>;
	color: <?php echo $head_font_six['color']; ?>;
	font-style: <?php echo $head_font_six['style']; ?>;
	font-size: <?php echo $head_font_six['size']; ?>; 
	
}




.blog_head h3 a { color:<?php echo $gcdata['blog_show_posts_meta_color']; ?>;}
.blog_head h3 a:hover { color:<?php echo $gcdata['blog_show_posts_meta_color_hover']; ?>;}



<?php if($gcdata['alt_stylesheet'] == 'blue.css') { ?>
.page_head .menu .current-menu-parent {background: #2d5c88;}
.page_head .menu li:hover {background: #2d5c88;}
.main_content_area .current-menu-item a {color: #2d5c88 !important;}
.tp-rightarrow:hover { background-color:#2d5c88 !important;}
.tp-leftarrow:hover { background-color:#2d5c88 !important;}
.main_content_area .recent-post-widget a:hover { color:#2d5c88 !important }
a { color: #2d5c88;}
.colored {color: #2d5c88 !important;}
.top_line {background-color: #2d5c88 !important;}
.page_head .menu .current-menu-item {background: #2d5c88 !important ;}
.page_head .menu ul li:hover a { background:#2d5c88 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#2d5c88 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #2d5c88 ;}
.page_head .menu ul ul a:hover { background:#2d5c88 !important }
.blog_item .view a.info:hover {background-color:#2d5c88;}
.blog_item .view a.link:hover {background-color:#2d5c88;}
.pride_pg .current {background-color: #2d5c88;}
.pride_pg a:hover  {background-color: #2d5c88;}
.blog_sidebar a{ color:#2d5c88;}
.filter_button:hover {background-color:#2d5c88;border-color:#2d5c88 }
.filter_current { background-color:#2d5c88; border-color:#2d5c88;}
.portfolio_item .view a.info:hover {background-color:#2d5c88;}
.portfolio_item .view a.link:hover {background-color:#2d5c88;}
.descr a { color: #2d5c88;}
.portfolio_sidebar a{ color:#2d5c88;}
.blog_item .meta a:hover { color:#2d5c88;}
.blog_post_item_description .meta a:hover { color:#2d5c88;}
.portfolio_post_item_description .meta a:hover { color:#2d5c88;}
.page_sidebar a{ color:#2d5c88;}

.blog_head h3 a:hover {color:#2d5c88;}
.well a { color:#333;}
.well a:hover { color:#2d5c88;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'cayan.css') { ?>
.page_head .menu .current-menu-parent {background: #2997ab;}
.page_head .menu li:hover {background: #2997ab;}
.main_content_area .current-menu-item a {color: #2997ab !important;}
.tp-rightarrow:hover { background-color:#2997ab !important;}
.tp-leftarrow:hover { background-color:#2997ab !important;}
.main_content_area .recent-post-widget a:hover { color:#2997ab !important }
a { color: #2997ab;}
.colored {color: #2997ab !important;}
.top_line {background-color: #2997ab !important;}
.page_head .menu .current-menu-item {background: #2997ab !important ;}
.page_head .menu ul li:hover a { background:#2997ab }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#2997ab ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #2997ab ;}
.page_head .menu ul ul a:hover { background:#2997ab !important }
.blog_item .view a.info:hover {background-color:#2997ab;}
.blog_item .view a.link:hover {background-color:#2997ab;}
.pride_pg .current {background-color: #2997ab;}
.pride_pg a:hover  {background-color: #2997ab;}
.blog_sidebar a{ color:#2997ab;}
.filter_button:hover {background-color:#2997ab;border-color:#2997ab }
.filter_current { background-color:#2997ab; border-color:#2997ab;}
.portfolio_item .view a.info:hover {background-color:#2997ab;}
.portfolio_item .view a.link:hover {background-color:#2997ab;}
.descr a { color: #2997ab;}
.portfolio_sidebar a{ color:#2997ab;}
.blog_item .meta a:hover { color:#2997ab;}
.blog_post_item_description .meta a:hover { color:#2997ab;}
.portfolio_post_item_description .meta a:hover { color:#2997ab;}
.page_sidebar a{ color:#2997ab;}

.blog_head h3 a:hover {color:#2997ab;}
.well a { color:#333;}
.well a:hover { color:#2997ab;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'green.css') { ?>
.page_head .menu .current-menu-parent {background: #719430;}
.page_head .menu li:hover {background: #719430;}
.main_content_area .current-menu-item a {color: #719430 !important;}
.tp-rightarrow:hover { background-color:#719430 !important;}
.tp-leftarrow:hover { background-color:#719430 !important;}
.main_content_area .recent-post-widget a:hover { color:#719430 !important }

a { color: #719430;}
.colored {color: #719430 !important;}
.top_line {background-color: #719430 !important;}
.page_head .menu .current-menu-item {background: #719430 !important ;}
.page_head .menu ul li:hover a { background:#719430 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#719430 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #719430 ;}
.page_head .menu ul ul a:hover { background:#719430 !important }
.blog_item .view a.info:hover {background-color:#719430;}
.blog_item .view a.link:hover {background-color:#719430;}
.pride_pg .current {background-color: #719430;}
.pride_pg a:hover  {background-color: #719430;}
.blog_sidebar a{ color:#719430;}
.filter_button:hover {background-color:#719430;border-color:#719430 }
.filter_current { background-color:#719430; border-color:#719430;}
.portfolio_item .view a.info:hover {background-color:#719430;}
.portfolio_item .view a.link:hover {background-color:#719430;}
.descr a { color: #719430;}
.portfolio_sidebar a{ color:#719430;}
.blog_item .meta a:hover { color:#719430;}
.blog_post_item_description .meta a:hover { color:#719430;}
.portfolio_post_item_description .meta a:hover { color:#719430;}
.page_sidebar a{ color:#719430;}

.blog_head h3 a:hover {color:#719430;}
.well a { color:#333;}
.well a:hover { color:#719430;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'grunge.css') { ?>
.page_head .menu .current-menu-parent {background: #85742e;}
.page_head .menu li:hover {background: #85742e;}
.main_content_area .current-menu-item a {color: #85742e !important;}
.tp-rightarrow:hover { background-color:#85742e !important;}
.tp-leftarrow:hover { background-color:#85742e !important;}
.main_content_area .recent-post-widget a:hover { color:#85742e !important }
a { color: #85742e;}
.colored {color: #85742e !important;}
.top_line {background-color: #85742e !important;}
.page_head .menu .current-menu-item {background: #85742e !important ;}
.page_head .menu ul li:hover a { background:#85742e }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#85742e ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #85742e ;}
.page_head .menu ul ul a:hover { background:#85742e !important }
.blog_item .view a.info:hover {background-color:#85742e;}
.blog_item .view a.link:hover {background-color:#85742e;}
.pride_pg .current {background-color: #85742e;}
.pride_pg a:hover  {background-color: #85742e;}
.blog_sidebar a{ color:#85742e;}
.filter_button:hover {background-color:#85742e;border-color:#85742e }
.filter_current { background-color:#85742e; border-color:#85742e;}
.portfolio_item .view a.info:hover {background-color:#85742e;}
.portfolio_item .view a.link:hover {background-color:#85742e;}
.descr a { color: #85742e;}
.portfolio_sidebar a{ color:#85742e;}
.blog_item .meta a:hover { color:#85742e;}
.blog_post_item_description .meta a:hover { color:#85742e;}
.portfolio_post_item_description .meta a:hover { color:#85742e;}
.page_sidebar a{ color:#85742e;}

.blog_head h3 a:hover {color:#85742e;}
.well a { color:#333;}
.well a:hover { color:#85742e;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'light_blue.css') { ?>
.page_head .menu .current-menu-parent {background: #04bfea;}
.page_head .menu li:hover {background: #04bfea;}
.main_content_area .current-menu-item a {color: #04bfea !important;}
.tp-rightarrow:hover { background-color:#04bfea !important;}
.tp-leftarrow:hover { background-color:#04bfea !important;}
.main_content_area .recent-post-widget a:hover { color:#04bfea !important }
a { color: #04bfea;}
.colored {color: #04bfea !important;}
.top_line {background-color: #04bfea !important;}
.page_head .menu .current-menu-item {background: #04bfea !important ;}
.page_head .menu ul li:hover a { background:#04bfea }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#04bfea ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #04bfea ;}
.page_head .menu ul ul a:hover { background:#04bfea !important }
.blog_item .view a.info:hover {background-color:#04bfea;}
.blog_item .view a.link:hover {background-color:#04bfea;}
.pride_pg .current {background-color: #04bfea;}
.pride_pg a:hover  {background-color: #04bfea;}
.blog_sidebar a{ color:#04bfea;}
.filter_button:hover {background-color:#04bfea;border-color:#04bfea }
.filter_current { background-color:#04bfea; border-color:#04bfea;}
.portfolio_item .view a.info:hover {background-color:#04bfea;}
.portfolio_item .view a.link:hover {background-color:#04bfea;}
.descr a { color: #04bfea;}
.portfolio_sidebar a{ color:#04bfea;}
.blog_item .meta a:hover { color:#04bfea;}
.blog_post_item_description .meta a:hover { color:#04bfea;}
.portfolio_post_item_description .meta a:hover { color:#04bfea;}
.page_sidebar a{ color:#04bfea;}

.blog_head h3 a:hover {color:#04bfea;}
.well a { color:#333;}
.well a:hover { color:#04bfea;}
.twitter-block .well a { color:#fff;}

<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'light_green.css') { ?>
.page_head .menu .current-menu-parent {background: #8eccb3;}
.page_head .menu li:hover {background: #8eccb3 !important;}
.main_content_area .current-menu-item a {color: #8eccb3 !important;}
.tp-rightarrow:hover { background-color:#8eccb3 !important;}
.tp-leftarrow:hover { background-color:#8eccb3 !important;}
.main_content_area .recent-post-widget a:hover { color:#8eccb3 !important }
a { color: #8eccb3;}
.colored {color: #8eccb3 !important;}
.top_line {background-color: #8eccb3 !important;}
.page_head .menu .current-menu-item {background: #8eccb3 !important ;}
.page_head .menu ul li:hover a { background:#8eccb3 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#8eccb3 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #8eccb3 ;}
.page_head .menu ul ul a:hover { background:#8eccb3 !important }
.blog_item .view a.info:hover {background-color:#8eccb3;}
.blog_item .view a.link:hover {background-color:#8eccb3;}
.pride_pg .current {background-color: #8eccb3;}
.pride_pg a:hover  {background-color: #8eccb3;}
.blog_sidebar a{ color:#8eccb3;}
.filter_button:hover {background-color:#8eccb3;border-color:#8eccb3 }
.filter_current { background-color:#8eccb3; border-color:#8eccb3;}
.portfolio_item .view a.info:hover {background-color:#8eccb3;}
.portfolio_item .view a.link:hover {background-color:#8eccb3;}
.descr a { color: #8eccb3;}
.portfolio_sidebar a{ color:#8eccb3;}
.blog_item .meta a:hover { color:#8eccb3;}
.blog_post_item_description .meta a:hover { color:#8eccb3;}
.portfolio_post_item_description .meta a:hover { color:#8eccb3;}
.page_sidebar a{ color:#8eccb3;}

.blog_head h3 a:hover {color:#8eccb3;}
.well a { color:#333;}
.well a:hover { color:#8eccb3;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'lime.css') { ?>
.page_head .menu .current-menu-parent {background: #aec71e;}
.page_head .menu li:hover {background: #aec71e;}
.main_content_area .current-menu-item a {color: #aec71e !important;}
.tp-rightarrow:hover { background-color:#aec71e !important;}
.tp-leftarrow:hover { background-color:#aec71e !important;}
.main_content_area .recent-post-widget a:hover { color:#aec71e !important }
a { color: #aec71e;}
.colored {color: #aec71e !important;}
.top_line {background-color: #aec71e !important;}
.page_head .menu .current-menu-item {background: #aec71e !important ;}
.page_head .menu ul li:hover a { background:#aec71e }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#aec71e ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #aec71e ;}
.page_head .menu ul ul a:hover { background:#aec71e !important }
.blog_item .view a.info:hover {background-color:#aec71e;}
.blog_item .view a.link:hover {background-color:#aec71e;}
.pride_pg .current {background-color: #aec71e;}
.pride_pg a:hover  {background-color: #aec71e;}
.blog_sidebar a{ color:#aec71e;}
.filter_button:hover {background-color:#aec71e;border-color:#aec71e }
.filter_current { background-color:#aec71e; border-color:#aec71e;}
.portfolio_item .view a.info:hover {background-color:#aec71e;}
.portfolio_item .view a.link:hover {background-color:#aec71e;}
.descr a { color: #aec71e;}
.portfolio_sidebar a{ color:#aec71e;}
.blog_item .meta a:hover { color:#aec71e;}
.blog_post_item_description .meta a:hover { color:#aec71e;}
.portfolio_post_item_description .meta a:hover { color:#aec71e;}
.page_sidebar a{ color:#aec71e;}

.blog_head h3 a:hover {color:#aec71e;}
.well a { color:#333;}
.well a:hover { color:#aec71e;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'navy.css') { ?>
.page_head .menu .current-menu-parent {background: #435960;}
.page_head .menu li:hover {background: #435960;}
.main_content_area .current-menu-item a {color: #435960 !important;}
.tp-rightarrow:hover { background-color:#435960 !important;}
.tp-leftarrow:hover { background-color:#435960 !important;}
.main_content_area .recent-post-widget a:hover { color:#435960 !important }
a { color: #435960;}
.colored {color: #435960 !important;}
.top_line {background-color: #435960 !important;}
.page_head .menu .current-menu-item {background: #435960 !important ;}
.page_head .menu ul li:hover a { background:#435960 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#435960 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #435960 ;}
.page_head .menu ul ul a:hover { background:#435960 !important }
.blog_item .view a.info:hover {background-color:#435960;}
.blog_item .view a.link:hover {background-color:#435960;}
.pride_pg .current {background-color: #435960;}
.pride_pg a:hover  {background-color: #435960;}
.blog_sidebar a{ color:#435960;}
.filter_button:hover {background-color:#435960;border-color:#435960 }
.filter_current { background-color:#435960; border-color:#435960;}
.portfolio_item .view a.info:hover {background-color:#435960;}
.portfolio_item .view a.link:hover {background-color:#435960;}
.descr a { color: #435960;}
.portfolio_sidebar a{ color:#435960;}
.blog_item .meta a:hover { color:#435960;}
.blog_post_item_description .meta a:hover { color:#435960;}
.portfolio_post_item_description .meta a:hover { color:#435960;}
.page_sidebar a{ color:#435960;}

.blog_head h3 a:hover {color:#435960;}
.well a { color:#333;}
.well a:hover { color:#435960;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'orange.css') { ?>
.page_head .menu .current-menu-parent {background: #EC5923;}
.page_head .menu li:hover {background: #EC5923;}
.main_content_area .current-menu-item a {color: #EC5923 !important;}
.tp-rightarrow:hover { background-color:#EC5923 !important;}
.tp-leftarrow:hover { background-color:#EC5923 !important;}
.main_content_area .recent-post-widget a:hover { color:#EC5923 !important }
a { color: #EC5923;}
.colored {color: #EC5923 !important;}
.top_line {background-color: #EC5923 !important;}
.page_head .menu .current-menu-item {background: #EC5923 !important ;}
.page_head .menu ul li:hover a { background:#EC5923 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#EC5923 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #EC5923 ;}
.page_head .menu ul ul a:hover { background:#EC5923 !important }
.blog_item .view a.info:hover {background-color:#EC5923;}
.blog_item .view a.link:hover {background-color:#EC5923;}
.pride_pg .current {background-color: #EC5923;}
.pride_pg a:hover  {background-color: #EC5923;}
.blog_sidebar a{ color:#EC5923;}
.filter_button:hover {background-color:#EC5923;border-color:#EC5923 }
.filter_current { background-color:#EC5923; border-color:#EC5923;}
.portfolio_item .view a.info:hover {background-color:#EC5923;}
.portfolio_item .view a.link:hover {background-color:#EC5923;}
.descr a { color: #EC5923;}
.portfolio_sidebar a{ color:#EC5923;}
.blog_item .meta a:hover { color:#EC5923;}
.blog_post_item_description .meta a:hover { color:#EC5923;}
.portfolio_post_item_description .meta a:hover { color:#EC5923;}
.page_sidebar a{ color:#EC5923;}

.blog_head h3 a:hover {color:#EC5923;}
.well a { color:#333;}
.well a:hover { color:#EC5923;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'pink.css') { ?>
.page_head .menu .current-menu-parent {background: #e44884;}
.page_head .menu li:hover {background: #e44884;}
.main_content_area .current-menu-item a {color: #e44884 !important;}
.tp-rightarrow:hover { background-color:#e44884 !important;}
.tp-leftarrow:hover { background-color:#e44884 !important;}
.main_content_area .recent-post-widget a:hover { color:#e44884 !important }
a { color: #e44884;}
.colored {color: #e44884 !important;}
.top_line {background-color: #e44884 !important;}
.page_head .menu .current-menu-item {background: #e44884 !important ;}
.page_head .menu ul li:hover a { background:#e44884 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#e44884 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #e44884 ;}
.page_head .menu ul ul a:hover { background:#e44884 !important }
.blog_item .view a.info:hover {background-color:#e44884;}
.blog_item .view a.link:hover {background-color:#e44884;}
.pride_pg .current {background-color: #e44884;}
.pride_pg a:hover  {background-color: #e44884;}
.blog_sidebar a{ color:#e44884;}
.filter_button:hover {background-color:#e44884;border-color:#e44884 }
.filter_current { background-color:#e44884; border-color:#e44884;}
.portfolio_item .view a.info:hover {background-color:#e44884;}
.portfolio_item .view a.link:hover {background-color:#e44884;}
.descr a { color: #e44884;}
.portfolio_sidebar a{ color:#e44884;}
.blog_item .meta a:hover { color:#e44884;}
.blog_post_item_description .meta a:hover { color:#e44884;}
.portfolio_post_item_description .meta a:hover { color:#e44884;}
.page_sidebar a{ color:#e44884;}

.blog_head h3 a:hover {color:#e44884;}
.well a { color:#333;}
.well a:hover { color:#e44884;}
.twitter-block .well a { color:#fff;}
<?php } ?>


<?php if($gcdata['alt_stylesheet'] == 'purple.css') { ?>
.page_head .menu .current-menu-parent {background: #46424f;}
.page_head .menu li:hover {background: #46424f;}
.main_content_area .current-menu-item a {color: #46424f !important;}
.tp-rightarrow:hover { background-color:#46424f !important;}
.tp-leftarrow:hover { background-color:#46424f !important;}
.main_content_area .recent-post-widget a:hover { color:#46424f !important }
a { color: #46424f;}
.colored {color: #46424f !important;}
.top_line {background-color: #46424f !important;}
.page_head .menu .current-menu-item {background: #46424f !important ;}
.page_head .menu ul li:hover a { background:#46424f }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#46424f ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #46424f ;}
.page_head .menu ul ul a:hover { background:#46424f !important }
.blog_item .view a.info:hover {background-color:#46424f;}
.blog_item .view a.link:hover {background-color:#46424f;}
.pride_pg .current {background-color: #46424f;}
.pride_pg a:hover  {background-color: #46424f;}
.blog_sidebar a{ color:#46424f;}
.filter_button:hover {background-color:#46424f;border-color:#46424f }
.filter_current { background-color:#46424f; border-color:#46424f;}
.portfolio_item .view a.info:hover {background-color:#46424f;}
.portfolio_item .view a.link:hover {background-color:#46424f;}
.descr a { color: #46424f;}
.portfolio_sidebar a{ color:#46424f;}
.blog_item .meta a:hover { color:#46424f;}
.blog_post_item_description .meta a:hover { color:#46424f;}
.portfolio_post_item_description .meta a:hover { color:#46424f;}
.page_sidebar a{ color:#46424f;}

.blog_head h3 a:hover {color:#46424f;}
.well a { color:#333;}
.well a:hover { color:#46424f;}
.twitter-block .well a { color:#fff;}
<?php } ?>

<?php if($gcdata['alt_stylesheet'] == 'red.css') { ?>
.page_head .menu .current-menu-parent {background: #a81010;}
.page_head .menu li:hover {background: #a81010;}
.main_content_area .current-menu-item a {color: #a81010 !important;}
.tp-rightarrow:hover { background-color:#a81010 !important;}
.tp-leftarrow:hover { background-color:#a81010 !important;}
.main_content_area .recent-post-widget a:hover { color:#a81010 !important }
a { color: #a81010;}
.colored {color: #a81010 !important;}
.top_line {background-color: #a81010 !important;}
.page_head .menu .current-menu-item {background: #a81010 !important ;}
.page_head .menu ul li:hover a { background:#a81010 }
.page_head .menu ul li:hover:first-child > a:after { border-bottom-color:#a81010 ;}
.page_head .menu ul ul li:hover:first-child > a:after { border-right-color: #a81010 ;}
.page_head .menu ul ul a:hover { background:#a81010 !important }
.blog_item .view a.info:hover {background-color:#a81010;}
.blog_item .view a.link:hover {background-color:#a81010;}
.pride_pg .current {background-color: #a81010;}
.pride_pg a:hover  {background-color: #a81010;}
.blog_sidebar a{ color:#a81010;}
.filter_button:hover {background-color:#a81010;border-color:#a81010 }
.filter_current { background-color:#a81010; border-color:#a81010;}
.portfolio_item .view a.info:hover {background-color:#a81010;}
.portfolio_item .view a.link:hover {background-color:#a81010;}
.descr a { color: #a81010;}
.portfolio_sidebar a{ color:#a81010;}
.blog_item .meta a:hover { color:#a81010;}
.blog_post_item_description .meta a:hover { color:#a81010;}
.portfolio_post_item_description .meta a:hover { color:#a81010;}
.page_sidebar a{ color:#a81010;}

.blog_head h3 a:hover {color:#a81010;}
.well a { color:#333;}
.well a:hover { color:#a81010;}
.twitter-block .well a { color:#fff;}

<?php } ?>

<?php echo $gcdata['custom_css']; ?>
	
</style>