<?php get_header(); ?>
   <div class="main_content_area">
   <div class="container">
        <div class="row">
            <!--Page contetn-->
            <div class="span12 gc_error">
            	<h3><strong class="colored"><?php _e("Oops, 404 Error!","builder"); ?></strong><br> <?php _e("The page you were looking for could not be found.","builder"); ?></h3>
            </div>
            <!--/Page contetn-->
		</div>
    </div>
    </div>
<?php get_footer(); ?>