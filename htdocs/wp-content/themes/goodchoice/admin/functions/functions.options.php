<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories = array();  
		$of_categories_obj = get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp = array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages = array();
		$of_pages_obj = get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		$of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp = array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select = array("one","two","three","four","five"); 
		$of_options_radio = array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" => "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		$of_theme_layout = array( "1" => "Fullwidth and 960px container", "2" => "Boxed and 1170px container", "3" => "Fullwidth and 1170px container", "4" => "Boxed and 960px container"   );
		$of_tag_line_position = array("1" => "Before Slider", "2" => "After Slider");
		$of_portfolio_style = array("1" => "6 Columns Portfolio", "2" => "4 Columns Portfolio", "3" => "3 Columns Portfolio", "4" => "2 Columns Portfolio", "5" => "Portfolio with Sidebar");
		$of_portfolio_details_style = array("1" => "Landscape Style", "2" => "Portrait Style", "3" => "With Sidebar");
		$of_blog_style = array("1" => "Large Images", "2" => "Medium Images");
		$of_blog_sidebar = array("1" => "Right Sidebar", "2" => "Left Sidebar");
		$of_portfolio_sidebar = array("1" => "Right Sidebar", "2" => "Left Sidebar");
		$of_blog_image_hover_icons = array("1" => "Zoom icon + Link icon", "2" => "Zoom icon only", "3" => "Link icon only");
		$of_blog_date_format = array("1" => "American Style", "2" => "European Style");
		$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr = wp_upload_dir();
		$all_uploads_path = $uploads_arr['path'];
		$all_uploads = get_option('of_uploads');
		$other_entries = array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat = array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos = array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

//Layout
$of_options[] = array( "name" => __("General Settings","goodchoice"),
					"type" => "heading");


$of_options[] = array( "name" => __("GENERAL SETTINGS","goodchoice"),
					"desc" => __("Select your themes alternative color scheme","goodchoice"),
					"id" => "alt_stylesheet",
					"std" => "empty.css",
					"type" => "select",
					"options" => $alt_stylesheets);


$of_options[] = array( "name" => "",
					"desc" => __("Choose WebSite Layout","goodchoice"),
					"id" => "theme_layout",
					"std" => "1",
					"type" => "select",
					"options" => $of_theme_layout);

$of_options[] = array( "name" => "",
					"desc" => __("Upload your favicon.ico","goodchoice"),
					"id" => "header_favicon",
					"std" => "http://www.orange-idea.com/assets/builder/favicon.ico",
					"type" => "media");

					  
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Container Background","goodchoice"),
					"id" => "theme_conatiner_bg_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  __("BOXED LAYOUT SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the Background","goodchoice"),
					"id" => "theme_boxed_bg_color",
					"std" => "#fdfdfd",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Past the value for top and bottom margin for main container","goodchoice"),
					"id" => "theme_boxed_margin",
					"std" => "0",
					"type" => "text");
					
$of_options[] = array( "name" => " ",
					"desc" => __("Pick a image for the Background ( Choose first image to show only background color )","goodchoice"),
					"id" => "theme_boxed_bg",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);



$of_options[] = array("name" =>  __("HEADERS TYPOGRAPHY","goodchoice"),
					"desc" => __("Specify the h1 header font properties","goodchoice"),
					"id" => "headers_font_one",
					"std" => array('size' => '36px', 'face' => 'Open Sans','style' => 'normal','color' => '#555555'),
					"type" => "typography");  

$of_options[] = array("name" =>  "",
					"desc" => __("Specify the h2 header font properties","goodchoice"),
					"id" => "headers_font_two",
					"std" => array('size' => '30px','face' => 'Open Sans','style' => 'normal','color' => '#555555'),
					"type" => "typography");  


$of_options[] = array("name" =>  "",
					"desc" => __("Specify the h3 header font properties","goodchoice"),
					"id" => "headers_font_three",
					"std" => array('size' => '24px','face' => 'Open Sans','style' => 'normal','color' => '#444444'),
					"type" => "typography");  

$of_options[] = array("name" =>  "",
					"desc" => __("Specify the h4 header font properties","goodchoice"),
					"id" => "headers_font_four",
					"std" => array('size' => '18px','face' => 'Open Sans','style' => 'normal','color' => '#444444'),
					"type" => "typography");  

$of_options[] = array("name" =>  "",
					"desc" => __("Specify the h5 header font properties","goodchoice"),
					"id" => "headers_font_five",
					"std" => array('size' => '14px','face' => 'Open Sans','style' => 'normal','color' => '#444444'),
					"type" => "typography");  

$of_options[] = array("name" =>  "",
					"desc" => __("Specify the h6 header font properties","goodchoice"),
					"id" => "headers_font_six",
					"std" => array('size' => '12px','face' => 'Open Sans','style' => 'normal','color' => '#444444'),
					"type" => "typography");


$of_options[] = array("name" =>  __("BODY TYPOGRAPHY","goodchoice"),
					"desc" => __("Specify body font properties","goodchoice"),
					"id" => "body_font",
					"std" => array('size' => '12px', 'face' => 'arial','style' => 'normal','color' => '#777777'),
					"type" => "typography");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the links","goodchoice"),
					"id" => "main_conent_links",
					"std" => "#04bfea",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the links, when hover","goodchoice"),
					"id" => "main_conent_links_hover",
					"std" => "#000000",
					"type" => "color");



$of_options[] = array( "name" => __("REVOLUTION SLIDER","goodchoice"),
					"desc" => __("Show Revolution Slider on the HomePage?","goodchoice"),
					"id" => "revolution_homepage",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => "",
					"desc" => __("Show Revolution Slider on other pages?","goodchoice"),
					"id" => "revolution_index",
					"std" => 0,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" => __("CUSTOM CSS","goodchoice"),
					"desc" => __("Please put your custom css here","goodchoice"),
					"id" => "custom_css",
					"std" => "",
					"type" => "textarea");



//Top Line
$of_options[] = array( "name" => __("Top Line","goodchoice"),
					"type" => "heading");

$of_options[] = array( "name" => __("Show/Hide Top Line","goodchoice"),
					"desc" => __("Show Top Line","goodchoice"),
					"id" => "top_line_show",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" =>  __("TOP LINE BACKGROND","goodchoice"),
					"desc" => __("Pick a color for the 'Top Line' area background","goodchoice"),
					"id" => "theme_colors_top_line",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a image for the 'Top Line' area background ( Choose first image to show only background color )","goodchoice"),
					"id" => "theme_colors_top_line_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" => __("TOP LINE TEXT","goodchoice"),
					"desc" => __("Past your text or HTML","goodchoice"),
					"id" => "header_top_line",
					"std" => "",
					"type" => "text");					

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Top Line text","goodchoice"),
					"id" => "theme_colors_top_line_text",
					"std" => "#FFFFFF",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Top Line links","goodchoice"),
					"id" => "theme_colors_top_line_a",
					"std" => "#FFFFFF",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Top Line mouse over links","goodchoice"),
					"id" => "theme_colors_top_line_a_hover",
					"std" => "#FFFFFF",
					"type" => "color");


					
$of_options[] = array( "name" => __("SOCIAL ICONS","goodchoice"),
					"desc" => __("Twitter","goodchoice"),
					"id" => "header_social_tw",
					"std" => "",
					"type" => "text");	

$of_options[] = array( "name" => "",
					"desc" => __("Facebook","goodchoice"),
					"id" => "header_social_fb",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Google +","goodchoice"),
					"id" => "header_social_g",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Dribbble","goodchoice"),
					"id" => "header_social_dr",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Flickr","goodchoice"),
					"id" => "header_social_fl",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("YouTube","goodchoice"),
					"id" => "header_social_yt",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Linkedin","goodchoice"),
					"id" => "header_social_in",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Pinterest","goodchoice"),
					"id" => "header_social_pi",
					"std" => "",
					"type" => "text");



//Logo And Menu
$of_options[] = array( "name" => __("Logo And Menu","goodchoice"),
					"type" => "heading");
          

$of_options[] = array( "name" => __("LOGO AND MENU AREA","goodchoice"),
					"desc" => __("Top padding","goodchoice"),
					"id" => "logo_and_menu_t_padding",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Bottom padding","goodchoice"),
					"id" => "logo_and_menu_b_padding",
					"std" => "0",
					"type" => "text");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the 'Logo and Menu' area background","goodchoice"),
					"id" => "logo_and_menu_bg",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a image for the 'Logo and Menu' area background ( Choose first image to show only background color )","goodchoice"),
					"id" => "logo_and_menu_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);



$of_options[] = array( "name" => __("LOGO OPTIONS","goodchoice"),
					"desc" => __("Upload your logo","goodchoice"),
					"id" => "header_logo",
					"std" => "http://www.orange-idea.com/assets/goodchoice/logo.png",
					"type" => "media");

$of_options[] = array( "name" => "",
					"desc" => __("Top margin for logo"),
					"id" => "logo_and_menu_logo_margin",
					"std" => "20",
					"type" => "text");
					

$of_options[] = array( "name" => __("MENU SETTINGS","builder"),
					"desc" => __("Top margin for menu","builder"),
					"id" => "logo_and_menu_menu_margin",
					"std" => "0",
					"type" => "text");


$of_options[] = array( "name" => "",
					"desc" => __("Menu items Horizontal Padding","builder"),
					"id" => "logo_and_menu_menu_item_horizontal_padding",
					"std" => "22",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Menu items Vertical Padding","builder"),
					"id" => "logo_and_menu_menu_item_vertical_padding",
					"std" => "23",
					"type" => "text");


$of_options[] = array( "name" => "",
					"desc" => __("Pick a color for menu item text","builder"),
					"id" => "logo_and_menu_menu_item_color",
					"std" => "#3a3a3a",
					"type" => "color"); 


$of_options[] = array( "name" => "",
					"desc" => __("Menu items FontSize","builder"),
					"id" => "logo_and_menu_menu_item_fontsize",
					"std" => "13",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Menu items UPPERCASE?","builder"),
					"id" => "logo_and_menu_menu_item_uppercase",
					"std" => 1,
					"type" => "checkbox"); 

$of_options[] = array( "name" => "",
					"desc" => __("Menu items FontWeight","builder"),
					"id" => "logo_and_menu_menu_item_fontweight",
					"std" => "700",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a background color for menu item on hover","builder"),
					"id" => "logo_and_menu_menu_item_li_hover",
					"std" => "#04bfea",
					"type" => "color"); 

$of_options[] = array( "name" => "",
					"desc" => __("Pick a color for menu item text on hover","builder"),
					"id" => "logo_and_menu_menu_item_li_text_hover",
					"std" => "#ffffff",
					"type" => "color"); 

$of_options[] = array( "name" => "",
					"desc" => __("Pick a background for current menu item","builder"),
					"id" => "logo_and_menu_menu_item_bg_current",
					"std" => "#04bfea",
					"type" => "color"); 

$of_options[] = array( "name" => "",
					"desc" => __("Pick a color for current menu item text","builder"),
					"id" => "logo_and_menu_menu_item_color_current",
					"std" => "#ffffff",
					"type" => "color");



$of_options[] = array( "name" => __("MENU SUBTITLE SETTINGS","builder"),
					"desc" => __("Menu items SUBTITLE FontSize","builder"),
					"id" => "logo_and_menu_menu_item_sub_fontsize",
					"std" => "10",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a color for menu item SUBTITLE text","builder"),
					"id" => "logo_and_menu_menu_item_sub_color",
					"std" => "#888888",
					"type" => "color");


$of_options[] = array( "name" => "",
					"desc" => __("Menu items SUBTITLE top margin","builder"),
					"id" => "logo_and_menu_menu_item_sub_top_margin",
					"std" => "-6",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a color for menu item SUBTITLE text on hover","builder"),
					"id" => "logo_and_menu_menu_item_sub_text_hover",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a color for current menu item SUBTITLE text","builder"),
					"id" => "logo_and_menu_menu_item_sub_color_current",
					"std" => "#ffffff",
					"type" => "color");



$of_options[] = array( "name" => __("MENU SUBLEVEL SETTINGS","builder"),
					"desc" => __("Menu items SUBLEVEL background","builder"),
					"id" => "logo_and_menu_menu_item_ul_color",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Menu items SUBLEVEL text color","builder"),
					"id" => "logo_and_menu_menu_item_ul_textcolor",
					"std" => "#ffffff",
					"type" => "color");


$of_options[] = array( "name" => "",
					"desc" => __("Menu items SUBLEVEL background on hover","builder"),
					"id" => "logo_and_menu_menu_item_ul_bg_hover",
					"std" => "#04bfea",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Menu items SUBLEVEL text color on hover","builder"),
					"id" => "logo_and_menu_menu_item_ul_text_hover",
					"std" => "#fff",
					"type" => "color");

















					
//Tag Line
$of_options[] = array( "name" => __("Tag Line","goodchoice"),
					"type" => "heading");
					
$of_options[] = array( "name" => __("Show/Hide Tag Line","goodchoice"),
					"desc" => __("Show Tag Line?","goodchoice"),
					"id" => "tag_line_show",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => "",
					"desc" => __("Show Breadcumbs?","goodchoice"),
					"id" => "breadcumbs",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" => "",
					"desc" => __("Choose Tag Line Position on Home Page","goodchoice"),
					"id" => "tag_line_position",
					"std" => "1",
					"type" => "select",
					"options" => $of_tag_line_position);

$of_options[] = array( "name" => __("TAG LINE SETTINGS","goodchoice"),
					"desc" => __("Homepage Tag Line content","goodchoice"),
					"id" => "header_tagline",
					"std" => "<div style='text-align:center'>Powerful & responsive wordpress theme with hundreds options.</div><div style='text-align:center; margin-top:-10px'>Your most important message or cacthy phrase goes here.</div>",
					"type" => "text");	

$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the tag line background","goodchoice"),
					"id" => "tag_line_bg",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the tag line top border","goodchoice"),
					"id" => "tag_line_border_top",
					"std" => "#ededed",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the tag line bottom border","goodchoice"),
					"id" => "tag_line_border_bottom",
					"std" => "#ededed",
					"type" => "color");

$of_options[] = array( "name" => " ",
					"desc" => __("Tag Line top padding","goodchoice"),
					"id" => "tag_line_padding_top",
					"std" => "20",
					"type" => "text");
					
$of_options[] = array( "name" => "",
					"desc" => __("Tag Line bottom padding","goodchoice"),
					"id" => "tag_line_padding_bottom",
					"std" => "20",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Tag Line bottom margin","goodchoice"),
					"id" => "main_content_margin_top",
					"std" => "30",
					"type" => "text");

$of_options[] = array( "name" => __("BACKGROUND PATTERNS","goodchoice"),
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "tag_line_custom_bg",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);





/* Page Sidebar */
$of_options[] = array( "name" => __("Page Sidebar","goodchoice"),
					"type" => "heading");

$of_options[] = array( "name" =>  __("PAGE SIDEBAR","goodchoice"),
					"desc" => __("How many sidebars do you want?","goodchoice"),
					"id" => "page_sidebar_generator",
					"std" => "8",
					"type" => "text");



$of_options[] = array( "name" =>  __("SIDEBAR AREA SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the widget background","goodchoice"),
					"id" => "page_sidebar_bg_color",
					"std" => "",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Border radius value","goodchoice"),
					"id" => "page_sidebar_border_radius",
					"std" => "0",
					"type" => "text");



$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "page_sidebar_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" =>  __("WIDGET SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the widget background","goodchoice"),
					"id" => "page_sidebar_widget_bg_color",
					"std" => "#ffffff",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget border","goodchoice"),
					"id" => "page_sidebar_widget_border_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Border radius value","goodchoice"),
					"id" => "page_sidebar_widget_border_radius",
					"std" => "4",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "page_sidebar_widget_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the widget header","goodchoice"),
					"id" => "page_sidebar_widget_header_color",
					"std" => "#333333",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for 'separator' after widget header","goodchoice"),
					"id" => "page_sidebar_widget_hr",
					"std" => "#ffffff",
					"type" => "color");					


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget text","goodchoice"),
					"id" => "page_sidebar_widget_text_color",
					"std" => "#666666",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget links","goodchoice"),
					"id" => "page_sidebar_widget_links_color",
					"std" => "#777777",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget links on hover","goodchoice"),
					"id" => "page_sidebar_widget_links_color_hover",
					"std" => "#04bfea",
					"type" => "color");




/* Portfolio */
$of_options[] = array( "name" => __("Portfolio","goodchoice"),
					"type" => "heading");



$of_options[] = array( "name" => __("CHOOSE PORTFOLIO STYLE","goodchoice"),
					"desc" => "",
					"id" => "sl_portfolio_style",
					"std" => "1",
					"type" => "select",
					"options" => $of_portfolio_style);  
					
$of_options[] = array("name" =>  "",
					"desc" => __("Amount of projects on one page","goodchoice"),
					"id" => "sl_portfolio_projects",
					"std" => "10",
					"type" => "text");


$of_options[] = array( "name" => __("FILTER BUTTONS SETTINGS","goodchoice"),
					"desc" => __("Vertical padding amount","goodchoice"),
					"id" => "portfolio_filter_padding_v",
					"std" => "4",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Horizontal padding amount","goodchoice"),
					"id" => "portfolio_filter_padding_h",
					"std" => "8",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Right margin amount","goodchoice"),
					"id" => "portfolio_filter_margin",
					"std" => "3",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for buttons background","goodchoice"),
					"id" => "portfolio_filter_bg_color",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for buttons border","goodchoice"),
					"id" => "portfolio_filter_border_color",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for buttons text","goodchoice"),
					"id" => "portfolio_filter_text_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for text shadow","goodchoice"),
					"id" => "portfolio_filter_text_shadow",
					"std" => "#111111",
					"type" => "color");


$of_options[] = array( "name" => "",
					"desc" => __("Font size","goodchoice"),
					"id" => "portfolio_filter_text_size",
					"std" => "11",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Border radius amount","goodchoice"),
					"id" => "portfolio_filter_border_radius",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for buttons background when hover","goodchoice"),
					"id" => "portfolio_filter_bg_color_hover",
					"std" => "#04bfea",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for buttons text when hover","goodchoice"),
					"id" => "portfolio_filter_text_color_hover",
					"std" => "#ffffff",
					"type" => "color");



$of_options[] = array( "name" =>  __("IMAGE HOVER EFFECTS (RGBA)","goodchoice"),
					"desc" => __("First rgba value (red color)","goodchoice"),
					"id" => "portfolio_image_bg_1",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Second rgba value (green color)","goodchoice"),
					"id" => "portfolio_image_bg_2",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Third rgba value (blue color)","goodchoice"),
					"id" => "portfolio_image_bg_3",
					"std" => "0",
					"type" => "text");


$of_options[] = array( "name" =>  "",
					"desc" => __("Fourth rgba value (opacity)","goodchoice"),
					"id" => "portfolio_image_bg_op",
					"std" => "0.15",
					"type" => "text");
					
					

$of_options[] = array( "name" => __("ZOOM AND LINK ICONS SETTINGS","goodchoice"),
					"desc" => __("Choose Icons for image hover","goodchoice"),
					"id" => "portfolio_image_hover_icons",
					"std" => "1",
					"type" => "select",
					"options" => $of_blog_image_hover_icons);

$of_options[] = array( "name" => "",
					"desc" => __("Upload Link icon","goodchoice"),
					"id" => "portfolio_image_icons_link",
					"std" => "http://www.orange-idea.com/assets/builder/link.png",
					"type" => "media");

$of_options[] = array( "name" => "",
					"desc" => __("Upload Zoom icon","goodchoice"),
					"id" => "portfolio_image_icons_zoom",
					"std" => "http://www.orange-idea.com/assets/builder/zoom.png",
					"type" => "media");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the icons background","goodchoice"),
					"id" => "portfolio_image_icons_bg",
					"std" => "#000000",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the icons background when hover","goodchoice"),
					"id" => "portfolio_image_icons_bg_hover",
					"std" => "#04bfea",
					"type" => "color");


$of_options[] = array( "name" => __("SMALL DESCRIPTIOS SETTINGS","goodchoice"),
					"desc" => "Show small description?",
					"id" => "portfolio_descr_show",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the background","goodchoice"),
					"id" => "portfolio_descr_bg_color",
					"std" => "#f5f5f5",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the border","goodchoice"),
					"id" => "portfolio_descr_border_color",
					"std" => "#f5f5f5",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "portfolio_descr_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);

$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the links","goodchoice"),
					"id" => "portfolio_descr_links_color",
					"std" => "#04bfea",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the links when hover","goodchoice"),
					"id" => "portfolio_descr_links_color_hover",
					"std" => "#000000",
					"type" => "color");


$of_options[] = array( "name" => "",
					"desc" => __("Show small description?","goodchoice"),
					"id" => "portfolio_descr_clo_text",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color small description","goodchoice"),
					"id" => "portfolio_descr_text_color",
					"std" => "#747474",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Small description font size","goodchoice"),
					"id" => "portfolio_descr_text_size",
					"std" => "11",
					"type" => "text");



					
/* Portfolio Post */
$of_options[] = array( "name" => __("Portfolio Post","goodchoice"),
					"type" => "heading");


$of_options[] = array( "name" => __("CHOOSE DETAILS PAGE STYLE","goodchoice"),
					"desc" => "",
					"id" => "sl_portfolio_details_style",
					"std" => "1",
					"type" => "select",
					"options" => $of_portfolio_details_style); ;

					
$of_options[] = array( "name" => __("DESCRIPTION SETTINGS","goodchoice"),
					"desc" => __("Show 'Next post and Previous post' pagination?","goodchoice"),
					"id" => "portfolio_details_pagination",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the 'Next post and Previous post","goodchoice"),
					"id" => "portfolio_post_show_posts_meta_color",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the 'Next post and Previous post color when hover","goodchoice"),
					"id" => "portfolio_post_show_posts_meta_color_hover",
					"std" => "#04bfea",
					"type" => "color");




$of_options[] = array( "name" =>  "",
					"desc" => __("Padding value","goodchoice"),
					"id" => "portfolio_post_item_description_padding",
					"std" => "0",
					"type" => "text");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for background","goodchoice"),
					"id" => "portfolio_post_item_description_bg_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for text","goodchoice"),
					"id" => "portfolio_post_item_description_text_color",
					"std" => "#747474",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for border","goodchoice"),
					"id" => "portfolio_post_item_description_border_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "portfolio_post_item_description_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" => __("RELATED PROJECTS","goodchoice"),
					"desc" => __("Show 'Related Projects'?","goodchoice"),
					"id" => "portfolio_details_related",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" =>  "",
					"desc" => __("Related Projects title","goodchoice"),
					"id" => "portfolio_details_related_title",
					"std" => "Related Projects",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Related Projects description","goodchoice"),
					"id" => "portfolio_details_related_description",
					"std" => "Venis potest flens ibidem quod eam in. Ambo una litus vita Apolloni codicellos iam custodio vocem magno dies tuum abscondere.",
					"type" => "text");



/* Portfolio Sidebar */
$of_options[] = array( "name" => __("Portfolio Sidebar","goodchoice"),
					"type" => "heading");
				

$of_options[] = array( "name" => __("CHOOSE SIDEBAR POSITION","goodchoice"),
					"desc" => "",
					"id" => "portfolio_sidebar_position",
					"std" => "1",
					"type" => "select",
					"options" => $of_portfolio_sidebar);


$of_options[] = array( "name" =>  __("SIDEBAR AREA SETTINGS","goodchoice"),
					"desc" => "Pick a color for the widget background",
					"id" => "portfolio_sidebar_bg_color",
					"std" => "",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Border radius value","goodchoice"),
					"id" => "portfolio_sidebar_border_radius",
					"std" => "0",
					"type" => "text");



$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "portfolio_sidebar_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);




$of_options[] = array( "name" =>  __("WIDGET SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the widget background","goodchoice"),
					"id" => "portfolio_sidebar_widget_bg_color",
					"std" => "#ffffff",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget border","goodchoice"),
					"id" => "portfolio_sidebar_widget_border_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Border radius value","goodchoice"),
					"id" => "portfolio_sidebar_widget_border_radius",
					"std" => "4",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "portfolio_sidebar_widget_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the widget header","goodchoice"),
					"id" => "portfolio_sidebar_widget_header_color",
					"std" => "#333333",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for 'separator' after widget header","goodchoice"),
					"id" => "portfolio_sidebar_widget_hr",
					"std" => "#ffffff",
					"type" => "color");					


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget text","goodchoice"),
					"id" => "portfolio_sidebar_widget_text_color",
					"std" => "#666666",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget links","goodchoice"),
					"id" => "portfolio_sidebar_widget_links_color",
					"std" => "#777777",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget links on hover","goodchoice"),
					"id" => "portfolio_sidebar_widget_links_color_hover",
					"std" => "#04bfea",
					"type" => "color");


/* Blog Settings */
$of_options[] = array( "name" => __("Blog","goodchoice"),
					"type" => "heading");


$of_options[] = array( "name" => __("CHOOSE BLOG STYLE","goodchoice"),
					"desc" => "",
					"id" => "sl_blog_style",
					"std" => "1",
					"type" => "select",
					"options" => $of_blog_style);
					
					
$of_options[] = array( "name" => __("DATE SETTINGS","goodchoice"),
					"desc" => __("Show Posts Date?","goodchoice"),
					"id" => "blog_show_posts_date",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => "",
					"desc" => __("Choose Date Format","goodchoice"),
					"id" => "blog_date_format",
					"std" => "1",
					"type" => "select",
					"options" => $of_blog_date_format);


$of_options[] = array( "name" => "",
					"desc" => __("Show Date Icon?","goodchoice"),
					"id" => "blog_show_date_icon",
					"std" => 0,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog date background","goodchoice"),
					"id" => "blog_date_bg",
					"std" => "#f1f1f1",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog date text","goodchoice"),
					"id" => "blog_date_color",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog date text shadow","goodchoice"),
					"id" => "blog_date_text_shadow",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array("name" =>  "",
					"desc" => __("Past the value for border radius","goodchoice"),
					"id" => "blog_date_border_radius",
					"std" => "0",
					"type" => "text");


$of_options[] = array( "name" => __("META TAGS SETTINGS","goodchoice"),
					"desc" => __("Show Post Author?","goodchoice"),
					"id" => "blog_show_posts_meta_author",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" => "",
					"desc" => __("Show Post Category?","goodchoice"),
					"id" => "blog_show_posts_meta_category",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => "",
					"desc" => __("Show Post Comments?","goodchoice"),
					"id" => "blog_show_posts_meta_comments",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog meta color","goodchoice"),
					"id" => "blog_show_posts_meta_color",
					"std" => "#3a3a3a",
					"type" => "color");
					

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog meta color when hover","goodchoice"),
					"id" => "blog_show_posts_meta_color_hover",
					"std" => "#04bfea",
					"type" => "color");



$of_options[] = array( "name" =>  __("IMAGE HOVER EFFECTS (RGBA)","goodchoice"),
					"desc" => __("First rgba value (red color)","goodchoice"),
					"id" => "blog_image_bg_1",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Second rgba value (green color)","goodchoice"),
					"id" => "blog_image_bg_2",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Third rgba value (blue color)","goodchoice"),
					"id" => "blog_image_bg_3",
					"std" => "0",
					"type" => "text");


$of_options[] = array( "name" =>  "",
					"desc" => __("Fourth rgba value (opacity)","goodchoice"),
					"id" => "blog_image_bg_op",
					"std" => "0.15",
					"type" => "text");
					
					

$of_options[] = array( "name" => __("ZOOM AND LINK ICONS SETTINGS","goodchoice"),
					"desc" => __("Choose Icons for image hover","goodchoice"),
					"id" => "blog_image_hover_icons",
					"std" => "1",
					"type" => "select",
					"options" => $of_blog_image_hover_icons);

$of_options[] = array( "name" => "",
					"desc" => __("Upload Link icon","goodchoice"),
					"id" => "blog_image_icons_link",
					"std" => "http://www.orange-idea.com/assets/builder/link.png",
					"type" => "media");

$of_options[] = array( "name" => "",
					"desc" => __("Upload Zoom icon","goodchoice"),
					"id" => "blog_image_icons_zoom",
					"std" => "http://www.orange-idea.com/assets/builder/zoom.png",
					"type" => "media");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the icons background","goodchoice"),
					"id" => "blog_image_icons_bg",
					"std" => "#000000",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the icons background when hover","goodchoice"),
					"id" => "blog_image_icons_bg_hover",
					"std" => "#04bfea",
					"type" => "color");


$of_options[] = array( "name" =>  __("POST PREVIEW SETTINGS","goodchoice"),
					"desc" => __("Pick a color for text","goodchoice"),
					"id" => "blog_item_description_text_color",
					"std" => "#666666",
					"type" => "color");



$of_options[] = array( "name" =>  "",
					"desc" => __("Padding value","goodchoice"),
					"id" => "blog_item_description_padding",
					"std" => "20",
					"type" => "text");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for background","goodchoice"),
					"id" => "blog_item_description_bg_color",
					"std" => "#f6f6f6",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for border","goodchoice"),
					"id" => "blog_item_description_border_color",
					"std" => "#f6f6f6",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "blog_item_description_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" => __("BLOG ARCHIVE","goodchoice"),
					"desc" => __("Show Featured Images In Blog Archive?","goodchoice"),
					"id" => "blog_archive_show_imges",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" =>  "",
					"desc" => __("'Blog Archive' page title","goodchoice"),
					"id" => "blog_archive_title",
					"std" => "Blog Archive",
					"type" => "text");






					
/* Blog Post */
$of_options[] = array( "name" => __("Blog Post","goodchoice"),
					"type" => "heading");



$of_options[] = array( "name" => __("FEATURED IMAGE SETTINGS","goodchoice"),
					"desc" => __("Show Featured Image?","goodchoice"),
					"id" => "blog_post_show_featured_image",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => __("META TAGS SETTINGS","goodchoice"),
					"desc" => __("Show Post Title?","goodchoice"),
					"id" => "blog_post_show_posts_meta_title",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");



$of_options[] = array( "name" => "",
					"desc" => __("Show Post Author?","goodchoice"),
					"id" => "blog_post_show_posts_meta_author",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");




$of_options[] = array( "name" => "",
					"desc" => __("Show Post Category?","goodchoice"),
					"id" => "blog_post_show_posts_meta_category",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => "",
					"desc" => __("Show Post Comments?","goodchoice"),
					"id" => "blog_post_show_posts_meta_comments",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");



$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog meta color","goodchoice"),
					"id" => "blog_post_show_posts_meta_color",
					"std" => "#b7b7b7",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Blog meta color when hover","goodchoice"),
					"id" => "blog_post_show_posts_meta_color_hover",
					"std" => "#04bfea",
					"type" => "color");



$of_options[] = array( "name" => __("SHARE OPTIONS","goodchoice"),
					"desc" => "Show 'Share This' button?",
					"id" => "blog_post_show_share_button",
					"std" => 0,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" =>  "",
					"desc" => __("'Share' text","goodchoice"),
					"id" => "blog_post_show_share_button_text",
					"std" => "Share This Story:",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Padding value","goodchoice"),
					"id" => "blog_share_padding",
					"std" => "7",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for Background","goodchoice"),
					"id" => "blog_share_bg_color",
					"std" => "#ededed",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for text","goodchoice"),
					"id" => "blog_share_text_color",
					"std" => "#3d3d3d",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "blog_share_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);

$of_options[] = array( "name" => __("ABOUT AUTHOR OPTIONS","goodchoice"),
					"desc" => __("Show 'About Author'?","goodchoice"),
					"id" => "blog_post_show_author",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" => "",
					"desc" => __("Upload your avatar","goodchoice"),
					"id" => "blog_post_show_author_avatar",
					"std" => "http://1.s3.envato.com/files/31496845/oi-80.jpg",
					"type" => "media");


$of_options[] = array( "name" =>  "",
					"desc" => __("Header text","goodchoice"),
					"id" => "blog_post_show_author_header",
					"std" => "About The Author",
					"type" => "text");


$of_options[] = array( "name" =>  "",
					"desc" => __("Padding value","goodchoice"),
					"id" => "blog_author_item_description_padding",
					"std" => "20",
					"type" => "text");



$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for text","goodchoice"),
					"id" => "blog_author_item_description_text_color",
					"std" => "#747474",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for background","goodchoice"),
					"id" => "blog_author_item_description_bg_color",
					"std" => "#f9f9f9",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for border","goodchoice"),
					"id" => "blog_author_item_description_border_color",
					"std" => "#ededed",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "blog_author_item_description_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);



$of_options[] = array( "name" => __("COMMENTS OPTIONS","goodchoice"),
					"desc" => __("Show 'comments'?","goodchoice"),
					"id" => "blog_post_show_comments",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" =>  "",
					"desc" => __("Left padding value","goodchoice"),
					"id" => "blog_comments_padding",
					"std" => "20",
					"type" => "text");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for MAIN LEFT border","goodchoice"),
					"id" => "blog_comments_border_color",
					"std" => "#ededed",
					"type" => "color");
					
$of_options[] = array( "name" =>  " ",
					"desc" => __("Left padding value","goodchoice"),
					"id" => "blog_comments_li_padding",
					"std" => "20",
					"type" => "text");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for comments border","goodchoice"),
					"id" => "blog_comments_border_color",
					"std" => "#ededed",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for comments background","goodchoice"),
					"id" => "blog_comments_bg_color",
					"std" => "#f9f9f9",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for text","goodchoice"),
					"id" => "blog_comments_text_color",
					"std" => "#747474",
					"type" => "color");


$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "blog_comments_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);




















/* Blog Sidebar */
$of_options[] = array( "name" => __("Blog Sidebar","goodchoice"),
					"type" => "heading");


$of_options[] = array( "name" => __("CHOOSE SIDEBAR POSITION","goodchoice"),
					"desc" => "",
					"id" => "blog_sidebar_position",
					"std" => "1",
					"type" => "select",
					"options" => $of_blog_sidebar);


$of_options[] = array( "name" =>  __("SIDEBAR AREA SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the widget background","goodchoice"),
					"id" => "blog_sidebar_bg_color",
					"std" => "",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Border radius value","goodchoice"),
					"id" => "blog_sidebar_border_radius",
					"std" => "0",
					"type" => "text");



$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "blog_sidebar_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);




$of_options[] = array( "name" =>  __("WIDGET SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the widget background","goodchoice"),
					"id" => "blog_sidebar_widget_bg_color",
					"std" => "#ffffff",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget border","goodchoice"),
					"id" => "blog_sidebar_widget_border_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Border radius value","goodchoice"),
					"id" => "blog_sidebar_widget_border_radius",
					"std" => "0",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "blog_sidebar_widget_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the widget header","goodchoice"),
					"id" => "blog_sidebar_widget_header_color",
					"std" => "#333333",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for 'separator' after widget header","goodchoice"),
					"id" => "blog_sidebar_widget_hr",
					"std" => "#ffffff",
					"type" => "color");					


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget text","goodchoice"),
					"id" => "blog_sidebar_widget_text_color",
					"std" => "#666666",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget links","goodchoice"),
					"id" => "blog_sidebar_widget_links_color",
					"std" => "#777777",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the widget links on hover","goodchoice"),
					"id" => "blog_sidebar_widget_links_color_hover",
					"std" => "#04bfea",
					"type" => "color");







/* Pagination */
$of_options[] = array( "name" => __("Pagination","goodchoice"),
					"type" => "heading");

$of_options[] = array( "name" =>  __("PAGINATION SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the element background","goodchoice"),
					"id" => "pagination_bg_color",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the element text","goodchoice"),
					"id" => "pagination_text_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the element text shadow","goodchoice"),
					"id" => "pagination_text_shadow",
					"std" => "#222222",
					"type" => "color");


$of_options[] = array( "name" =>  "",
					"desc" => __("Vertical padding value","goodchoice"),
					"id" => "pagination_padding_v",
					"std" => "4",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Horizontal padding value","goodchoice"),
					"id" => "pagination_padding_h",
					"std" => "10",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Horizontal padding value","goodchoice"),
					"id" => "pagination_border_radius",
					"std" => "0",
					"type" => "text");


$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the current and hover elements background","goodchoice"),
					"id" => "pagination_hover_bg_color",
					"std" => "#04bfea",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the current and hover elements text","goodchoice"),
					"id" => "pagination_hover_text_color",
					"std" => "#ffffff",
					"type" => "color");
					

$of_options[] = array( "name" => " ",
					"desc" => __("Select a background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "pagination_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);




//Footer
$of_options[] = array( "name" => __("Footer","goodchoice"),
					"type" => "heading");
          

$of_options[] = array( "name" => __("TWITTER FEED SETTINGS","goodchoice"),
					"desc" => __("Show Twitter Feed?","goodchoice"),
					"id" => "show_twitter_feed",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" => "",
					"desc" => __("Text for Header","goodchoice"),
					"id" => "footer_social_tw_header",
					"std" => "Twitter Feed",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Description","goodchoice"),
					"id" => "footer_social_tw_descr",
					"std" => "Find out what's happening, right now, with the people and organizations you care about.",
					"type" => "textarea");

$of_options[] = array( "name" => "",
					"desc" => __("Twitter user name","goodchoice"),
					"id" => "footer_social_tw_user",
					"std" => "Orange_Idea_RU",
					"type" => "text");


$of_options[] = array( "name" => __("FOOTER LOGO","goodchoice"),
					"desc" => __("Upload your logo for footer","goodchoice"),
					"id" => "footer_logo",
					"std" => "http://www.orange-idea.com/assets/builder/logo-footer.png",
					"type" => "media");
					

$of_options[] = array( "name" => __("SOCIAL ICONS","goodchoice"),
					"desc" => __("Twitter","goodchoice"),
					"id" => "footer_social_tw",
					"std" => "http://twitter.com/",
					"type" => "text");	

$of_options[] = array( "name" => "",
					"desc" => __("Facebook","goodchoice"),
					"id" => "footer_social_fb",
					"std" => "https://facebook.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Google +","goodchoice"),
					"id" => "footer_social_g",
					"std" => "http://plus.google.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Dribbble","goodchoice"),
					"id" => "footer_social_dr",
					"std" => "http://dribbble.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Flickr","goodchoice"),
					"id" => "footer_social_fl",
					"std" => "http://flickr.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("YouTube","goodchoice"),
					"id" => "footer_social_yt",
					"std" => "http://youtube.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Linkedin","goodchoice"),
					"id" => "footer_social_in",
					"std" => "http://linkedin.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Pinterest","goodchoice"),
					"id" => "footer_social_pi",
					"std" => "http://pinterest.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Skype","goodchoice"),
					"id" => "footer_social_skype",
					"std" => "http://www.skype.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("MySpace","goodchoice"),
					"id" => "footer_social_myspace",
					"std" => "http://myspace.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("ICQ","goodchoice"),
					"id" => "footer_social_icq",
					"std" => "http://www.icq.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Envato","goodchoice"),
					"id" => "footer_social_envato",
					"std" => "http://envato.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Bing","goodchoice"),
					"id" => "footer_social_bing",
					"std" => "http://www.bing.com/",
					"type" => "text");


$of_options[] = array( "name" => "",
					"desc" => __("Forrst","goodchoice"),
					"id" => "footer_social_forrst",
					"std" => "http://forrst.com/",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("DeviantArt","goodchoice"),
					"id" => "footer_social_da",
					"std" => "http://deviantart.com/",
					"type" => "text");


$of_options[] = array( "name" =>  __("FOOTER SETTINGS","goodchoice"),
					"desc" => __("Pick a color for the footer background","goodchoice"),
					"id" => "footer_bg_color",
					"std" => "#303030",
					"type" => "color");


$of_options[] = array( "name" => " ",
					"desc" => __("Select a footer background pattern ( Choose first image to show only background color )","goodchoice"),
					"id" => "footer_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);

$of_options[] = array( "name" => " ",
					"desc" => __("Footer top margin","goodchoice"),
					"id" => "footer_margin_top",
					"std" => "60",
					"type" => "text");
					
$of_options[] = array( "name" => "",
					"desc" => __("Footer top padding","goodchoice"),
					"id" => "footer_padding_top",
					"std" => "40",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Footer bottom padding","goodchoice"),
					"id" => "footer_padding_bottom",
					"std" => "10",
					"type" => "text");

$of_options[] = array( "name" => "",
					"desc" => __("Footer top bodrer value","goodchoice"),
					"id" => "footer_border_value",
					"std" => "1",
					"type" => "text");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the footer top border","goodchoice"),
					"id" => "footer_border_color",
					"std" => "#444444",
					"type" => "color");

$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the footer headers","goodchoice"),
					"id" => "footer_text_header_color",
					"std" => "#ffffff",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the lines after headers","goodchoice"),
					"id" => "footer_hr_color",
					"std" => "#444444",
					"type" => "color");

$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the footer text","goodchoice"),
					"id" => "footer_text_color",
					"std" => "#a8a8a8",
					"type" => "color");	
									
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the footer strong text elements","goodchoice"),
					"id" => "footer_text_strong_color",
					"std" => "#ffffff",
					"type" => "color");						
					

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the footer small text elements","goodchoice"),
					"id" => "footer_text_small_color",
					"std" => "#666666",
					"type" => "color");				
					

$of_options[] = array( "name" =>  " ",
					"desc" => __("Pick a color for the footer links","goodchoice"),
					"id" => "footer_text_a_color",
					"std" => "#a8a8a8",
					"type" => "color");

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the footer hovered links","goodchoice"),
					"id" => "footer_text_a_hover_color",
					"std" => "#ffffff",
					"type" => "color");						




// Bottom Line

$of_options[] = array( "name" => __("Bottom Line","goodchoice"),
					"type" => "heading");

$of_options[] = array( "name" => __("Show/Hide Bottom Line","goodchoice"),
					"desc" => __("Show Bottom Line?","goodchoice"),
					"id" => "bottom_line_show",
					"std" => 1,
          			"folds" => 1,
					"type" => "checkbox");


$of_options[] = array( "name" =>  __("BOTTOM LINE BACKGROND","goodchoice"),
					"desc" => __("Pick a color for the 'Bottom Line' area background","goodchoice"),
					"id" => "theme_colors_bottom_line",
					"std" => "#3a3a3a",
					"type" => "color");

$of_options[] = array( "name" => "",
					"desc" => __("Pick a image for the 'Bottom Line' area background ( Choose first image to show only background color )","goodchoice"),
					"id" => "theme_colors_bottom_line_bg_image",
					"std" => $bg_images_url."1_px.png",
					"type" => "tiles",
					"options" => $bg_images,
					);


$of_options[] = array( "name" => __("BOTTOM LINE TEXT","goodchoice"),
					"desc" =>  __("Past your text or HTML","goodchoice"),
					"id" => "bottom_line_text",
					"std" => "Copyright 2013 GoodChoice - Company. Design by <a href='http://themeforest.net/user/OrangeIdea?ref=OrangeIdea'>OrangeIdea</a>",
					"type" => "text");					

$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Bottom Line text","goodchoice"),
					"id" => "theme_colors_bottom_line_text",
					"std" => "#FFFFFF",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Bottom Line links","goodchoice"),
					"id" => "theme_colors_bottom_line_a",
					"std" => "#FFFFFF",
					"type" => "color");
					
$of_options[] = array( "name" =>  "",
					"desc" => __("Pick a color for the Top Line mouse over links","goodchoice"),
					"id" => "theme_colors_bottom_line_a_hover",
					"std" => "#FFFFFF",
					"type" => "color");





					
// Backup Options
$of_options[] = array( "name" => __("Backup Options","goodchoice"),
					"type" => "heading");
					
$of_options[] = array( "name" => __("Backup and Restore Options","goodchoice"),
                    "id" => "of_backup",
                    "std" => "",
                    "type" => "backup",
					"desc" => __("You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.","goodchoice"),
					);
					
$of_options[] = array( "name" => __("Transfer Theme Options Data","goodchoice"),
                    "id" => "theme_update",
                    "std" => "",
                    "type" => "transfer",
					"desc" => __("You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click 'Import Options'","goodchoice"));



	}
}
?>
