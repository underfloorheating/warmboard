<?php get_header(); ?>
<?php   global $more; $more = 0;
?>


	<div class="main_content_area">
    <div class="container">
        <div class="row">
        	<?php if ($gcdata['blog_sidebar_position'] == "Left Sidebar") { ?>
            <!--Sidebar-->
            <div class="span3 blog_sidebar left my_search">
                <div class="sidde">
                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>                
                <?php endif; ?> 
                </div>
            </div>
            <!--/Sidebar-->
            <?php } ?>
            <!--Page contetn-->
            <div class="span9">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="blockquote4">
                	<a href="<?php the_permalink(); ?>"><h4 class="colored"><?php the_title(); ?></h4></a><?php the_time('l, F j, Y'); ?>
					<br><br>
					<?php the_content(''); ?>
                </div>
                <?php endwhile; else: ?>
				<div class="alert">
                	<strong><?php _e("Nothing was found!","builder"); ?></strong> <?php _e("Change a few things up and try submitting again.","builder"); ?>
                </div>
                <?php endif; ?>
                <section class="nopaddding">
                    <hr class="notopmargin">
                    <?php if (function_exists('wp_corenavi')) { ?><div class="pride_pg"><?php wp_corenavi(); ?></div><?php }?>
                </section>
        	</div>
            <?php if ($gcdata['blog_sidebar_position'] == "Right Sidebar") { ?>
            <!--Sidebar-->
            <div class="span3 blog_sidebar my_search">
                <div class="sidde">
                <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>                
                <?php endif; ?> 
                </div>
            </div>
            <!--/Sidebar-->
            <?php } ?>
        </div>
    </div>
    </div>


<?php get_footer(); ?>