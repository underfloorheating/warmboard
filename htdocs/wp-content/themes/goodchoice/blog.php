<?php
	// Template Name: Blog Template
?>
<?php 
get_header(); 
global $more;
$more = 0;
?>
<?php
$title = get_the_title();
if ( $title == "Left SideBar")  $gcdata['blog_sidebar_position'] = "Left Sidebar";
if ( $title == "Mini Image + Right Sidebar")  $gcdata['sl_blog_style'] = "Medium Images";
if ( $title == "Mini Image + Left Sidebar")  $gcdata['sl_blog_style'] = "Medium Images";
if ( $title == "Mini Image + Left Sidebar")  $gcdata['blog_sidebar_position'] = "Left Sidebar";

?>	
            <div class="main_content_area">
                <div class="container">
                    <div class="row">
                        <?php include_once("framework/blogs/style1.php"); ?>
                    </div>
                </div>
            </div>
<?php get_footer(); ?>