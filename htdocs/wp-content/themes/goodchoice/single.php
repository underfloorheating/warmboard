<?php 
/*
Default Post Template
*/
get_header(); ?>
<?php
$title = get_the_title();
if ( $title == "Post + Left Sidebar")  $gcdata['blog_sidebar_position'] = "Left Sidebar";

?>
            
        	<div class="main_content_area blog_item_page">
            <div class="container">
                <div class="row">
                	<?php if ($gcdata['blog_sidebar_position'] == "Left Sidebar") { ?>
                    <!--Sidebar-->
                    <div class="span3 blog_sidebar left">
                        <div class="sidde">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>                
                        <?php endif; ?> 
                        </div>
                    </div>
                    <!--/Sidebar-->
                    <?php } ?>
                    <!--Page contetn-->
                    <div class="span9 blog_item blog_item_long">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'framework/post-format/single', get_post_format() ); ?>
						<?php endwhile;  ?> 
						<?php endif; ?>
                    <?php if($gcdata['blog_post_show_author'] == true ) { ?>
                    <div class="row-fluid">
                    <div class="span12">
                        <div class="blog_author_item_description">
                            <img class="img-polaroid" src="<?php echo stripslashes($gcdata['blog_post_show_author_avatar']) ?>" alt="<?php bloginfo('name'); ?>" />
                            <h5><?php echo $gcdata['blog_post_show_author_header']; ?></h5>
                            <?php the_author_meta('description'); ?> 
                            <div class="clearfix"></div>                       
                        </div><!--end author-bio-->
                    </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="span9">
                            <div class="comments_div">
                            <h4><?php comments_number('There are no comments yet, but you can be the first','1 Comment:','% Comments:')?></h4>
                            <?php comments_template(); ?>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    <!--/Page contetn-->
                    <?php if ($gcdata['blog_sidebar_position'] == "Right Sidebar") { ?>
                    <!--Sidebar-->
                    <div class="span3 blog_sidebar">
                        <div class="sidde">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>                
                        <?php endif; ?> 
                        </div>
                    </div>
                    <!--/Sidebar-->
                    <?php } ?>
                </div>
            </div>
            </div>




<?php get_footer(); ?>