<?php
$kk = $gcdata['page_sidebar_generator'];
if ( function_exists('register_sidebar') ){
	
	register_sidebar(array(
		'name' => 'Blog Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5><hr>',
    ));
	
	 register_sidebar(array(
		'name' => 'Portfolio Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5><hr>',
    ));
	
	register_sidebar(array(
		'name' => 'Footer',
		'before_widget' => '<div class="span3">',
		'after_widget' => '</div>',
		'before_title' => '<h5>',
		'after_title' => '</h5><hr>',
	));
	
	register_sidebar(array(
		'name' => 'Right Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5><hr>',
    ));
	
	register_sidebar(array(
		'name' => 'Left Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h5>',
        'after_title' => '</h5><hr>',
    ));

		
	 for($i;$i<=$kk;$i++){
		register_sidebar(array(
			'name' => 'Page Sidebar '.$i,
			'before_widget' => '<div class="page_sidebar"><div class="well">',
			'after_widget' => '</div></div>',
			'before_title' => '<h5>',
			'after_title' => '</h5><hr>',
		));
 }}

?>