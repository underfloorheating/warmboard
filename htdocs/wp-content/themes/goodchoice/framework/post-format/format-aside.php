<?php  global $gcdata; ?>
<div class="row-fluid">
	<div class="span12">
        <div class="blog_item_content">
        	<div class="blog_head">
            	<div>
                    <a href="<?php echo get_post_format_link('aside'); ?>"><div class="postformat"></div></a>
                </div>
				<?php if($gcdata['blog_show_posts_date'] == true ) { ?><div class="date"><h6><?php if($gcdata['blog_show_date_icon'] == true ) { ?><i class="icon-calendar icon-white"></i> <?php } ?><?php the_time('d') ?> <?php the_time('M') ?> <?php the_time('Y') ?></h6></div><?php } ?>
                <h5><a href="<?php echo the_permalink(); ?>"><?php the_content(''); ?></a></h5>
            </div>
        </div>
    </div>
</div>