<?php  global $gcdata; ?>
<div class="row-fluid">
    <div class="span1">
        <a href="<?php echo the_permalink(); ?>"><div class="postformat"></div></a>
    </div>
    <div class="span11">
        <div class="blog_item_content">
        	<div class="blog_head my_status mystsing">
                <div class="meta">
                    <?php if($gcdata['blog_show_posts_date'] == true ) { ?><span><?php the_time('d') ?> <?php if ($gcdata['blog_date_format'] == "American Style") { ?><?php the_time('M') ?><?php } ?> <?php if ($gcdata['blog_date_format'] == "European Style") { ?><?php the_time('m') ?> / <?php } ?> <?php the_time('Y') ?></span><?php } ?>
                    <?php if($gcdata['blog_show_posts_meta_author'] == true ) { ?><span <?php if(($gcdata['blog_show_posts_meta_category'] == false ) & ($gcdata['blog_show_posts_meta_comments'] == false )) { ?>  class="last_item"<?php } ?> ><strong>By</strong> <?php the_author_posts_link() ?></span><?php } ?>
                    <?php if($gcdata['blog_show_posts_meta_category'] == true ) { ?><span <?php if(($gcdata['blog_show_posts_meta_comments'] == false )) { ?>  class="last_item"<?php } ?>><?php $tag = get_the_tags(); if (! $tag) { ?> There are no tags<?php } else { ?><?php the_tags(''); ?><?php } ?></span><?php } ?>
                    <?php if($gcdata['blog_show_posts_meta_comments'] == true ) { ?><span class="last_item"><a href="<?php the_permalink() ?>#comments"><?php comments_number('0','1','%')?>  comments</a></span><?php } ?>
                </div>
            	<h5><?php the_content(''); ?></h5>
            </div>
        </div>
    </div>
</div>