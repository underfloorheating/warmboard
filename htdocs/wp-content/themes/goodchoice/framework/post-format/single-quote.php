<?php  global $gcdata; ?>
<div class="row-fluid">
	<div class="span12">
        <div class="blog_item_content">
        	<div class="blog_head quote_post">
            	<div>
                    <a href="<?php echo get_post_format_link('quote'); ?>"><div class="postformat"></div></a>
                </div>
				<?php if($gcdata['blog_show_posts_date'] == true ) { ?><div class="date"><h6><?php if($gcdata['blog_show_date_icon'] == true ) { ?><i class="icon-calendar icon-white"></i> <?php } ?><?php the_time('d') ?> <?php the_time('M') ?> <?php the_time('Y') ?></h6></div><?php } ?>
            </div>
            <div class="clearfix"></div>
			<div class="blog_item_content">
                <div class="my_quote">
                    <h3><a href="<?php echo the_permalink(); ?>"><?php the_content(''); ?></a></h3>
                </div>
            </div>
            <div class="the-author"><a href="<?php echo get_post_meta($post->ID, '_format_quote_source_url', true); ?>"><?php echo get_post_meta($post->ID, '_format_quote_source_name', true); ?></a></div>
        </div>
    </div>
</div>