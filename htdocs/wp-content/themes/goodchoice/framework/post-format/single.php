<?php  global $gcdata; ?>
<div class="row-fluid">
	<div class="span12">
        <div class="blog_item_content">
        	<div class="blog_head">
            	<div>
                    <a href="<?php echo get_post_format_link(''); ?>"><div class="postformat"></div></a>
                </div>
				<?php if($gcdata['blog_show_posts_date'] == true ) { ?><div class="date"><h6><?php if($gcdata['blog_show_date_icon'] == true ) { ?><i class="icon-calendar icon-white"></i> <?php } ?><?php the_time('d') ?> <?php the_time('M') ?> <?php the_time('Y') ?></h6></div><?php } ?>
                <h3><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?> </a></h3>
				<div class="meta">
                    <?php if($gcdata['blog_show_posts_meta_author'] == true ) { ?><span <?php if(($gcdata['blog_show_posts_meta_category'] == false ) & ($gcdata['blog_show_posts_meta_comments'] == false )) { ?>  class="last_item"<?php } ?> ><strong>By</strong> <?php the_author_posts_link() ?></span><?php } ?>
                    <?php if($gcdata['blog_show_posts_meta_category'] == true ) { ?><span <?php if(($gcdata['blog_show_posts_meta_comments'] == false )) { ?>  class="last_item"<?php } ?>><?php $tag = get_the_tags(); if (! $tag) { ?> There are no tags<?php } else { ?><?php the_tags(''); ?><?php } ?></span><?php } ?>
                    <?php if($gcdata['blog_show_posts_meta_comments'] == true ) { ?><span class="last_item"><a href="<?php the_permalink() ?>#comments"><?php comments_number('0','1','%')?>  comments</a></span><?php } ?>
                </div>
            </div>
            <div class="blog_item_description">
                <?php the_content('<h6 class="read_more"><a href="'. get_permalink($post->ID) . '">'. __("Read More","commander") .'</a></h6>'); ?>
				 <?php if($gcdata['blog_post_show_share_button'] == true ) { ?>
                    <div class="share">
                        <span class="share_title"><?php echo $gcdata['blog_post_show_share_button_text']; ?></span>
                        <div class="floatleft">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style ">
                        <a class="addthis_counter addthis_pill_style"></a>
                        </div>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f88195d6026781e"></script>
                        <!-- AddThis Button END -->
                        </div>
                    <div class="my_clear"></div>
                    </div>
                    
                <?php } ?>
            </div>
        </div>
    </div>
</div>