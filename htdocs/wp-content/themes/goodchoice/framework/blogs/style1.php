						<?php if ($gcdata['blog_sidebar_position'] == "Left Sidebar") { ?>
                        <!--Sidebar-->
                        <div class="span3 blog_sidebar left">
                            <div class="myls">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>                
                            <?php endif; ?> 
                            </div>
                        </div>
                        <!--/Sidebar-->
                        <?php } ?>
                        <!--Page contetn-->
                        <div class="span9">
                            <?php if ( !is_archive() ) { ?>
                                <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; query_posts('paged='.$paged.'&cat='.$cat); ?>		
                            <?php } ?> 
                            <?php if (!(have_posts())) { ?><h3 class="colored">There are no posts</h3><?php }  ?>   
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                           
                            
                                <div <?php post_class('row mbi'); ?> id="post-<?php the_ID(); ?>">
                                    <div class="span9">
                                        <div class="blog_item">
                                            <?php $format = get_post_format(); get_template_part( 'framework/post-format/format', $format );   ?>
                                        </div>
                                    </div>
                                </div>
                                
                            <?php endwhile;  ?> 
                            <?php endif; ?>
                            <section class="nopaddding">
                                <hr class="notopmargin">
                                <?php if (function_exists('wp_corenavi')) { ?><div class="pride_pg"><?php wp_corenavi(); ?></div><?php }?>
                            </section>
                        </div>
                        <!--/Page contetn-->
                        <?php if ($gcdata['blog_sidebar_position'] == "Right Sidebar") { ?>
                        <!--Sidebar-->
                        <div class="span3 blog_sidebar">
                            <div class="myrs">
                            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?>                
                            <?php endif; ?> 
                            </div>
                        </div>
                        <!--/Sidebar-->
                        <?php } ?>