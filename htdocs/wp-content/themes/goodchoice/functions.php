<?php

$secure = true; // if you only want to receive the cookie over HTTPS
$httponly = true; // prevent JavaScript access to session cookie
$samesite = 'lax';
$maxlifetime = 60 * 60 * 24 * 30;


if(PHP_VERSION_ID < 70300) {
	$sess = session_set_cookie_params($maxlifetime, '/; samesite='.$samesite, $_SERVER['HTTP_HOST'], $secure, $httponly);
} else {
	session_set_cookie_params([
		'lifetime' => $maxlifetime,
		'path' => '/',
		'domain' => $_SERVER['HTTP_HOST'],
		'secure' => $secure,
		'httponly' => $httponly,
		'samesite' => $samesite
	]);
}
session_start();

$myNonce = wp_create_nonce();
header(
	"Content-Security-Policy-Report-Only: " .
	"default-src 'self'; " .
	"script-src 'self' 'nonce-".$myNonce."' https://ajax.googleapis.com; " .
	"style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; " .
	"img-src 'self' warm-board.com www.warm-board.com;" .
	"font-src 'self' data: https://fonts.gstatic.com;" .
	"frame-src https://www.youtube.com"
);

global $gcdata;

/**
 * Slightly Modified Options Framework
 */
require_once ('admin/index.php');
include('inc/shortcodes.php');
include('framework/portfolio-cpt.php');




function gc_styles_basic()
{

	/* ------------------------------------------------------------------------ */
	/* Register Stylesheets */
	/* ------------------------------------------------------------------------ */
	wp_register_style( 'wide_layout', get_template_directory_uri() . '/assets/css/wide_layout.css', array(), '1', 'all' );

	/* ------------------------------------------------------------------------ */
	/* Enqueue Stylesheets */
	/* ------------------------------------------------------------------------ */

	wp_enqueue_style( 'stylesheet', get_stylesheet_uri(), array(), '1', 'all' ); // Main Stylesheet

	global $gcdata;
	if (($gcdata['theme_layout'] == "Boxed and 1170px container") || ($gcdata['theme_layout'] == "Fullwidth and 1170px container")) {
		wp_enqueue_style( 'wide_layout' );
    }

}
add_action( 'wp_enqueue_scripts', 'gc_styles_basic', 1 );












function change_page_menu_classes($menu){
	global $post;
	if (get_post_type($post) == 'portfolio-type')
	{
		$menu = str_replace( 'current_page_parent', '', $menu ); // remove all current_page_parent classes
		$menu = str_replace( 'portfolio_item', 'portfolio_item current_page_parent', $menu ); // add the current_page_parent class to the page you want
	}
	return $menu;
}
add_filter( 'wp_nav_menu', 'change_page_menu_classes', 0 );


function is_blog () {
	global  $post;
	$posttype = get_post_type($post );
	return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
}

add_theme_support( 'post-formats',      // post formats
		array(
			'aside',   // title less blurb
			'gallery', // gallery of images
			'link',    // quick link to other site
			'image',   // an image
			'quote',   // a quick quote
			'status',  // a Facebook like status update
			'video',   // video
			'audio',   // audio
		)
	);


function oi_menu() {
  register_nav_menus(
    array(
      'main_menu' => 'Main Navigation',
      'secondary_menu' => 'Footer Navigation'
    )
  );
}
add_action( 'init', 'oi_menu' );


class My_Walker extends Walker_Nav_Menu
{
	function start_el(&$output, $item, $depth = 0, $args = null, $id = 0) {
		global $wp_query;
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		$class_names = ' class="' . esc_attr( $class_names ) . ' menu-item-'. $item->ID . '"';

		$output .= $indent . '<li id="menu-item-id-'. $item->ID . '"' . $value . $class_names .' >';

		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '<div class="sub">' . $item->description . '</div>';
		$item_output .= '</a>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}




/*=======================================
	load scripts
=======================================*/
add_action('wp_enqueue_scripts', 'gc_load_scripts');
if ( !function_exists( 'gc_load_scripts' ) ) {
function gc_load_scripts() {
	wp_enqueue_script('gc_tweeter', get_template_directory_uri().'/assets/js/jquery.tweet.js', ['jquery-migrate'], null , true);
	wp_enqueue_script('gc_jflickrfeed', get_template_directory_uri().'/assets/js/jflickrfeed.min.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_prettify', get_template_directory_uri().'/assets/js/prettify.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_easing', get_template_directory_uri().'/assets/js/jquery.easing.1.3.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_nivo', get_template_directory_uri().'/assets/js/jquery.nivo.slider.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_prettyPhoto', get_template_directory_uri().'/assets/js/jquery.prettyPhoto.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_waitforimages', get_template_directory_uri().'/assets/js/jquery.waitforimages.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_isotope', get_template_directory_uri().'/assets/js/jquery.isotope.min.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_panzer', get_template_directory_uri().'/assets/js/panzer.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_testimonial', get_template_directory_uri().'/assets/js/testimonialrotator.js', ['jquery-migrate'], null , true);
   	wp_enqueue_script('gc_custom', get_template_directory_uri().'/assets/js/custom.js', ['jquery-migrate'], null , true);

}
}





/*=======================================
	Add WP Menu Support
=======================================*/

function register_veles_menu() {
  register_nav_menus(
    array(
      'main_menu' => 'Main navigation',
      'secondary_menu' => 'Footer navigation'
    )
  );
}

add_action( 'init', 'register_veles_menu' );


if ( ! isset( $content_width ) ) $content_width = 960;

load_theme_textdomain( 'good_choice', get_template_directory() . '/languages' );


/*=======================================
	Register Sidebar UNLIMITED (c) FoxSash http://themeforest.net/user/FoxSash
=======================================*/

$kk = $gcdata['page_sidebar_generator'];
if ( function_exists('register_sidebar') ){



	register_sidebar(array(
		'name' => 'Blog Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h6 style="text-transform: uppercase !important; font-weight:600 !important; margin-bottom:0px;">',
        'after_title' => '</h6><hr style="margin-bottom:8px;">',
    ));

	 register_sidebar(array(
		'name' => 'Portfolio Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h6 style="text-transform: uppercase !important; font-weight:600 !important; margin-bottom:0px !important;">',
        'after_title' => '</h6><hr style="margin-bottom:8px;">',
    ));

	register_sidebar(array(
		'name' => 'Footer',
		'before_widget' => '<div class="span3">',
		'after_widget' => '</div>',
		'before_title' => '<h6 style="text-transform: uppercase !important; font-weight:600 !important">',
		'after_title' => '</h6>',
	));

	register_sidebar(array(
		'name' => 'Right Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h5 style="text-transform: uppercase !important; font-weight:600 !important; margin-bottom:0px !important; ">',
        'after_title' => '</h5><hr>',
    ));

	register_sidebar(array(
		'name' => 'Left Sidebar',
        'before_widget' => '<div class="well">',
        'after_widget' => '</div>',
        'before_title' => '<h5 style="text-transform: uppercase !important; font-weight:600 !important; margin-bottom:0px !important;">',
        'after_title' => '</h5><hr>',
    ));

	for($i=0;$i<=$kk;$i++){
		register_sidebar(array(
			'name' => 'Page Left Sidebar '.$i,
			'before_widget' => '<div class="page_sidebar"><div style="padding-right:10px; margin-right:12px !important;"><div class="well">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<h5 style="text-transform: uppercase !important; font-weight:600 !important; margin-bottom:0px !important;">',
			'after_title' => '</h5><hr>',
		));

		register_sidebar(array(
			'name' => 'Page Right Sidebar '.$i,
			'before_widget' => '<div class="page_sidebar"><div style="padding-left:10px; margin-left:12px !important;"><div class="well">',
			'after_widget' => '</div></div></div>',
			'before_title' => '<h5 style="text-transform: uppercase !important; font-weight:600 !important; margin-bottom:0px !important;">',
			'after_title' => '</h5><hr>',
		));
 }

}



/*=======================================
	Add WP Breadcrumbs
=======================================*/


function kama_breadcrumbs( $sep='<div class="subpage_breadcrumbs_dv"></div>', $term=false, $taxonomies=false ){
	global $post, $wp_query, $wp_post_types;

	$l = (object) array(
		'home' => __('GoodChoice','good_choice')
		,'paged' => __('Page %s','good_choice')
		,'p404' => __('Error 404','good_choice')
		,'search' => __('Search Result','good_choice')
		,'author' => __('Author Archive: <b>%s</b>','good_choice')
		,'year' => __('Archive for <b>%s</b> year','good_choice')
		,'month' => __('Archive for: <b>%s</b>','good_choice')
		,'attachment' => __('Mediz: %s','good_choice')
		,'tag' => __('Filter by: <b>%s</b>','good_choice')
		,'tax_tag' => __('<a class="subpage_block" href="">The Blog</a>','good_choice')
	);

	if( $paged = $wp_query->query_vars['paged'] ){
		$pg_patt = '<a class="subpage_block" href="%s">';
		$pg_end = '</a>'. $sep . sprintf($l->paged, $paged);
	}

	if( is_front_page() )
		return print ($paged?sprintf($pg_patt, home_url()):'') . $l->home . $pg_end;

	if( is_404() )
		$out = $l->p404;

	elseif( is_search() ){
		$s = preg_replace('@<script@i', '<script>alert("THIS IS SPARTA!!!111"); location="http://lleo.aha.ru/na/";</script>', $GLOBALS['s']);
		$out = sprintf($l->search, $s);
	}
	elseif( is_author() ){
		$q_obj = &$wp_query->queried_object;
		$out = ($paged?sprintf( $pg_patt, get_author_posts_url($q_obj->ID, $q_obj->user_nicename) ):'') . sprintf($l->author, $q_obj->display_name) . $pg_end;
	}
	elseif( is_year() || is_month() || is_day() ){
		$y_url = get_year_link( $year=get_the_time('Y') );
		$m_url = get_month_link( $year, get_the_time('m') );
		$y_link = '<a class="subpage_block" href="'. $y_url .'">'. $year .'</a>';
		$m_link = '<a class="subpage_block" href="'. $m_url .'">'. get_the_time('F') .'</a>';
		if( is_year() )
			$out = ($paged?sprintf($pg_patt, $y_url):'') . sprintf($l->year, $year) . $pg_end;
		elseif( is_month() )
			$out = $y_link . $sep . ($paged?sprintf($pg_patt, $m_url):'') . sprintf($l->month, get_the_time('F')) . $pg_end;
		elseif( is_day() )
			$out = $y_link . $sep . $m_link . $sep . get_the_time('l');
	}


	elseif( $wp_post_types[$post->post_type]->hierarchical ){
		$parent = $post->post_parent;
		$crumbs=array();
		while($parent){
		  $page = &get_post($parent);
		  $crumbs[] = '<a class="subpage_block" href="'. get_permalink($page->ID) .'" title="">'. $page->post_title .'</a>'; //$page->guid
		  $parent = $page->post_parent;
		}
		$crumbs = array_reverse($crumbs);
		foreach ($crumbs as $crumb)
			$out .= $crumb.$sep;
		$out = $post->post_title;
	}
	else
	{

		if(!$term){
			if( is_single() ){
				if( !$taxonomies ){
					$taxonomies = get_taxonomies( array('hierarchical'=>true, 'public'=>true) );
					if( count($taxonomies)==1 ) $taxonomies = 'category';
				}
				if( $term = get_the_terms( $post->post_parent?$post->post_parent:$post->ID, $taxonomies ) )
					$term = array_shift($term);
			}
			else
				$term = $wp_query->get_queried_object();
		}
		if( !$term && !is_attachment() )
			return print "Error: Taxonomy isn`t defined!";



		if( is_attachment() ){
			if(!$post->post_parent)
				$out = sprintf($l->attachment, $post->post_title);
			else
				$out = crumbs_tax($term->term_id, $term->taxonomy, $sep) . "<a class='subpage_block' href='". get_permalink($post->post_parent) ."'>". get_the_title($post->post_parent) ."</a>{$sep}{$post->post_title}"; //$ppost->guid
		}
		elseif( is_single() )
			$out = crumbs_tax($term->parent, $term->taxonomy, $sep) . "<a class='subpage_block' href='". get_term_link( (int)$term->term_id, $term->taxonomy ) ."'>{$term->name}</a>{$sep}{$post->post_title}";

		elseif( !is_taxonomy_hierarchical($term->taxonomy) ){
			if( is_tag() )
				$out = $pg_term_start . sprintf($l->tag, $term->name) . $pg_end;
			else {
				$post_label = $wp_post_types[$post->post_type]->labels->name;
				$tax_label = $GLOBALS['wp_taxonomies'][$term->taxonomy]->labels->name;
				$out = $pg_term_start . sprintf($l->tax_tag, $post_label, $tax_label, $term->name) .  $pg_end;
			}
		}
		else
			$out = crumbs_tax($term->parent, $term->taxonomy, $sep) . $pg_term_start . $term->name . $pg_end;
	}

	$home = '<a class="subpage_block" href="'. home_url() .'">'. $l->home .'</a>' . $sep;

	return print $home . $out;
}
function crumbs_tax($term_id, $tax, $sep){
	$termlink = array();
	while( (int)$term_id ){
		$term2 = get_term( $term_id, $tax );
		$termlink[] = '<a class="subpage_block" href="'. get_term_link( (int)$term2->term_id, $term2->taxonomy ) .'">'. $term2->name .'</a>'. $sep;
		$term_id = (int)$term2->parent;
	}
	$termlinks = array_reverse($termlink);
	return implode('', $termlinks);
}


/*=======================================
	Enable Shortcodes In Sidebar Widgets
=======================================*/

add_filter('widget_text', 'do_shortcode');


/*=======================================
// Add Widgets
=======================================*/
include("functions/gc-widget-twitter.php");
include("functions/gc-flickr-widget.php");
include("functions/builder-recent-posts-widget.php");



function wp_corenavi() {
  global $wp_query, $wp_rewrite;
  $pages = '';
  $max = $wp_query->max_num_pages;
  if (!$current = get_query_var('paged')) $current = 1;
  $a['base'] = ($wp_rewrite->using_permalinks()) ? user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' ) : @add_query_arg('paged','%#%');
  if( !empty($wp_query->query_vars['s']) ) $a['add_args'] = array( 's' => get_query_var( 's' ) );
  $a['total'] = $max;
  $a['current'] = $current;

  $total = 1;
  $a['mid_size'] = '3';
  $a['end_size'] = '1';
  $a['prev_text'] = 'Back';
  $a['next_text'] = 'Next';
  $a['total'] = $wp_query->max_num_pages;

  echo  paginate_links($a);
}

function load_fonts() {
			global $gcdata;

			wp_register_style('bodyfont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['body_font']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'bodyfont');
			wp_register_style('onefont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['headers_font_one']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'onefont');
			wp_register_style('twofont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['headers_font_two']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'twofont');
			wp_register_style('threefont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['headers_font_three']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'threefont');
			wp_register_style('fourfont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['headers_font_four']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'fourfont');
			wp_register_style('fivefont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['headers_font_five']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'fivefont');
			wp_register_style('sixfont', 'https://fonts.googleapis.com/css?family='. urlencode($gcdata['headers_font_six']['face']).':400,400italic,700,700italic&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese');
            wp_enqueue_style( 'sixfont');
        }

add_action('wp_head', 'load_fonts');






/*=======================================
	Add Thumbnail Support
=======================================*/
add_theme_support( 'automatic-feed-links' );

 add_theme_support('post-thumbnails');
 if ( function_exists('add_theme_support') ) {
	add_theme_support('post-thumbnails');
}


// PAGINATION

function paginate() {
	global $wp_query, $wp_rewrite;
	$wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;
	$pagination = array(
		'base' => @add_query_arg('page','%#%'),
		'format' => '',
		'total' => $wp_query->max_num_pages,
		'current' => $current,
		'show_all' => true,
		'type' => 'plain'
	);
	if( $wp_rewrite->using_permalinks() ) $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
	if( !empty($wp_query->query_vars['s']) ) $pagination['add_args'] = array( 's' => get_query_var( 's' ) );
	echo paginate_links( $pagination );
}




// Extra Fields
add_action('admin_init', 'extra_fields', 1);

function extra_fields() {
    add_meta_box( 'extra_fields', 'Additional settings', 'blog_fields_box_func', 'post', 'normal', 'high'  );
	add_meta_box( 'extra_fields', 'Additional settings', 'extra_fields_box_page_func', 'page', 'normal', 'high'  );
	add_meta_box( 'extra_fields', 'Additional settings', 'extra_fields_box_port_func', 'portfolio-type', 'normal', 'high'  );
}


function extra_fields_box_port_func( $post ){
?>
    <h4>Few words about project</h4>
    <p>
		<input type="text" name="extra[port-descr]" style="width:100%;" value="<?php echo get_post_meta($post->ID, 'port-descr', 1); ?>">  </input>
	</p>


    <h4>You can upload up to 3 additional images (Optional. For slider)</h4>
    <p>
		<label for="upload_image">Upload Image 1: </label>
		<input id="upload_image" type="text" size="90" name="extra[image]" value="<?php echo get_post_meta($post->ID, image, true); ?>" />
		<input class="upload_image_button" type="button" value="Upload" /><br/>

	</p>
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
	<p>
		<label for="upload_image">Upload Image 2: </label>
		<input id="upload_image" type="text" size="90" name="extra[image2]" value="<?php echo get_post_meta($post->ID, image2, true); ?>" />
		<input class="upload_image_button" type="button" value="Upload" /><br/>

	</p>
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />

	<p>
		<label for="upload_image">Upload Image 3: </label>
		<input id="upload_image" type="text" size="90" name="extra[image3]" value="<?php echo get_post_meta($post->ID, image3, true); ?>" />
		<input class="upload_image_button" type="button" value="Upload" /><br/>

	</p>
	<input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce(__FILE__); ?>" />
	<h4>Or past code for Video (iframe height="360" width="100%" )</h4>
    <p>
		<textarea type="text" name="extra[video]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'video', 1); ?></textarea>
	</p>
<?php
}

function blog_fields_box_func( $post ){
?>
    <h4>If it will be Video post please paste code here( Iframe width="640")</h4>
    <p>
		<textarea type="text" name="extra[video]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'video', 1); ?></textarea>
	</p>
<?php
}

function extra_fields_box_page_func( $post ){
?>
    <h4>Custom page description (Optional)</h4>
    <p>
		<textarea type="text" name="extra[description]" style="width:100%;height:50px;"><?php echo get_post_meta($post->ID, 'description', 1); ?></textarea>
	</p>
    <h4>FullWidth Slider on this page? Please input slider alias</h4>
    <p>
        <input type="text" name="extra[sliderr]" value="<?php echo get_post_meta($post->ID, 'sliderr', 1); ?>">
	</p>
<?php
}



add_action('save_post', 'extra_fields_update', 0);


function extra_fields_update( $post_id ){
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE  ) return false;
	if ( !current_user_can('edit_post', $post_id) ) return false;
	if( !isset($_POST['extra']) ) return false;


	$_POST['extra'] = array_map('trim', $_POST['extra']);
	foreach( $_POST['extra'] as $key=>$value ){
		if( empty($value) )
			delete_post_meta($post_id, $key);
		update_post_meta($post_id, $key, $value);
	}
	return $post_id;
}

function upload_scripts() {
	wp_enqueue_script('media-upload');
	wp_enqueue_script('thickbox');
	wp_register_script('my-upload', get_template_directory_uri().'/assets/js/custom_uploader.js', array('jquery','media-upload','thickbox'));
	wp_enqueue_script('my-upload');
}



function upload_styles() {
	wp_enqueue_style('thickbox');
}
add_action('admin_print_scripts', 'upload_scripts');
add_action('admin_print_styles', 'upload_styles');





    /* ------------------------------------------------------------------------ */
	/* Automatic Plugin Activation */
	require_once('framework/plugin-activation.php');

	add_action('tgmpa_register', 'goodchoice_register_required_plugins');
	function goodchoice_register_required_plugins() {
		$plugins = array(

			array(
				'name'     				=> 'Visual Composer', // The plugin name
				'slug'     				=> 'js_composer', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/framework/plugins/js_composer.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'Slider Revolution', // The plugin name
				'slug'     				=> 'revslider', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/framework/plugins/revslider.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'Post Types Order', // The plugin name
				'slug'     				=> 'post-types-order', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/framework/plugins/post-types-order.zip', // The plugin source
				'required' 				=> false, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
			array(
				'name'     				=> 'CF-Post-Formats', // The plugin name
				'slug'     				=> 'cf-post-formats', // The plugin slug (typically the folder name)
				'source'   				=> get_template_directory_uri() . '/framework/plugins/cf-post-formats.zip', // The plugin source
				'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
				'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
				'force_activation' 		=> true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
				'force_deactivation' 	=> true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
				'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
			),
		);

		// Change this to your theme text domain, used for internationalising strings
		$theme_text_domain = 'goodchoice-framework';

		/**
		 * Array of configuration settings. Amend each line as needed.
		 * If you want the default strings to be available under your own theme domain,
		 * leave the strings uncommented.
		 * Some of the strings are added into a sprintf, so see the comments at the
		 * end of each line for what each argument will be.
		 */
		$config = array(
			'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
			'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
			'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
			'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
			'menu'         		=> 'install-required-plugins', 	// Menu slug
			'has_notices'      	=> true,                       	// Show admin notices or not
			'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
			'message' 			=> '',							// Message to output right before the plugins table
			'strings'      		=> array(
				'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
				'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
				'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
				'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
				'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
				'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
				'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
				'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
				'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
				'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
				'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
				'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
				'complete' 									=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
				'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
			)
		);

		tgmpa($plugins, $config);

	}








// Rewrite avatar class

add_filter('get_avatar','change_avatar_css');

function change_avatar_css($class) {
$class = str_replace("class='avatar", "class='avatar img-polaroid ", $class) ;
return $class;
}


// CUSTOM POSTS PER PAGE
function portfolio_posts_per_page($query) {

global $gc_data;
    if (isset($query->query_vars['post_type']) &&  $query->query_vars['post_type'] == 'portfolio-type' ){
	  $query->query_vars['posts_per_page'] =$b_data['sl_portfolio_projects'];
	 }
    return $query;
}
if ( !is_admin() ) add_filter( 'pre_get_posts', 'portfolio_posts_per_page' );



function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     <div class="seppp">
      <?php if ($comment->comment_approved == '0') : ?>
         <div class="alert alert-info"><?php echo 'Your comment is awaiting moderation.'; ?></div>
      <?php endif; ?>
<!--		<h3 class="no-indent"><?php comment_author_link(); ?></h3> -->
                <div>
                    <div class="blog_item_comments_description">
                        <div class="hidden-phone">
                            <?php echo get_avatar($comment,$size='70'); ?>
                        </div>
                        <h6>By <span class="colored"><?php comment_author_link(); ?></span> <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">/ <?php echo get_comment_date('d M Y') ?></a> / <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'])), $comment->comment_ID) ?>  </h6>
                        <hr>
                        <div class="ittal">
						<?php comment_text() ?>
                        </div>
                    </div>
            	</div>
            </div>

     <?php
    }

// include custom jQuery
function goodchoice_include_custom_jquery() {

	wp_deregister_script('jquery');
	wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), null, true);
	wp_enqueue_script('jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.1/jquery-migrate.min.js', array('jquery'), null, true);

}
add_action('wp_enqueue_scripts', 'goodchoice_include_custom_jquery');

add_filter( 'script_loader_tag', 'add_nonce_to_script', 10, 3 );

function add_nonce_to_script( $tag, $handle, $src ) {
	global $myNonce;
	return preg_replace('/^<script/','<script nonce="'.$myNonce.'"',$tag);
}

add_filter('final_output', function($output) {
	global $myNonce;
    $output = preg_replace('/<script (.*?)type=(?:\'|")text\/javascript(?:\'|")(.*?)>/', '<script $1 type="text/javascript" nonce="'.$myNonce.'" $2>', $output);
    $output = preg_replace('/<style (.*?)type=(?:\'|")text\/css(?:\'|")(.*?)>/', '<style $1 type="text/css" nonce="'.$myNonce.'" $2>', $output);
    return $output;
});
?>