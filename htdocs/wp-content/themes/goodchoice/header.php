<!DOCTYPE html>
<html <?php language_attributes(); ?>><head>
        <meta charset="utf-8">
        <title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); ?></title>
        <meta name="description" content="<?php bloginfo('description'); ?>" />
        <meta name="keywords" content="<?php bloginfo('name'); ?>" />

        <?php  global $gcdata; ?>

        <?php
		$title = get_the_title();
		if ( $title == "Home Page 2 Example") { $gcdata['revolution_homepage'] = true; };
		if ( $title == "Home Page 4 Example") { $gcdata['revolution_homepage'] = true; };
		if ( $title == "Home Page 5 Example") { $gcdata['revolution_homepage'] = true; };
		?>
        <link rel="shortcut icon" href="<?php echo stripslashes($gcdata['header_favicon']) ?>">
        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js"></script>
        <![endif]-->
        <!--[if lte IE 8]>
    	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/ie.css" />
		<![endif]-->

        <?php $title = get_the_title(); ?>
        <?php wp_link_pages(); ?>
		<?php wp_head(); ?>
    </head>

	<body  <?php body_class(); ?>>

	 <?php
	if ( $title == "Home Page 2 Example"){
		$gcdata['top_line_show'] = true;
		$gcdata['header_top_line'] = "Subscribe to be notified for updates: RSS Feed";
		$gcdata['header_social_yt'] = "#";
		$gcdata['header_social_fl'] = "#";
		$gcdata['header_social_dr'] = "#";
		$gcdata['header_social_g'] = "#";
		$gcdata['header_social_fb'] = "#";
		$gcdata['header_social_in'] = "#";
		$gcdata['header_social_tw'] = "#";
		$gcdata['header_social_p'] = "#";
		$gcdata['tag_line_position'] = "After Slider";
		}
	?>


    <div class="wide_cont">
    <!--TOP-->
    <?php if($gcdata['top_line_show'] == true ) { ?>
  	<div class="top_line">
    	<div class="container">
        	<div class="row">
            	<div class="span6">
					<p class="feed"><?php echo stripslashes($gcdata['header_top_line']) ?></p>
    			</div>
                <div class="span6 soc_icons">
                	<?php if($gcdata['header_social_yt']) { ?>
                		<a href="<?php echo stripslashes($gcdata['header_social_yt']) ?>" target="_blank"><div class="icon_youtube"></div></a>
                    <?php } ?>
					<?php if($gcdata['header_social_fl']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_fl']) ?>" target="_blank"><div class="icon_flickr"></div></a>
                    <?php } ?>
					<?php if($gcdata['header_social_dr']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_dr']) ?>" target="_blank"><div class="icon_dribbble"></div></a>
                    <?php } ?>
					<?php if($gcdata['header_social_g']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_g']) ?>" target="_blank"><div class="icon_google"></div></a>
                    <?php } ?>
					<?php if($gcdata['header_social_fb']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_fb']) ?>" target="_blank"><div class="icon_facebook"></div></a>
                    <?php } ?>
                    <?php if($gcdata['header_social_in']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_in']) ?>" target="_blank"><div class="icon_in"></div></a>
                    <?php } ?>
                    <?php if($gcdata['header_social_pi']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_pi']) ?>" target="_blank"><div class="icon_pi"></div></a>
                    <?php } ?>
                    <?php if($gcdata['header_social_tw']) { ?>
                    	<a href="<?php echo stripslashes($gcdata['header_social_tw']) ?>" target="_blank"><div class="icon_t"></div></a>
                    <?php } ?>
                </div>
    		</div>
    	</div>
    </div>
    <!--/TOP-->
    <?php } ?>


    <!--PAGE HEAD-->
    <div class="page_head">
    	<div class="container">
        	<div class="row">
            	<div class="span3">
                	<div class="logo">
                    	<a href="<?php echo home_url(); ?>"> <img src="<?php echo stripslashes($gcdata['header_logo']) ?>" alt="<?php bloginfo('name'); ?>" /></a>
                    </div>
                </div>
                <div class="span9 mc_top_menu">
                	<nav>
                        <div class="my_menu">
                        <?php
						if ( has_nav_menu( 'main_menu' ) ){
                        		$walker = new My_Walker;
                            	$menu = wp_nav_menu(array(
                                'echo' => false,
                                'container' => '',
                                'theme_location' => 'main_menu',
                                'menu_class' => 'menu',
                                'walker' => $walker
                            	));

							$links = explode('<li', $menu);

							$newNav = '';

							$first = true;

							foreach($links as $link) {

								if (!$first) {
									if(strstr($link, '{br}')) {
										$newBreak = str_replace("{br}", "<br />", $link);
										$newNav .= "<li style='line-height: 20px; text-align: center;'" . $newBreak;
									}
									else $newNav .= '<li' . $link;
								}
								else {
									$newNav .= $link;
									$first = false;
								}
							}

							echo $newNav;

						} else { echo '<div class="alert alert-info"><h4>Set Up Your Menu</h4> <p>Apperance -> Menus -> Theme Location block</p></div>';}
                        ?>
                        </div>
                    </nav>
                </div>
    		</div>
    	</div>
    </div>
    <!--/PAGE HEAD-->

    <?php if(($gcdata['tag_line_show'] == true) & ($gcdata['tag_line_position'] == "Before Slider")) { ?>
    <!--WELCOME AREA-->
    <div class="tag_line">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="welcome">
                        <h3 class="<?php echo is_front_page() ? 'homepage' : '' ;?>">
                        	<?php if ((is_front_page() || ($title == "Home Page 2 Example") || ($title == "Home Page 3 Example") || ($title == "Home Page 5 Example") || ($title == "Home Page 6 Example") || ($title == "Home Page 7 Example") || ($title == "Home Page 8 Example"))){ ?>
                        		<?php echo stripslashes($gcdata['header_tagline']) ?>
                        	<?php } else { ?>
	                        	<?php if (!(is_archive()) & (!(is_search()) & (!(is_blog())))) { ?>
									<span class="colored"><?php wp_title(''); ?></span>
									<?php if (get_post_meta($post->ID, 'description', true)) { ?>
										<span class="colored">:</span>
									<?php } ?>
									<?php echo get_post_meta($post->ID, 'description', 1); ?>
									<?php if (get_post_meta($post->ID, 'port-descr', true)) { ?>
										<span class="colored">:</span>
									<?php } ?>
									<?php echo get_post_meta($post->ID, 'port-descr', 1); ?>
								<?php } ?>
							<?php } ?>
							<?php if (is_search()) { ?>
								<span class="colored"><?php _e("Search Results:","commander"); ?></span> <?php the_search_query(); ?>
							<?php } ?>
							<?php if (is_404('404.php')){?><?php } ?>
							<?php if ((is_post_type_archive('portfolio-type'))) { ?>
								<span class="colored"><?php the_title(); ?></span>
								<?php if (get_post_meta($post->ID, 'description', true)) { ?>
									<span class="colored">:</span>
								<?php } ?>
								<?php echo get_post_meta($post->ID, 'description', 1); ?>
								<?php if (get_post_meta($post->ID, 'port-descr', true)) { ?>
									<span class="colored">:</span>
								<?php } ?>
								<?php echo get_post_meta($post->ID, 'port-descr', 1); ?>
							<?php } ?>
							<?php if (is_blog()) { echo '<span class="colored">The Blog</span>'; } ?>
                        	</h3>

                        	<?php if(is_front_page()) :?>
								<img src="<?php echo get_template_directory_uri(); ?>/images/exclusive-to-city-plumbing.jpg" id="exclusive-to-cp" />
                        	<?php endif;?>

						<?php if($gcdata['breadcumbs'] == true ) { ?>
						<div class="bbb">
						<?php if (!is_front_page() & ($title != "Home Page 2 Example") & ($title != "Home Page 3 Example") & ($title != "Home Page 5 Example") & ($title != "Home Page 6 Example") & ($title != "Home Page 7 Example")  & ($title != "Home Page 8 Example")){ ?>
							<div class="bbread">
                            </div>
                        <?php } ?>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>

	<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
    <?php if (is_plugin_active('revslider/revslider.php')) { ?>

    <?php if($gcdata['revolution_homepage'] == true ) { ?>
	<?php if (is_front_page() || $title == "Home Page 2 Example" || $title == "Home Page 4 Example" || $title == "Home Page 7 Example"){ ?><?php putRevSlider("main_slider") ?><?php }?>
    <?php } ?>

	<?php } ?>

        <?php if(($gcdata['tag_line_show'] == true) & ($gcdata['tag_line_position'] == "After Slider")) { ?>
    <!--WELCOME AREA-->
    <div class="tag_line">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="welcome">
                        <h3><?php if ((is_front_page() || ($title == "Home Page 2 Example") || ($title == "Home Page 4 Example"))){ ?><?php echo stripslashes($gcdata['header_tagline']) ?><?php } else { ?><?php if (!(is_archive()) & (!(is_search()) & (!(is_blog())))) { ?> <span class="colored"><?php wp_title(''); ?></span><?php if (get_post_meta($post->ID, 'description', true)) { ?><span class="colored">:</span><?php } ?> <?php echo get_post_meta($post->ID, 'description', 1); ?><?php if (get_post_meta($post->ID, 'port-descr', true)) { ?><span class="colored">:</span><?php } ?> <?php echo get_post_meta($post->ID, 'port-descr', 1); ?> <?php } ?><?php } ?><?php if (is_search()) { ?><span class="colored"><?php _e("Search Results:","commander"); ?></span> <?php the_search_query(); ?><?php } ?><?php if (is_404('404.php')){?><?php } ?><?php if ((is_post_type_archive('portfolio-type'))) { ?><span class="colored"><?php the_title(); ?></span><?php if (get_post_meta($post->ID, 'description', true)) { ?><span class="colored">:</span><?php } ?><?php echo get_post_meta($post->ID, 'description', 1); ?><?php if (get_post_meta($post->ID, 'port-descr', true)) { ?><span class="colored">:</span><?php } ?> <?php echo get_post_meta($post->ID, 'port-descr', 1); ?><?php } ?><?php if (is_blog()) { echo '<span class="colored">The Blog</span>'; } ?></h3>
						<?php if($gcdata['breadcumbs'] == true ) { ?>
						<div class="bbb">
						<?php if (!is_front_page() & ($title != "Home Page 2 Example") & ($title != "Home Page 4 Example")){ ?>
                        	<div class="bbread">
							<?php @kama_breadcrumbs(); ?>
                            </div>
                        <?php } ?>
                        </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
